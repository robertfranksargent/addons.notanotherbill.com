jQuery(
	function ( $ )
	{
		var $subsFilter = $( 'form#subscriptions-filter' ) ,
			$bulkActionSelect = $( 'select#bulk-action-selector-top' , $subsFilter ) ;

		$( $bulkActionSelect ).append( '<option value="expired">Expire</option>' ) ;

		$( 'input#doaction' , $subsFilter ).on( 'click' ,
			function ( e )
			{
				var $selected = $( $bulkActionSelect ).find( ':selected' ) ;

				if ( $selected && $selected.val( ) == 'expired' )
				{
					return confirm( 'Are you sure?' ) ;
				}
			}
		) ; 

		$( 'td.status .row-actions span.expire a' ).on( 'click' ,
			function ( e )
			{
				return confirm( 'Are you sure?' ) ;
			}
		) ;

		$( 'a.immediate-payment' ).on( 'click' ,
			function ( e )
			{
				var $pickerDiv = $( this ).parents( '.date-picker-div' ) ,
					$editDiv = $pickerDiv.parents( '.edit-date-div' ) ,
					$timeDiv = $editDiv.siblings( '.next-payment-date') ,
					$subscriptionRow = $pickerDiv.parents( 'tr' ) ;

				if ( confirm( 'Payment will be taken 1 minute from now. Are you sure you want to proceed?' ) )
				{
					$pickerDiv.slideUp( 'fast' ) ;
					$pickerDiv.parents( '.row-actions' ).css( { 'background-image' : 'url(' + WCSubscriptions.ajaxLoaderImage + ')' } ) ;

					$.ajax( {
						type : 'POST' ,
						url : ajaxurl ,
						data : {
							action : 'wcs_immediate_payment' ,
							wcs_subscription_key : $( '.subscription_key' ,$subscriptionRow ).val( ) ,
							wcs_nonce : WCSubscriptions.ajaxDateChangeNonce
						},
						success : function ( response ) 
						{
							response = $.parseJSON( response ) ;

							if ( 'error' == response.status )
							{
								$editDiv.css( { 'background-image' : '' } ) ;
								$( response.message ).hide( ).prependTo( $timeDiv.parent( ) ).slideDown( 'fast' ).fadeIn( 'fast' ) ;
								$pickerDiv.slideDown( 'fast' ) ;
								setTimeout(
									function( )
									{
										$( '.error' , $timeDiv.parent( ) ).slideUp( ) ;
									} , 4000 
								) ;
							} 
							else 
							{ 
								$editDiv.removeAttr( 'style' ) ;
								$timeDiv.fadeOut( 'fast' ,
									function ( )
									{
										$timeDiv.html( response.dateToDisplay ) ;
										$timeDiv.attr( 'title' , response.timestamp ) ;
										$timeDiv.fadeIn( 'fast' ) ;
										$pickerDiv.siblings( 'a.edit-timestamp' ).fadeIn( 'fast' ) ;
										
										$( response.message ).hide( ).prependTo( $timeDiv.parent( ) ).slideDown( 'fast' ).fadeIn( 'fast' ) ;

										setTimeout( 
											function ( )
											{
												$( '.updated' , $timeDiv.parent( ) ).slideUp( ) ;
											} , 3000
										) ;
									}
								) ;
							}
						}
					} ) ;
				}
				else
				{
					$editDiv.removeAttr( 'style' );
					$pickerDiv.slideUp('fast');
					$pickerDiv.siblings('a.edit-timestamp').show();
				}

				e.preventDefault( ) ;
			}
		) ;
	}
) ;