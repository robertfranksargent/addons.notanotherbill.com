jQuery(
	function ( $ )
	{
		var product_gallery_frame ,
			$image_gallery_ids = $( '#additional_product_image_gallery' ) ,
			$product_images = $( '#additional_product_images_container ul.product_images' ) ;

		// run tip tip
		function runTipTip ( )
		{
			// remove any lingering tooltips
			$( '#tiptip_holder' ).removeAttr( 'style' ) ;
			$( '#tiptip_arrow' ).removeAttr( 'style' ) ;
			
			// init tiptip
			$( '.tips' ).tipTip( {
				'attribute' : 'data-tip' ,
				'fadeIn' : 50 ,
				'fadeOut' : 50 ,
				'delay' : 200
			} ) ;
		}

		runTipTip( ) ;

		// Product gallery file uploads
		$( '.add_additional_product_images' ).on( 'click' , 'a' ,
			function ( event )
			{
				var $el = $( this ) ;
				var attachment_ids = $image_gallery_ids.val( ) ;

				event.preventDefault( ) ;

				// If the media frame already exists, reopen it.
				if ( product_gallery_frame )
				{
					product_gallery_frame.open( ) ;
					return ;
				}

				// Create the media frame.
				product_gallery_frame = wp.media.frames.additional_product_gallery = wp.media( {
					// Set the title of the modal.
					title : $el.data( 'choose' ) ,
					button : {
						text: $el.data( 'update' ) ,
					} ,
					states : [
						new wp.media.controller.Library( {
							title : $el.data( 'choose' ) ,
							filterable : 'all' ,
							multiple: true ,
						} ) 
					]
				} ) ;

				// When an image is selected, run a callback.
				product_gallery_frame.on( 'select' ,
					function ( ) 
					{
						var selection = product_gallery_frame.state( ).get( 'selection' ) ;

						selection.map(
							function( attachment )
							{
								attachment = attachment.toJSON( ) ;

								if ( attachment.id )
								{
									attachment_ids = attachment_ids ? attachment_ids + "," + attachment.id : attachment.id ;

									$product_images.append( '\
										<li class="image" data-attachment_id="' + attachment.id + '">\
											<img src="' + attachment.url + '" />\
											<ul class="actions">\
												<li><a href="#" class="delete" title="' + $el.data( 'delete' ) + '">' + $el.data( 'text' ) + '</a></li>\
											</ul>\
										</li>'
									) ;
								}
							}
						) ;

						$image_gallery_ids.val( attachment_ids ) ;
					}
				) ;

				// Finally, open the modal.
				product_gallery_frame.open( ) ;
			}
		) ;

		// Image ordering
		$product_images.sortable( {
			items : 'li.image' ,
			cursor : 'move' ,
			scrollSensitivity : 40 ,
			forcePlaceholderSize : true ,
			forceHelperSize : false ,
			helper : 'clone' ,
			opacity : 0.65 ,
			placeholder : 'wc-metabox-sortable-placeholder' ,
			start : function ( event , ui )
			{
				ui.item.css( 'background-color' ,'#f6f6f6' ) ;
			} ,
			stop : function ( event , ui )
			{
				ui.item.removeAttr( 'style' ) ;
			} ,
			update : function ( event , ui )
			{
				var attachment_ids = '' ;

				$( '#additional_product_images_container ul li.image' ).css( 'cursor' , 'default' ).each(
					function ( )
					{
						var attachment_id = jQuery(this).attr( 'data-attachment_id' ) ;
						attachment_ids = attachment_ids + attachment_id + ',' ;
					}
				) ;

				$image_gallery_ids.val( attachment_ids ) ;
			}
		} ) ;

		// Remove images
		$( '#additional_product_images_container' ).on( 'click' , 'a.delete' ,
			function ( )
			{
				$( this ).closest( 'li.image' ).remove( ) ;

				var attachment_ids = '' ;

				$( '#additional_product_images_container ul li.image' ).css( 'cursor' , 'default' ).each(
					function ( )
					{
						var attachment_id = jQuery( this ).attr( 'data-attachment_id' ) ;
						attachment_ids = attachment_ids + attachment_id + ',' ;
					}
				) ;

				$image_gallery_ids.val( attachment_ids ) ;

			runTipTip( ) ;

			return false ;
		} ) ;


		// Add product addons options
		var $addonsData = $( '#product_addons_data' ) ;

		// Bind change events
		$( '.wc-metaboxes' , $addonsData ).on( 'change keypress keyup' , updateAddonsBinding ) ;

		function updateAddonsBinding ( )
		{
			// console.log( 'updateAddonsBinding' ) ;

			var $optionsGroup = $( '.options_group' , $addonsData ) ,
				$addonMetaBoxes = $( '.wc-metabox-content' , $addonsData ) ,
				$formFieldBinding = $( '.form-field-binding' , $optionsGroup ) ,
				selectName = '_product_addons_bind_required' ,
				wc_product_addons_bind_required ;

			// Save existing options on change
			$formFieldBinding.on( 'change' ,
				function ( )
				{
					wc_product_addons_bind_required = [ ] ;
					
					$( '#' + selectName , $addonsData ).each(
						function ( )
						{
							wc_product_addons_bind_required = $( this ).val( ) ;
						}
					) ;
				}
			) ;

			// Remove existing field
			$formFieldBinding.remove( ) ;

			// Add new field
			if ( $optionsGroup.length && $addonMetaBoxes.length )
			{
				var html = '<p class="form-field form-field-binding">' ;
				
				html += '<label for="' + selectName + '">Conditional addons</label>' ;
				html += '<select multiple size="' + $addonMetaBoxes.length + '" id="' + selectName + '" name="' + selectName + '[]" style="width: 200px;">' ;

				$addonMetaBoxes.each(
					function ( )
					{
						var addonName = $( '[id^=addon_name_]' , this ).val( ) ;

						if ( addonName )
						{
							var selected = '' ;

							if ( wc_product_addons_bind_required && wc_product_addons_bind_required.indexOf( addonName ) >= 0 )
							{
								selected = ' selected' ;
							}

							html += '<option value="' + addonName + '"' + selected + '>' + addonName + '</option>' ;
						}
					}
				) ;

				html += '</select>' ;
				html += '<span class="description">Select the fields where completing one requires the other to be completed too.</span>'
				html += '</p>' ;

				$optionsGroup.append( html ) ;
			}
		}
	} 
) ;