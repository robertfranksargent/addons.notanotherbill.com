jQuery( document ).ready(
	function( $ )
	{
		$( '#nab_wc_customer_order_csv_export_from_subscription_end_date' ).datepicker( {
			dateFormat: 'yy-mm-dd' ,
			numberOfMonths: 1,
			showButtonPanel: !0,
			showOn: 'button',
			buttonImage: wc_customer_order_csv_export_admin_params.calendar_icon_url,
			buttonImageOnly: !0
		} ) ;

		$( '#nab_wc_customer_order_csv_export_to_subscription_end_date' ).datepicker( {
			dateFormat: 'yy-mm-dd' ,
			numberOfMonths: 1,
			showButtonPanel: !0,
			showOn: 'button',
			buttonImage: wc_customer_order_csv_export_admin_params.calendar_icon_url,
			buttonImageOnly: !0
		} ) ;

		if ( getParameterByName( 'tab' ) == 'import-previous-gifts' || getParameterByName( 'tab' ) == 'import-subscribers' )
		{
			$( '#mainform input[name="submit"]' ).val( 'Import' ) ;
		}

		function getParameterByName ( name , url )
		{
			if ( ! url ) url = window.location.href ;
			name = name.replace( /[\[\]]/g , '\\$&' ) ;
			var regex = new RegExp( '[?&]' + name + '(=([^&#]*)|&|#|$)' ) ,
			results = regex.exec( url ) ;
			if ( ! results ) return null ;
			if ( ! results[ 2 ] ) return '' ;
			return decodeURIComponent( results[ 2 ].replace( /\+/g , ' ' ) ) ;
		}	
	}
) ;