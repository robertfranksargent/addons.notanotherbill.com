<?php

/*
Plugin Name: 	Woocommerce Not Another Bill Add-ons
Version: 		2.2.23.15
Description: 	Additional WooCommerce functionality for Not Another Bill.
Author: 		Robert Sargent
Author:			Robert Sandiford
Author URI: 	http://www.robertsargent.co.uk/
Author URI: 	http://www.robertsandiford.co.uk/
Text Domain: 	notanotherbill
Domain Path: 	/languages
*/

/**
 * WooCommerce Not Another Bill Add-ons Class
 *
 * @class WooCommerce_NAB_Addons
 * @version	2.2.23.15
 * @author Robert Sargent
 */
final class WooCommerce_NAB_Addons
{
	/**
	 * @var string
	 */
	public $version = '2.2.23.15' ;

	/**
	 * @var WooCommerce_NAB_Addons The single instance of the class
	 * @since 2.1
	 */
	protected static $_instance = null ;

	/**
	 * Main WooCommerce_NAB_Addons Instance
	 *
	 * Ensures only one instance of WooCommerce_NAB_Addons is loaded or can be loaded.
	 *
	 * @since 2.1
	 * @static
	 * @see WC_NAB_Addons( )
	 * @return WooCommerce_NAB_Addons - Main instance
	 */
	public static function instance ( )
	{
		if ( is_null( self::$_instance ) )
		{
			self::$_instance = new self( ) ;
		}
		return self::$_instance ;
	}

	/**
	 * WooCommerce_NAB_Addons Constructor.
	 *
	 * @access public
	 * @return WooCommerce_NAB_Addons
	 */
	public function __construct ( )
	{
		define( 'WC_NAB_VERSION' , $this->version ) ;

		// Auto-load classes on demand
		if ( function_exists( '__autoload' ) )
		{
			spl_autoload_register( '__autoload' ) ;
		}

		spl_autoload_register( array( $this , 'autoload' ) ) ;

		// Include required files
		$this->includes( ) ;

		// Hooks
		add_action( 'widgets_init' , array( $this , 'include_widgets' ) ) ;
		add_action( 'init' , array( 'WC_NAB_Shortcodes' , 'init' ) ) ;

		// Disable admin password change notification
		if ( ! function_exists( 'wp_password_change_notification' ) )
		{
			function wp_password_change_notification ( ) { }
		}		
	}

	/**
	 * Include required core files used in admin and on the frontend.
	 *
	 * @since 2.2.22
	 */
	private function includes ( )
	{
		include_once( 'includes/class-wc-nab-install.php' ) ;
		include_once( 'includes/class-wc-nab-ssl.php' ) ;

		if ( is_admin( ) )
		{
			include_once( 'includes/admin/class-wc-nab-admin.php' ) ;
		}
		else
		{
			include_once( 'includes/class-wc-nab-coupons.php' ) ;
		}

		// include_once( 'includes/class-wc-nab-paypal.php' ) ;
		include_once( 'includes/class-wc-nab-account.php' ) ;
		include_once( 'includes/class-wc-nab-conversion.php' ) ;
		include_once( 'includes/class-wc-nab-post-types.php' ) ;
		include_once( 'includes/class-wc-nab-query.php' ) ;
		include_once( 'includes/class-wc-nab-checkout.php' ) ;
		include_once( 'includes/class-wc-nab-api.php' ) ;
		include_once( 'includes/class-wc-nab-emails.php' ) ;
		include_once( 'includes/class-wc-nab-gateways.php' ) ;
		include_once( 'includes/class-wc-nab-subscriptions.php' ) ;
		include_once( 'includes/class-wcs-nab-date-sync.php' ) ;
	}

	/**
	 * Include core widgets
	 *
	 * @since 2.2.18.13
	 */
	public function include_widgets ( )
	{
		include_once( 'includes/widgets/class-wc-nab-widget-price-filter.php' ) ;
		include_once( 'includes/widgets/class-wc-nab-widget-product-categories.php' ) ;
		include_once( 'includes/widgets/class-wc-nab-widget-product-filters.php' ) ;
		include_once( 'includes/widgets/class-wc-nab-widget-product-order-options.php' ) ;
	}

	/**
	 * Auto-load WC classes on demand to reduce memory consumption.
	 *
	 * @param mixed $class
	 * @return void
	 */
	public function autoload ( $class )
	{
		$path  = null ;
		$class = strtolower( $class ) ;
		$file = 'class-' . str_replace( '_' , '-' , $class ) . '.php' ;

		if ( strpos( $class , 'wc_nab_meta_box' ) === 0 )
		{
			$path = $this->plugin_path( ) . '/includes/admin/post-types/meta-boxes/' ;
		}
		elseif ( strpos( $class , 'wc_shortcode_' ) === 0 )
		{
			$path = $this->plugin_path( ) . '/includes/shortcodes/' ;
		}

		if ( $path && is_readable( $path . $file ) )
		{
			include_once( $path . $file ) ;
			return ;
		}

		// Fallback
		if ( strpos( $class , 'wc_' ) === 0 )
		{
			$path = $this->plugin_path( ) . '/includes/' ;
		}

		if ( $path && is_readable( $path . $file ) )
		{
			include_once( $path . $file ) ;
			return ;
		}
	}

	/** Helper functions ******************************************************/

	/**
	 * Get the plugin url.
	 *
	 * @return string
	 */
	public function plugin_url ( )
	{
		return untrailingslashit( plugins_url( '/' , __FILE__ ) ) ;
	}

	/**
	 * Get the plugin path.
	 *
	 * @return string
	 */
	public function plugin_path ( )
	{
		return untrailingslashit( plugin_dir_path( __FILE__ ) ) ;
	}
}

WooCommerce_NAB_Addons::instance( ) ;

/**
 * Returns the main instance of WC_NAB_Addons to prevent the need to use globals.
 *
 * @since  2.1
 * @return WooCommerce
 */
function WC_NAB_Addons ( )
{
	return WooCommerce_NAB_Addons::instance( ) ;
}

// Global for backwards compatibility.
$GLOBALS[ 'woocommerce_nab_addons' ] = WC_NAB_Addons( ) ;