<?php
/**
 * Gateways
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce
 * @version     2.2.23.5
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_NAB_Gateways' ) ) :

/**
 * WC_NAB_Gateways Class
 */
class WC_NAB_Gateways
{
	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		add_action( 'subscription_expired_paypal' , array( 'WC_PayPal_Standard_Subscriptions' , 'suspend_subscription_with_paypal' ) , 10 , 2 ) ;

		add_filter( 'woocommerce_available_payment_gateways' , array( $this , 'filter_payment_gateways' ) , 10 , 1 ) ;
	}

	/**
	* Filter payment gateways (e.g. Stripe, PayPal, GoCardless) depending on cart contents.
	*
	* @param array $payment_gateways
	* @return array
	* @since 2.2.23.5
	*/
	public function filter_payment_gateways ( $payment_gateways )
	{
		$cart = WC( )->cart ;

		// Disable GoCardless if cart total is zero
		if ( $cart->total <= 0 )
		{
			unset( $payment_gateways[ 'gocardless' ] ) ;
		}

		// Any gateway can be used if the cart contains a rolling subscription and no discount is applied
		if ( $cart->cart_contents_count == 1 && WC_Subscriptions_Cart::cart_contains_subscription( ) )
		{
			if ( WC_Subscriptions_Cart::get_cart_subscription_period( ) == 'month' && WC_Subscriptions_Cart::get_cart_subscription_interval( ) == 1 && $cart->get_order_discount_total( ) <= 0 )
			{
				return $payment_gateways ;
			}
		}

		// Disable GoCardless gateway
		if ( array_key_exists( 'gocardless' , $payment_gateways ) )
		{
			unset( $payment_gateways[ 'gocardless' ] ) ;
		}

		return $payment_gateways ;
	}
}

endif ;

return new WC_NAB_Gateways( ) ;