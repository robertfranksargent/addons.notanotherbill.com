<?php

/**
 * Query
 *
 * @class 		WC_NAB_Query
 * @version		2.2.23.3
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_Query
{
	/**
	 * Products per page
	 *
	 * @since 2.2.17.12
	 */
	public static $products_per_page = 18 ;

	/**
	 * Constructor
	 *
	 * @since 2.2.18.9
	 */
	public function __construct ( )
	{
		add_action( 'pre_get_posts' , array( $this , 'pre_get_posts' ) , 11 , 1 ) ;
		
		add_filter( 'loop_shop_post_in' , array( $this , 'price_filter' ) ) ;
	}

	/**
	 * Remove past presents from shop
	 *
	 * @param object $query
	 * @since 2.2.23.3
	 */
	public function pre_get_posts ( $query )
	{
		if ( ! $query->is_main_query( ) ) return ;
		
		if ( $query->get( 'product_cat' ) == 'past-presents' )
		{
			if ( array_key_exists( 'filter' , $_GET ) )
			{
				$query->set( 'tax_query' ,
					array(
						array(
							'taxonomy' => 'product_cat' ,
							'field' => 'slug' ,
							'terms' => array( $_GET[ 'filter' ] ) ,
							'operator' => 'AND'
						)
					)
				) ;
			}

			return ;
		}
		
		if ( $query->get( 'page_id' ) == get_option( 'page_on_front' ) ) return ;

		if ( is_single( ) || is_admin( ) ) return ;

		// Products per page
		if ( is_shop( ) || is_search( ) || is_product_category( ) )
		{
			if ( array_key_exists( 'products_per_page' , $_GET ) )
			{
				if ( preg_match( '/^\d+$/' , $_GET[ 'products_per_page' ] ) )
				{
					$products_per_page = $_GET[ 'products_per_page' ] ;
				}
				else if ( $_GET[ 'products_per_page' ] == 'all' )
				{
					$products_per_page = 999999 ;
				}
			}
			else
			{
				$products_per_page = self::$products_per_page ;
			}

			$query->set( 'posts_per_page' , $products_per_page ) ;
		}

		// Search to include only "post" and "product" post types
		if ( is_search( ) )
		{
			$query->set( 'post_type' , array( 'product' ) ) ;
		}
		else if ( is_woocommerce( ) )
		{
			// Build meta query
			$meta_query = array( ) ;

			$meta_query[ ] = array(
				'relation' => 'AND' ,
				array(
					'key' => '_visibility' ,
					'value' => 'visible' ,
					'compare' => '=='
				) ,
				array(
					'key' => '_stock_status' ,
					'value' => 'instock' ,
					'compare' => '=='
				) ,
				array(
					'relation' => 'OR' ,
					array(
						'key' => '_stock' ,
						'value' => 0 ,
						'compare' => '>' ,
						'type' => 'numeric'
					) ,
					array(
						'key' => '_manage_stock' ,
						'value' => 'no' ,
						'compare' => '=='
					)
				)
			) ;

			// Min/max pricing filter
			if ( is_shop( ) || is_product_category( ) )
			{
				// Price is greater than zero
				$price_query = array(
					'relation' => 'AND' ,
					array(
						'key' => '_price' ,
						'value' => 0 ,
						'compare' => '>' ,
						'type' => 'numeric'
					)
				) ;

				// Min price
				if ( array_key_exists( 'min_price' , $_GET ) )
				{
					$min_price = $_GET[ 'min_price' ] ;

					if ( $min_price > 0 )
					{
						$price_query[ ] = array(
							'key' => '_price' ,
							'value' => $min_price ,
							'compare' => '>' ,
							'type' => 'numeric'
						) ;
					}
				}
				
				// Max price
				if ( array_key_exists( 'max_price' , $_GET ) )
				{
					$max_price = $_GET[ 'max_price' ] ;

					if ( $max_price > 0 )
					{
						$price_query[ ] = array(
							'key' => '_price' ,
							'value' => $max_price ,
							'compare' => '<' ,
							'type' => 'numeric'
						) ;
					}
				}

				if ( array_key_exists( 'sale' , $_GET ) && $_GET[ 'sale' ] == 1 )
				{
					$product_ids_on_sale = wc_get_product_ids_on_sale( ) ;
					
					$query->set( 'post__in' , array_merge( array( 0 ) , $product_ids_on_sale ) ) ;
				}
			}

			if ( isset( $price_query ) ) $meta_query[ ] = $price_query ;

			$query->set( 'meta_query' , $meta_query ) ;

			// pre( $query ) ;
		}
	 
		remove_action( 'pre_get_posts' , 'pre_get_posts' ) ;
	}

	/**
	 * Price Filter post filter
	 *
	 * @param array $filtered_posts
	 * @return array
	 */
	public function price_filter ( $filtered_posts )
	{
		global $wpdb ;

		if ( isset( $_GET[ 'max_price' ] ) || isset( $_GET[ 'min_price' ] ) )
		{
			$matched_products = array( ) ;

			$min = isset( $_GET[ 'min_price' ] ) ? floatval( $_GET[ 'min_price' ] ) : 0 ;
			$max = isset( $_GET[ 'max_price' ] ) ? floatval( $_GET[ 'max_price' ] ) : 9999999 ;

			$matched_products_query = apply_filters( 'woocommerce_price_filter_results' , $wpdb->get_results( $wpdb->prepare( "
				SELECT DISTINCT ID, post_parent, post_type FROM $wpdb->posts
				INNER JOIN $wpdb->postmeta ON ID = post_id
				WHERE post_type IN ( 'product', 'product_variation' ) AND post_status = 'publish' AND meta_key = %s AND meta_value BETWEEN %d AND %d
			" , '_price' , $min , $max ) , OBJECT_K ) , $min , $max ) ;

			if ( $matched_products_query )
			{
				foreach ( $matched_products_query as $product )
				{
					if ( $product->post_type == 'product' )
						$matched_products[ ] = $product->ID ;

					if ( $product->post_parent > 0 && ! in_array( $product->post_parent , $matched_products ) )
						$matched_products[ ] = $product->post_parent ;
				}
			}

			// Filter the IDs
			if ( sizeof( $filtered_posts ) == 0 )
			{
				$filtered_posts = $matched_products ;
				$filtered_posts[ ] = 0 ;
			}
			else 
			{
				$filtered_posts = array_intersect( $filtered_posts , $matched_products ) ;
				$filtered_posts[ ] = 0 ;
			}

		}

		return ( array ) $filtered_posts ;
	}
}

new WC_NAB_Query( ) ;
