<?php

/**
 * Query
 *
 * @class 		WC_NAB_SSL
 * @version		2.2.16.8
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_SSL
{
	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		ob_start( array( $this , 'filter_content_match_protocols' ) ) ;

		add_action( 'shutdown' , array( $this , 'filter_content_match_protocols_end' ) , -10 ) ;

		if ( WP_ENV != 'development' && ! is_admin( ) && ! strpos( $_SERVER[ 'REQUEST_URI' ] , 'wp-login.php' ) )
		{
			add_action( 'plugins_loaded' , array( $this , 'force_ssl' ) ) ;
		}
	}

	/**
	* Filter URLs in content
	* 
	* @param string $content
	* @return string
	*/
	public function filter_content_match_protocols ( $content )
	{
		$search = $replace = get_bloginfo( 'url' ) ;
		
		if ( ! preg_match( '|/$|' , $search ) )
		{
			$search = $replace = $search . '/' ;
		}
		
		if ( is_ssl( ) )
		{
			$search = str_replace( 'https://' , 'http://' , $search ) ;
			$replace = str_replace( 'http://' , 'https://' , $replace ) ;
		}
		else
		{
			$search = str_replace( 'http://' , 'https://' , $search ) ;
			$replace = str_replace( 'https://' , 'http://' , $replace ) ;
		}
		
		$content = str_replace( $search , $replace , $content ) ;
		
		return $content ;
	}	

	/**
	* End content filtering
	* 
	* @return void
	*/
	public function filter_content_match_protocols_end ( )
	{
		ob_end_flush( ) ;
	}

	/**
	* Force SSL for all users
	* 
	* @return void
	*/
	public function force_ssl ( )
	{
		if ( ! is_ssl( ) )
		{
			$secure_url = 'https://' ;

			if ( array_key_exists( 'HTTP_HOST' , $_SERVER ) )
			{
				$secure_url .= $_SERVER[ 'HTTP_HOST' ] ;
			}
			else if ( array_key_exists( 'SERVER_NAME' , $_SERVER ) )
			{
				$secure_url .= $_SERVER[ 'SERVER_NAME' ] ;
			}
			else
			{
				return ;
			}
			
			$secure_url .= $_SERVER[ 'REQUEST_URI' ] ;

			wp_redirect( $secure_url , 301 ) ;

			exit( ) ;
		}
	}
}

new WC_NAB_SSL( ) ;