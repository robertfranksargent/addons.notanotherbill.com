<?php

/**
 * A custom email verification email
 *
 * @extends 	WC_Email
 * @class 		WC_Account_Verification_Email
 * @version		2.2.22
 * @package		WooCommerce/Classes
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly
 
if ( ! class_exists( 'WC_Account_Verification_Email' ) ) :

/**
 * WC_Account_Verification_Email Class
 */
class WC_Account_Verification_Email extends WC_Email
{
	/** @var string */
	var $user_login ;

	/** @var string */
	var $user_email ;

	/** @var string */
	var $verify_key ;

	/**
	* Set email defaults
	*
	* @since 2.2.22
	*/
	public function __construct ( )
	{
		// set ID, this simply needs to be a unique name
		$this->id = 'wc_account_verification' ;

		// this is the title in WooCommerce Email settings
		$this->title = 'Account Verification' ;

		// this is the description in WooCommerce email settings
		$this->description = 'Account email verification sent when a user needs to verify their email address.' ;

		// these are the default heading and subject lines that can be overridden using the settings
		$this->heading = 'Please verify your email address on {site_title}' ;
		$this->subject = 'Please verify your email address on {site_title}' ;

		// these define the locations of the templates that this email should use, we'll just use the new order template since this email is similar
		$this->template_html  = 'emails/customer-account-verification.php' ;

		// Trigger on order meta update
		add_action( 'woocommerce_account_verification_notification' , array( $this , 'trigger' ) , 10 , 2 ) ;

		// Call parent constructor to load any other defaults not explicity defined here
		parent::__construct( ) ;
	}

	/**
	* Determine if the email should actually be sent and setup email merge variables
	*
	* @since 2.2.22
	* @param int $order_id
	*/
	public function trigger ( $user_login = '' , $verify_key = '' )
	{
		if ( $user_login && $verify_key )
		{
			$this->object = get_user_by( 'login' , $user_login ) ;
			$this->user_login = $user_login ;
			$this->verify_key = $verify_key ;
			$this->user_email = stripslashes( $this->object->user_email ) ;
			$this->recipient = $this->user_email ;
		}

		if ( ! $this->is_enabled( ) || ! $this->get_recipient( ) ) return ;

		// woohoo, send the email!
		$this->send( $this->get_recipient( ) , $this->get_subject( ) , $this->get_content( ) , $this->get_headers( ) , $this->get_attachments( ) ) ;
	}	

	/**
	 * get_type function.
	 *
	 * @since 2.2.22
	 * @return string
	 */
	public function get_email_type ( )
	{
		return $this->email_type ? $this->email_type : 'html' ;
	}

	/**
	* get_content_html function.
	*
	* @since 2.2.22
	* @return string
	*/
	public function get_content_html ( )
	{
		ob_start( ) ;

		woocommerce_get_template( $this->template_html ,
			array(
				'email_heading' => $this->get_heading( ) ,
				'user_login'	=> $this->user_login ,
				'verify_key'	=> $this->verify_key ,
				'blogname'		=> $this->get_blogname( ) ,
				'sent_to_admin' => false ,
				'plain_text'    => false
			)
		) ;
		
		return ob_get_clean( ) ;
	}
}

endif ;