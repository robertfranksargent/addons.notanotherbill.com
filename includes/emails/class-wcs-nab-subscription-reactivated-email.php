<?php

/**
 * A custom subscription WooCommerce Email class for when a subscription is reactivated.
 *
 * @extends 	WC_Email
 * @class 		WCS_NAB_Subscription_Reactivated_Email
 * @version		2.2.11.9
 * @package		WooCommerce/Classes
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly
 
if ( ! class_exists( 'WCS_NAB_Subscription_Reactivated_Email' ) ) :

/**
 * WCS_NAB_Subscription_Reactivated_Email Class
 */
class WCS_NAB_Subscription_Reactivated_Email extends WC_Email
{
	/**
	* Set email defaults
	*
	* @since 2.2.10.17
	*/
	public function __construct ( )
	{
		// set ID, this simply needs to be a unique name
		$this->id = 'wcs_subscription_reactivated' ;

		// this is the title in WooCommerce Email settings
		$this->title = 'Subscription Reactivated' ;

		// this is the description in WooCommerce email settings
		$this->description = 'Subscription notitification sent when a customer\'s subscription has been reactivated.' ;

		// these are the default heading and subject lines that can be overridden using the settings
		$this->heading = 'Subscription Reactivated' ;
		$this->subject = 'Subscription Reactivated' ;

		// these define the locations of the templates that this email should use, we'll just use the new order template since this email is similar
		$this->template_html  = 'emails/admin-subscription-reactivated.php' ;

		// Trigger on subscription put on hold
		add_action( 'woocommerce_subscription_reactivated_notification' , array( $this , 'trigger' ) ) ;

		// Call parent constructor to load any other defaults not explicity defined here
		parent::__construct( ) ;

		// this sets the recipient to the settings defined below in init_form_fields()
		$this->recipient = $this->get_option( 'recipient' ) ;

		// if none was entered, just use the WP admin email as a fallback
		if ( ! $this->recipient ) $this->recipient = get_option( 'admin_email' ) ;
	}

	/**
	* Determine if the email should actually be sent and setup email merge variables
	*
	* @since 2.2.10.17
	* @param int $user_id
	* @param string $subscription_key
	*/
	public function trigger ( $order_id )
	{
		// bail if no order ID is present
		if ( ! $order_id ) return ;

		// setup order object
		$this->object = new WC_Order( $order_id ) ;

		// replace variables in the subject/headings
		$this->find[ ] = '{order_date}' ;
		$this->replace[ ] = date_i18n( woocommerce_date_format( ) , strtotime( $this->object->order_date ) ) ;

		$this->find[ ] = '{order_number}' ;
		$this->replace[ ] = $this->object->get_order_number( ) ;

		if ( ! $this->is_enabled( ) || ! $this->get_recipient( ) ) return ;

		// woohoo, send the email!
		$this->send( $this->get_recipient( ) , $this->get_subject( ) , $this->get_content( ) , $this->get_headers( ) , $this->get_attachments( ) ) ;
	}	

	/**
	 * get_type function.
	 *
	 * @return string
	 */
	public function get_email_type ( )
	{
		return $this->email_type ? $this->email_type : 'html' ;
	}

	/**
	* get_content_html function.
	*
	* @since 2.2.10.16
	* @return string
	*/
	public function get_content_html ( )
	{
		ob_start( ) ;

		woocommerce_get_template( $this->template_html ,
			array(
				'order' => $this->object ,
				'email_heading' => $this->get_heading( )
			)
		) ;
		
		return ob_get_clean( ) ;
	}

	/**
	* Initialize Settings Form Fields
	*
	* @since 2.2.10.16
	*/
	public function init_form_fields ( )
	{
		$this->form_fields = array(
			'enabled' => array(
				'title' => 'Enable/Disable' ,
				'type' => 'checkbox' ,
				'label' => 'Enable this email notification' ,
				'default' => 'yes'
			) ,
			'recipient' => array(
				'title' => 'Recipient(s)' ,
				'type' => 'text' ,
				'description' => sprintf( 'Enter recipients (comma separated) for this email. Defaults to <code>%s</code>.' , esc_attr( get_option( 'admin_email' ) ) ) ,
				'placeholder' => '' ,
				'default' => ''
			) ,
			'subject' => array(
				'title' => 'Subject' ,
				'type' => 'text' ,
				'description' => sprintf( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>.' , $this->subject ) ,
				'placeholder' => '' ,
				'default' => ''
			) 
		) ;
	}
}

endif ;