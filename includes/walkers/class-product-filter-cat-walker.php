<?php
/**
 * WC_NAB_Product_Filter_Cat_Walker class.
 *
 * @extends 	Walker
 * @class 		WC_NAB_Product_Filter_Cat_Walker
 * @version		2.2.8.3
 * @package		WooCommerce/Classes/Walkers
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_Product_Filter_Cat_Walker extends Walker_Category
{
	var $db_fields = array ( 'parent' => 'parent' , 'id' => 'term_id' , 'slug' => 'slug' ) ;

	var $count = 0 ;

	public function start_el ( &$output , $cat , $depth = 0 , $args = array( ) , $current_object_id = 0 )
	{

		$output .= '<li class="cat-item cat-item-' . $cat->term_id ;

		if ( $args[ 'current_category' ] == $cat->term_id )
		{
			$output .= ' current-cat' ;
		}

		$query_str = $_SERVER[ 'QUERY_STRING' ] ;
		$max_price = array_key_exists( 'max_price' , $_GET ) ? $_GET[ 'max_price' ] : null ;
		$min_price = array_key_exists( 'min_price' , $_GET ) ? $_GET[ 'min_price' ] : null ;

		global $wp_query ;

		if ( $wp_query->get( 'product_cat' ) == 'past-presents' )
		{
			if ( $max_price )
			{
				$price_str = ( strlen( $query_str ) ? '&' : '?' ) . 'max_price=' . $max_price ;
			}
			else if ( $min_price )
			{
				$price_str = ( strlen( $query_str ) ? '&' : '?' ) . 'min_price=' . $min_price ;
			}
			else
			{
				$price_str = '' ;
			}

			$output .=  '"><a href="?filter=' . $cat->slug . $price_str . '">' . __( $cat->name , 'woocommerce' ) . '</a>' ;
		}
		else
		{
			$output .=  '"><a href="' . get_term_link( ( int ) $cat->term_id , 'product_cat' ) . ( strlen( $query_str ) ? '?' . $query_str : '' ) . '">' . __( $cat->name , 'woocommerce' ) . '</a>' ;
		}

		$output .= '</li>' . PHP_EOL ;

		if ( ++$this->count % $args[ 'column' ] == 0 )
		{
			$output .= '</ul>' . PHP_EOL . '<ul class="col-lg-6">' . PHP_EOL ;
		}
	}

	public function end_el ( &$output , $cat , $depth = 0 , $args = array( ) )
	{
		$output .= '</li>' . PHP_EOL ;
	}
}