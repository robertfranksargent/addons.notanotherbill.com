<?php

/**
 * Checkout
 *
 * @class 		WC_NAB_Checkout
 * @version		2.2.22.10
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_Checkout
{
	/**
	 * Constructor
	 *
	 * @since 2.2.19.4
	 */
	public function __construct ( )
	{
		add_action( 'woocommerce_init' , array( $this , 'init' ) ) ;
		add_action( 'woocommerce_mention_me' , array( $this , 'mention_me_referrer' ) , 11 , 1 ) ;
		add_action( 'woocommerce_after_cart' , array( $this , 'mention_me_referee' ) ) ;
		add_action( 'woocommerce_checkout_order_processed' , array( $this , 'checkout_order_processed' ) , 10 , 2 ) ;

		add_filter( 'woocommerce_checkout_fields' , array( $this , 'checkout_fields' ) ) ;
		add_filter( 'checkout_buyer_fields' , array( $this , 'buyer_fields' ) ) ;

		add_filter( 'checkout_billing_fields' , array( $this , 'billing_fields' ) ) ;
		add_filter( 'checkout_shipping_fields' , array( $this , 'shipping_fields' ) ) ;
		add_filter( 'checkout_order_fields' , array( $this , 'order_fields' ) ) ;
		add_filter( 'checkout_account_fields' , array( $this , 'account_fields' ) ) ;

		add_filter( 'woocommerce_billing_fields' , array( $this , 'billing_fields' ) ) ;
		add_filter( 'woocommerce_ship_to_different_address_checked' , array( $this , 'ship_to_different_address_checked' ) ) ;
		add_filter( 'woocommerce_unforce_ssl_checkout' , array( $this , 'unforce_ssl_checkout' ) ) ;
		add_filter( 'woocommerce_cart_needs_payment' , array( $this , 'cart_needs_payment' ) , 11 , 2 ) ;
		add_filter( 'woocommerce_terms_is_checked_default' , array( $this , 'terms_is_checked_default' ) , 10 , 1 ) ; 

		add_filter( 'default_checkout_order_comments' , array( $this , 'filter_order_comments' ) , 10 , 2 ) ;
	}

	/**
	 * WooCommerce init
	 */
	public function init ( )
	{
		remove_action( 'woocommerce_before_checkout_form' , 'woocommerce_checkout_login_form' , 10 ) ;
		remove_action( 'woocommerce_before_checkout_form' , 'woocommerce_checkout_coupon_form' , 10 ) ;
	}

	/**
	* Add order post meta.
	*
	* @since 2.2.21.12
	*/
	public function checkout_order_processed ( $order_id , $posted )
	{
		$order = new WC_Order( $order_id ) ;
		$contains_shop_item = 0 ;

		if ( $order ) foreach ( $order->get_items( ) as $order_item )
		{
			if ( ! empty( $order_item ) && ! WC_Subscriptions_Order::is_item_subscription( $order , $order_item ) )
			{
				$contains_shop_item = 1 ;
				break ;
			}
		}

		update_post_meta( $order_id , '_order_contains_shop_item' , $contains_shop_item ) ;
	}

	/**
	 * WooCommerce mention me referrer
	 *
	 * @since 2.2.13.9
	 * @param int $order_id
	 */
	public function mention_me_referrer ( $order_id )
	{
		$order = new WC_Order( $order_id ) ;

		if ( $order )
		{
			$user = $order->get_user( ) ;
			$coupons = $order->get_used_coupons( ) ;

			$mm_params = array(
				'email' => $user->user_email ,
				'order_number' => $order_id ,
				'order_total' => $order->get_total( ) ,
				'order_currency' => $order->get_order_currency( ) ,
				'situation' => 'postpurchase' ,
				'surname' => $user->last_name ,
				'firstname' => $user->first_name ,
				'order_date' => $order->order_date ,
				'customer_id' => $user->ID ,
				'coupon_code' => count( $coupons ) > 0 ? $coupons[ 0 ] : '' ,
				'address_line_1' => $order->billing_address_1 ,
				'address_line_2' => $order->billing_address_2 ,
				'address_city' => $order->billing_city ,
				'address_county' => $order->billing_state ,
				'address_postcode' => $order->billing_postcode ,
				'locale' => get_locale( )
			) ;

			$mm_url = 'https://tag' . ( is_dev( ) || is_staging( ) ? '-demo' : '' ) . '.mention-me.com/api/v2/referreroffer/mmf596014d?' . http_build_query( $mm_params ) ;

			?>

				<!-- Begin Mention Me referrer integration -->
				<script type="text/javascript" src="<?php echo $mm_url ; ?>"></script>
				<!-- End Mention Me referrer integration -->	

			<?php
		}
	}

	/**
	 * WooCommerce mention me referrer
	 *
	 * @since 2.2.13.9
	 */
	public function mention_me_referee ( )
	{
		$mm_params = array( 'situation' => 'checkout' ) ;

		if ( get_current_user_id( ) > 0 )
		{
			$user = wp_get_current_user( ) ;

			$mm_params[ 'email' ] = $user->user_email ;
			$mm_params[ 'surname' ] = $user->last_name ;
			$mm_params[ 'firstname' ] = $user->first_name ;
			$mm_params[ 'customer_id' ] = $user->ID ;
		}

		$mm_url = 'https://tag' . ( is_dev( ) || is_staging( ) ? '-demo' : '' ) . '.mention-me.com/api/v2/refereefind/mmf596014d?' . http_build_query( $mm_params ) ;

		?>

			<!-- Begin Mention Me referrer integration -->
			<script type="text/javascript" src="<?php echo $mm_url ; ?>"></script>
			<!-- End Mention Me referrer integration -->	

		<?php
	}

	/**
	 * Checkout fields filter
	 *
	 * Adds buyer fields to checkout
	 *
	 * @param array $checkout_fields
	 * @return array
	 */
	public function checkout_fields ( $checkout_fields )
	{

		// add buyer fields to the beginning of the checkout fields array
		$checkout_fields = array(

			'buyer' => array(

				'buyer_first_name' => array(
					'label' 		=> 'Buyer First Name',
					'placeholder' 	=> 'First Name',
					'class' 		=> array('form-row-first'),
					'required' 		=> true
				),

				'buyer_last_name' => array(
					'label' 		=> 'Buyer Last Name',
					'placeholder' 	=> 'Last Name',
					'class' 		=> array('form-row-first'),
					'required'		=> true
				),

			)

		) + $checkout_fields ;

		return $checkout_fields ;
	}

	/**
	 * Checkout buyer fields filter
	 *
	 * Adds buyer fields
	 *
	 * @param array $checkout_fields
	 * @return array
	 */
	public function buyer_fields ( $checkout_fields )
	{

		$buyer_fields['buyer_first_name'] = array(
			'label' 		=> 'First Name',
			'placeholder' 	=> 'First Name',
			'class' 		=> array('form-row-first'),
			'required' 		=> true
		);

		$buyer_fields['buyer_last_name'] = array(
			'label' 		=> 'Last Name',
			'placeholder' 	=> 'Last Name',
			'class' 		=> array('form-row-first'),
			'required'		=> true
		);	

		return $buyer_fields ;
	}

	/**
	 * Checkout billing fields filter
	 *
	 * @param array $checkout_fields
	 * @return array
	 */
	public function billing_fields ( $checkout_fields )
	{
		// Reorder fields
		$billing_fields = array(
			'billing_first_name' => $checkout_fields[ 'billing_first_name' ] ,
			'billing_last_name' => $checkout_fields[ 'billing_last_name' ] ,
			'billing_email' => $checkout_fields[ 'billing_email' ] ,
			'billing_company' => $checkout_fields[ 'billing_company' ] ,
			'billing_address_1' => $checkout_fields[ 'billing_address_1' ] ,
			'billing_address_2' => $checkout_fields[ 'billing_address_2' ] ,
			'billing_city' => $checkout_fields[ 'billing_city' ] ,
			'billing_state' => $checkout_fields[ 'billing_state' ] ,
			'billing_country' => $checkout_fields[ 'billing_country' ] ,
			'billing_postcode' => $checkout_fields[ 'billing_postcode' ] ,
			'billing_phone' => $checkout_fields[ 'billing_phone' ]
		) ;

		// Add placeholder and remove label
		foreach ( $billing_fields as $key => $field )
		{
			if ( isset( $field[ 'label' ] ) || isset( $field[ 'placeholder' ] ) )
			{
				switch ( $key )
				{
					case 'billing_email' :
						$billing_fields[ $key ][ 'class' ][ ] = 'hidden' ;
						break ;

					case 'billing_company' :
						$billing_fields[ $key ][ 'class' ][ ] = 'hidden' ;
						break ;

					case 'billing_city' :
						$billing_fields[ $key ][ 'class' ][ 0 ] = 'form-row-first' ;
						break ;

					case 'billing_state' :
						$billing_fields[ $key ][ 'class' ][ 0 ] = 'form-row-last' ;
						$billing_fields[ $key ][ 'required' ] = 0 ;
						break ;

					case 'billing_country' :
						$billing_fields[ $key ][ 'clear' ] = false ;
						$billing_fields[ $key ][ 'class' ][ 0 ] = 'form-row-first' ;
						break ;

					case 'billing_phone' :
						$billing_fields[ $key ][ 'class' ][ 0 ] = 'form-row-wide' ;
						$billing_fields[ $key ][ 'required' ] = 0 ;

					default :
						if ( isset( $field[ 'label' ] ) )
						{
							$billing_fields[ $key ][ 'placeholder' ] = $field[ 'label' ] ;
						}
						break ;
				}

				unset( $billing_fields[ 'billing_state' ][ 'validate' ] ) ;
			}
		}

		return $billing_fields ;
	}

	/**
	 * Checkout shipping fields filter
	 *
	 * @param array $checkout_fields
	 * @return array
	 */
	public function shipping_fields ( $checkout_fields )
	{
		// Reorder fields
		$shipping_fields = array(
			'shipping_first_name' => $checkout_fields[ 'shipping_first_name' ] ,
			'shipping_last_name' => $checkout_fields[ 'shipping_last_name' ] ,
			'shipping_company' => $checkout_fields[ 'shipping_company' ] ,
			'shipping_address_1' => $checkout_fields[ 'shipping_address_1' ] ,
			'shipping_address_2' => $checkout_fields[ 'shipping_address_2' ] ,
			'shipping_city' => $checkout_fields[ 'shipping_city' ] ,
			'shipping_state' => $checkout_fields[ 'shipping_state' ] ,
			'shipping_country' => $checkout_fields[ 'shipping_country' ] ,
			'shipping_postcode' => $checkout_fields[ 'shipping_postcode' ]
		) ;

		// Add placeholder and remove label
		foreach ( $shipping_fields as $key => $field )
		{
			if ( isset( $field[ 'label' ] ) || isset( $field[ 'placeholder' ] ) )
			{
				switch ( $key )
				{
					case 'shipping_company' :
						$shipping_fields[ $key ][ 'class' ][ ] = 'hidden' ;
						break ;

					case 'shipping_city' :
						$shipping_fields[ $key ][ 'class' ][ 0 ] = 'form-row-first' ;
						break ;

					case 'shipping_state' :
						$shipping_fields[ $key ][ 'class' ][ 0 ] = 'form-row-last' ;
						$shipping_fields[ $key ][ 'required' ] = 0 ;
						break ;

					case 'shipping_country' :
						$shipping_fields[ $key ][ 'clear' ] = false ;
						$shipping_fields[ $key ][ 'class' ][ 0 ] = 'form-row-first' ;
						break ;

					default :
						if ( isset( $field[ 'label' ] ) )
						{
							$shipping_fields[ $key ][ 'placeholder' ] = $field[ 'label' ] ;
						}
						break ;
				}

				unset( $shipping_fields[ 'shipping_state' ][ 'validate' ] ) ;
			}
		}

		return $shipping_fields ;
	}

	/**
	 * Checkout order fields filter
	 *
	 * @param array $checkout_fields
	 * @return array
	 * @since 2.2.18.4
	 */
	public function order_fields ( $checkout_fields )
	{
		$cart_subscription = self::cart_contains_subscription( ) ;
		$add_placeholder = false ;

		// Customer is buying a subscription
		if ( $cart_subscription )
		{
			// Get recipient type
			$recipient = WCS_Utils::get_cart_addon( $cart_subscription , 'Recipient' ) ;

			// Customer is gifting the subscription
			if ( $recipient == 'For a gift' )
			{
				// Add gift message placeholder
				$add_placeholder = true ;
			}
		}
		else
		{
			// Customer is not buying a subscription -> add gift message placeholder
			$add_placeholder = true ;
		}

		if ( $add_placeholder )
		{
			// Add placeholder and remove label
			foreach ( $checkout_fields as $key => $field )
			{
				if ( isset( $field[ 'label' ] ) && $key == 'order_comments' )
				{
					$checkout_fields[ $key ][ 'placeholder' ] = __( 'Add gift message' , 'notanotherbill' ) ;
				}
			}
		}

		return $checkout_fields ;
	}

	/**
	* Copy personalised message into customer note. 
	*
	* @since 2.2.18.4
	*/
	public function filter_order_comments ( $value , $input )
	{
		if ( ! empty( $value ) ) return $value ;

		$cart_subscription = self::cart_contains_subscription( ) ;

		// Customer is buying a subscription
		if ( $cart_subscription )
		{
			// Get recipient type
			$recipient = WCS_Utils::get_cart_addon( $cart_subscription , 'Recipient' ) ;

			// Customer is gifting the subscription
			if ( $recipient == 'For a gift' )
			{
				// Copy personalised message into field
				$message = WCS_Utils::get_cart_addon( $cart_subscription , 'Personalised Message - Text' ) ;

				if ( ! empty( $message ) ) return $message ;
			}
		}

		return $value ;
	}

	/**
	 * Checkout account fields filter
	 *
	 * @param array $checkout_fields
	 * @return array
	 */
	public function account_fields ( $checkout_fields )
	{
		// Move billing email to account fields and reorder
		$checkout_fields = array(
			'billing_email' => array( 'placeholder' => __( 'Email' , 'woocommerce' ) , 'required' => 0 ) ,
			'account_password' => $checkout_fields[ 'account_password' ]
		) ;

		// Add placeholder and remove label
		foreach ( $checkout_fields as $key => $field )
		{
			if ( isset( $field[ 'label' ] ) )
			{
				switch ( $key )
				{
					case 'account_password' :
						$checkout_fields[ $key ][ 'placeholder' ] = __( 'Password' , 'notanotherbill' ) ;
						break ;

					case 'billing_email' :
						$subscription = $this->cart_contains_subscription( ) ;

						if ( isset( $subscription ) )
						{
							$addons = $subscription[ 'addons' ] ;

							if ( isset( $addons ) )
							{
								foreach ( $addons as $addon_key => $addon )
								{
									if ( $addon[ 'name' ] == 'Subscriber Email Address - Text' )
									{
										$checkout_fields[ $key ][ 'value' ] = $addon[ 'value' ] ;
									}
								}
							}
						}

					default :
						$checkout_fields[ $key ][ 'placeholder' ] = $field[ 'label' ] ;
						break ;
				}
			}
		}

		return $checkout_fields ;
	}

	/**
	 * Ship to different address filter
	 *
	 * @param array $ship_to_address_address
	 * @return array
	 */
	public function ship_to_different_address_checked ( $ship_to_address_address )
	{
		$cart_subscription = WC_NAB_Checkout::cart_contains_subscription( ) ;

		if ( $cart_subscription )
		{
			foreach ( $cart_subscription[ 'addons' ] as $addon )
			{
				if ( $addon[ 'name' ] == 'Recipient' )
				{
					if ( sanitize_title( $addon[ 'value' ] ) == 'for-a-gift' )
					{
						return true ;
					}
				}
			}
		}

		return $ship_to_address_address ;
	}

	/**
	 * Ensures local API requests are made securely.
	 *
	 * @param array $ship_to_address_address
	 * @return array
	 */
	public function unforce_ssl_checkout ( )
	{
		return ! WC_NAB_API::is_api( ) ;
	}

	/**
	 * Decide whether a payment is needed. If the cart only contains a subscription and nothing else return false.
	 *
	 * @param bool $needs_payment
	 * @param WC_Cart $cart
	 * @return bool
	 * @since 2.2.22.10
	 */
	public function cart_needs_payment ( $needs_payment , $cart )
	{
		if ( WC_Subscriptions_Cart::cart_contains_subscription( ) && $cart->total <= 0 && $cart->cart_contents_count == 1 )
		{
			if ( WC_Subscriptions_Cart::get_cart_subscription_interval( ) == 1 && WC_Subscriptions_Cart::get_cart_subscription_period( ) == 'month' )
			{
				return $needs_payment ;
			}

			return false ;
		}

		return $needs_payment ;
	}

	/**
	* Should the terms and conditions checkbox be checked by default.
	*
	* @since 2.2.17.3
	*/
	public function terms_is_checked_default ( $checked )
	{
		if ( is_dev( ) ) return true ;

		return $checked ;
	}

	/**
	* Does the cart contain a subscription product
	*/
	public static function cart_contains_subscription ( )
	{
		foreach ( WC( )->cart->get_cart( ) as $cart_item_key => $cart_item )
		{
			if ( WC_Subscriptions_Product::is_subscription( $cart_item[ 'product_id' ] ) )
			{
				return $cart_item ;
			}
		}

		return false ;
	}

	/**
	* Does the package contain a subscription product
	*/
	public static function package_contains_subscription ( $package )
	{
		foreach ( $package[ 'contents' ] as $cart_item_key => $cart_item )
		{
			if ( WC_Subscriptions_Product::is_subscription( $cart_item[ 'product_id' ] ) )
			{
				return $cart_item ;
			}
		}

		return false ;
	}
}

new WC_NAB_Checkout( ) ;
