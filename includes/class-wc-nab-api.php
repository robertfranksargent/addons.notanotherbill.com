<?php

/**
 * API
 *
 * @class 		WC_NAB_API
 * @version		2.2.8.7
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_API
{
	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		add_filter( 'json_api_controllers' , array( $this , 'wc_controller' ) , 10 , 1 ) ;
		add_filter( 'json_api_wc_controller_path' , array( $this , 'wc_controller_path' ) , 10 , 1 ) ;
	}

	public function wc_controller ( $controllers )
	{
		$controllers[ ] = 'wc' ;
		return $controllers ;
	}

	public function wc_controller_path ( $default_path )
	{
		return __DIR__ . '/controllers/class-wc-api-controller.php' ;
	}

	public static function is_api ( )
	{
		$query = new JSON_API_Query( ) ;
		$json = $query->get( 'json' ) ;

		return isset( $json ) ;
	}
}

new WC_NAB_API( ) ;