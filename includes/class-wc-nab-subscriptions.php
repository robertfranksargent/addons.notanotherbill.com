<?php
/**
 * Subscriptions
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce
 * @version     2.2.21.14
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_NAB_Subscriptions' ) ) :

/**
 * WC_NAB_Subscriptions Class
 */
class WC_NAB_Subscriptions
{
	/**
	 * Constructor
	 * 
	 * @since 2.2.18.7
	 */
	public function __construct ( )
	{
		add_action( 'init' , array( $this , 'maybe_update_subscription_renew_on_expiry' ) , 100 ) ;
		add_action( 'cancelled_subscription' , array( $this , 'subscription_cancelled' ) , 10 , 2 ) ;
		add_action( 'reactivated_subscription' , array( $this , 'subscription_reactivated' ) , 10 , 2 ) ;
		add_action( 'subscription_put_on-hold' , array( $this , 'subscription_paused' ) , 10 , 2 ) ;
		add_action( 'subscriptions_put_on_hold_for_order' , array( $this , 'subscription_paused_for_order' ) , 10 , 1 ) ;

		add_action( 'woocommerce_order_status_processing' , array( $this , 'subscription_maybe_dispatch_today' ) , 11 , 1 ) ;
		add_action( 'woocommerce_order_status_processing' , array( $this , 'subscription_create_first_present_order' ) , 12 , 1 ) ;
		add_action( 'woocommerce_order_status_processing' , array( $this , 'subscription_update_associated_orders' ) , 11 , 1 ) ;

		add_action( 'scheduled_subscription_reactivation' , array( $this , 'subscription_reactivation' ) , 10 , 2 ) ;
		add_action( 'scheduled_subscription_paused_notification' , array( $this , 'subscription_paused_notification' ) , 10 , 1 ) ;

		add_action( 'woocommerce_subscriptions_renewal_order_created' , array( $this , 'subscriptions_renewal_order_created' ) , 10 , 4 ) ;
		add_action( 'processed_subscription_payment_failure' , array( $this , 'subscription_payment_failure' ) , 10 , 2 ) ;
		add_action( 'subscription_sign_up_failed' , array( $this , 'subscription_payment_failure' ) , 10 , 2 ) ;
		add_action( 'woocommerce_renewal_order_payment_complete' , array( $this , 'subscription_renewal_payment_complete' ) , 10 , 1 ) ;
		add_action( 'woocommerce_checkout_order_processed' , array( $this , 'checkout_order_processed' ) , 10 , 2 ) ;

		add_action( 'wp_ajax_renew_dont_show_again' , array( $this , 'renew_dont_show_again' ) ) ;
		add_action( 'wp_ajax_nopriv_renew_dont_show_again' , array( $this , 'renew_dont_show_again' ) ) ;

		add_filter( 'woocommerce_subscription_calculated_expiration_date' , array( $this , 'calculate_subscription_expiration_date' ) , 10 , 3 ) ;
		add_filter( 'woocommerce_my_account_my_subscriptions_actions' , array( $this , 'my_account_my_subscriptions_actions' ) , 11 , 2 ) ;
		add_filter( 'woocommerce_subscription_can_be_changed_to_new-payment-method' , array( $this , 'subscription_can_change_payment_method' ) , 10 , 3 ) ;
	
		add_filter( 'woocommerce_order_again_cart_item_data', array( $this , 'order_again_cart_item_data' ) , 20 , 3 ) ;
		add_filter( 'woocommerce_subscriptions_max_failed_payments_exceeded' , array( $this , 'subscription_max_failed_payments_exceeded' ) , 10 , 3 ) ;
		add_filter( 'woocommerce_subscriptions_thank_you_message' , array( $this , 'subscriptions_thank_you_message' ) , 10 , 2 ) ;
	}

	/**
	* Add original order post meta.
	*
	* @since 2.2.17.7
	*/
	function checkout_order_processed ( $order_id , $posted )
	{
		// Check basket contains subscription
		if ( WC_Subscriptions_Cart::cart_contains_subscription( ) )
		{
			$order = new WC_Order( $order_id ) ;
			$user_id = get_current_user_id( ) ;

			// Check ownership
			if ( $user_id == $order->get_user_id( ) )
			{
				// Get customer's existing subscription orders
				$existing_order_ids = WC_Subscriptions_Order::get_users_subscription_orders( $user_id ) ;

				// Find customer's subscription order with "_renew_cart_item_hash" meta
				foreach ( $existing_order_ids as $existing_order_id )
				{
					$renew_cart_item_hash = get_post_meta( $existing_order_id , '_renew_cart_item_hash' , true ) ;

					if ( ! empty( $renew_cart_item_hash ) ) break ;
				}

				// Get cart item from hash
				if ( isset( $renew_cart_item_hash ) && ! empty( WC( )->cart->get_cart_item( $renew_cart_item_hash ) ) )
				{
					// Set "_original_order" meta to $existing_order_id
					update_post_meta( $order_id , '_original_order' , $existing_order_id ) ;

					// Delete "_renew_cart_item_hash" meta
					delete_post_meta( $existing_order_id , '_renew_cart_item_hash' , $renew_cart_item_hash ) ;
				}
			}
		}
	}

	/**
	* Remove subscriptions thank you message.
	*
	* @since 2.2.17.7
	*/
	public function subscriptions_thank_you_message ( $thank_you_message, $order_id )
	{
		return '' ;
	}

	/**
	* Cancel 48-hour payment schedule.
	*
	* @since 2.2.14.5
	* @param int $order_id
	*/
	public function subscription_renewal_payment_complete ( $order_id )
	{
		$order = new WC_Order( $order_id ) ;

		// Order contains a subscription and it is a renewal
		if ( WC_Subscriptions_Order::order_contains_subscription( $order ) && WC_Subscriptions_Renewal_Order::is_renewal( $order ) )
		{
			// Get the subscription
			$subscription = WCS_Utils::get_order_subscription( $order ) ;
			$subscription_key = WC_Subscriptions_Manager::get_subscription_key( $order_id , $subscription[ 'product_id' ] ) ;

			// Get the next payment date
			$next_payment_date = WC_Subscriptions_Manager::get_next_payment_date( $subscription_key ) ;

			// Next payment must be in the future
			if ( strtotime( $next_payment_date ) < time( ) )
			{
				// Get the subscription period and interval
				$subscription_period = WC_Subscriptions_Order::get_subscription_period( $order , $subscription[ 'product_id' ] ) ;
				$subscription_interval = WC_Subscriptions_Order::get_subscription_interval( $order , $subscription[ 'product_id' ] ) ;

				// Only for 1 month rolling subscriptions
				if ( $subscription_period == 'month' && $subscription_interval == 1 )
				{
					// Add the subscription period to today's date to get the next renewal month, then set the next payment date to the 1st of that month
					WC_Subscriptions_Manager::set_next_payment_date( $subscription_key , '' , strtotime( date( 'Y-m-01 H:i:s' , strtotime( '+' . $subscription_interval . ' ' . $subscription_period ) ) ) ) ;
				}
			}
		}
	}

	/**
	* Handle failed payments on subscriptions
	*
	* @since 2.2.15.11
	* @param int $user_id
	* @param string $subscription_key
	*/
	public function subscription_payment_failure ( $user_id , $subscription_key )
	{
		// Get the order ID
		$order_product_id = explode( '_' , $subscription_key ) ;
		$order_id = $order_product_id[ 0 ] ;
		$order = new WC_Order( $order_id ) ;
		
		// Order must contain renewals
		if ( WC_Subscriptions_Renewal_Order::get_renewal_order_count( $order_id ) > 0 )
		{
			// Get the subscription
			$subscription = WC_Subscriptions_Manager::get_subscription( $subscription_key ) ;

			if ( ! empty( $subscription ) )
			{
				$failed_payment_count = WC_Subscriptions_Manager::get_subscriptions_failed_payment_count( $subscription_key , $user_id ) ;

				if ( $failed_payment_count < 2 )
				{
					if ( $failed_payment_count == 0 )
					{
						// Get next shipping date
						$shipping_dates = explode( ',' , get_option( 'nab-shipping-dates' ) ) ;

						asort( $shipping_dates ) ;

						// Get next shipping date in the future
						foreach ( $shipping_dates as $shipping_date )
						{
							if ( strtotime( $shipping_date ) > time( ) )
							{
								$future_shipping_date = $shipping_date ;

								break ;
							}
						}

						// Future shipping date exists
						if ( isset( $future_shipping_date ) )
						{
							// Get shipping turnaround
							$shipping_turnaround = get_option( 'nab-shipping-turnaround' ) ;

							// Get time to future shipping date
							$time_to_shipping_date = strtotime( $future_shipping_date ) - time( ) ;

							// Next shipping is less than 48 hours away
							if ( $time_to_shipping_date - $shipping_turnaround < 172800 )
							{
								// Send customer a payment reminder
								WC_Subscriptions_Renewal_Order::send_customer_renewal_order_email( $order ) ;

								$order->add_order_note( 'Sending the customer a renewal email.' ) ;

								return ;
							}
						}

						// Stripe customers only
						if ( $order->recurring_payment_method == 'stripe' )
						{
							// Reactivate subscription
							WC_Subscriptions_Manager::reactivate_subscription( $user_id , $subscription_key ) ;

							// Retry payment in 48 hrs
							WC_Subscriptions_Manager::set_next_payment_date( $subscription_key , $user_id , strtotime( '+2 days' ) ) ;

							$order->add_order_note( 'Reactivating the subscription and will retry payment in 48 hours.' ) ;
						}
					}
					else
					{
						// Send customer a payment reminder
						WC_Subscriptions_Renewal_Order::send_customer_renewal_order_email( $order ) ;

						$order->add_order_note( 'Sending the customer a renewal email.' ) ;
					}
				}
			}
		}
	}

	/**
	* Determine whether max failed payments has been exceeded
	*
	* @since 2.2.13.6
	* @param bool $exceeded
	* @param int $user_id
	* @param string $subscription_key
	*/
	public function subscription_max_failed_payments_exceeded ( $exceeded , $user_id , $subscription_key )
	{
		$failed_payment_count = WC_Subscriptions_Manager::get_subscriptions_failed_payment_count( $subscription_key , $user_id ) ;

		return $failed_payment_count >= 3 ;
	}

	/**
	* Maybe add a dispatch today flag to the order
	*
	* @since 2.2.17
	*/
	public function subscription_maybe_dispatch_today ( $order_id )
	{
		$order = new WC_Order( $order_id ) ;

		// Order contains a subscription and it is not a renewal
		if ( WC_Subscriptions_Order::order_contains_subscription( $order ) && ! WC_Subscriptions_Renewal_Order::is_renewal( $order ) )
		{
			$subscription = WCS_Utils::get_order_subscription( $order ) ;
			$subscription_key = WC_Subscriptions_Manager::get_subscription_key( $order_id , $subscription[ 'product_id' ] ) ;
			$subscription_period = WC_Subscriptions_Order::get_subscription_period( $order , $subscription[ 'product_id' ] ) ;
			$subscription_interval = WC_Subscriptions_Order::get_subscription_interval( $order , $subscription[ 'product_id' ] ) ;

			$start_date = $subscription[ 'Start Date - Date' ] ;

			// Subscription start date is today
			if ( strtotime( $start_date ) == strtotime( date( 'Y-m-d' ) ) )
			{
				// Add dispatch today post meta
				add_post_meta( $order_id , '_dispatch_today' , 1 , true ) ;

				// Get order item
				$order_item = WC_Subscriptions_Order::get_item_by_subscription_key( $subscription_key ) ;

				// Update order item meta start date to 1st of month
				wc_update_order_item_meta( $order_item[ 'order_item_id' ] , 'Start Date - Date' , date( 'Y-m-01' , strtotime( $start_date ) ) , $start_date ) ;

				// Update subscription start date to 1st of month
				wc_update_order_item_meta( $order_item[ 'order_item_id' ] , '_subscription_start_date' , date( 'Y-m-01 H:i:s' , strtotime( $start_date ) ) ) ;

				// Apply expiry date to non-rolling subscriptions
				if ( $subscription_period == 'year' || $subscription_interval > 1 )
				{
					// Get user ID
					$user_id = $order->get_user( )->ID ;

					// Get the 10th of the final month of the subscription
					$expiry_date = strtotime( '+' . $subscription_interval . ' ' . $subscription_period , strtotime( $start_date ) ) ;
					$expiry_date = strtotime( '-1 month' , $expiry_date ) ;
					$expiry_date = strtotime( date( 'Y-m-10 H:i:s' , $expiry_date ) ) ;

					// Update expiry date
					WC_Subscriptions_Manager::set_expiration_date( $subscription_key , $user_id , $expiry_date ) ;
				}
			}

			// Only for 1 month rolling subscriptions
			if ( $subscription_period == 'month' && $subscription_interval == 1 )
			{
				// Calculate renewal date			
				$start_date_time = date( 'Y-m-d H:i:s' , strtotime( $start_date . ' ' . date( 'H:i:s' , time( ) ) ) ) ;
				$renewal_date_time = date( 'Y-m-01 H:i:s' , strtotime( '+' . $subscription_interval . ' ' . $subscription_period , strtotime( $start_date_time ) ) ) ;
				
				// Set next payment to the 1st of the renewal month
				WC_Subscriptions_Manager::set_next_payment_date( $subscription_key , '' , strtotime( $renewal_date_time ) ) ;
			}
		}
	}

	/**
	* Create an additional order for the subscriber's first present.
	*
	* @since 2.2.21.14
	*/
	public function subscription_create_first_present_order ( $original_order_id )
	{
		// Check original order is not a renewal
		if ( ! WC_Subscriptions_Renewal_Order::is_renewal( $original_order_id ) )
		{
			// Might get an existing first present order
			$first_present_order_id = get_post_meta( $original_order_id , '_first_present_order_id' , true ) ;
			$first_present_order_status = get_post_status( $first_present_order_id ) ;

			// There is not an existing first present order
			if ( ! is_string( $first_present_order_status ) || $first_present_order_status == 'trash' )
			{
				// Get order by ID
				$original_order = new WC_Order( $original_order_id ) ;

				// Check order contains subscription
				if ( WC_Subscriptions_Order::order_contains_subscription( $original_order ) )
				{
					// Extract subscription from order
					$subscription = WCS_Utils::get_order_subscription( $original_order ) ;

					if ( array_key_exists( 'First Present - ID' , $subscription ) && is_numeric( $subscription[ 'First Present - ID' ] ) )
					{
						// Get first present
						if ( $first_present = wc_get_product( $subscription[ 'First Present - ID' ] ) )
						{
							// Build order data
							$order_data = array(
								'post_name' => $original_order->post->post_name . '-subscription-' . $original_order_id ,
								'post_type' => 'shop_order' ,
								'post_title' => $original_order->post->post_title . ' (Subscription #' . $original_order_id . ')' ,
								'post_status' => 'wc-processing' ,
								'ping_status' => 'closed' ,
								'post_excerpt' => $original_order->customer_note ,
								'post_author' => $original_order->customer_user ,
								'post_password' => uniqid( 'order_' ) ,
								'post_date' => $original_order->order_date ,
								'comment_status' => 'open'
							) ;

							// Create order
							$first_present_order_id = wp_insert_post( $order_data , true ) ;

							if ( ! is_wp_error( $first_present_order_id ) )
							{
								// Add order meta data
								add_post_meta( $first_present_order_id , '_order_total', 0 , true ) ;
								add_post_meta( $first_present_order_id , '_customer_user' , $original_order->customer_user , true ) ;
								add_post_meta( $first_present_order_id , '_completed_date' , $original_order->order_date ) ;
								add_post_meta( $first_present_order_id , '_order_currency' , $original_order->currency , true ) ;
								add_post_meta( $first_present_order_id , '_paid_date' , $original_order->order_date ) ;

								// Add billing information
								add_post_meta( $first_present_order_id , '_billing_address_1' , $original_order->billing_address_1 , true ) ;
								add_post_meta( $first_present_order_id , '_billing_address_2' , $original_order->billing_address_2 , true ) ;
								add_post_meta( $first_present_order_id , '_billing_city' , $original_order->billing_city , true ) ;
								add_post_meta( $first_present_order_id , '_billing_state' , $original_order->billing_state , true ) ;
								add_post_meta( $first_present_order_id , '_billing_postcode' , $original_order->billing_postcode , true ) ;
								add_post_meta( $first_present_order_id , '_billing_country' , $original_order->billing_country , true ) ;
								add_post_meta( $first_present_order_id , '_billing_email' , $original_order->billing_email , true ) ;
								add_post_meta( $first_present_order_id , '_billing_first_name' , $original_order->billing_first_name , true ) ;
								add_post_meta( $first_present_order_id , '_billing_last_name' , $original_order->billing_last_name , true ) ;
								add_post_meta( $first_present_order_id , '_billing_phone' , $original_order->billing_phone , true ) ;

								// Get shipping packages
								$packages = get_post_meta( $original_order->id , '_wcms_packages' , true ) ;

								// Add shipping information
								if ( isset( $packages ) && count( $packages ) > 1 )
								{
									foreach ( $packages as $package )
									{
										foreach ( $package[ 'contents' ] as $package_item )
										{
											// Assume only one subscription per order
											if ( WC_Subscriptions_Product::is_subscription( $package_item[ 'product_id' ] ) )
											{
												$destination = ! empty( $package[ 'destination' ] ) ? $package[ 'destination' ] : $package[ 'full_address' ] ;

												add_post_meta( $first_present_order_id , '_shipping_first_name' , $destination[ 'first_name' ] , true ) ;
												add_post_meta( $first_present_order_id , '_shipping_last_name' , $destination[ 'last_name' ] , true ) ;
												add_post_meta( $first_present_order_id , '_shipping_address_1' , $destination[ 'address_1' ] , true ) ;
												add_post_meta( $first_present_order_id , '_shipping_address_2' , $destination[ 'address_2' ] , true ) ;
												add_post_meta( $first_present_order_id , '_shipping_city' , $destination[ 'city' ] , true ) ;
												add_post_meta( $first_present_order_id , '_shipping_state' , $destination[ 'state' ] , true ) ;
												add_post_meta( $first_present_order_id , '_shipping_postcode' , $destination[ 'postcode' ] , true ) ;
												add_post_meta( $first_present_order_id , '_shipping_country' , $destination[ 'country' ] , true ) ;
											}
										}
									}
								}
								else
								{
									add_post_meta( $first_present_order_id , '_shipping_first_name' , $original_order->shipping_first_name , true ) ;
									add_post_meta( $first_present_order_id , '_shipping_last_name' , $original_order->shipping_last_name , true ) ;
									add_post_meta( $first_present_order_id , '_shipping_address_1' , $original_order->shipping_address_1 , true ) ;
									add_post_meta( $first_present_order_id , '_shipping_address_2' , $original_order->shipping_address_2 , true ) ;
									add_post_meta( $first_present_order_id , '_shipping_city' , $original_order->shipping_city , true ) ;
									add_post_meta( $first_present_order_id , '_shipping_state' , $original_order->shipping_state , true ) ;
									add_post_meta( $first_present_order_id , '_shipping_postcode' , $original_order->shipping_postcode , true ) ;
									add_post_meta( $first_present_order_id , '_shipping_country' , $original_order->shipping_country , true ) ;
									add_post_meta( $first_present_order_id , '_shipping_phone' , $original_order->shipping_phone , true ) ;
								}

								// Add send today flag
								add_post_meta( $first_present_order_id , '_dispatch_today' , 1 , true ) ;

								// Add item
								$item_id = wc_add_order_item( $first_present_order_id ,
									array(
										'order_item_name' => $first_present->get_title( ) ,
										'order_item_type' => 'line_item'
									)
								) ;

								if ( $item_id )
								{
									// Add item meta data
									wc_add_order_item_meta( $item_id , '_qty', 1 ); 
									wc_add_order_item_meta( $item_id , '_tax_class', $first_present->get_tax_class( ) ) ;
									wc_add_order_item_meta( $item_id , '_product_id' , $first_present->id ) ;
									wc_add_order_item_meta( $item_id , '_variation_id' , '' ) ;
									wc_add_order_item_meta( $item_id , '_line_subtotal' , 0 ) ;
									wc_add_order_item_meta( $item_id , '_line_total' , 0 ) ;
									wc_add_order_item_meta( $item_id , '_line_tax' , 0 ) ;
									wc_add_order_item_meta( $item_id , '_line_subtotal_tax' , 0 ) ;

									// Add item attributes
									if ( array_key_exists( 'First Present - Attributes' , $subscription ) )
									{
										$first_present_order_atts = $subscription[ 'First Present - Attributes' ] ;

										if ( ! empty( $first_present_order_atts ) )
										{
											$atts = json_decode( $first_present_order_atts ) ;

											if ( json_last_error( ) == JSON_ERROR_NONE )
											{
												foreach ( $atts as $key => $value )
												{
													wc_add_order_item_meta( $item_id , str_replace( 'attribute_' , '' , $key ) , $value ) ;
												}
											}
										}
									}
								}
							}

							// Add note to first present order
							$first_present_order = new WC_Order( $first_present_order_id ) ;
							$first_present_order->add_order_note( sprintf( 'This is the first present choice for Subscription Order <a href="%s">#%d</a>.' , admin_url( 'post.php?action=edit&post=' . $original_order_id ) , $original_order_id ) ) ;

							// Update/add first present order meta to original order
							update_post_meta( $original_order_id , '_first_present_order_id' , $first_present_order_id ) ;

							// Add note to original order
							$original_order->add_order_note( sprintf( 'An order has been created for the subscriber\'s first present choice – <a href="%s">#%d</a>.' , admin_url( 'post.php?action=edit&post=' . $first_present_order_id ) , $first_present_order_id ) ) ;

							// Reduce product stock
							$first_present->reduce_stock( ) ;

							// Send order email to admin
							WC( )->mailer( ) ;

							do_action( 'woocommerce_order_status_pending_to_processing_notification' , $first_present_order_id ) ;
						}
					}
				}
			}
		}
	}

	/**
	* Add associated order IDs to the order's post meta and in reverse
	*
	* @since 2.2.21.5
	*/
	public function subscription_update_associated_orders ( $order_id )
	{
		if ( WC_Subscriptions_Order::order_contains_subscription( $order_id ) && ! WC_Subscriptions_Renewal_Order::is_renewal( $order_id ) )
		{
			$associated_order_ids = self::get_associated_subscription_orders( $order_id ) ;

			if ( ! empty( $associated_order_ids ) )
			{
				update_post_meta( $order_id , '_associated_subscription_orders' , $associated_order_ids ) ;

				foreach ( $associated_order_ids as $associated_order_id )
				{
					if ( $associated_order_id != $order_id )
					{
						$reverse_associated_order_ids = self::get_associated_subscription_orders( $associated_order_id ) ;

						update_post_meta( $associated_order_id , '_associated_subscription_orders' , $reverse_associated_order_ids ) ;
					}
				}
			}
		}
	}

	/**
	* Copy previous gifts from original order to renewal order
	*
	* @since 2.2.12.17
	* @param WC_Order
	* @param WC_Order
	* @param int
	* @param int
	*/
	public function subscriptions_renewal_order_created ( $renewal_order , $original_order , $product_id , $new_order_role )
	{
		if ( WC_Subscriptions_Order::order_contains_subscription( $renewal_order ) && WC_Subscriptions_Order::order_contains_subscription( $original_order ) )
		{
			$renewal_subscriptions = WC_Subscriptions_Order::get_recurring_items( $renewal_order ) ;
			$original_subscriptions = WC_Subscriptions_Order::get_recurring_items( $original_order ) ;

			if ( sizeof( $renewal_subscriptions ) && sizeof( $original_subscriptions ) )
			{
				$renewal_subscription = $renewal_subscriptions[ 0 ] ;
				$renewal_subscription_key = WC_Subscriptions_Manager::get_subscription_key( $renewal_order->ID , $subscription[ 'product_id' ] ) ;
				$renewal_order_item_id = WC_Subscriptions_Order::get_item_id_by_subscription_key( $renewal_subscription_key ) ;

				$original_subscription = $original_subscriptions[ 0 ] ;
				$original_subscription_key = WC_Subscriptions_Manager::get_subscription_key( $original_order->ID , $subscription[ 'product_id' ] ) ;
				$original_order_item_id = WC_Subscriptions_Order::get_item_id_by_subscription_key( $original_subscription_key ) ;

				$original_previous_gifts = wc_get_order_item_meta( $original_order_item_id , '_subscription_previous_gifts' ) ;

				wc_update_order_item_meta( $renewal_order_item_id , '_subscription_previous_gifts' , $original_previous_gifts ) ;
			}
		}
	}

	/**
	* Send subscription paused notification
	*
	* @since 2.2.12.17
	* @param string $subscription_key
	*/
	public function subscription_paused_notification ( $subscription_key )
	{
		if ( ! $subscription_key ) return ;

		global $woocommerce ;

		$woocommerce->mailer( ) ;
		
		$subscription = WC_Subscriptions_Manager::get_subscription( $subscription_key ) ;

		if ( $subscription[ 'status' ] == 'on-hold' )
		{
			do_action( 'woocommerce_subscription_paused_notification' , $subscription[ 'order_id' ] ) ;
		}
	}

	/**
	* Reactivates a subscription
	*
	* @since 2.2.12.8
	* @param int $user_id
	* @param string $subscription_key
	*/
	public function subscription_reactivation ( $user_id , $subscription_key )
	{
		WC_Subscriptions_Manager::reactivate_subscription( $user_id , $subscription_key ) ;
	}

	/**
	* Subscription reactivated notification
	*
	* @since 2.2.12.22
	* @param int $user_id
	* @param string $subscription_key
	*/
	public function subscription_reactivated ( $user_id , $subscription_key )
	{
		// Check if scheduled pause notification is pending
		$scheduled_actions = wc_get_scheduled_actions( 
			array( 
				'hook' => 'scheduled_subscription_paused_notification' ,
				'args' => array(
					'subscription_key' => $subscription_key
				) ,
				'status' => 'pending'
			)
		) ;

		// Send email if no scheduled pause actions pending
		if ( sizeof( $scheduled_actions ) == 0 )
		{
			global $woocommerce ;

			$woocommerce->mailer( ) ;

			if ( ! $subscription_key ) return ;

			$subscription = WC_Subscriptions_Manager::get_subscription( $subscription_key ) ;

			do_action( 'woocommerce_subscription_reactivated_notification' , $subscription[ 'order_id' ] ) ;
		}
	}

	/**
	* Schedule subscription paused notification after 30 minutes
	*
	* @since 2.2.12.17
	* @param int $user_id
	* @param string $subscription_key
	*/
	public function subscription_paused ( $user_id , $subscription_key )
	{
		$notification_time = gmdate( 'U' , strtotime( '+30 minutes' ) ) ;

		wc_schedule_single_action( $notification_time , 'scheduled_subscription_paused_notification' , array( 'subscription_key' => $subscription_key ) ) ;	
	}

	/**
	* Subscription paused notification
	*
	* @since 2.2.11.9
	* @param int $order
	*/
	public function subscription_paused_for_order ( $order )
	{
		global $woocommerce ;

		$woocommerce->mailer( ) ;

		do_action( 'woocommerce_subscription_paused_notification' , is_object( $order ) ? $order->id : $order ) ;
	}

	/**
	* Set cancelled rolling subscription expiry date to 10th of next shipping date month.
	*
	* @since 2.2.18.7
	*/
	public function subscription_cancelled ( $user_id , $subscription_key )
	{
		$subscription = WC_Subscriptions_Manager::get_subscription( $subscription_key ) ;
		$order = new WC_Order( $subscription[ 'order_id' ] ) ;
		$subscription_period = WC_Subscriptions_Order::get_subscription_period( $order , $subscription[ 'product_id' ] ) ;
		$subscription_interval = WC_Subscriptions_Order::get_subscription_interval( $order , $subscription[ 'product_id' ] ) ;

		// 1 month rolling subscriptions only
		if ( $subscription_period == 'month' && $subscription_interval == 1 )
		{
			// Get order item
			$order_item = WC_Subscriptions_Order::get_item_by_subscription_key( $subscription_key ) ;

			// Get the next and last shipping dates
			$next_shipping_date = strtotime( WCS_Utils::get_next_shipping_date( ) ) ;
			$last_shipping_date = strtotime( WCS_Utils::get_last_shipping_date( ) ) ;

			// Get the last payment date
			$last_payment_date = strtotime( WC_Subscriptions_Manager::get_last_payment_date( $subscription_key , $user_id ) ) ;

			// Last payment was before or after previous shipping date
			if ( ! empty( $last_shipping_date ) && $last_payment_date < $last_shipping_date )
			{
				// Match expiry date to end date
				$expiry_date = wc_get_order_item_meta( $order_item[ 'order_item_id' ] , '_subscription_end_date' ) ;
			}
			else
			{
				if ( empty( $next_shipping_date ) ) $next_shipping_date = time( ) ;

				// Base the expiry date on the next shipping date
				$expiry_date = date( 'Y-m-10 00:00:00' , $next_shipping_date ) ;
			}

			// Update expiry date order item meta
			wc_update_order_item_meta( $order_item[ 'order_item_id' ] , '_subscription_expiry_date' , $expiry_date ) ;
		}
	}

	/**
	 * Set non-rolling subscriptions to expire on the 10th of the month before their renewal date.
	 *
	 * @since 2.2.11.9
	 * @param array $ship_to_address_address
	 * @return array
	 */
	public function calculate_subscription_expiration_date ( $expiration_date , $subscription_key , $user_id )
	{
		// Get the subscription
		$subscription = WC_Subscriptions_Manager::get_subscription( $subscription_key ) ;

		// Ensure subscription exists
		if ( empty( $subscription ) ) return $expiration_date ;

		// Get the order associated with this subscription
		$order = new WC_Order( $subscription[ 'order_id' ] ) ;
		$item = WC_Subscriptions_Order::get_item_by_product_id( $order ) ;

		// Get subscription details
		$subscription_period = WC_Subscriptions_Order::get_subscription_period( $order , $subscription[ 'product_id' ] ) ;
		$subscription_length = WC_Subscriptions_Order::get_subscription_length( $order , $subscription[ 'product_id' ] ) ;
		$subscription_interval = WC_Subscriptions_Order::get_subscription_interval( $order , $subscription[ 'product_id' ] ) ;

		// Apply changes to subscriptions that are not set to renew on expiry and longer than 1 month
		if ( ( ! isset( $item[ 'subscription_renew_on_expiry' ] ) or ! $item[ 'subscription_renew_on_expiry' ] ) and ( $subscription_period == 'year' || $subscription_interval > 1 ) )
		{
			$is_mysql = date( 'Y-m-d H:i:s' , $expiration_date ) === $expiration_date ;
			$start_date = $item[ 'Start Date - Date' ] ;

			// Get expiration date based on start date and subscription length
			$expiration_date = WC_Subscriptions::add_months( strtotime( $start_date ) , $subscription_length ) ;

			// The 10th of the previous month
			$expiration_date = strtotime( '-1 month' , $expiration_date ) ;
			$expiration_date = strtotime( '+9 days' , $expiration_date ) ;

			// 12 pm midday
			$expiration_date = strtotime( date( 'Y-m-d 12:00:00', $expiration_date ) );

			// Convert to MySQL date if necessary
			if ( $is_mysql ) $expiration_date = date( 'Y-m-d H:i:s' , $expiration_date ) ;
		}
		else
		{
			// Ensure expiration date is not before now
			if ( strtotime( $expiration_date ) < time( ) ) $expiration_date = 0 ;
		}

		return $expiration_date ;
	}

	/**
	 * Edit subscription account actions.
	 *
	 * @since 2.2.17
	 * @param array $all_actions
	 * @param array $subscritpions
	 * @return array
	 */
	public function my_account_my_subscriptions_actions ( $all_actions , $subscriptions )
	{
		$filtered_actions = array( ) ;

		foreach ( $all_actions as $subscription_key => $actions )
		{
			foreach ( $actions as $key => $value )
			{
				switch ( $key )
				{
					case 'renew' :
						$actions[ $key ][ 'name' ] = __( 'Renew Subscription' , 'notanotherbill' ) ;
						break ;

					case 'reactivate' :
						$actions[ $key ][ 'name' ] = __( 'Resume now' , 'notanotherbill' ) ;
						break ;

					case 'pay' :
						$actions[ $key ][ 'name' ] = __( 'Pay for order' , 'notanotherbill' ) ;
						break ;
				}
			}

			unset( $actions[ 'change_address' ] ) ;
			unset( $actions[ 'cancel' ] ) ;
			unset( $actions[ 'suspend' ] ) ;

			$subscription = WC_Subscriptions_Manager::get_subscription( $subscription_key ) ;
			$order = new WC_Order( $subscription[ 'order_id' ] ) ;
			$order_item = WC_Subscriptions_Order::get_item_by_subscription_key( $subscription_key ) ;

			$subscription_status = $subscription[ 'status' ] ;
			$subscription_period = WC_Subscriptions_Order::get_subscription_period( $order , $subscription[ 'product_id' ] ) ;
			$subscription_interval = WC_Subscriptions_Order::get_subscription_interval( $order , $subscription[ 'product_id' ] ) ;
			$requires_manual_renewal = WC_Subscriptions_Order::get_meta( $order , '_wcs_requires_manual_renewal' ) ;
			$renew_on_expiry = wc_get_order_item_meta( $order_item[ 'order_item_id' ] , '_subscription_renew_on_expiry' ) ;

			if ( $subscription_status == 'active' && ( $subscription_period == 'year' || $subscription_interval > 1 ) && $requires_manual_renewal !== 'true' && $renew_on_expiry )
			{
				$renew_on_expiry = wc_get_order_item_meta( $order_item[ 'order_item_id' ] , '_subscription_renew_on_expiry' ) ;

				$action_link = add_query_arg( array( 'subscription_key' => $subscription_key , 'update_subscription_renew_on_expiry' => $renew_on_expiry ? 0 : 1 ) ) ;
				$action_link = wp_nonce_url( $action_link , $subscription_key ) ;

				$actions[ 'renew-on-expiry' ] = array(
					'name' => ( bool ) $renew_on_expiry ? 'Cancel renew on expiry' : 'Renew on expiry' ,
					'url' => $action_link 
				) ;	
			}

			if ( $subscription_status == 'pending' || $requires_manual_renewal === 'true' )
			{
				unset( $actions[ 'change_payment_method' ] ) ;
			}
			else
			{
				if ( $order->recurring_payment_method == 'stripe' && array_key_exists( 'change_payment_method' , $actions ) )
				{
					$actions[ 'change_card_details' ] = $actions[ 'change_payment_method' ] ;
					$actions[ 'change_card_details' ][ 'name' ] = __( 'Change card details' , 'notanotherbill' ) ;
				}
			}

			$filtered_actions[ $subscription_key ] = $actions ;
		}

		return $filtered_actions ;
	}

	/**
	 * Maybe update subscription renew on expiry flag.
	 *
	 * @since 2.2.16.8
	 */
	public function maybe_update_subscription_renew_on_expiry ( )
	{
		if ( isset( $_GET[ 'update_subscription_renew_on_expiry' ] ) && isset( $_GET[ 'subscription_key' ] ) && isset( $_GET[ '_wpnonce' ] )  )
		{
			$subscription = WC_Subscriptions_Manager::get_subscription( $_GET[ 'subscription_key' ] ) ;

			if ( wp_verify_nonce( $_GET[ '_wpnonce' ], $_GET[ 'subscription_key' ] ) === false )
			{
				WC_Subscriptions::add_notice( __( 'Something went wrong. Please contact us if you need assistance.' , 'woocommerce-subscriptions' ) , 'error' ) ;
			}
			elseif ( empty( $subscription ) )
			{
				WC_Subscriptions::add_notice( __( 'That doesn\'t appear to be one of your subscriptions.' , 'woocommerce-subscriptions' ) , 'error' ) ;
			} 
			else
			{
				$subscription_key = $_GET[ 'subscription_key' ] ;
				$renew_on_expiry = $_GET[ 'update_subscription_renew_on_expiry' ] ;
				$order = new WC_Order( $subscription[ 'order_id' ] ) ;
				$order_item = WC_Subscriptions_Order::get_item_by_subscription_key( $subscription_key ) ;
				$user_id = $order->get_user( )->ID ;
				$success = wc_update_order_item_meta( $order_item[ 'order_item_id' ] , '_subscription_renew_on_expiry' , $renew_on_expiry ) ;

				if ( $success )
				{
					WCS_NAB_Date_Sync::synchronise_start_expiry_dates( $order ) ;

					if ( $renew_on_expiry )
					{
						// Set the next payment date
						$next_payment_date = WC_Subscriptions_Manager::get_next_payment_date( $subscription_key , $user_id ) ;

						// Notify the customer
						WC_Subscriptions::add_notice( sprintf( __( 'Your subscription will renew on %s' , 'woocommerce-subscriptions' ) , date( 'jS F, Y' , strtotime( $next_payment_date ) ) ) , 'success' ) ;
					}
					else
					{
						// Notify the customer
						WC_Subscriptions::add_notice( __( 'Your subscription will not renew on expiry.' , 'woocommerce-subscriptions' ) , 'error' ) ;
					}

					$order->add_order_note( sprintf( __( 'The renew on expiry flag for subscription %s has been updated to %s by the subscriber from their account page.' , 'woocommerce-subscriptions' ) , $_GET[ 'subscription_key' ] , $renew_on_expiry ) ) ;

					do_action( 'updated_subscription_renew_on_expiry' , $_GET[ 'subscription_key' ] , $renew_on_expiry ) ;
				}
				else
				{
					WC_Subscriptions::add_notice( __( 'Something went wrong. Please try again.' , 'woocommerce-subscriptions' ) , 'error' ) ;
				}

				wp_safe_redirect( get_permalink( woocommerce_get_page_id( 'myaccount' ) ) ) ;
				exit ;
			}
		}
	}

	/**
	* Allow changing payment method in non-production environments
	*
	* @since 2.2.20.8
	* @param bool
	* @param WC_Subscription
	* @param WC_Order
	*/
	public function subscription_can_change_payment_method ( $subscription_can_be_changed , $subscription , $order )
	{
		return is_dev( ) || is_staging( ) || $subscription_can_be_changed ;
	}

	/**
	* Get subscription orders associated with an original subscription:
	*	- All customer's subscriptions
	*	- Subscriptions with a matching postcode and similar first line of address
	*
	* @since 2.2.21.4
	* @param array
	* @param int
	*/
	public static function get_associated_subscription_orders ( $order_id )
	{
		$associated_order_ids = array( ) ;

		if ( ! empty( $order_id ) )
		{
			global $wpdb ;

			$order = new WC_Order( $order_id ) ;
			$user_id = $order->get_user_id( ) ;

			if ( ! empty( $user_id ) )
			{
				$users_order_ids = WC_Subscriptions_Order::get_users_subscription_orders( $user_id ) ;

				if ( count( $users_order_ids ) > 1 )
				{
					$users_order_ids = array_diff( $users_order_ids , array( $order_id ) ) ;
					$associated_order_ids = array_merge( $associated_order_ids , $users_order_ids ) ;
				}

				$order_meta = get_post_meta( $order_id ) ;

				$shipping_country = $order_meta[ '_shipping_country' ][ 0 ] ;
				$shipping_postcode = $order_meta[ '_shipping_postcode' ][ 0 ] ;
				$shipping_house_no = filter_var( $order_meta[ '_shipping_address_1' ][ 0 ] , FILTER_SANITIZE_NUMBER_INT ) ;

				// TODO: get address from multiple shipping packages
				if ( ! empty( $shipping_country ) && ! empty( $shipping_postcode ) )
				{
					$found_ids = $wpdb->get_results(
						"SELECT {$wpdb->prefix}posts.ID FROM {$wpdb->prefix}posts
							INNER JOIN {$wpdb->prefix}postmeta ON ( {$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.post_id )
							INNER JOIN {$wpdb->prefix}postmeta AS mt1 ON ( {$wpdb->prefix}posts.ID = mt1.post_id )
							INNER JOIN {$wpdb->prefix}postmeta AS mt2 ON ( {$wpdb->prefix}posts.ID = mt2.post_id )
							INNER JOIN {$wpdb->prefix}postmeta AS mt3 ON ( {$wpdb->prefix}posts.ID = mt3.post_id )
							LEFT JOIN {$wpdb->prefix}postmeta AS mt4 ON ( {$wpdb->prefix}posts.ID = mt4.post_id AND mt4.meta_key = '_original_order' )
							WHERE 1=1 AND (
								( {$wpdb->prefix}postmeta.meta_key = '_shipping_country' AND {$wpdb->prefix}postmeta.meta_value = '$shipping_country' ) 
								AND ( mt1.meta_key = '_shipping_postcode' AND mt1.meta_value LIKE '%$shipping_postcode%' )
								AND ( mt2.meta_key = '_shipping_address_1' AND mt2.meta_value LIKE '%$shipping_house_no%' )
								AND ( mt3.meta_key = '_order_recurring_total' AND mt3.meta_value > 0 )
								AND mt4.post_id IS NULL
							)
							AND {$wpdb->prefix}posts.post_type = 'shop_order'
							AND {$wpdb->prefix}posts.post_status <> 'trash'
							AND {$wpdb->prefix}posts.ID <> '$order_id'
						GROUP BY {$wpdb->prefix}posts.ID
						ORDER BY {$wpdb->prefix}posts.post_date DESC"
					) ;

					if ( ! empty( $found_ids ) )
					{
						$found_ids = array_map( function ( $e ) { return $e->ID ; } , $found_ids ) ;

						$associated_order_ids = array_merge( $associated_order_ids , $found_ids ) ;
						$associated_order_ids = array_values( array_unique( $associated_order_ids ) ) ;
					}
				}
			}
		}

		return $associated_order_ids ;
	}

	public static function nab_print_renew_bars ( )
	{
		if ( is_user_logged_in( ) )
		{
			$user_id = get_current_user_id( ) ;
			$subs = WC_Subscriptions_Manager::get_users_subscriptions( $user_id ) ;
			$bars = array( ) ;

			foreach ( $subs as $sub_key => $sub )
			{
				if ( $sub[ 'status' ] == 'expired' )
				{
					$order = new WC_Order( $sub[ 'order_id' ] ) ;
					$item = WC_Subscriptions_Order::get_item_by_product_id( $order , $sub[ 'product_id' ] ) ;
					$item_id = WC_Subscriptions_Order::get_item_id_by_subscription_key( $sub_key ) ;

					if ( wc_get_order_item_meta( $item_id , '_renew_dont_show_again' , true ) !== 'true' ) // without don't show me this again meta
					{ 
						$recipient_names = $item[ 'Recipient Name 1 - Text' ] . ( ( ! empty( $item[ 'Recipient Name 2 - Text' ] ) ) ? " &amp; {$item['Recipient Name 2 - Text']}" : '' ) ;
						$bars[ $sub_key ] = $sub ;
						$bars[ $sub_key ][ 'recipient_names' ] = $recipient_names ;
					}
				}
			}

			if ( count( $bars ) )
			{
				?>
				<div class="renew-bars">
					<ul>
					<?php
					foreach ( $bars as $sub_key => $bar )
					{
						if ( WC_Subscriptions_Renewal_Order::can_subscription_be_renewed( $sub_key ) )
						{
							$renew = array(
								'url'  => add_query_arg( 'renew' , $sub[ 'order_id' ] , get_permalink( get_page_by_path( 'my-account' ) ) ) ,
								'name' => __( 'Renew' , 'woocommerce-subscriptions' )
							) ;

							?>
							<li id="bar-<?php echo $bar[ 'recipient_names' ] ; ?>" class="renew-bar" data-nab-sub-key="<?php echo $sub_key ; ?>">
								<a class="renew-close"></a>
								Subscription for
								<a href="#order-<?php echo $bar[ 'order_id' ] ; ?>" class="renew-sub-link" data-parent="#subscriptions"><?php echo $bar[ 'recipient_names' ]; ?></a>
								has expired&nbsp;&nbsp;-&nbsp;&nbsp;
								<a href="<?php echo esc_url( $renew[ 'url' ] ) ; ?>" class="button <?php echo sanitize_html_class( 'renew' ) ; ?>"><?php echo esc_html( $renew[ 'name' ] ) ; ?></a>
								&nbsp;&nbsp;<a class="renew-dont-show-again">(don't show me this again)</a>
							</li>
							<?php

						}
					}
					?>
					</ul>
				</div>
				<?php
			}

		}
	}

	public function renew_dont_show_again ( $data )
	{
		if ( ! is_user_logged_in( ) ) exit ;

		global $wpdb ;

		$sub_key = $_REQUEST[ 'sub_key' ] ;
		$sub = WC_Subscriptions_Manager::get_subscription( $sub_key ) ;
		$order = new WC_Order( $sub[ 'order_id' ] ) ;

		if ( $order->get_user( )->ID == get_current_user_id( ) )
		{
			$item = WC_Subscriptions_Order::get_item_by_product_id( $order, $sub[ 'product_id' ] ) ;
			$item_id = WC_Subscriptions_Order::get_item_id_by_subscription_key( $sub_key ) ;

			wc_update_order_item_meta( $item_id , '_renew_dont_show_again' , 'true' ) ;
		}

		exit ;
	}

	/*
	*	Fires on the checkout page when a renewal order is being made,
	*	Filters the data for the new subscription
	*
	*	Runs after re_add_cart_item_data in Woocommerce Product Addons, Product_Addon_Cart
	*	in plugins/woocommerce-product-addons/classes/class-product-addon-cart.php
	*/
	public static function order_again_cart_item_data ( $cart_item_data , $item , $original_order )
	{
		$found = false ;

		// take 5 days off current date to allow signups up to the 5th of the next month, then return the 1st of the month
		$current_working_month = date( 'Y-m-01' , strtotime( '-5 days' , time( ) ) ) ;
		
		// then add one month
		$start_date = date( 'Y-m-01' , strtotime( '+1 month' , strtotime( $current_working_month ) ) ) ;

		// update start date meta item
		foreach ( $cart_item_data[ 'addons' ] as $i => $addon )
		{
			if ( $addon[ 'name' ] == 'Start Date - Date' )
			{
				$cart_item_data[ 'addons' ][ $i ][ 'value' ] = $start_date ;
				$found = true ;
			}
		}

		// set addon if not found already
		if ( ! $found )
		{
			$cart_item_data[ 'addons' ][ ] = array(
				'name'  => 'Start Date - Date' ,
				'value' => $start_date ,
				'price' => null
			) ;
		}

		// return
		return $cart_item_data ;
	}
}

endif ;

return new WC_NAB_Subscriptions( ) ;