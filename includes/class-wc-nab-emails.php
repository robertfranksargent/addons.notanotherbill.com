<?php

/**
 * Emails
 *
 * @class 		WC_NAB_Emails
 * @version		2.2.22
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_Emails
{
	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		add_filter( 'woocommerce_email_classes' , array( $this , 'add_email_classes' ) ) ;
		add_filter( 'woocommerce_email_enabled_customer_completed_order' , array( $this , 'enabled_customer_completed_order' ) , 10 , 2 ) ;
	}

	/**
	*  Add custom emails to the list of emails WooCommerce should load
	*
	* @since 2.2.22
	* @param array $email_classes available email classes
	* @return array filtered available email classes
	*/
	public function add_email_classes ( $email_classes )
	{
		// include our custom email class
		require( 'emails/class-wcs-nab-meta-update-email.php' ) ;
		require( 'emails/class-wcs-nab-subscription-paused-email.php' ) ;
		require( 'emails/class-wcs-nab-subscription-reactivated-email.php' ) ;
		require( 'emails/class-wc-nab-account-verification-email.php' ) ;

		// add the email class to the list of email classes that WooCommerce loads
		$email_classes[ 'WCS_NAB_Meta_Update_Email' ] = new WCS_NAB_Meta_Update_Email( ) ;
		$email_classes[ 'WCS_NAB_Subscription_Paused_Email' ] = new WCS_NAB_Subscription_Paused_Email( ) ;
		$email_classes[ 'WCS_NAB_Subscription_Reactivated_Email' ] = new WCS_NAB_Subscription_Reactivated_Email( ) ;
		$email_classes[ 'WC_Account_Verification_Email' ] = new WC_Account_Verification_Email( ) ;

		return $email_classes ;
	}

	/**
	*  Decide whether completed order email should be enabled based on order contents. If the order contains a non-subscription product then
	*  enable the email according to the settings. If the order only contains a subscription then do not send the email.
	*
	* @since 2.2.11.21
	* @param bool $enabled 
	* @param WC_Order $order
	* @return bool
	*/
	public function enabled_customer_completed_order ( $enabled , $order )
	{
		if ( $enabled )
		{
			foreach ( $order->get_items( ) as $item )
			{
				$product = $order->get_product_from_item( $item ) ;

				if ( ! WC_Subscriptions_Product::is_subscription( $product->id ) )
				{
					return $enabled ;
				}
			}

			return false ;
		}

		return $enabled ;
	}
}

new WC_NAB_Emails( ) ;