<?php

/**
 * Account
 *
 * @class 		WC_NAB_Checkout
 * @version		2.2.22.1
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_Account
{
	/**
	 * Constructor
	 *
	 * @since 2.2.22.1
	 */
	public function __construct ( )
	{
		add_action( 'wp' , array( $this , 'check_query_vars' ) ) ;
		add_action( 'profile_update' , array( $this , 'profile_update' ) , 10 , 2 ) ;
	}

	/**
	* @since 2.2.22.1
	*/
	public function profile_update ( $user_id , $old_user )
	{
		global $wpdb ;

		$user = get_user_by( 'ID' , $user_id ) ;

		if ( $user->data->user_email != $old_user->data->user_email )
		{
			$linked_orders = $wpdb->get_results( "
				SELECT {$wpdb->prefix}posts.*, {$wpdb->prefix}woocommerce_order_items.*
				FROM {$wpdb->prefix}posts
					INNER JOIN {$wpdb->prefix}postmeta ON ( {$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.post_id )
					LEFT OUTER JOIN {$wpdb->prefix}woocommerce_order_items ON ( {$wpdb->prefix}posts.ID = {$wpdb->prefix}woocommerce_order_items.order_id )
					LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta ON ( {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id )
				WHERE post_type = 'shop_order'
					AND {$wpdb->prefix}postmeta.meta_key = '_customer_user'
					AND {$wpdb->prefix}postmeta.meta_value != {$user_id}
					AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Recipient Email Address - Text'
					AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_value = '{$old_user->data->user_email}'
			" ) ;

			if ( ! empty( $linked_orders ) )
			{
				$meta_ids = array( ) ;

				foreach ( $linked_orders as $linked_order )
				{
					$meta_ids[ ] = wc_update_order_item_meta( $linked_order->order_item_id , 'Recipient Email Address - Text' , $user->data->user_email ) ;
				}

				if ( ! empty( $meta_ids ) ) update_user_meta( $user_id , 'user_verified' , 0 ) ;
			}
		}
	}

	/**
	* @since 2.2.22
	*/
	public function check_query_vars ( )
	{
		if ( is_account_page( ) )
		{
			if ( array_key_exists( 'login' , $_GET ) && array_key_exists( 'verify_key' , $_GET ) )
			{
				$current_user = wp_get_current_user( ) ;
				$user = get_user_by( 'login' , $_GET[ 'login' ] ) ;

				if ( $current_user->id == $user->ID )
				{
					$expiry = get_user_meta( $user->ID , 'user_verification_expiry' , true ) ;

					if ( time( ) <= $expiry )
					{
						$key = get_user_meta( $user->ID , 'user_verification_key' , true ) ;

						if ( $key == $_GET[ 'verify_key' ] )
						{
							if ( update_user_meta( $user->ID , 'user_verified' , 1 ) )
							{
								$notice_type = 'success' ;
								$notice = __( 'Thank you. Your email address has been verified.' , 'notanotherbill' ) ;
							}
							else
							{
								$notice_type = 'error' ;
								$notice = __( 'Something went wrong trying to verify your email address. Please try again later.' , 'notanotherbill' ) ;
							}
						}
						else
						{
							$notice_type = 'error' ;
							$notice = __( 'Something went wrong trying to verify your email address. Please try again later.' , 'notanotherbill' ) ;
						}
					}
					else
					{
						$notice_type = 'notice' ;
						$notice = __( 'Your verification link has expired. Please click the <strong>Verify my email address</strong> button below to receive a new one.' , 'notanotherbill' ) ;
					}
				}
				else
				{
					$notice_type = 'error' ;
					$notice = __( 'Something went wrong trying to verify your email address. Please try again later.' , 'notanotherbill' ) ;
				}

				wc_add_notice( $notice , $notice_type ) ;
			}
		}
	}
}

new WC_NAB_Account( ) ;