<?php

/**
 * PayPal
 *
 * @class 		WC_NAB_PayPal
 * @version		2.2.19
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_PayPal
{
	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		add_action( 'valid-paypal-standard-ipn-request' , array( $this , 'process_paypal_ipn_request' , 1 , 1 ) ) ;
	}

	/**
	 * Handle PayPal IPNs that are not correctly dealt with in the WC core.
	 *
	 * @since 2.2.19
	 */
	public function process_paypal_ipn_request ( $transaction_details )
	{
		if ( $transaction_details[ 'txn_type' ] == 'recurring_payment_suspended_due_to_max_failed_payment' && isset( $transaction_details[ 'rp_invoice_id' ] ) )
		{
			$subscription_id_and_key = self::get_order_id_and_key( $transaction_details ) ;
			$subscription = WCS_Utils::get_order_subscription( $subscription_id_and_key[ 'order_id' ] ) ;
			
			if ( $subscription )
			{
				unset( $transaction_details[ 'rp_invoice_id' ] ) ;

				WC_PayPal_Standard_Subscriptions::process_paypal_ipn_request( $transaction_details ) ;
			}
		}
	}

	/**
	 * Checks a set of args and derives an Order ID with backward compatibility for WC < 1.7 where 'custom' was the Order ID.
	 *
	 * @since 2.2.19
	 */
	private static function get_order_id_and_key ( $args )
	{
		// First try and get the order ID by the subscr_id
		if ( isset( $args[ 'subscr_id' ] ) )
		{
			$posts = get_posts(
				array(
					'numberposts'      => 1 ,
					'orderby'          => 'ID' ,
					'order'            => 'ASC' ,
					'meta_key'         => 'PayPal Subscriber ID' ,
					'meta_value'       => $args[ 'subscr_id' ] ,
					'post_type'        => 'shop_order' ,
					'post_status'      => 'any' ,
					'post_parent'      => 0 ,
					'suppress_filters' => true
				)
			) ;

			if ( ! empty( $posts ) )
			{
				$order_id  = $posts[ 0 ]->ID ;
				$order_key = get_post_meta( $order_id , '_order_key' , true ) ;
			}
		}

		// Couldn't find the order ID by subscr_id, so it's either not set on the order yet or the $args doesn't have a subscr_id, either way, let's get it from the args
		if ( ! isset( $order_id ) )
		{
			// WC < 1.6.5
			if ( is_numeric( $args[ 'custom' ] ) )
			{
				$order_id  = $args[ 'custom' ] ;
				$order_key = $args[ 'invoice' ] ;
			}
			else
			{
				$args[ 'custom' ] = maybe_unserialize( $args[ 'custom' ] ) ;

				if ( is_array( $args[ 'custom' ] ) ) // WC 2.0+
				{
					$order_id  = ( int ) $args[ 'custom' ][ 0 ] ;
					$order_key = $args[ 'custom' ][ 1 ] ;
				}
				else // WC 1.6.5 = WC 2.0
				{
					$order_id  = ( int ) str_replace( self::$invoice_prefix , '' , $args[ 'invoice' ] ) ;
					$order_key = $args[ 'custom' ] ;
				}
			}
		}

		return array( 'order_id' => $order_id , 'order_key' => $order_key ) ;
	}
}

new WC_NAB_PayPal( ) ;