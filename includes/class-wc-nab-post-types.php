<?php

/**
 * Post types
 *
 * Registers post types and taxonomies
 *
 * @class 		WC_NAB_Post_types
 * @version		2.2.10.15
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_Post_types
{
	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		add_action( 'init' , array( __CLASS__ , 'register_post_types' ) ) ;

		add_filter( 'manage_edit-promotion_columns' , array( __CLASS__ , 'edit_promotion_columns' ) ) ;
		add_action( 'manage_promotion_posts_custom_column' , array( __CLASS__ , 'manage_promotion_columns' ) , 10 , 2 ) ;
	}

	/**
	 * Register post types
	 */
	public static function register_post_types ( )
	{
		if ( post_type_exists( 'promotion' ) ) return ;

		register_post_type( 'promotion' ,
			array(
				'labels' => array(
						'name' 					=> __( 'Promotions' , 'notanotherbill' ) ,
						'singular_name' 		=> __( 'Promotion' , 'notanotherbill' ) ,
						'menu_name'				=> _x( 'Promotions' , 'Admin menu name' , 'notanotherbill' ) ,
						'add_new' 				=> __( 'Add Promotion' , 'notanotherbill' ) ,
						'add_new_item' 			=> __( 'Add New Promotion' , 'notanotherbill' ) ,
						'edit' 					=> __( 'Edit' , 'notanotherbill' ) ,
						'edit_item' 			=> __( 'Edit Promotion' , 'notanotherbill' ) ,
						'new_item' 				=> __( 'New Promotion' , 'notanotherbill' ) ,
						'view' 					=> __( 'View Promotion' , 'notanotherbill' ) ,
						'view_item' 			=> __( 'View Promotion' , 'notanotherbill' ) ,
						'search_items' 			=> __( 'Search Promotions' , 'notanotherbill' ) ,
						'not_found' 			=> __( 'No Promotions found' , 'notanotherbill' ) ,
						'not_found_in_trash' 	=> __( 'No Promotions found in trash' , 'notanotherbill' ) ,
						'parent' 				=> __( 'Parent Promotion' , 'notanotherbill' )
					) ,
				'description' 			=> __( 'This is where you can add new promotions to your store.' , 'notanotherbill' ) ,
				'menu_icon'				=> 'dashicons-megaphone' ,
				'public' 				=> true ,
				'show_ui' 				=> true ,
				'capability_type' 		=> 'product' ,
				'map_meta_cap'			=> true ,
				'publicly_queryable' 	=> true ,
				'exclude_from_search' 	=> true ,
				'hierarchical' 			=> false , // Hierarchical causes memory issues - WP loads all records!
				'rewrite' 				=> array( 'with_front' => false ) ,
				'query_var' 			=> true ,
				'supports' 				=> array( 'title' ) ,
				'has_archive' 			=> false ,
				'show_in_nav_menus' 	=> true
			)
		) ;

		if ( post_type_exists( 'press' ) ) return ;

		register_post_type( 'press' ,
			array(
				'labels' => array(
						'name' 					=> __( 'Press' , 'notanotherbill' ) ,
						'singular_name' 		=> __( 'Publication' , 'notanotherbill' ) ,
						'menu_name'				=> _x( 'Press' , 'Admin menu name' , 'notanotherbill' ) ,
						'add_new' 				=> __( 'Add Publication' , 'notanotherbill' ) ,
						'add_new_item' 			=> __( 'Add New Publication', 'notanotherbill' ) ,
						'edit' 					=> __( 'Edit', 'notanotherbill' ) ,
						'edit_item' 			=> __( 'Edit Publication', 'notanotherbill' ) ,
						'new_item' 				=> __( 'New Publication', 'notanotherbill' ) ,
						'view' 					=> __( 'View Publications', 'notanotherbill' ) ,
						'view_item' 			=> __( 'View Publication', 'notanotherbill' ) ,
						'search_items' 			=> __( 'Search Publications', 'notanotherbill' ) ,
						'not_found' 			=> __( 'No Publications found', 'notanotherbill' ) ,
						'not_found_in_trash' 	=> __( 'No Publications found in trash', 'notanotherbill' ) ,
						'parent' 				=> __( 'Parent Publication', 'notanotherbill' )
					) ,
				'description' 			=> __( 'This is where you can add new press articles to your store.' , 'notanotherbill' ) ,
				'menu_icon'				=> 'dashicons-heart' ,
				'public' 				=> true ,
				'show_ui' 				=> true ,
				'capability_type' 		=> 'product' ,
				'map_meta_cap'			=> true ,
				'publicly_queryable' 	=> true ,
				'exclude_from_search' 	=> true ,
				'hierarchical' 			=> false , // Hierarchical causes memory issues - WP loads all records!
				'rewrite' 				=> array( 'with_front' => false ) ,
				'query_var' 			=> true ,
				'supports' 				=> array( 'title' , 'editor' , 'thumbnail' ) ,
				'has_archive' 			=> true ,
				'show_in_nav_menus' 	=> true
			)
		) ;
	}

	public static function edit_promotion_columns ( $columns )
	{
		return array(
			'cb' => '<input type="checkbox" />' ,
			'title' =>  _x( 'Title' , 'column name' ) ,
			'link' => _x( 'Link' , 'column name' ) ,
			'position' => _x( 'Position' , 'column name' )
		) ;
	}

	public static function manage_promotion_columns ( $column_name , $id )
	{
		switch ( $column_name )
		{
			case 'id' :
				echo $id ;
				break ;

			case 'link' :
				$link = get_field( 'promotion_link' ) ;
				echo '<a href="' . $link . '" target="_blank">' . $link . '</a>' ;
				break ;

			case 'position' :
				the_field( 'promotion_position' ) ;
				break ;

			default :
				break ;
		}
	}
}

new WC_NAB_Post_types( ) ;
