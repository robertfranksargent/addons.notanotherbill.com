<?php
/**
 * Installation related functions and actions.
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Classes
 * @version     2.2.8.12
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_NAB_Install' ) ) :

/**
 * WC_NAB_Install
 */
class WC_NAB_Install
{
	/**
	 * Hook in tabs.
	 */
	public function __construct ( )
	{
		add_action( 'init' , array( $this , 'init' ) ) ;

		register_activation_hook( __CLASS__ , array( $this , 'install' ) ) ;
		register_deactivation_hook( __CLASS__ , array( $this , 'uninstall' ) ) ;
	}

	public function init ( )
	{
		add_filter( 'rewrite_rules_array', array( $this , 'rewrite_rules' ) ) ;
	}

	/**
	 * Install WC_NAB
	 */
	public function install ( )
	{
		// Rewrites
		global $wp_rewrite ;

		add_filter( 'rewrite_rules_array' , array( $this , 'rewrite_rules' ) ) ;
		$wp_rewrite->flush_rules( ) ;		

		// Register post types
		include_once( 'class-wc-nab-post-types.php' ) ;
		
		WC_NAB_Post_types::register_post_types( ) ;
		WC_NAB_Post_types::register_taxonomies( ) ;
	}

	public function uninstall ( )
	{
		global $wp_rewrite ;
		$wp_rewrite->flush_rules( ) ;
	}

	public function rewrite_rules ( $wp_rules )
	{
		return array_merge(
			array(
				'paypal_ipn.php'                => '/index.php?wc-api=WC_Gateway_Paypal' ,
				'past-presents/page/([0-9]+)/?' => 'index.php?post_type=product&product_cat=past-presents&paged=$matches[1]' ,
				'past-presents'                 => 'index.php?post_type=product&product_cat=past-presents'
			) ,
			$wp_rules
		) ;
	}
}

new WC_NAB_Install( ) ;

endif ;