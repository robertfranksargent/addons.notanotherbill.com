<?php

/**
 * Subscriptions date synchronisation
 *
 * @class 		WCS_NAB_Date_Sync
 * @version		2.2.22.7
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! defined( 'WCS_DATESYNC_STARTYEAR' ) ) define( 'WCS_DATESYNC_STARTYEAR' , 2014 ) ;
if ( ! defined( 'WCS_DATESYNC_STARTMONTH' ) ) define( 'WCS_DATESYNC_STARTMONTH' , 9 ) ;
if ( ! defined( 'WCS_DATESYNC_STARTDAY' ) ) define( 'WCS_DATESYNC_STARTDAY' , 1 ) ;

require_once 'utils/class-wcs-utils.php' ;

class WCS_NAB_Date_Sync
{
	/**
	 * @var int
	 * @since 2.2.11.7
	 */
	public $start_date ;

	/**
	 * @var int
	 * @since 2.2.11.7
	 */
	public $current_date ;
	
	/**
	 * @var int
	 * @since 2.2.11.7
	 */
	public $days_until_start ;
	
	/**
	* Constructor
	*
	* @since 2.2.11.7
	*/
	function __construct ( )
	{
		add_action( 'woocommerce_init' , array( $this , 'init' ) ) ;
	}

	/**
	* Initialise
	*
	* @since 2.2.11.7
	*/
	public function init ( )
	{
		$init_dates = $this->init_dates( ) ;

		add_filter( 'woocommerce_paypal_args' ,  array( $this , 'paypal_standard_subscription_args' ) , 11 ) ;
		add_filter( 'woocommerce_gocardless_args' , array( $this , 'gocardless_subscription_args' ) , 11 ) ;
		add_filter( 'woocommerce_subscriptions_calculated_next_payment_date', array( $this , 'calculated_next_payment_date' ) , 10 , 6 ) ;

		//add_action( 'pending_subscription_created_for_order' , array( $this , 'update_start_expiry_dates' ) , 10 ) ;
		//add_action( 'subscriptions_activated_for_order' , array( $this , 'update_start_expiry_dates' ) , 10 ) ;
		add_action( 'activated_subscription' , array( $this , 'activated_reactivated_subscription' ) , 10 , 2 ) ;
		add_action( 'reactivated_subscription' , array( $this , 'activated_reactivated_subscription' ) , 11 , 2 ) ;
		add_action( 'reactivated_subscription' , array( $this , 'reactivated_subscription' ) , 10 , 2 ) ;
	}
	
	/**
	* Initialise dates
	*
	* @since 2.2.11.7
	*/
	public function init_dates ( )
	{
		$cart_subscription_start_date = WCS_Utils::get_cart_subscription_start_date( ) ;

		$this->start_date = strtotime( $cart_subscription_start_date ) ;
		$this->current_date = time( ) ;
		$this->days_until_start = floor( ( $this->start_date - $this->current_date ) / ( 60 * 60 * 24 ) ) ;
		
		if ( ( $this->start_date < $this->current_date ) OR ( $this->days_until_start < 0 ) )
		{
			$this->start_date = time( ) ;
			$this->days_until_start = 0 ;
		}
	}

	/**
	* Action when subscription is activated or reactivated
	*
	* @since 2.2.11.7
	* @param int $user_id
	* @param string $subscription_key
	*/
	public function activated_reactivated_subscription ( $user_id , $subscription_key )
	{
		// Split order and product IDs
		$order_product_ids = explode( '_' , $subscription_key ) ;
		$order_id = $order_product_ids[ 0 ] ;

		if ( $order_id )
		{
			// Create an order object
			$order = new WC_Order( $order_id ) ;

			// Clear scheduled subscription reactivation
			wc_unschedule_action( 'scheduled_subscription_reactivation' , array( 'user_id' => ( int ) $user_id , 'subscription_key' => $subscription_key ) ) ;

			// Synchronise start and expiry dates
			WCS_NAB_Date_Sync::synchronise_start_expiry_dates( $order ) ;
		}
	}

	/**
	* Action when subscription is reactivated
	*
	* @since 2.2.16.13
	* @param int $user_id
	* @param string $subscription_key
	*/
	public function reactivated_subscription ( $user_id , $subscription_key )
	{
		// Split order and product IDs
		$order_product_ids = explode( '_' , $subscription_key ) ;
		$order_id = $order_product_ids[ 0 ] ;
		$product_id = $order_product_ids[ 1 ] ;

		if ( $order_id )
		{
			// Get subscription
			$subscription = WC_Subscriptions_Manager::get_subscription( $subscription_key ) ;

			if ( ! empty( $subscription[ 'start_date' ] ) )
			{
				// Subscription start date
				$start_date = strtotime( $subscription[ 'start_date' ] ) ;

				if ( time( ) > $start_date )
				{
					// Create an order object
					$order = new WC_Order( $order_id ) ;

					// Get subscription details
					$subscription_period = WC_Subscriptions_Order::get_subscription_period( $order , $product_id ) ;
					$subscription_interval = WC_Subscriptions_Order::get_subscription_interval( $order , $product_id ) ;

					// For rolling orders paid for by credit card
					if ( $subscription_period == 'month' && $subscription_interval == 1 && $order->recurring_payment_method == 'stripe' )
					{
						// Get the last payment date
						$last_payment_date = WC_Subscriptions_Manager::get_last_payment_date( $subscription_key , $user_id ) ;
						$last_payment_date = strtotime( $last_payment_date ) ;

						// Get next shipping date
						$shipping_dates = explode( ',' , get_option( 'nab-shipping-dates' ) ) ;

						// Sort the shipping dates in time order
						asort( $shipping_dates ) ;

						// Get next shipping date in the future
						foreach ( $shipping_dates as $shipping_date )
						{
							if ( strtotime( $shipping_date ) > time( ) )
							{
								$future_shipping_date = strtotime( $shipping_date ) ;
								break ;
							}
						}

						// Future shipping date exists
						if ( isset( $future_shipping_date ) )
						{
							// Get time to future shipping date
							$time_to_shipping_date = $future_shipping_date - time( ) ;

							// First of the month of future shipping date
							$first_of_month = strtotime( date( 'Y-m-01 00:00:00' , $future_shipping_date ) ) ;

							// The current time is after the 1st of the future shipping date month and the future shipping date is more than 24 hours away
							if ( time( ) > $first_of_month && $time_to_shipping_date > 86400 )
							{
								// If last payment was before the 1st of this month
								if ( $last_payment_date < $first_of_month )
								{
									// Reactivated subscription requires payment today
									$calc_payment_date = strtotime( '+1 minute' , time( ) ) ;
								}
							}
						}

						// Either the next shipping date is not set or it is less than 24 hours away from now
						if ( empty( $calc_payment_date ) )
						{
							// Payment in one month by default
							$payment_in = '+1 month' ;
							$this_month = date( 'm-Y' , time( ) ) ;
							$order_month = date( 'm-Y' , strtotime( $order->order_date ) ) ;
							$start_month = date( 'm-Y' , $start_date ) ;

							// If the current month, order month and start month are the same, the next payment will always be on the 1st of the next month
							if ( $this_month != $order_month || $order_month != $start_month || $start_month != $this_month )
							{
								// Get this month's shipping date, if it exists
								foreach ( $shipping_dates as $shipping_date )
								{
									$shipping_date_month = date( 'm-Y' , strtotime( $shipping_date ) ) ;

									if ( $shipping_date_month == $this_month )
									{
										$this_month_shipping_date = strtotime( $shipping_date ) ;
										break ;
									}
								}

								// This month's shipping date exists
								if ( isset( $this_month_shipping_date ) )
								{
									// Payment was made less than 24 hours before, or after, this month's shipping date
									if ( $last_payment_date > $this_month_shipping_date - 86400 )
									{
										// Payment is required in two months
										$payment_in = '+2 months' ;
									}
								}
							}

							// Set the payment to occur on the 1st of the month
							$calc_payment_date = strtotime( $payment_in , time( ) ) ;
							$calc_payment_date = strtotime( date( 'Y-m-01 H:i:s' , $calc_payment_date ) ) ;
						}

						// Get the next payment date
						$next_payment_date = WC_Subscriptions_Manager::get_next_payment_date( $subscription_key , $user_id ) ;

						// Update next payment date if there's not already a payment setup for that date
						if ( date( 'Y-m-d' , $calc_payment_date ) != date( 'Y-m-d' , strtotime( $next_payment_date ) ) )
						{
							WC_Subscriptions_Manager::update_next_payment_date( $calc_payment_date , $subscription_key , $user_id ) ; 

							$order->add_order_note( sprintf( 'The next payment will be taken on %s.' , date( 'jS F, Y' , $calc_payment_date ) ) ) ;
						}
					}
				}
			}
		}
	}

	/**
	* Calculate next payment date
	*
	* @since 2.2.17
	* @param string $next_payment_date
	* @param WC_Order $order
	* @param int $product_id
	* @param string $type
	* @param string $from_date
	* @param string $from_date_arg
	*/
	public function calculated_next_payment_date ( $next_payment_date , $order , $product_id , $type , $from_date , $from_date_arg )
	{
		$subscription_key = WC_Subscriptions_Manager::get_subscription_key( $order->id , $product_id ) ;
		$order_item = WC_Subscriptions_Order::get_item_by_subscription_key( $subscription_key ) ;
		$subscription_period = WC_Subscriptions_Order::get_item_meta( $order->id , '_subscription_period' ) ;
		$subscription_interval = WC_Subscriptions_Order::get_item_meta( $order->id , '_subscription_interval' ) ;
		$subscription_renew_on_expiry = wc_get_order_item_meta( $order_item[ 'order_item_id' ] , '_subscription_renew_on_expiry' ) ;

		// Calculate next payment date for monthly rolling subscriptions and 3, 6 & 12 month subscriptions which are set to auto-renew
		if ( ( $subscription_period == 'month' && $subscription_interval == 1 ) || ( ! empty( $subscription_renew_on_expiry ) && $subscription_renew_on_expiry == 1 ) )
		{
			$start_date = WC_Subscriptions_Order::get_item_meta( $order->id , 'Start Date - Date' ) ;
			
			if ( $start_date )
			{
				$start_date .= date( ' H:i:s' , time( ) ) ;
				$calc_payment_date = strtotime( '+' . $subscription_interval . ' ' . $subscription_period , strtotime( $start_date ) ) ;
			
				if ( $next_payment_date < $calc_payment_date ) $next_payment_date = $calc_payment_date ;
			}
		}
		else
		{
			$next_payment_date = 0 ; // Non-auto-renewing 3, 6 & 12 month subscriptions should not have a next payment date
		}
		
		return $next_payment_date ;
	}

	/**
	* Add information to PayPal payment argument string
	*
	* @since 2.2.17.3
	* @param array $paypal_args
	*/
	public function paypal_standard_subscription_args ( $paypal_args )
	{
		if ( ( $paypal_args[ 'cmd' ] == '_xclick-subscriptions' ) )
		{
			$paypal_args[ 'item_name' ] = '' ;	

			if ( WC_Subscriptions_Cart::cart_contains_subscription( ) )
			{
				$subscription_period = WC_Subscriptions_Cart::get_cart_subscription_period( ) ;
				$subscription_interval = WC_Subscriptions_Cart::get_cart_subscription_interval( ) ;

				$paypal_args[ 'item_name' ] .= $subscription_interval . ' ' . $subscription_period ;

				if ( $subscription_interval == 1 && $subscription_period == 'month' )
				{
					$paypal_args[ 'item_name' ] .= ' rolling' ;
				}

				// Use trial periods to push subscription payment start date forward
				$order_id_and_key = WCS_Utils::get_order_id_and_key( $paypal_args ) ;
				$order = new WC_Order( $order_id_and_key[ 'order_id' ] ) ;

				if ( $order )
				{
					if ( $subscription_period == 'month' && $subscription_interval == 1 )
					{
						// Get real subscription start date
						$start_date = WC_Subscriptions_Order::get_item_meta( $order->id , 'Start Date - Date' ) ;

						if ( $start_date )
						{
							// Add current time
							$start_date .= date( ' H:i:s' , time( ) ) ;

							// Add one month to start date to get next payment date
							$next_payment_date = strtotime( '+' . $subscription_interval . ' ' . $subscription_period , strtotime( $start_date ) ) ;

							// Set next payment day to the 1st of the month
							$next_payment_date = strtotime( date( 'Y-m-01 H:i:s' , $next_payment_date ) ) ;
							
							// Calculate trial period in days
							$seconds_until_next_payment = $next_payment_date - gmdate( 'U' ) ;
							$days_until_next_payment = ceil( $seconds_until_next_payment / ( 60 * 60 * 24 ) ) ;

							// Get the order total
							$paypal_args[ 'a1' ] = $order->get_total( ) ;

							// PayPal trial period cannot be longer than 90 days
							if ( $days_until_next_payment <= 90 )
							{
								$paypal_args[ 'p1' ] = $days_until_next_payment ;
								$paypal_args[ 't1' ] = 'D' ;
							}
							else
							{
								// Difference between first payment date (today) and regular payment date (~ 1st of month)
								$weeks_until_next_payment = floor( $days_until_next_payment / 7 ) ;

								// PayPal allows a maxiumum of 52 weeks
								$weeks_until_next_payment = min( $weeks_until_next_payment , 52 ) ;

								// Convert 52 weeks to 1 year
								if ( $weeks_until_next_payment == 52 )
								{
									$paypal_args[ 'p1' ] = 1 ;
									$paypal_args[ 't1' ] = 'Y' ;
								}
								else
								{
									$paypal_args[ 'p1' ] = $weeks_until_next_payment ;
									$paypal_args[ 't1' ] = 'W' ;
								}
							}
						}
					}
					else
					{
						// Change to standard cart checkout
						$paypal_args[ 'cmd' ] = '_cart' ;

						// Get index of subscription item in cart
						$subscription_item_index = 1 ;

						foreach ( WC( )->cart->cart_contents as $cart_item_key => $cart_item )
						{
							if ( WC_Subscriptions_Product::is_subscription( $cart_item[ 'product_id' ] ) )
							{
								break ;
							}

							$subscription_item_index++ ;
						}

						// Copy item name
						$paypal_args[ 'item_name_' . $subscription_item_index ] = sprintf( '%s %s' , $paypal_args[ 'item_name' ] , __( 'subscription' , 'woocommerce-subscriptions' ) ) ;

						// Remove unnecessary arguments
						unset( $paypal_args[ 'a3' ] , $paypal_args[ 'p3' ] , $paypal_args[ 't3' ] ) ; 
					}
				}
			}
			
			$paypal_args[ 'item_name' ] = sprintf( '%s %s %s' , $paypal_args[ 'item_name' ] , __( 'subscription starting on' , 'notanotherbill' ) , date( 'jS F, Y' , $this->start_date ) ) ;
		}
		
		return $paypal_args ;
	}

	/**
	* Set correct start date for GoCardless subscriptions
	*
	* @since 2.2.22.7
	* @param array $payment_details
	*/
	public function gocardless_subscription_args ( $payment_details )
	{
		$payment_details[ 'start_at' ] = date( 'Y-m-d H:i:s' , $this->start_date + 3600 ) ;

		return $payment_details ;
	}

	/**
	* Synchronise start and expiry dates with order item meta
	*
	* @since 2.2.13.14
	* @param WC_Order $order
	* @param int $product_id
	*/
	public static function synchronise_start_expiry_dates ( $order , $product_id = false )
	{
		// Get user
		$user_id = $order->get_user( )->ID ;

		// Get product ID if necessary
		if ( ! $product_id )
		{
			$product = WCS_Utils::get_subscription_post( ) ;
			$product_id = $product->ID ;
		}

		// Get original order item
		$subscription_key = WC_Subscriptions_Manager::get_subscription_key( is_object( $order ) ? $order->id : $order , $product_id ) ;
		$order_item = WC_Subscriptions_Order::get_item_by_subscription_key( $subscription_key ) ;
		$order_item_id = $order_item[ 'order_item_id' ] ;

		// Get order start date
		$start_date = woocommerce_get_order_item_meta( $order_item_id , 'Start Date - Date' ) ;

		// Append the current time
		$start_date .= date( ' H:i:s' , time( ) ) ;

		// Update start date
		wc_update_order_item_meta( $order_item_id , '_subscription_start_date' , date( 'Y-m-d H:i:s' , strtotime( $start_date ) ) ) ;

		// Get subscription period
		$subscription_period = WC_Subscriptions_Order::get_subscription_period( $order , $product_id ) ;
		$subscription_interval = WC_Subscriptions_Order::get_subscription_interval( $order , $product_id ) ;

		// Apply expiry date to non-rolling subscriptions
		if ( $subscription_period == 'year' || $subscription_interval > 1 )
		{
			$subscription_renew_on_expiry = wc_get_order_item_meta( $order_item_id , '_subscription_renew_on_expiry' ) ;

			if ( $subscription_renew_on_expiry )
			{
				// Set the expiration date
				WC_Subscriptions_Manager::set_expiration_date( $subscription_key, $user_id, '1970-01-01 00:00:00' ) ;

				woocommerce_update_order_item_meta( $order_item_id , '_subscription_expiry_date' , 0 ) ;
			}
			else
			{
				// TODO: need to take into account that the payment date might be before the start date, otherwise PayPal will take payment before the profile is suspended.

				// 10th of the last month
				$expiry_date = strtotime( '+' . $subscription_interval . ' ' . $subscription_period , strtotime( $start_date ) ) ;
				$expiry_date = strtotime( '-1 month' , $expiry_date ) ;
				$expiry_date = strtotime( date( 'Y-m-10 H:i:s' , $expiry_date ) ) ;
				
				// Set the expiration date
				WC_Subscriptions_Manager::set_expiration_date( $subscription_key , $user_id , $expiry_date ) ;

				woocommerce_update_order_item_meta( $order_item_id , '_subscription_expiry_date' , date( 'Y-m-d H:i:s' , $expiry_date ) ) ;

				// Unschedule the next payment
				wc_unschedule_action( 'scheduled_subscription_payment' , array( 'user_id' => ( int ) $user_id , 'subscription_key' => $subscription_key ) ) ;
			}
		}
	}
}

new WCS_NAB_Date_Sync( ) ;