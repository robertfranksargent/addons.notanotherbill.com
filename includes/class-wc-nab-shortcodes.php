<?php
/**
 * WC_NAB_Shortcodes class.
 *
 * @class 		WC_NAB_Shortcodes
 * @version		2.2.23.11
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */
class WC_NAB_Shortcodes
{
	/**
	 * Init shortcodes
	 *
	 * @since 2.2.22.2
	 */
	public static function init ( )
	{
		// Define shortcodes
		$shortcodes = array(
			'mention_me_refer' => __CLASS__ . '::mention_me_refer' ,
			'sharing_display' => __CLASS__ . '::sharing_display' ,
			'homepage_recent_past_presents' => __CLASS__ . '::homepage_recent_past_presents' ,
			'homepage_recent_products' => __CLASS__ . '::homepage_recent_products' ,
			'homepage_recent_posts' => __CLASS__ . '::homepage_recent_posts' ,
			'subscription_plans' => __CLASS__ . '::subscription_plans' ,
			'woocommerce_my_account_login' => __CLASS__ . '::woocommerce_my_account_login' ,
			'woocommerce_my_account_register' => __CLASS__ . '::woocommerce_my_account_register' ,
			'vimeo_video' => __CLASS__ . '::vimeo_video' ,
			'viral_sweep' => __CLASS__ . '::viral_sweep'
		) ;

		foreach ( $shortcodes as $shortcode => $function )
		{
			add_shortcode( apply_filters( '{$shortcode}_shortcode_tag' , $shortcode ) , $function ) ;
		}
	}

	/**
	 * Mention Me refer shortcode
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 */
	public static function mention_me_refer ( $atts )
	{
		extract( shortcode_atts( array( 
			'situation' => 'landingpage' ,
			'implementation' => 'embed'
		) , $atts ) ) ;

		$mm_params = array(
			'situation' => $situation ,
			'implementation' => $implementation ,
			'locale' => get_locale( )
		) ;

		if ( get_current_user_id( ) > 0 )
		{
			$user = wp_get_current_user( ) ;

			$mm_params[ 'email' ] = $user->user_email ;
			$mm_params[ 'surname' ] = $user->last_name ;
			$mm_params[ 'firstname' ] = $user->first_name ;
			$mm_params[ 'customer_id' ] = $user->ID ;
		}

		$mm_url = 'https://tag' . ( is_dev( ) || is_staging( ) ? '-demo' : '' ) . '.mention-me.com/api/v2/referreroffer/mmf596014d?' . http_build_query( $mm_params ) ;

		?>

			<div class="container">
				<!-- Begin Mention Me referrer placeholder div -->
				<div id="mmWrapper"></div>
				<!-- End Mention Me referrer placeholder div -->
			</div>

			<!-- Begin Mention Me referrer integration -->
			<script type="text/javascript" src="<?php echo $mm_url ; ?>"></script>
			<!-- End Mention Me referrer integration -->	

		<?php
	}

	/**
	 * Display sharing buttons
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 */
	public static function sharing_display ( $atts )
	{
		extract( shortcode_atts( array(
			'text' => ''
		) , $atts ) ) ;

		$sharer = new Sharing_Service( ) ;
		$global = $sharer->get_global_options( ) ;

		$sharing_content = '' ;

		$enabled = apply_filters( 'sharing_enabled' , $sharer->get_blog_services( ) ) ;

		if ( count( $enabled[ 'all' ] ) > 0 )
		{
			global $post ;

			$dir = get_option( 'text_direction' ) ;

			// Wrapper
			$sharing_content .= '<div class="sharedaddy sd-sharing-enabled"><div class="robots-nocontent sd-block sd-social sd-social-' . $global[ 'button_style' ] . ' sd-sharing">' ;
			
			if ( $global['sharing_label'] != '' )
			{
				$sharing_content .= '<h3 class="sd-title">' . $global[ 'sharing_label' ] . '</h3>' ;
			}
			
			$sharing_content .= '<div class="sd-content"><ul>' ;

			// Visible items
			$visible = '' ;
			foreach ( $enabled['visible'] as $id => $service )
			{
				// Individual HTML for sharing service
				$visible .= '<li class="share-' . $service->get_class( ) . '">' . $service->get_display( $post ) . '</li>' ;
			}

			$parts = array( ) ;
			$parts[ ] = $visible ;

			if ( count( $enabled[ 'hidden' ] ) > 0 )
			{
				if ( count( $enabled['visible'] ) > 0 )
				{
					$expand = __( 'More' , 'jetpack' ) ;
				}
				else
				{
					$expand = __( 'Share' , 'jetpack' ) ;
				}
				$parts[ ] = '<li><a href="#" class="sharing-anchor sd-button share-more"><span>' . $expand . '</span></a></li>' ;
			}

			if ( $dir == 'rtl' )
			{
				$parts = array_reverse( $parts ) ;
			}

			$sharing_content .= implode( '' , $parts ) ;
			$sharing_content .= '<li class="share-end"></li></ul>' ;

			if ( count( $enabled[ 'hidden' ] ) > 0 )
			{
				$sharing_content .= '<div class="sharing-hidden"><div class="inner" style="display: none;' ;

				if ( count( $enabled['hidden'] ) == 1 )
				{
					$sharing_content .= 'width:150px;' ;
				}

				$sharing_content .= '">' ;

				if ( count( $enabled['hidden'] ) == 1 )
				{
					$sharing_content .= '<ul style="background-image:none;">' ;
				}
				else
				{
					$sharing_content .= '<ul>' ;
				}

				$count = 1 ;

				foreach ( $enabled['hidden'] as $id => $service )
				{
					// Individual HTML for sharing service
					$sharing_content .= '<li class="share-'.$service->get_class().'">' ;
					$sharing_content .= $service->get_display( $post ) ;
					$sharing_content .= '</li>' ;

					if ( ( $count % 2 ) == 0 )
					{
						$sharing_content .= '<li class="share-end"></li>' ;
					}

					$count ++ ;
				}

				// End of wrapper
				$sharing_content .= '<li class="share-end"></li></ul></div></div>' ;
			}

			$sharing_content .= '</div></div></div>' ;

			// Register our JS
			wp_register_script( 'sharing-js' , plugin_dir_url( __FILE__ ) . 'sharing.js' , array( 'jquery' ) , '20121205' ) ;
			add_action( 'wp_footer' , 'sharing_add_footer' ) ;
		}

		return $sharing_content ;
	}

	/**
	 * List recent past presents in shortcode
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 */
	public static function homepage_recent_past_presents ( $atts )
	{
		global $woocommerce_loop ;

		extract( shortcode_atts( array(
			'per_page' => '6' ,
			'columns' => '3' ,
			'orderby' => 'meta_value date' ,
			'order' => 'desc'
		) , $atts ) ) ;

		$products_meta = WC( )->query->get_meta_query( ) ;
		$products_meta[ ] = array(
			'key' => '_featured'
		) ;

		$args = array(
			'post_type' => 'product' ,
			'post_status' => 'publish' ,
			'product_cat' => 'past-presents' ,
			'ignore_sticky_posts' => 1 ,
			'posts_per_page' => $per_page ,
			'orderby' => $orderby ,
			'order' => $order ,
			'meta_query' => $products_meta ,
			'meta_key' => '_featured'
		) ;

		ob_start( ) ;

		$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query' , $args , $atts ) ) ;

		if ( ! count( $products->posts ) )
		{
			unset( $args[ 'meta_query' ] ) ;
		}

		$woocommerce_loop[ 'columns' ] = $columns ;

		if ( $products->have_posts( ) ) : ?>

			<ul class="products past-presents">

				<?php while ( $products->have_posts( ) ) : $products->the_post( ) ; ?>

					<?php wc_get_template_part( 'content' , 'past-present' ) ; ?>

				<?php endwhile ; // end of the loop. ?>

			</ul>

		<?php endif ;

		wp_reset_postdata( ) ;

		return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean( ) . '</div>' ;
	}

	/**
	 * Recent Products shortcode
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 */
	public static function homepage_recent_products ( $atts )
	{
		global $woocommerce_loop ;

		extract( shortcode_atts( array(
			'per_page' => '6' ,
			'columns' => '3' ,
			'orderby' => 'meta_value date' ,
			'order' => 'desc'
		) , $atts ) ) ;

		$products_meta = WC( )->query->get_meta_query( ) ;
		$products_meta[ ] = array(
			'key' => '_featured'
		) ;

		$args = array(
			'post_type' => 'product' ,
			'post_status' => 'publish' ,
			'ignore_sticky_posts' => 1 ,
			'posts_per_page' => $per_page ,
			'orderby' => $orderby ,
			'order' => $order ,
			'meta_query' => $products_meta ,
			'meta_key' => '_featured'
		) ;

		ob_start( ) ;

		$products = new WP_Query( ) ;
		$products->query_vars = apply_filters( 'woocommerce_shortcode_products_query' , $args , $atts ) ;

		$products->set( 'meta_query' ,
			array(
				'relation' => 'AND' ,
				array(
					'key' => '_price' ,
					'value' => 0 ,
					'compare' => '>' ,
					'type' => 'numeric'
				) ,
				array(
					'relation' => 'OR' ,
					array(
						'key' => '_stock' ,
						'value' => 0 ,
						'compare' => '>' ,
						'type' => 'numeric'
					) ,
					array(
						'key' => '_manage_stock' ,
						'value' => 'no' ,
						'compare' => '=='
					)
				)
			)
		) ;

		$products->get_posts( ) ;

		$woocommerce_loop[ 'columns' ] = $columns ;

		if ( $products->have_posts( ) ) : ?>
		
			<?php $woocommerce_loop[ 'total' ] = $products->post_count ; ?>

			<?php woocommerce_product_loop_start( ) ; ?>

				<?php while ( $products->have_posts( ) ) : $products->the_post( ) ; ?>

					<?php wc_get_template_part( 'content' , 'product' ) ; ?>

				<?php endwhile ; // end of the loop. ?>

			<?php woocommerce_product_loop_end( ) ; ?>

		<?php endif ;

		wp_reset_postdata( ) ;

		return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean( ) . '</div>' ;
	}

	/**
	 * Recent Products shortcode
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 * @since 2.2.18.10
	 */
	public static function homepage_recent_posts ( $atts )
	{
		extract( shortcode_atts( array(
			'per_page' => '6' ,
			'orderby' => 'date' ,
			'order' => 'desc'
		) , $atts ) ) ;

		$args = array(
			'post_type' => 'post' ,
			'post_status' => 'publish' ,
			'posts_per_page' => $per_page ,
			'orderby' => $orderby ,
			'order' => $order
		) ;

		ob_start( ) ;

		$loop = new WP_Query( $args ) ;

		if ( $loop->have_posts ( ) ) : ?>

		<div id="gallery-posts" class="gallery gallery-posts thumbnails no-loop">
			<ul class="slides" data-slide-count="<?php echo $loop->post_count ; ?>">

				<?php while ( $loop->have_posts( ) ) : $loop->the_post( ) ; ?>

					<li>

						<a href="<?php the_permalink( ) ; ?>" title="<?php the_title( ) ; ?>">
							<?php
								$src = wp_get_attachment_image_src( get_post_thumbnail_id( ) , 'full' ) ;
								echo apply_filters( 'woocommerce_single_product_image_thumbnail_html' , '<img class="lazy" data-src="' . $src[ 0 ] . '" />' , 'full' ) ;
							?>
						</a>

					</li>

				<?php endwhile ; ?>

			</ul>
			<div class="controls"></div>
		</div>

		<div class="clear"></div>

		<?php wp_reset_query( ) ; endif ;

		return ob_get_clean( ) ;
	}

	/**
	 * Display subscription plans
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 * @since 2.2.22.3
	 */
	public static function subscription_plans ( $atts )
	{
		global $woocommerce_loop ;

		extract( shortcode_atts( array( 
			'show' => '1,3,6,12' ,
			'shipping_zone' => '2'
		) , $atts ) ) ;

		ob_start( ) ;

	?>

		<div class="plans">

		<?php

			$args = array( 'post_type' => 'product' , 'name' => 'subscription' ) ;
			$loop = new WP_Query( $args ) ;

			wp_reset_query( ) ;

			$product_factory = new WC_Product_Factory( ) ;
			$product = $product_factory->get_product( $loop->post ) ;
			$variations = $product->get_available_variations( ) ;

			$wc_attribute_taxonomy = wc_attribute_taxonomy_name( 'plan' ) ;
			$plan_terms = wc_get_product_terms( $loop->post->ID , $wc_attribute_taxonomy , array( 'fields' => 'all' ) ) ;
			$show_plans = explode( ',' , $show ) ;

			foreach ( $plan_terms as $plan_term )
			{
				foreach ( $variations as $variation )
				{
					if ( $variation[ 'attributes' ][ 'attribute_pa_plan' ] == $plan_term->slug && $variation[ 'attributes' ][ 'attribute_pa_shipping-zone' ] == $shipping_zone )
					{
						$product = $product_factory->get_product( $variation[ 'variation_id' ] ) ;

						switch ( $product->subscription_period )
						{
							case 'month' :
								$months = $product->subscription_period_interval ;
								break ;

							case 'year' :
								$months = $product->subscription_period_interval * 12 ;
								break ;
						}
						break ;
					}
				}

				if ( ! in_array( $months , $show_plans ) ) continue ;
			?>

				<a class="plan col-lg-3 col-md-3 col-sm-6 col-xs-6" href="<?php echo get_permalink( get_page_by_path( 'subscribe' ) ) . '?plan=' . $plan_term->slug ; ?>">
					<div class="months">
						<div class="icon-<?php echo $months == 1 ? 'monthly' : 'month' . $months ; ?>"></div>
					</div>
					<div class="name">
						<?php
							switch ( $months )
							{
								case 1 :
									echo __( 'Monthly' , 'notanotherbill' ) ;
									break ;

								case 12 :
									echo $months / 12 . ' ' . __( 'Year' , 'notanotherbill' ) ;
									break ;

								default :
									echo $months . ' ' . __( 'Months' , 'notanotherbill' ) ;
									break ;
							}
						?>
					</div>
					<div class="price">
						<div class="content">
							<span class="amount"><?php echo sprintf( get_woocommerce_price_format( ) , get_woocommerce_currency_symbol( ) , $product->price ) ; ?></span>
						</div>
					</div>
					<div class="monthly">
						<span class="content">(<span class="amount"><?php echo sprintf( get_woocommerce_price_format( ) , get_woocommerce_currency_symbol( ) , round( $product->price / $months ) ) ; ?> </span>per <?php echo __( 'month' , 'notanotherbill' ) ; ?>)</span>
					</div>
					<div class="button-wrapper">
						<div class="buy-now-button"><?php echo __( 'Buy now' , 'notanotherbill' ) ; ?></div>
					</div>
				</a>
	
			<?php } ?>

		</div>

		<?php return ob_get_clean( ) ;
	}

	/**
	 * My account login shortcode
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 */
	public static function woocommerce_my_account_login ( $atts )
	{
		wc_get_template( 'myaccount/my-account-login.php' ) ;
	}

	/**
	 * My account register shortcode
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 */
	public static function woocommerce_my_account_register ( $atts )
	{
		wc_get_template( 'myaccount/my-account-register.php' ) ;
	}

	/**
	 * Vimeo video shortcode
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 * @since 2.2.22.2
	 */
	public static function vimeo_video ( $atts )
	{
		extract( $atts ) ;

		ob_start( ) ;

		if ( ! empty( $id ) ) : ?>

		<div class="vimeo-video-wrapper">
			<iframe id="vimeo-video" class="vimeo-video-iframe" width="100%" height="100%" src="https://player.vimeo.com/video/<?php echo $id ; ?>?api=1&player_id=vimeo-video<?php if ( ! wp_is_mobile( ) ) echo '&background=1' ; ?>" width="100%" height="100%" frameborder="0" data-mobile="<?php echo wp_is_mobile( ) ? 'true' : 'false' ; ?>"></iframe>
			<div class="vimeo-video-overlay">
				<div class="vimeo-video-image">
					<?php if ( ! empty( $overlay ) ) : ?>
						<img src="<?php echo $overlay ; ?>" />
					<?php endif ; ?>
				</div>
				<div class="vimeo-video-cta">
					<div class="arrow-right"></div>
				</div>
			</div>
		</div>

		<?php endif ; return ob_get_clean( ) ;
	}

	/**
	 * ViralSweep shortcode
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 * @since 2.2.23.11
	 */
	public static function viral_sweep ( $atts )
	{
		extract( $atts ) ;
		
		ob_start( ) ;

		if ( ! empty( $id ) && ! empty( $src ) ) : ?>

		<script async id="<?php echo $id ; ?>" type="text/javascript" src="<?php echo $src ; ?>"></script>

		<?php endif ; return ob_get_clean( ) ;
	}

}