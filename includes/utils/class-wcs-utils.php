<?php
/**
 * Subscription utilities
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Utils
 * @version     2.2.18.7
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WCS_Utils
{
	public static function csv_to_array ( $filename , $delimiter = ',' )
	{
		if ( ! file_exists( $filename ) || ! is_readable( $filename ) ) return false ;

		$header = null ;
		$data = array( ) ;

		if ( ( $handle = fopen( $filename , 'r' ) ) !== false )
		{
			while ( ( $row = fgetcsv( $handle , 0 , $delimiter ) ) !== false )
			{
				if ( ! $header )
				{
					$header = $row ;
				}
				else
				{
					$data[ ] = array_combine( $header , $row ) ;
				}
			}

			fclose( $handle ) ;
		}

		return $data ;
	}

	public static function get_order_subscription ( $order )
	{
		foreach ( $order->get_items( ) as $order_item )
		{
			if ( WC_Subscriptions_Product::is_subscription( $order_item[ 'product_id' ] ) )
			{
				return $order_item ;
			}
		}

		return null ;
	}

	public static function get_subscription_post ( )
	{
		$args = array( 'post_type' => 'product' , 'name' => 'subscription' ) ;
		$loop = new WP_Query( $args ) ;

		wp_reset_query( ) ;

		return $loop->post ;
	}

	public static function get_addon ( $post_id , $name )
	{
		$addon = array_filter( get_product_addons( $post_id ) ,
			function ( $e ) use ( $name )
			{
				return $e[ 'name' ] == $name ;
			}
		) ;
		
		return array_shift( $addon ) ;
	}

	/**
	* @since 2.2.18.3
	*/
	public static function get_cart_addon ( $cart_item , $addon_name )
	{
		if ( array_key_exists( 'addons' , $cart_item ) )
		{
			$addons = $cart_item[ 'addons' ] ;

			if ( is_array( $addons ) )
			{
				foreach ( $addons as $addon )
				{
					if ( $addon[ 'name' ] == $addon_name )
					{
						return $addon[ 'value' ] ;
					}
				}
			}
		}
		
		return false ;
	}

	public static function get_add_to_cart_url ( )
	{
		$_pf = new WC_Product_Factory( ) ;
		$product = $_pf->get_product( WCS_Utils::get_subscription_post( )->ID ) ;

		return $product->add_to_cart_url( ) ;
	}

	public static function get_subscription_variation ( $length , $country_code )
	{
		$shipping_zone = woocommerce_get_shipping_zone(
			array( 
				'destination' => array(
					'country' => $country_code ,
					'state' => '' ,
					'postcode' => ''
				)
			)
		) ;

		$product_factory = new WC_Product_Factory( ) ;
		$variations = WCS_Utils::get_subscription_variations( ) ;

		foreach ( $variations as $variation )
		{
			if ( $variation[ 'attributes' ][ 'attribute_pa_shipping-zone' ] == $shipping_zone->zone_id )
			{
				$product = $product_factory->get_product( $variation[ 'variation_id' ] ) ;

				switch ( $product->subscription_period )
				{
					case 'month' :
						if ( $product->subscription_period_interval == $length )
						{
							return $variation ;
						}
						break ;

					case 'year' :
						if ( $product->subscription_period_interval == $length * 12 )
						{
							return $variation ;
						}
						break ;
				}
			}
		}

		return false ;
	}

	public static function get_subscription_variations ( $country_code = '' ) 
	{
		$_pf = new WC_Product_Factory( ) ;
		$product = $_pf->get_product( WCS_Utils::get_subscription_post( )->ID ) ;
		$variations = $product->get_available_variations( ) ;

		// Filter variations by country code
		if ( ! empty( $country_code ) )
		{
			$shipping_zone = woocommerce_get_shipping_zone(
				array( 
					'destination' => array(
						'country' => $country_code ,
						'state' => '' ,
						'postcode' => ''
					)
				)
			) ;

			$filtered_variations = array( ) ;

			foreach ( $variations as $variation )
			{
				if ( $variation[ 'attributes' ][ 'attribute_pa_shipping-zone' ] == $shipping_zone->zone_id )
				{
					$filtered_variations[ ] = $variation ;
				}
			}

			$variations = $filtered_variations ;
		}

		return $variations ;
	}

	public static function get_subscription_attributes ( $term )
	{
		$_pf = new WC_Product_Factory( ) ;
		$product = $_pf->get_product( WCS_Utils::get_subscription_post( )->ID ) ;
		$attributes = $product->get_variation_attributes( ) ;

		return $attributes[ $term ] ;
	}

	public static function get_cart_subscription ( )
	{
		global $woocommerce ;

		if ( isset( $woocommerce->session->cart ) )
		{
			$subscription_post_id = WCS_Utils::get_subscription_post( )->ID ;

			foreach ( $woocommerce->session->cart as $item )
			{
				if ( $item[ 'product_id' ] == $subscription_post_id )
				{
					return $item ;
				}
			}
		}

		return null ;
	}

	public static function get_product_addon_value ( $product , $addon_name )
	{
		if ( isset( $product[ 'addons' ] ) )
		{
			foreach ( $product[ 'addons' ] as $addon )
			{
				if ( $addon[ 'name' ] == $addon_name )
				{
					return $addon[ 'value' ] ;
				}
			}
		}

		return null ;
	}

	public static function get_cart_subscription_start_date ( )
	{
		$cart_subscription = WCS_Utils::get_cart_subscription( ) ;

		if ( isset( $cart_subscription ) && isset( $cart_subscription[ 'addons' ] ) )
		{
			foreach ( $cart_subscription[ 'addons' ] as $addon )
			{
				if ( $addon[ 'name' ] == 'Start Date - Date' )
				{
					return $addon[ 'value' ] ;
				}
			}
		}

		return null ;
	}

	public static function get_order_id_and_key ( $paypal_args )
	{
		// First try and get the order ID by the subscr_id
		if ( isset( $paypal_args[ 'subscr_id' ] ) )
		{
			$posts = get_posts(
				array(
				'numberposts' => 1 ,
				'orderby' => 'ID' ,
				'order' => 'ASC' ,
				'meta_key' => 'PayPal Subscriber ID' ,
				'meta_value' => $paypal_args[ 'subscr_id' ] ,
				'post_type' => 'shop_order' ,
				'post_status' => 'any' ,
				'post_parent' => 0 ,
				'suppress_filters' => true
				)
			) ;

			if ( ! empty( $posts ) )
			{
				$order_id  = $posts[ 0 ]->ID ;
				$order_key = get_post_meta( $order_id , '_order_key' , true ) ;
			}
		}

		// Couldn't find the order ID by subscr_id, so it's either not set on the order yet or the $args doesn't have a subscr_id, either way, let's get it from the args
		if ( ! isset( $order_id ) ) 
		{
			$paypal_args[ 'custom' ] = maybe_unserialize( $paypal_args[ 'custom' ] ) ;
			$order_id = ( int ) $paypal_args[ 'custom' ][ 0 ] ;
			$order_key = $paypal_args[ 'custom' ][ 1 ] ;
		}

		return array( 'order_id' => $order_id , 'order_key' => $order_key ) ;
	}

	public static function get_user_rating ( $product_id , $user_id )
	{
		global $wpdb ;

		$rating = $wpdb->get_var( $wpdb->prepare( "
			SELECT SUM(meta_value) FROM $wpdb->commentmeta
			LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
			WHERE meta_key = 'rating'
			AND comment_post_ID = %d
			AND comment_approved = '1'
			AND user_id = %d
			AND meta_value > 0
		" , $product_id , $user_id ) ) ;

		return (int)$rating ;
	}

	/**
	* @since 2.2.18.7
	*/
	public static function get_next_shipping_date ( )
	{
		// Get shipping dates
		$shipping_dates = explode( ',' , get_option( 'nab-shipping-dates' ) ) ;

		// Sort shipping dates
		asort( $shipping_dates ) ;

		// Get the first shipping date in the future
		foreach ( $shipping_dates as $shipping_date )
		{
			if ( strtotime( $shipping_date ) > time( ) )
			{
				return $shipping_date ;
			}
		}

		return false ;
	}

	/**
	* @since 2.2.18.7
	*/
	public static function get_last_shipping_date ( )
	{
		// Get shipping dates
		$shipping_dates = explode( ',' , get_option( 'nab-shipping-dates' ) ) ;

		// Sort shipping dates in reverse order
		rsort( $shipping_dates ) ;

		// Get the first shipping date in the past
		foreach ( $shipping_dates as $shipping_date )
		{
			if ( strtotime( $shipping_date ) < time( ) )
			{
				return $shipping_date ;
			}
		}
	}

	/* 
	 * return the next shipping date
	 */
	public static function next_shipping_date ( )
	{ 
		$shipping_dates = explode( ',' , get_option( 'nab-shipping-dates' ) ) ;
        $today = new DateTime( 'now' ) ;

        foreach ( $shipping_dates as $shipping_date )
        {
        	$shipping_date = new DateTime( $shipping_date ) ;

        	if ( $shipping_date->format( 'H:i:s' ) == '00:00:00' )
        	{
        		$shipping_date->add( new DateInterval( 'PT12H' ) ) ;
        	}
        	
            if ( $today < $shipping_date )
            {
                return $next_shipping_date = $shipping_date->format( 'Y-m-d H:i:s' ) ;
            }
        }
	}

	/* 
	 * return the time until the next shipping date
	 */
	public static function time_until_next_shipping_date ( )
	{ 
		$next_shipping_time = strtotime( self::next_shipping_date( ) ) ;
		$diff = $next_shipping_time - time( ) ;
		$minute = 60 ;
		$hour = $minute * 60 ;
		$day = $hour * 24 ;
		$days = floor( $diff / $day ) ;
		$hours = floor( ( $diff % $day ) / $hour ) ;
		$minutes = floor( ( $diff % $hour ) / $minute ) ;
		$seconds = floor( $diff % $minute ) ;
		$time_until_next_shipping_date = "$days<span class=\"light-italic\">day" . ( ( $days != 1 ) ? 's' : '' ) . "</span> : $hours<span class=\"light-italic\">hour" . ( ( $hours != 1 ) ? 's' : '' ) . "</span> : $minutes<span class=\"light-italic\">min" . ( ( $minutes != 1 ) ? 's' : '' ) . "</span>" ;

		return $time_until_next_shipping_date ;
	}
}