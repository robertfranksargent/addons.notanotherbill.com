<?php
/**
 * General utilities
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Utils
 * @version     2.2.21.11
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_Utils
{
	/**
	* @since 2.2.21.11
	*/
	public static function get_products_select_options ( )
	{
		$products = array( ) ;

		$query = new WP_Query( ) ;
		$query->set( 'post_type' , 'product' ) ;
		$query->set( 'posts_per_page' , 9999999 ) ;
		$query->set( 'order' , 'ASC' ) ;
		$query->set( 'orderby' , 'post_title' ) ;
		$query->get_posts( ) ;

		if ( $query->have_posts( ) )
		{
			foreach ( $query->posts as $product )
			{
				$products[ $product->ID ] = sprintf( '%s (#%s)' , $product->post_title , $product->ID ) ;
			}
		}

		wp_reset_postdata( ) ;

		return $products ;
	}
}