<?php
/**
 * Subscriptions Admin
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin
 * @version     2.2.21.2
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_NAB_Admin_Subscriptions' ) ) :

/**
 * WC_NAB_Admin_Subscriptions Class
 */
class WC_NAB_Admin_Subscriptions
{
	var $message_transient_prefix = '_subscriptions_messages_' ;
	var $subscriptions_screen_id = 'woocommerce_page_subscriptions' ;

	/**
	 * Constructor
	 *
	 * @since 2.2.18.12
	 */
	public function __construct ( )
	{
		add_action( 'admin_menu' , array( $this , 'init_admin_menu' ) ) ;
		add_action( 'admin_enqueue_scripts' , array( $this , 'load_styles_scripts' ) ) ;
		add_action( 'wp_ajax_wcs_immediate_payment' , array( $this , 'ajax_immediate_payment' ) ) ;

		add_filter( 'woocommerce_subscriptions_list_table_actions' , array( $this , 'subscriptions_list_table_actions' ) , 10 , 2 ) ;
		add_filter( 'woocommerce_subscriptions_list_table_column_content' , array( $this , 'subscriptions_list_table_columns' ) , 10 , 3 ) ;
		add_filter( 'acf/fields/relationship/result' , array( $this , 'relationship_result' ) , 10 , 4 ) ;
		add_filter( 'woocommerce_subscriptions_is_large_site' , array( $this , 'subscriptions_is_large_site' ) ) ;
		add_filter( 'manage_' . WC_Subscriptions_Admin::$admin_screen_id . '_columns' , array( $this , 'edit_subscription_table_columns' ) , 10 , 1 ) ;
 	}

 	/**
 	* Call process actions method.
 	*
 	* @since 2.2.18.12
 	*/
 	public function init_admin_menu ( )
 	{
 		$this->process_actions( ) ;
 	}

 	/**
	 * Load admin styles & scripts only on needed pages
	 *
	 * @since 2.2.18.12
	 * @param $hook_suffix
	 */
	public function load_styles_scripts ( $hook_suffix )
	{
		if ( $hook_suffix == $this->subscriptions_screen_id )
		{
			wp_enqueue_script( 'wc-nab-admin-subscriptions' , WC_NAB_Addons( )->plugin_url( ) . '/assets/js/admin/subscriptions' . ( is_dev( ) ? '' : '.min' ) . '.js', array( ) , WC_NAB_Addons( )->version , true ) ;
		}
	}

 	/**
	 * Handle immediate payment request
	 *
	 * @since 2.2.16.7
	 */
	public function ajax_immediate_payment ( )
	{
		$response = array( 'status' => 'error' ) ;

		if ( ! wp_verify_nonce( $_POST[ 'wcs_nonce' ] , 'woocommerce-subscriptions' ) )
		{
			$response[ 'message' ] = sprintf( '<div class="error">%s</div>' , __( 'Invalid security token, please reload the page and try again.' , 'woocommerce-subscriptions' ) ) ;
		}
		elseif ( ! current_user_can( 'manage_woocommerce' ) )
		{
			$response[ 'message' ] = sprintf( '<div class="error">%s</div>' , __( 'Only store managers can edit payment dates.' , 'woocommerce-subscriptions' ) ) ;
		}
		else 
		{
			// Add one minute to the current time
			$new_payment_date = date( 'Y-m-d H:i:s' , strtotime( '+1 minute' ) ) ;
			$new_payment_timestamp = WC_Subscriptions_Manager::update_next_payment_date( $new_payment_date , $_POST[ 'wcs_subscription_key' ] , WC_Subscriptions_Manager::get_user_id_from_subscription_key( $_POST[ 'wcs_subscription_key' ] ) , 'user' ) ;

			if ( is_wp_error( $new_payment_timestamp ) )
			{
				$response[ 'message' ] = sprintf( '<div class="error">%s</div>' , $new_payment_timestamp->get_error_message( ) ) ;
			}
			else
			{
				$new_payment_timestamp_user_time = $new_payment_timestamp + ( get_option( 'gmt_offset' ) * 3600 ) ; // The timestamp is returned in server time
				$time_diff = $new_payment_timestamp - gmdate( 'U' ) ;

				if ( $time_diff > 0 && $time_diff < 7 * 24 * 60 * 60 )
				{
					$date_to_display = sprintf( __( 'In %s', 'woocommerce-subscriptions' ) , human_time_diff( gmdate( 'U' ) , $new_payment_timestamp ) ) ;
				}
				else
				{
					$date_to_display = date_i18n( woocommerce_date_format( ) , $new_payment_timestamp_user_time ) ;
				}

				$response[ 'status' ] = 'success' ;
				$response[ 'message' ] = sprintf( '<div class="updated">%s</div>' , __( 'Date Changed' , 'woocommerce-subscriptions' ) ) ;
				$response[ 'dateToDisplay' ] = $date_to_display ;
				$response[ 'timestamp' ] = $new_payment_timestamp_user_time ;
			}
		}

		echo json_encode( $response ) ;

		exit ;
	}

	/**
	* Returns false so that subscriptions search field is displayed.
	*
	* @since 2.2.11.23
	* @return bool
	*/
	public function subscriptions_is_large_site ( )
	{
		return false ;
	}

	/**
	* Add expire action to subscription list table actions.
	*
	* @since 2.2.18.12
	*/
	public function subscriptions_list_table_actions ( $actions , $item )
	{
		$new_status = 'expired' ;

		if ( WC_Subscriptions_Manager::can_subscription_be_changed_to( $new_status , $item[ 'subscription_key' ] , $item[ 'user_id' ] ) && $item[ 'status' ] != $new_status )
		{
			$action_url = add_query_arg(
				array(
					'page' => $_REQUEST[ 'page' ] ,
					'user' => $item[ 'user_id' ] ,
					'subscription' => $item[ 'subscription_key' ] ,
					'_wpnonce' => wp_create_nonce( $item[ 'subscription_key' ] )
				)
			) ;

			$label = __( 'Expire' , 'woocommerce-subscriptions' ) ;
			
			$actions[ 'expire' ] = sprintf( '<a href="%s">%s</a>' , esc_url( add_query_arg( 'new_status' , $new_status , $action_url ) ) , $label ) ;
		}

		return $actions ;
	}

	/**
	* Edit manage subscriptions table columns.
	*
	* @since 2.2.17.6
	*/
 	public function edit_subscription_table_columns ( $columns )
 	{
 		$new_columns = array(
 			'failed_payment_count' => __( 'Failed Payments' , 'notanotherbill' ) ,
 			'associated_subscriptions' => __( 'Associated Subscriptions' , 'notanotherbill' )
 		) ;

 		return array_merge( $columns , $new_columns ) ;
 	}

	/**
	* Edit subscriptions table columns
	*
	* @since 2.2.21.2
	*/
	public function subscriptions_list_table_columns ( $column_content , $item , $column_name )
	{
		$order_id = $item[ 'order_id' ] ;
		$failed_payments = $item[ 'failed_payments' ] ;

		switch ( $column_name )
		{
			case 'title' :
				$period = $item[ 'period' ] ;
				$interval = $item[ 'interval' ] ;

				if ( $period == 'year' )
				{
					$period = 'month' ;
					$interval *= 12 ;
				}

				$column_content = sprintf( '<a href="%s">%d %s%s</a>' , get_edit_post_link( $order_id ) , $interval , $period , $interval > 1 ? 's' : ' rolling' ) ;
				
				break ;

			case 'next_payment_date' :
				$current_gmt_time = gmdate( 'U' ) ;
				$next_payment_timestamp_gmt = WC_Subscriptions_Manager::get_next_payment_date( $item[ 'subscription_key' ] , $item[ 'user_id' ] , 'timestamp' ) ;
				$next_payment_timestamp = $next_payment_timestamp_gmt + get_option( 'gmt_offset' ) * 3600 ;

				if ( $next_payment_timestamp_gmt == 0 )
				{
					$column_content = '-' ;
				}
				else
				{
					// Convert to site time
					$time_diff = $next_payment_timestamp_gmt - $current_gmt_time ;

					if ( $time_diff > 0 && $time_diff < 7 * 24 * 60 * 60 )
					{
						$next_payment = sprintf( __( 'In %s' , 'woocommerce-subscriptions' ) , human_time_diff( $current_gmt_time , $next_payment_timestamp_gmt ) ) ;
					}
					else
					{
						$next_payment = date_i18n( woocommerce_date_format( ) , $next_payment_timestamp ) ;
					}

					$column_content = sprintf( '<time class="next-payment-date" title="%s">%s</time>' , esc_attr( $next_payment_timestamp ) , $next_payment ) ;

					if ( WC_Subscriptions_Manager::can_subscription_be_changed_to( 'new-payment-date' , $item[ 'subscription_key' ] , $item[ 'user_id' ] ) )
					{
						$column_content .= '<div class="edit-date-div row-actions hide-if-no-js">' ;
						$column_content .= '<img class="date-picker-icon" src="' . admin_url( 'images/date-button.gif' ) . '" title="' . __( 'Date Picker Icon', 'woocommerce-subscriptions' ) . '" />' ;
						$column_content .= '<a href="#edit_timestamp" class="edit-timestamp" tabindex="4">' . __( 'Change' , 'woocommerce-subscriptions' ) . '</a>' ;
						$column_content .= '<div class="date-picker-div hide-if-js">' ;
						$column_content .= WC_Subscriptions_Manager::touch_time(
							array(
								'date' => date( 'Y-m-d' , $next_payment_timestamp ) ,
								'echo' => false ,
								'multiple' => true ,
								'include_time' => false
							)
						) ;
						$column_content .= '<a class="immediate-payment hide-if-no-js button" title="' . __( 'Immediate payment' , 'notanotherbill' ) . '">' . __( 'Immediate payment' , 'notanotherbill' ) . '</a>' ;
						$column_content .= '</div>' ;
						$column_content .= '</div>' ;
					}
				}

				break ;

			case 'failed_payment_count' :
				if ( $failed_payments > 0 )
				{
					$column_content = '<a href="' . admin_url( 'edit.php?post_status=wc-failed&post_type=shop_order&post_parent=' . $order_id ) . '">' . $item[ 'failed_payments' ] . '</a>' ;
				}
				else
				{
					$column_content = '-' ;
				}

				break ;

			case 'last_payment_date' :
				$renewal_order_count = WC_Subscriptions_Renewal_Order::get_renewal_order_count( $order_id ) ;
				$last_payment_date = $column_content ;

				if ( $renewal_order_count > 0 )
				{
					$renewal_orders = WC_Subscriptions_Renewal_Order::get_renewal_orders( $order_id , 'WC_Order' ) ;
					$order = end( $renewal_orders ) ;
				}
				else
				{
					$order = new WC_Order( $order_id ) ;
				}

				$payment_pending = in_array( $order->get_status( ) , array( 'failed' , 'pending' ) ) ;
				$payment_class = $payment_pending ? 'failed' : 'paid' ;

				$column_content = '<a href="' . get_edit_post_link( $order->id ) . '" class="' . $payment_class . '">' . $last_payment_date . '</a>' ;

				break ;

			case 'associated_subscriptions' :
				$associated_order_ids = get_post_meta( $order_id , '_associated_subscription_orders' , true ) ;

				if ( ! empty( $associated_order_ids ) )
				{
					$parts = explode( '_' , $item[ 'subscription_key' ] ) ;
					$original_order_id = $parts[ 0 ] ;
					$actions = array( ) ;

					foreach ( $associated_order_ids as $associated_order_id )
					{
						if ( $original_order_id != $associated_order_id && get_post_status( $associated_order_id ) != 'trash' )
						{
							$action_link = get_edit_post_link( $associated_order_id ) ;

							if ( ! empty( $action_link ) )
							{
								$actions[ $associated_order_id ] = sprintf( '<a href="%s">%s #%s</a>' , $action_link , __( 'Order' , 'woocommerce' ) , $associated_order_id ) ;
							}
						}
					}

					$column_content = implode( '<br /> ' , $actions ) ;
				}
				else
				{
					$column_content = '-' ;
				}

				break ;
		}

		return $column_content ;
	}

	public function relationship_result ( $result , $object , $field , $post )
	{
		$order = new WC_Order( $object->ID ) ;

		$first_name = get_post_meta( $object->ID , '_shipping_first_name' , true ) ;
		$last_name = get_post_meta( $object->ID , '_shipping_last_name' , true ) ;

		$interval = WC_Subscriptions_Order::get_item_meta( $object->ID , '_subscription_interval' ) ;
		$period = WC_Subscriptions_Order::get_item_meta( $object->ID , '_subscription_period' ) ;

		if ( $period == 'year' )
		{
			$period = 'month' ;
			$interval *= 12 ;
		}

		$result = sprintf( '#%d – %s %s (%d %s%s)' , $object->ID , $first_name , $last_name , $interval , $period , $interval > 1 ? 's' : ' rolling' ) ;

		return $result ;
	}

 	/**
 	* Process expire action.
 	*
 	* @since 2.2.18.12
 	*/
 	private function process_actions ( )
 	{
		$current_action = false;

		if ( isset( $_REQUEST[ 'new_status' ] ) )
		{
			$current_action = $_REQUEST[ 'new_status' ] ;
		}

		if ( isset( $_REQUEST[ 'action' ] ) && -1 != $_REQUEST[ 'action' ] )
		{
			$current_action = $_REQUEST[ 'action' ] ;
		}

 		if ( $current_action && $current_action == 'expired' )
 		{
			$messages = array( ) ;
			$error_messages = array( ) ;
			$query_args = array( ) ;

			if ( isset( $_GET[ 'subscription' ] ) ) // Single subscription action
			{
				if ( ! wp_verify_nonce( $_REQUEST[ '_wpnonce' ] , $_GET[ 'subscription' ] ) )
				{
					wp_die( __( 'Action failed. Invalid Nonce.' , 'woocommerce-subscriptions' ) ) ;
				}

				$subscriptions = array( $_GET[ 'user' ] => array( $_GET[ 'subscription' ] ) ) ;
			}
			elseif ( isset( $_GET[ 'subscription_keys' ] ) ) // Bulk actions
			{
				if ( ! wp_verify_nonce( $_REQUEST[ '_wpnonce' ] , 'bulk-subscriptions' ) )
				{
					wp_die( __( 'Bulk edit failed. Invalid Nonce.' , 'woocommerce-subscriptions' ) ) ;
				}

				$subscriptions = $_GET[ 'subscription_keys' ] ;
			}

			$subscription_count = 0 ;
			$error_count = 0 ;

			foreach ( $subscriptions as $user_id => $subscription_keys )
			{
				foreach ( $subscription_keys as $subscription_key )
				{
					if ( ! WC_Subscriptions_Manager::can_subscription_be_changed_to( $current_action , $subscription_key ) )
					{
						$error_count++ ;
					}
					else
					{
						$subscription_count++ ;

						WC_Subscriptions_Manager::expire_subscription( $user_id , $subscription_key ) ;
						do_action( 'admin_changed_subscription_to_' . $current_action , $subscription_key ) ;
					}
				}
			}

			if ( $subscription_count > 0 )
			{
				$messages[ ] = sprintf( _n( 'Subscription changed to %2$s.' , '%1$d subscriptions changed to %2$s.' , $subscription_count , 'woocommerce-subscriptions' ) , $subscription_count , $current_action ) ;
			}

			if ( $error_count > 0 )
			{
				$error_messages[ ] = sprintf( _n( '%d subscription could not be expired - only active and on-hold subscriptions can be expired.' , '%d subscriptions could not be expired - only active and on-hold subscriptions can be expired.' , $error_count , 'woocommerce-subscriptions' ) , $error_count ) ;
			}

			if ( ! empty( $messages ) || ! empty( $error_messages ) )
			{
				$message_nonce = wp_create_nonce( __FILE__ ) ;
				set_transient( $this->message_transient_prefix . $message_nonce , array( 'messages' => $messages , 'error_messages' => $error_messages ) , 60 * 60 ) ;
			}

			// See WC_Subscriptions_List_Table::process_actions( )
			if ( isset( $_GET[ '_customer_user' ] ) || isset( $_GET[ '_product_id' ] ) )
			{
				if ( ! empty( $_GET['_customer_user'] ) )
				{
					$user_id = intval( $_GET[ '_customer_user' ] ) ;
					$user = get_user_by( 'id' , absint( $_GET[ '_customer_user' ] ) ) ;

					if ( false === $user )
					{
						wp_die( __( 'Action failed. Invalid user ID.' , 'woocommerce-subscriptions' ) ) ;
					}

					$query_args[ '_customer_user' ] = $user_id ;
				}

				if ( ! empty( $_GET[ '_product_id' ] ) )
				{
					$product_id = intval( $_GET[ '_product_id' ] ) ;
					$product = get_product( $product_id ) ;

					if ( false === $product )
					{
						wp_die( __( 'Action failed. Invalid product ID.' , 'woocommerce-subscriptions' ) ) ;
					}

					$query_args[ '_product_id' ] = $product_id ;
				}

			}

			$query_args[ 'status' ] = ( isset( $_GET[ 'status' ] ) ) ? $_GET[ 'status' ] : 'all' ;

			if ( ! empty( $messages ) || ! empty( $error_messages ) ) $query_args[ 'message' ] = $message_nonce ;
			if ( isset( $_GET['paged'] ) ) $query_args[ 'paged' ] = $_GET[ 'paged' ] ;

			$search_query = _admin_search_query( ) ;

			if ( ! empty( $search_query ) ) $query_args[ 's' ] = $search_query ;

			$redirect_to = add_query_arg( $query_args , admin_url( 'admin.php?page=subscriptions' ) ) ;

			wp_safe_redirect( $redirect_to ) ;
			exit ;
		}
 	}
}

endif ;

return new WC_NAB_Admin_Subscriptions( ) ;