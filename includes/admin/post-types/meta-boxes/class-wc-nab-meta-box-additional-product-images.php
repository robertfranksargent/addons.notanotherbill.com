<?php
/**
 * Additional Product Images
 *
 * Display an additional product images meta box.
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin/Meta Boxes
 * @version     2.2.4
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

/**
 * WC_NAB_Meta_Box_Additional_Product_Images
 */
class WC_NAB_Meta_Box_Additional_Product_Images
{
	/**
	 * Output the metabox
	 */
	public static function output ( $post ) {
		?>
		<div id="additional_product_images_container">
			<ul class="product_images">
				<?php
					if ( metadata_exists( 'post' , $post->ID , '_additional_product_image_gallery' ) )
					{
						$additional_product_image_gallery = get_post_meta( $post->ID , '_additional_product_image_gallery' , true ) ;
					}
					else
					{
						// Backwards compat
						$attachment_ids = get_posts( 'post_parent=' . $post->ID . '&numberposts=-1&post_type=attachment&orderby=menu_order&order=ASC&post_mime_type=image&fields=ids&meta_key=_woocommerce_exclude_image&meta_value=0' );
						$attachment_ids = array_diff( $attachment_ids , array( get_post_thumbnail_id( ) ) ) ;
						$additional_product_image_gallery = implode( ',' , $attachment_ids ) ;
					}

					$attachments = array_filter( explode( ',' , $additional_product_image_gallery ) ) ;

					if ( $attachments )
					{
						foreach ( $attachments as $attachment_id )
						{
							echo '<li class="image" data-attachment_id="' . esc_attr( $attachment_id ) . '">
								' . wp_get_attachment_image( $attachment_id , 'thumbnail' ) . '
								<ul class="actions">
									<li><a href="#" class="delete tips" data-tip="' . __( 'Delete image' , 'woocommerce' ) . '">' . __( 'Delete' , 'woocommerce' ) . '</a></li>
								</ul>
							</li>' ;
						}
					}
				?>
			</ul>

			<input type="hidden" id="additional_product_image_gallery" name="additional_product_image_gallery" value="<?php echo esc_attr( $additional_product_image_gallery ) ; ?>" />

		</div>
		<p class="add_additional_product_images hide-if-no-js">
			<a href="#" data-choose="<?php _e( 'Add Images to Product Gallery' , 'woocommerce' ) ; ?>" data-update="<?php _e( 'Add to gallery' , 'woocommerce' ) ; ?>" data-delete="<?php _e( 'Delete image' , 'woocommerce' ) ; ?>" data-text="<?php _e( 'Delete' , 'woocommerce' ) ; ?>"><?php _e( 'Add product gallery images' , 'woocommerce' ) ; ?></a>
		</p>
		<?php
	}

	/**
	 * Save meta box data
	 */
	public static function save ( $post_id , $post )
	{
		$attachment_ids = array_filter( explode( ',' , wc_clean( $_POST[ 'additional_product_image_gallery' ] ) ) ) ;

		update_post_meta( $post_id , '_additional_product_image_gallery' , implode( ',' , $attachment_ids ) ) ;
	}
}