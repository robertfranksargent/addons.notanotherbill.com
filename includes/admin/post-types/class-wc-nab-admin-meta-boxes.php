<?php
/**
 * WooCommerce Meta Boxes
 *
 * Sets up the write panels used by products and orders (custom post types)
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin/Meta Boxes
 * @version     2.2.4
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

/**
 * WC_NAB_Admin_Meta_Boxes
 */
class WC_NAB_Admin_Meta_Boxes
{
	private static $meta_box_errors = array( ) ;

	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		add_action( 'add_meta_boxes' , array( $this , 'add_meta_boxes' ) , 40 ) ;
		add_action( 'save_post' , array( $this , 'save_meta_boxes' ) , 1 , 2 ) ;

		// Save Product Meta Boxes
		add_action( 'woocommerce_process_product_meta' , 'WC_NAB_Meta_Box_Additional_Product_Images::save' , 30 , 2 ) ;

		// Error handling (for showing errors from meta boxes on next page load)
		add_action( 'admin_notices' , array( $this , 'output_errors' ) ) ;
		add_action( 'shutdown' , array( $this , 'save_errors' ) ) ;
	}

	/**
	 * Add an error message
	 * @param string $text
	 */
	public static function add_error ( $text )
	{
		self::$meta_box_errors[ ] = $text ;
	}

	/**
	 * Save errors to an option
	 */
	public function save_errors ( )
	{
		update_option( 'woocommerce_meta_box_errors' , self::$meta_box_errors ) ;
	}

	/**
	 * Show any stored error messages.
	 */
	public function output_errors ( )
	{
		$errors = maybe_unserialize( get_option( 'woocommerce_meta_box_errors' ) ) ;

		if ( ! empty( $errors ) )
		{
			echo '<div id="woocommerce_errors" class="error fade">' ;
			
			foreach ( $errors as $error )
			{
				echo '<p>' . esc_html( $error ) . '</p>' ;
			}
			
			echo '</div>' ;

			// Clear
			delete_option( 'woocommerce_meta_box_errors' ) ;
		}
	}

	/**
	 * Add WC Meta boxes
	 */
	public function add_meta_boxes ( )
	{
		// Products
		add_meta_box( 'woocommerce-additional-product-images' , __( 'Additional Product Gallery' , 'notanotherbill' ) , 'WC_NAB_Meta_Box_Additional_Product_Images::output' , 'product' , 'side' ) ;
	}

	/**
	 * Check if we're saving, the trigger an action based on the post type
	 *
	 * @param  int $post_id
	 * @param  object $post
	 */
	public function save_meta_boxes ( $post_id , $post )
	{
		// $post_id and $post are required
		if ( empty( $post_id ) || empty( $post ) )
		{
			return ;
		}

		// Dont' save meta boxes for revisions or autosaves
		if ( defined( 'DOING_AUTOSAVE' ) || is_int( wp_is_post_revision( $post ) ) || is_int( wp_is_post_autosave( $post ) ) )
		{
			return ;
		}
		
		// Check the nonce
		if ( empty( $_POST[ 'woocommerce_meta_nonce' ] ) || ! wp_verify_nonce( $_POST[ 'woocommerce_meta_nonce' ] , 'woocommerce_save_data' ) )
		{
			return ;
		} 

		// Check the post being saved == the $post_id to prevent triggering this call for other save_post events
		if ( empty( $_POST[ 'post_ID' ] ) || $_POST[ 'post_ID' ] != $post_id )
		{
			return ;
		}

		// Check user has permission to edit
		if ( ! current_user_can( 'edit_post' , $post_id ) )
		{
			return ;
		}

		// Check the post type
		if ( ! in_array( $post->post_type , array( 'product' ) ) )
		{
			return ;
		}

		do_action( 'woocommerce_process_product_meta' , $post_id , $post ) ;
	}

}

new WC_NAB_Admin_Meta_Boxes( ) ;