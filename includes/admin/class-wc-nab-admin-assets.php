<?php
/**
 * Load assets.
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin
 * @version     2.2.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'WC_NAB_Admin_Assets' ) ) :

/**
 * WC_NAB_Admin_Assets Class
 */
class WC_NAB_Admin_Assets
{
	/**
	 * Hook in tabs.
	 */
	public function __construct ( )
	{
		add_action( 'admin_enqueue_scripts' , array( $this , 'admin_styles' ) ) ;
		add_action( 'admin_enqueue_scripts' , array( $this , 'admin_scripts' ) ) ;
	}

	/**
	 * Enqueue styles
	 */
	public function admin_styles ( )
	{
		$screen = get_current_screen( ) ;

		if ( in_array( $screen->id , array( 'product' , 'edit-product' , WC_Subscriptions_Admin::$admin_screen_id ) ) )
		{
			// Admin styles for WC pages only
			wp_enqueue_style( 'woocommerce_nab_admin_styles' , WC_NAB_Addons( )->plugin_url( ) . '/assets/css/admin.css', array( ) , WC_NAB_VERSION ) ;
		}

		do_action( 'woocommerce_admin_css' ) ;
	}

	/**
	 * Enqueue scripts
	 */
	public function admin_scripts ( )
	{
		$screen = get_current_screen( ) ;
		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min' ;

		// Register scripts
		wp_register_script( 'woocommerce_nab_admin_meta_boxes' , WC_NAB_Addons( )->plugin_url( ) . '/assets/js/admin/meta-boxes' . $suffix . '.js' , array( 'jquery' , 'jquery-ui-sortable' ) , WC_NAB_VERSION ) ;

		// Product
		if ( in_array( $screen->id , array( 'product' , 'edit-product' ) ) )
		{
			wp_enqueue_script( 'woocommerce_nab_admin_meta_boxes' ) ;
		}
	}
}

endif ;

return new WC_NAB_Admin_Assets( ) ;
