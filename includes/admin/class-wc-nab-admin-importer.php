<?php
/**
 * Importer
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin
 * @version     2.2.23.13
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_NAB_Admin_Importer' ) ) :

	require_once WP_PLUGIN_DIR . '/woocommerce-nab-addons/includes/utils/class-wcs-utils.php' ;

/**
 * WC_NAB_Admin_Importer Class
 */
class WC_NAB_Admin_Importer
{
	/**
	 * Constructor
	 *
	 * Monkey patching CSV Orders/Customers Export plugin.
	 *
	 * @since 2.2.12.9
	 */
	public function __construct ( )
	{
		global $wc_customer_order_csv_export ;

		$wc_customer_order_csv_export->admin->tabs[ 'import-previous-gifts' ] = __( 'Import Previous Gifts' , 'notanotherbill' ) ;

		// Classes for the super administrator only
		if ( get_current_user_id( ) == 1 )
		{
			$wc_customer_order_csv_export->admin->tabs[ 'import-subscribers' ] = __( 'Import Subscribers' , 'notanotherbill' ) ;
		}

		add_filter( 'wc_customer_order_csv_export_settings' , array( $this , 'csv_import_settings' ) , 10 , 2 ) ;
		
		add_action( 'woocommerce_admin_field_upload' , array( $this , 'output_fields' ) ) ;
		add_action( 'admin_init' , array( $this , 'process_import' ) ) ;
	}

	/**
	 * Add import settings
	 *
	 * @since 2.2.23.10
	 */
	public function csv_import_settings ( $settings , $tab_id )
	{
		if ( array_key_exists( 'tab' , $_GET ) && $tab_id == 'export' )
		{
			switch ( $_GET[ 'tab' ] )
			{
				case 'import-subscribers' :

					$settings = array(
						'import' => array(
							'name' => __( 'Import Subscribers' , 'notanotherbill' ) ,
							'type' => 'title'
						) ,
						array(
							'id' => 'wc_csv_import_upload' ,
							'name' => __( 'Upload CSV File' , 'notanotherbill' ) ,
							'type' => 'upload'
						) ,
						array( 'type' => 'sectionend' )
					) ;

					break ;

				case 'import-previous-gifts' :

					$options = array( '' => '-' ) ;
					$first_of_month = date( 'Y-m-01' , time( ) ) ;

					for ( $i = 0 ; $i < 60 ; $i++ )
					{
						$key = strtotime( '-' . $i . ' months' , strtotime( $first_of_month ) ) ;
						$options[ $key ] = date( 'M, Y' , $key ) ;
					}

					$settings = array(
						'import' => array(
							'name' => __( 'Import Previous Gifts' , 'notanotherbill' ) ,
							'type' => 'title'
						) ,
						array(
							'id' => 'wc_csv_import_upload' ,
							'name' => __( 'Upload CSV File' , 'notanotherbill' ) ,
							'type' => 'upload'
						) ,
						array(
							'id' => 'wc_csv_import_mailing_date' ,
							'name' => __( 'Mailing Date' , 'notanotherbill' ) ,
							'desc' => __( 'Optional date when gift was mailed to subscriber. If a date is not selected, make sure the date is present in the CSV file. Dates in the CSV file override this option.' ) ,
							'type' => 'select' ,
							'options' => $options
						) ,
						array( 'type' => 'sectionend' )
					) ;

					break ;
			}
		}

		return $settings ;
	}

	/**
	 * Process import
	 *
	 * @since 2.2.10.15
	 */
	public function process_import ( )
	{
		if ( ! isset( $_POST[ 'wc_customer_order_csv_export_bulk_export' ] ) ) return ;

		switch ( $_GET[ 'tab' ] )
		{
			case 'import-subscribers' :
				$this->process_import_subscribers( ) ;
				break ;

			case 'import-previous-gifts' :
				$this->process_import_previous_gifts( ) ;
				break ;
		}
	}

	/**
	 * Process import subscribers
	 *
	 * @since 2.2.10.15
	 */
	private function process_import_subscribers ( )
	{
		$file = array_key_exists( 'wc_csv_import_upload' , $_FILES ) ? $_FILES[ 'wc_csv_import_upload' ] : null ;
		$filename = $file[ 'tmp_name' ] ;

		$data = WCS_Utils::csv_to_array( $filename ) ;
		
		if ( sizeof( $data ) <= 1 )
		{
			echo '<div class="error"><p>Error! There doesn\'t appear to be any data in this CSV file.</p></div>' ;

			return ;
		}

		$factory = new WC_Product_Factory( ) ;
		$error_message = null ;

		global $wpdb ;

		while ( $row = array_pop( $data ) )
		{
			if ( array_key_exists( 'customer_email' , $row ) && $row[ 'customer_email' ] != '' )
			{
				// Create new customer or retrieve existing customer
				$customer = wc_create_new_customer( $row[ 'customer_email' ] , '' , wp_generate_password( ) ) ;
			}
			else
			{
				continue ;
			}

			// Check errors
			if ( is_wp_error( $customer ) )
			{
				$error = $customer->get_error_message( ) ;

				switch ( $error )
				{
					case 'An account is already registered with your email address. Please login.' :
						$user = get_user_by( 'email' , $row[ 'customer_email' ] ) ;
						if ( $user ) $customer = $user->ID ;
						else continue ;
						break ;

					case 'Please provide a valid email address.' :
						if ( ! $error_message ) $error_message = 'The following subscribers were not imported because a valid email address was not provided:\n\n' ;
						if ( array_key_exists( 'customer_first_name' , $row ) && $row[ 'customer_first_name' ] != '' )
						{
							$error_message .= '- ' . $row[ 'customer_first_name' ] ;

							if ( array_key_exists( 'customer_last_name' , $row ) && $row[ 'customer_last_name' ] != '' )
							{
								$error_message .= ' ' .$row[ 'customer_last_name' ] . '\n' ;
							}
						}
						else
						{
							$error_message .= '- Unknown' ;
						}
						continue ;
				}
			}

			// Update first name
			if ( array_key_exists( 'customer_first_name' , $row ) && $row[ 'customer_first_name' ] != '' )
			{
				wp_update_user(
					array(
						'ID' => $customer ,
						'display_name' => $row[ 'customer_first_name' ] ,
						'user_nicename' => $row[ 'customer_first_name' ]
					)
				) ;

				update_user_meta( $customer , 'first_name' , $row[ 'customer_first_name' ] , '' ) ;
				update_user_meta( $customer , 'nickname' , $row[ 'customer_first_name' ] , '' ) ;
			}

			// Update last name
			if ( array_key_exists( 'customer_last_name' , $row ) && $row[ 'customer_last_name' ] != '' )
			{
				update_user_meta( $customer , 'last_name' , $row[ 'customer_last_name' ] , '' ) ;
			}

			try
			{
				// Start transaction if available
				$wpdb->query( 'START TRANSACTION' ) ;

				// Create order
				$order = wc_create_order(
					array(
						'status' => 'pending' ,
						'customer_id' => $customer
					)
				) ;

				if ( is_wp_error( $order ) )
				{
					$wpdb->query( 'ROLLBACK' ) ;
					$error_message = 'Uh oh! Something went wrong.' ;
					continue ;
				}

				// Add subscription product
				$variation = WCS_Utils::get_subscription_variation( $row[ 'payment_interval' ] , $row[ 'recipient_country' ] ) ;
				$post = WCS_Utils::get_subscription_post( ) ;
				$product = $factory->get_product( $post->ID ) ;

				if ( ! $product )
				{
					$wpdb->query( 'ROLLBACK' ) ;
					$error_message = 'Uh oh! Something went wrong.' ;
					continue ;
				}

				if ( $row[ 'payment_interval' ] == 12 )
				{
					$subscription_period = 'year' ;
					$subscription_period_interval = 1 ;
				}
				else
				{
					$subscription_period = 'month' ;
					$subscription_period_interval = $row[ 'payment_interval' ] ;
				}

				$order_data = array( ) ;

				if ( array_key_exists( 'recipient_gender' , $row ) && $row[ 'recipient_gender' ] != '' )
				{
					switch ( $row[ 'recipient_gender' ] )
					{
						case 'male' :
							$order_data[ 'Gender' ] = 'Man' ;
							break ;

						case 'female' :
							$order_data[ 'Gender' ] = 'Woman' ;
							break ;

						case 'couple' :
							$order_data[ 'Gender' ] = 'Couple' ;
							break ;
					}
				}

				if ( array_key_exists( 'recipient_first_name' , $row ) && $row[ 'recipient_first_name' ] != ''  )
				{
					$order_data[ 'Recipient Name 1 - Text' ] = $row[ 'recipient_first_name' ] ;
				}

				if ( array_key_exists( 'recipient_age' , $row ) && $row[ 'recipient_age' ] != ''  )
				{
					$order_data[ 'Decade' ] = $row[ 'recipient_age' ] ;
				}

				if ( array_key_exists( 'recipient_country' , $row ) && $row[ 'recipient_country' ] != ''  )
				{
					$order_data[ 'Shipping Country - Text' ] = WC( )->countries->countries[ $row[ 'recipient_country' ] ] ;
				}

				if ( array_key_exists( 'subscription_additional_information' , $row ) && $row[ 'subscription_additional_information' ] != ''  )
				{
					$order_data[ 'Additional Information - Text' ] = $row[ 'subscription_additional_information' ] ;
				}

				if ( array_key_exists( 'customer_email' , $row ) && $row[ 'customer_email' ] != ''  )
				{
					$order_data[ 'Subscriber Email Address - Text' ] = $row[ 'customer_email' ] ;
				}

				if ( array_key_exists( 'recipient_email' , $row ) && $row[ 'recipient_email' ] != ''  )
				{
					$order_data[ 'Recipient Email Address - Text' ] = $row[ 'recipient_email' ] ;
				}

				if ( array_key_exists( 'subscription_start_date' , $row ) && $row[ 'subscription_start_date' ] != ''  )
				{
					$order_data[ 'Start Date - Date' ] = $row[ 'subscription_start_date' ] ;
				}

				if ( array_key_exists( 'order_id' , $row ) && $row[ 'order_id' ] != ''  )
				{
					$order_data[ 'Legacy Order ID' ] = $row[ 'order_id' ] ;
				}

				$item_id = $order->add_product( $product , 1 ,
					array(
						'variation' => array_merge( $order_data , 
							array(
								'_subscription_period' => $subscription_period ,
								'_subscription_interval' => $subscription_period_interval ,
								'_subscription_length' => $subscription_period_interval ,
								'_subscription_trial_length' => 0 ,
								'_subscription_trial_period' => 'day' ,
								'_subscription_recurring_amount' => $row[ 'payment_amount' ] ,
								'_subscription_sign_up_fee' => 0 ,
								'_recurring_line_total' => $row[ 'payment_amount' ] ,
								'_recurring_line_tax' => 0 ,
								'_recurring_line_subtotal' => $row[ 'payment_amount' ] ,
								'_recurring_line_subtotal_tax' => 0
							)
						) ,
						'totals' => array(
							'subtotal' => $row[ 'payment_amount' ] ,
							'total' => $row[ 'payment_amount' ] ,
						)
					)
				) ;

				if ( array_key_exists( 'subscription_preferences' , $row ) && $row[ 'subscription_preferences' ] != '' )
				{
					$subscription_preferences = explode( '|' , $row[ 'subscription_preferences' ] ) ;
					$suitable_presents = WCS_Utils::get_addon( $post->ID , 'Suitable Presents' ) ;

					if ( isset( $suitable_presents ) && array_key_exists( 'options' , $suitable_presents ) )
					{
						foreach ( $subscription_preferences as $subscription_preference )
						{
							$suitable_present = array_filter( $suitable_presents[ 'options' ] ,
								function ( $e ) use ( $subscription_preference )
								{
									if ( isset( $e ) && array_key_exists( 'label' , $e ) )
									{
										return sanitize_title( $e[ 'label' ] ) == $subscription_preference ;
									}
								}
							) ;

							$suitable_present = array_shift( $suitable_present ) ;

							if ( isset( $suitable_present ) && array_key_exists( 'label' , $suitable_present ) )
							{
								wc_add_order_item_meta( $item_id , 'Suitable Presents' , $suitable_present[ 'label' ] ) ;
							}
						}
					}
				}

				if ( array_key_exists( 'subscription_previous_gifts' , $row ) && $row[ 'subscription_previous_gifts' ] != '' )
				{
					wc_add_order_item_meta( $item_id , '_subscription_previous_gifts' , explode( '|' , $row[ 'subscription_previous_gifts' ] ) ) ;
				}

				if ( ! $item_id )
				{
					$wpdb->query( 'ROLLBACK' ) ;
					$error_message = 'Uh oh! Something went wrong.' ;
					continue ;
				}

				// Billing address
				$billing_address = array( ) ;
				$billing_address[ 'first_name' ] = isset( $row[ 'customer_first_name' ] ) ? $row[ 'customer_first_name' ] : '' ;
				$billing_address[ 'last_name' ] = isset( $row[ 'customer_last_name' ] ) ? $row[ 'customer_last_name' ] : '' ;
				$billing_address[ 'company' ] = '' ;
				$billing_address[ 'address_1' ] = isset( $row[ 'customer_address_1' ] ) ? $row[ 'customer_address_1' ] : '' ;
				$billing_address[ 'address_2' ] = isset( $row[ 'customer_address_2' ] ) ? $row[ 'customer_address_2' ] : '' ;
				$billing_address[ 'city' ] = isset( $row[ 'customer_city' ] ) ? $row[ 'customer_city' ] : '' ;
				$billing_address[ 'state' ] = isset( $row[ 'customer_state' ] ) ? $row[ 'customer_state' ] : '' ;
				$billing_address[ 'postcode' ] = isset( $row[ 'customer_postcode' ] ) ? $row[ 'customer_postcode' ] : '' ;
				$billing_address[ 'country' ] = isset( $row[ 'customer_country' ] ) ? $row[ 'customer_country' ] : '' ;
				$billing_address[ 'phone' ] = '' ;

				// Shipping address.
				$shipping_address = array( ) ;
				$shipping_address[ 'first_name' ] = isset( $row[ 'recipient_first_name' ] ) ? $row[ 'recipient_first_name' ] : '' ;
				$shipping_address[ 'last_name' ] = isset( $row[ 'recipient_last_name' ] ) ? $row[ 'recipient_last_name' ] : '' ;
				$shipping_address[ 'company' ] = '' ;
				$shipping_address[ 'address_1' ] = isset( $row[ 'recipient_address_1' ] ) ? $row[ 'recipient_address_1' ] : '' ;
				$shipping_address[ 'address_2' ] = isset( $row[ 'recipient_address_2' ] ) ? $row[ 'recipient_address_2' ] : '' ;
				$shipping_address[ 'city' ] = isset( $row[ 'recipient_city' ] ) ? $row[ 'recipient_city' ] : '' ;
				$shipping_address[ 'state' ] = isset( $row[ 'recipient_state' ] ) ? $row[ 'recipient_state' ] : '' ;
				$shipping_address[ 'postcode' ] = isset( $row[ 'recipient_postcode' ] ) ? $row[ 'recipient_postcode' ] : '' ;
				$shipping_address[ 'country' ] = isset( $row[ 'recipient_country' ] ) ? $row[ 'recipient_country' ] : '' ;
				$shipping_address[ 'notes' ] = '' ;
				
				// Set order meta
				$order->set_address( $billing_address , 'billing' ) ;
				$order->set_address( $shipping_address , 'shipping' ) ;

				// Set payment gateway
				switch ( $row[ 'payment_gateway' ] )
				{
					case 'paypal' :
						require_once WP_PLUGIN_DIR . '/woocommerce/includes/gateways/paypal/class-wc-gateway-paypal.php' ;
						$payment_method = new WC_Gateway_Paypal( ) ;
						break ;

					case 'gocardless' :
						require_once WP_PLUGIN_DIR . '/woocommerce-gateway-gocardless/classes/class-wc-gateway-gocardless.php' ;
						$payment_method = new WC_Gateway_GoCardless( ) ;
						break ;
				}

				if ( isset( $payment_method ) )
				{
					// Set payment method
					$order->set_payment_method( $payment_method ) ;

					// Set recurring payment method if necessary
					if ( $subscription_period == 'month' && $subscription_period_interval == 1 )
					{
						WC_Subscriptions_Order::set_recurring_payment_method( $order->id ) ;

						add_post_meta( $order->id , '_order_recurring_discount_cart' , 0 ) ;
						add_post_meta( $order->id , '_order_recurring_discount_total' , 0 ) ;
						add_post_meta( $order->id , '_order_recurring_shipping_tax_total' , 0 ) ;
						add_post_meta( $order->id , '_order_recurring_shipping_total' , 0 ) ;
						add_post_meta( $order->id , '_order_recurring_tax_total' , 0 ) ;
						add_post_meta( $order->id , '_order_recurring_total' , $row[ 'payment_amount' ] ) ;
					}

					// Clear payment method
					unset( $payment_method ) ;
				}

				// Set order price
				$order->set_total( $row[ 'payment_amount' ] ) ;

				// Complete payment with first transaction ID
				$order->payment_complete( $row[ 'payment_transaction_id' ] ) ;

				// Update order status
				$order->update_status( 'completed' ) ;

				// Get start date
				$start_date = $row[ 'subscription_start_date' ] != '' ? $row[ 'subscription_start_date' ] : $row[ 'payment_first_date' ] ;
				$start_date = strtotime( $start_date ) ;
				$start_date_str = date( 'Y-m-d H:i:s' , $start_date ) ;

				// Update order meta post commit
				wp_update_post(
					array(
						'ID' => $order->id ,
						'post_date' => $start_date_str ,
						'post_date_gmt' => $start_date_str
					)
				) ;

				// Update last payment date
				if ( array_key_exists( 'payment_last_date' , $row ) && $row[ 'payment_last_date' ] != '' )
				{
					$payment_last_date = $row[ 'payment_last_date' ] ;
					$payment_last_date = strtotime( $payment_last_date ) ;
					$payment_last_date_str = date( 'Y-m-d H:i:s' , $payment_last_date ) ;

					wp_update_post(
						array(
							'ID' => $order->id ,
							'post_modified' => $payment_last_date_str ,
							'post_modified_gmt' => $payment_last_date_str
						)
					) ;

					wc_update_order_item_meta( $item_id , '_subscription_completed_payments' , array( $payment_last_date_str ) ) ;
				}

				// Update order payment information
				switch ( $row[ 'payment_gateway' ] )
				{
					case 'paypal' :
						add_post_meta( $order->id , 'PayPal Subscriber ID' , $row[ 'payment_subscriber_id' ] ) ;
						add_post_meta( $order->id , 'PayPal Transaction ID' , $row[ 'payment_transaction_id' ] ) ;
						add_post_meta( $order->id , 'Payer PayPal first name' , $row[ 'customer_first_name' ] ) ;
						add_post_meta( $order->id , 'Payer PayPal last name' , $row[ 'customer_last_name' ] ) ;
						add_post_meta( $order->id , 'PayPal Payment type' , 'instant' ) ;
						add_post_meta( $order->id , 'Payer PayPal address' , $row[ 'customer_email' ] ) ;
						break ;

					case 'gocardless' :
						add_post_meta( $order->id , '_gocardless_id' , $row[ 'payment_transaction_id' ] ) ;
						add_post_meta( $order->id , '_gocardless_type' , 'subscription' ) ;
						break ;
				}

				// Activate subscriptions
				WC_Subscriptions_Manager::create_pending_subscription_for_order( $order->id , $post->ID ) ;

				// Update start and expiry date
				if ( $subscription_period == 'year' || $subscription_period_interval > 1 )
				{
					$expiry_date = strtotime( '+' . $subscription_period_interval . ' ' . $subscription_period , $start_date ) ;
					$expiry_date_str = date( 'Y-m-d H:i:s' , $expiry_date ) ;
				}
				else
				{
					$expiry_date_str = '0' ;
				}
				
				// Update start and expiry dates
				wc_update_order_item_meta( $item_id , '_subscription_start_date' , $start_date_str ) ;
				wc_update_order_item_meta( $item_id , '_subscription_expiry_date' , $expiry_date_str ) ;

				// Activate subscription
				WC_Subscriptions_Manager::activate_subscriptions_for_order( $order ) ;

				// Update completed payments
				if ( isset( $payment_last_date_str ) )
				{
					// Set next payment
					if ( $subscription_period == 'month' && $subscription_period_interval == 1 )
					{
						$subscription_key = WC_Subscriptions_Manager::get_subscription_key( $order->id , $post->ID ) ;

						if ( isset( $subscription_key ) )
						{
							// Calculate next payment date (+1 month from last payment)
							$next_payment = strtotime( '+' . $subscription_period_interval . ' ' . $subscription_period , strtotime( $payment_last_date_str ) ) ;

							// Set next expected payment
							WC_Subscriptions_Manager::set_next_payment_date( $subscription_key , $customer , $next_payment ) ;

							// Put subscription on hold if no payment expected
							if ( $next_payment < gmdate( 'U' ) )
							{
								WC_Subscriptions_Manager::put_subscription_on_hold( $customer , $subscription_key ) ;
							}
						}
					}

					unset( $payment_last_date_str ) ;
				}

				// Finally, commit changes
				$wpdb->query( 'COMMIT' ) ;
			}
			catch ( Exception $e )
			{
				$wpdb->query( 'ROLLBACK' ) ;
				$error_message = 'Uh oh! Something went wrong.' ;
			}

			unset( $row ) ;
		}

		if ( $error_message ) echo '<script>alert( "' . $error_message . '" ) ;</script>' ;
	}

	/**
	 * Process import previous gifts
	 *
	 * @since 2.2.23.13
	 */
	private function process_import_previous_gifts ( )
	{
		// CSV data
		$file = array_key_exists( 'wc_csv_import_upload' , $_FILES ) ? $_FILES[ 'wc_csv_import_upload' ] : null ;
		$filename = $file[ 'tmp_name' ] ;

		$data = WCS_Utils::csv_to_array( $filename ) ;
		$updated_ids = array( ) ;
		$not_updated_ids = array( ) ;
		
		if ( sizeof( $data ) <= 0 )
		{
			echo '<div class="error"><p>Error! There doesn\'t appear to be any data in this CSV file.</p></div>' ;

			return ;
		}

		// Mailing date
		if ( array_key_exists( 'wc_csv_import_mailing_date' , $_POST ) )
		{
			$mailing_date = $_POST[ 'wc_csv_import_mailing_date' ] ;
		}

		// Loop through data
		while ( $row = array_pop( $data ) )
		{
			// Check previous gift id exists
			if ( array_key_exists( 'previous_gift_id' , $row ) )
			{
				$previous_gift_id = $row[ 'previous_gift_id' ] ;

				if ( get_post_type( $previous_gift_id ) != 'product' )
				{
					$id_error = true ;
				}
			}
			else
			{
				$id_error = true ;
			}

			if ( isset( $id_error ) && $id_error == true )
			{
				echo '<div class="error"><p>Invalid previous_gift_id: ' . $previous_gift_id . '</p></div>' ;

				unset( $id_error ) ;

				continue ;
			}


			// Check previous gift date exists
			if ( array_key_exists( 'previous_gift_date' , $row ) && ! empty( $row[ 'previous_gift_date' ] ) )
			{
				$previous_gift_date = $row[ 'previous_gift_date' ] ;

				// Create date timestamp
				$previous_gift_timestamp = strtotime( $previous_gift_date ) ;

				if ( ! ( bool ) $previous_gift_timestamp )
				{
					$date_error = true ;
				}
				else
				{
					$parts = explode( '-' , $previous_gift_date ) ;

					if ( ! is_array( $parts ) || ! sizeof( $parts ) || ! checkdate( $parts[ 1 ] , $parts[ 2 ] , $parts[ 0 ] ) )
					{
						$date_error = true ;
					}
				}
			}
			elseif ( isset( $mailing_date ) )
			{
				$previous_gift_timestamp = $mailing_date ;
			}
			else
			{
				$date_error = true ;
			}
			
			if ( isset( $date_error ) && $date_error == true )
			{
				echo '<div class="error"><p>Invalid previous_gift_date: ' . $previous_gift_date . '</p></div>' ;

				unset( $date_error ) ;

				continue ;
			}

			// Check order ID exists
			if ( array_key_exists( 'order_id' , $row ) && $row[ 'order_id' ] != '' )
			{
				$order_id = $row[ 'order_id' ] ;

				if ( WC_Subscriptions_Order::order_contains_subscription( $order_id ) )
				{
					$subscriptions = WC_Subscriptions_Order::get_recurring_items( $order_id ) ;
				}
				else
				{
					echo '<div class="error"><p>Order #' . $order_id . ' does not contain a subscription.</p></div>' ;
				}
			}
			else if ( array_key_exists( 'subscriber_email_address' , $row ) && is_email( $row[ 'subscriber_email_address' ] ) )
			{
				// Get subscriber
				$subscriber = get_user_by( 'email' , $row[ 'subscriber_email_address' ] ) ;

				// Get subscribers subscriptions
				$subscriptions = WC_Subscriptions_Manager::get_users_subscriptions( $subscriber->ID ) ;
			}

			// Check if subscriptions exist
			if ( ! $subscriptions ) continue ;

			// Loop through subscriptions
			foreach ( $subscriptions as $subscription )
			{
				if ( ! isset( $order_id ) ) $order_id = $subscription[ 'order_id' ] ;

				// Get order item id
				$subscription_key = WC_Subscriptions_Manager::get_subscription_key( $order_id , $subscription[ 'product_id' ] ) ;
				$order_item_id = WC_Subscriptions_Order::get_item_id_by_subscription_key( $subscription_key ) ;

				// Get order item previous gifts
				$previous_gifts = wc_get_order_item_meta( $order_item_id , '_subscription_previous_gifts' ) ;

				// Maybe create a new array
				if ( ! is_array( $previous_gifts ) ) $previous_gifts = array( ) ;

				// Check for existing value
				if ( in_array( $previous_gift_id , $previous_gifts ) )
				{
					// Remove current value
					unset( $previous_gifts[ array_search( $previous_gift_id , $previous_gifts ) ] ) ;
				}

				// Add new value
				$previous_gifts[ $previous_gift_timestamp ] = $previous_gift_id ;

				// Finally, update order item meta
				$updated = wc_update_order_item_meta( $order_item_id , '_subscription_previous_gifts' , $previous_gifts ) ;

				if ( ! in_array( $order_id , $updated_ids ) )
				{
					if ( $updated )
					{
						$updated_ids[ ] = $order_id ;
					}
					else if ( ! in_array( $order_id , $not_updated_ids ) )
					{
						$not_updated_ids[ ] = $order_id ;
					}
				}
			}

			// Unset order ID
			unset( $order_id ) ;
		}

		// Show number of orders updated
		echo '<div class="updated"><p>' . sizeof( $updated_ids ) . ' orders were updated.</p></div>' ;

		// Show orders not updated
		if ( sizeof( $not_updated_ids ) )
		{
			echo '<div class="error"><p>' . sizeof( $not_updated_ids ) . ' orders were not updated: <ul>' ;
			
			foreach ( $not_updated_ids as $not_updated_id )
			{
				echo '<a href="' . get_edit_post_link( $not_updated_id ) . '" title="Order #' . $not_updated_id . '">#' . $not_updated_id . '</a> ' ;
			}
			
			echo '</p></div>' ;
		}
	}

	/**
	 * Output import fields
	 *
	 * @since 2.2.8.17
	 */
	public function output_fields ( $value )
	{
		switch ( $value[ 'type' ] ) :
			case 'upload' : ?>

				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="<?php echo esc_attr( $value[ 'id' ] ) ; ?>"><?php echo esc_html( $value[ 'title' ] ) ; ?></label>
					</th>
					<td class="forminp forminp-<?php echo sanitize_title( $value['type'] ) ?>">
						<input type="file" id="<?php echo esc_attr( $value[ 'id' ] ) ; ?>" name="<?php echo esc_attr( $value[ 'id' ] ) ; ?>" />
					</td>
				</tr>

		<?php 
			break ; 
		endswitch ;
	}
}

endif ;

return new WC_NAB_Admin_Importer( ) ;