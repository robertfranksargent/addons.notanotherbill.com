<?php
/**
 * Post Types Admin
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin
 * @version     2.2.19.6
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_NAB_Admin_Post_Types' ) ) :

/**
 * WC_NAB_Admin_Post_Types Class
 */
class WC_NAB_Admin_Post_Types
{
	private $factory ;

	/**
	 * Constructor
	 * 
	 * @since 2.2.19.3
	 */
	public function __construct ( )
	{
		// add_action( 'init' , array( $this , 'register_post_types' ) , 3 ) ;
		
		add_action( 'admin_init' , array( $this , 'include_post_type_handlers' ) , 10 ) ;
		add_action( 'manage_product_posts_custom_column', array( $this , 'add_product_column_rating' ) , 10 , 2 ) ;
		add_action( 'admin_footer' , array( $this , 'bulk_admin_footer' ) , 11 ) ;
		add_action( 'load-edit.php' , array( $this , 'bulk_action' ) ) ;
		add_action( 'admin_notices' , array( $this , 'bulk_admin_notices' ) ) ;

		add_filter( 'manage_edit-product_columns' , array( $this , 'edit_product_column_rating' ) , 15 ) ;
		add_filter( 'views_edit-shop_order', array( $this , 'edit_view_send_today' ) , 10 , 1 ) ;
		add_filter( 'views_edit-shop_order', array( $this , 'edit_view_subscription_orders' ) , 10 , 1 ) ;
		add_filter( 'views_edit-shop_order', array( $this , 'edit_view_subscription_renewal_orders' ) , 10 , 1 ) ;
		add_filter( 'views_edit-shop_order', array( $this , 'edit_view_non_subscription_orders' ) , 10 , 1 ) ;
		add_filter( 'parse_query' , array( $this , 'edit_parse_query' ) , 10 , 1 ) ;
	}

	/**
	 * Add "Send Today" filter to orders table
	 *
	 * @since 2.2.14.4
	 */
	public function edit_view_send_today ( $views )
	{
		global $post_type_object ;

		$post_type = $post_type_object->name ;

		// Get orders to be sent today
		$orders = new WP_Query(
			array(
				'post_type' => $post_type ,
				'post_status' => 'wc-processing' ,
				'meta_query' => array( 
					array(
						'key' => '_dispatch_today' ,
						'value' => 1 ,
						'compare' => '=='
				 	) ,
				 	array(
				 		'key' => '_recurring_payment_method' ,
				 		'compare' => 'NOT EXISTS'
				 	) ,
				 	array(
				 		'key' => '_original_order' ,
				 		'compare' => 'NOT EXISTS'
				 	) ,
				 	'compare' => 'AND'
				) 
			)
		) ;

		$orders->get_posts( ) ;

		$views[ 'send_today' ] = '<a href="edit.php?post_type=' . $post_type . '&send_today=1" ' . ( array_key_exists( 'send_today' , $_GET ) && $_GET[ 'send_today' ] ? 'class="current">' : '>' ) . __( 'Send Today' , 'notanotherbill' ) . ' <span class="count">(' . $orders->found_posts . ')</span></a>' ;

		wp_reset_postdata( ) ;

		return $views ;
	}

	/**
	 * Add "Subscription Orders" filter to orders table
	 *
	 * @since 2.2.15.10
	 */
	public function edit_view_subscription_orders ( $views )
	{
		global $post_type_object ;

		$post_type = $post_type_object->name ;

		// Get subscription orders
		$orders = new WP_Query(
			array(
				'post_type' => $post_type ,
				'post_status' => 'any' ,
				'meta_query' => array( 
					array(
						'key' => '_recurring_payment_method' ,
						'compare' => 'EXISTS'
				 	)
				)
			)
		) ;

		$orders->get_posts( ) ;

		$views[ 'subscription_orders' ] = '<a href="edit.php?post_type=' . $post_type . '&subscription_orders=1" ' . ( array_key_exists( 'subscription_orders' , $_GET ) && $_GET[ 'subscription_orders' ] ? 'class="current">' : '>' ) . __( 'Subscription Orders' , 'notanotherbill' ) . ' <span class="count">(' . $orders->found_posts . ')</span></a>' ;

		wp_reset_postdata( ) ;

		return $views ;
	}

	/**
	 * Add "Subscription Renewal Orders" filter to orders table
	 *
	 * @since 2.2.15.10
	 */
	public function edit_view_subscription_renewal_orders ( $views )
	{
		global $post_type_object ;

		$post_type = $post_type_object->name ;

		// Get subscription renewal orders
		$orders = new WP_Query(
			array(
				'post_type' => $post_type ,
				'post_status' => 'any' ,
				'meta_query' => array( 
					array(
						'key' => '_original_order' ,
						'compare' => 'EXISTS'
				 	)
				)
			)
		) ;

		$orders->get_posts( ) ;

		$views[ 'subscription_renewal_orders' ] = '<a href="edit.php?post_type=' . $post_type . '&subscription_renewal_orders=1" ' . ( array_key_exists( 'subscription_renewal_orders' , $_GET ) && $_GET[ 'subscription_renewal_orders' ] ? 'class="current">' : '>' ) . __( 'Subscription Renewal Orders' , 'notanotherbill' ) . ' <span class="count">(' . $orders->found_posts . ')</span></a>' ;

		wp_reset_postdata( ) ;

		return $views ;
	}	

	/**
	 * Add "Non-Subscription Orders" filter to orders table
	 *
	 * @since 2.2.19.6
	 */
	public function edit_view_non_subscription_orders ( $views )
	{
		global $post_type_object ;

		$post_type = $post_type_object->name ;

		// Get non-subscription orders
		$orders = new WP_Query(
			array(
				'post_type' => $post_type ,
				'post_status' => 'any' ,
				'meta_query' => array(
					array(
						'key' => '_order_contains_shop_item' ,
						'compare' => '==' ,
						'value' => 1
					)
				) 
			)
		) ;

		$orders->get_posts( ) ;

		$views[ 'non_subscription_orders' ] = '<a href="edit.php?post_type=' . $post_type . '&non_subscription_orders=1" ' . ( array_key_exists( 'non_subscription_orders' , $_GET ) && $_GET[ 'non_subscription_orders' ] ? 'class="current">' : '>' ) . __( 'Shop Orders' , 'notanotherbill' ) . ' <span class="count">(' . $orders->found_posts . ')</span></a>' ;

		wp_reset_postdata( ) ;
	
		return $views ;
	}

	/**
	 * Filter the orders in admin based on options
	 *
	 * @param mixed $query
	 * @since 2.2.19.6
	 */
	public function edit_parse_query ( $query )
	{
		global $typenow , $wp_query ;

		if ( is_admin( ) && $query == $wp_query && $typenow == 'shop_order' )
		{
			if ( array_key_exists( 'send_today' , $_GET ) && $_GET[ 'send_today' ] )
			{
				$query->query_vars[ 'post_status' ] = 'wc-processing' ;
				$query->query_vars[ 'meta_query' ] = array(
					array(
						'key' => '_dispatch_today' ,
						'value'   => 1 ,
						'compare' => '=='
					) ,
				 	array(
				 		'key' => '_recurring_payment_method' ,
				 		'compare' => 'NOT EXISTS'
				 	) ,
				 	array(
				 		'key' => '_original_order' ,
				 		'compare' => 'NOT EXISTS'
				 	) ,
				 	'compare' => 'AND'
				) ;
			}
			else if ( array_key_exists( 'subscription_orders' , $_GET ) && $_GET[ 'subscription_orders' ] )
			{
				$query->query_vars[ 'post_status' ] = 'any' ;
				$query->query_vars[ 'meta_query' ] = array(
					array(
						'key' => '_recurring_payment_method' ,
						'compare' => 'EXISTS'
					)
				) ;
			}
			else if ( array_key_exists( 'subscription_renewal_orders' , $_GET ) && $_GET[ 'subscription_renewal_orders' ] )
			{
				$query->query_vars[ 'post_status' ] = 'any' ;
				$query->query_vars[ 'meta_query' ] = array(
					array(
						'key' => '_original_order' ,
						'compare' => 'EXISTS'
					)
				) ;
			}
			else if ( array_key_exists( 'non_subscription_orders' , $_GET ) && $_GET[ 'non_subscription_orders' ] )
			{
				$query->query_vars[ 'post_status' ] = 'any' ;
				$query->query_vars[ 'meta_query' ] = array(
					array(
						'key' => '_order_contains_shop_item' ,
						'compare' => '==' ,
						'value' => 1
					)
				) ;
			}
		}
	}

	/**
	 * Register post types
	 */
	public function register_post_types ( )
	{
		if ( post_type_exists( 'gift_allocation' ) ) return ;

		register_post_type( 'gift_allocation' ,
			array(
				'labels' => array(
					'name'					=> __( 'Gift Allocation' , 'notanotherbill' ) ,
					'singular_name' 		=> __( 'Gift Allocation' , 'notanotherbill' ) ,
					'add_new' 				=> __( 'New Allocation' , 'notanotherbill' ) ,
					'add_new_item' 			=> __( 'New Allocation' , 'notanotherbill' ) ,
					'edit' 					=> __( 'Edit' , 'notanotherbill' ) ,
					'edit_item' 			=> __( 'Edit Allocation' , 'notanotherbill' ) ,
					'new_item' 				=> __( 'New Allocation' , 'notanotherbill' ) ,
					'view' 					=> __( 'View Allocation' , 'notanotherbill' ) ,
					'view_item' 			=> __( 'View Allocation' , 'notanotherbill' ) ,
					'search_items'			=> __( 'Search Allocations' , 'notanotherbill' ) ,
					'not_found' 			=> __( 'No Allocations found' , 'notanotherbill' ) ,
					'not_found_in_trash' 	=> __( 'No Allocations found in trash' , 'notanotherbill' ) ,
					'parent' 				=> __( 'Parent Allocations' , 'notanotherbill' ) ,
					'menu_name' 			=> _x( 'Gift Allocation' , 'Admin menu name' , 'notanotherbill' )
				) ,
				'description' 			=> __( 'This is where gift allocations are stored.' , 'notanotherbill' ) ,
				'menu_icon'				=> 'dashicons-networking' ,
				'public' 				=> false ,
				'show_ui' 				=> true ,
				'capability_type' 		=> 'shop_order' ,
				'map_meta_cap' 			=> true ,
				'publicly_queryable' 	=> false ,
				'exclude_from_search'	=> true ,
				'show_in_menu' 			=> current_user_can( 'manage_woocommerce' ) ? 'woocommerce' : true ,
				'hierarchical' 			=> false ,
				'rewrite' 				=> false ,
				'query_var'			 	=> false ,
				'supports' 				=> false ,
				'show_in_nav_menus' 	=> false ,
				'show_in_admin_bar'		=> true
			)
		) ;
	}

	/**
	 * Conditonally load classes and functions only needed when viewing a post type.
	 */
	public function include_post_type_handlers ( )
	{
		include( 'post-types/class-wc-nab-admin-meta-boxes.php' ) ;
	}

	/**
	 * Edit product rating column
	 */
	public function edit_product_column_rating ( $columns )
	{
		$columns[ 'rating' ] = __( 'Rating' ) ;

		return $columns ; 
	}

	/**
	 * Add product rating column
	 */
	public function add_product_column_rating ( $column , $product_id )
	{
		if ( $column == 'rating' )
		{
			if ( ! isset( $factory ) ) $factory = new WC_Product_Factory( ) ;

			$product = $factory->get_product( $product_id ) ;
			$rating_count = $product->get_rating_count( ) ;

			if ( $rating_count )
			{
				wp_star_rating( array( 'rating' => $product->get_average_rating( ) , 'number' => $rating_count ) ) ;
			}
			else
			{
				echo '&ndash;' ;
			}
		}
	}

	/**
	 * Add extra bulk action option to mark orders as trashed.
	 *
	 * Using Javascript until WordPress core fixes: http://core.trac.wordpress.org/ticket/16031
	 *
	 * @access public
	 * @return void
	 * @since 2.2.19.3
	 */
	public function bulk_admin_footer ( )
	{
		global $post_type ;

		if ( 'shop_order' == $post_type )
		{
			?>
			<script type="text/javascript">
			jQuery(
				function( $ )
				{
					$( '<option>' ).val( 'mark_trashed' ).text( '<?php _e( 'Mark trashed' , 'notanotherbill' )?>' ).appendTo( 'select[name="action"]' ) ;
					$( '<option>' ).val( 'mark_trashed' ).text( '<?php _e( 'Mark trashed' , 'notanotherbill' )?>' ).appendTo( 'select[name="action2"]' ) ;

					$	( '#doaction, #doaction2' ).click(
						function ( e )
						{
							return $( this ).prev( 'select' ).find( ':selected' ).val( ) == 'mark_trashed' ? confirm( '<?php _e( 'Are you sure? Trashing the order(s) in this way will NOT trash any subscriptions associated with it.' , 'notanotherbill' ) ; ?>' ) : true ;
						}
					) ;
				}
			) ;
			</script>
			<?php
		}
	}

	/**
	 * Process the new bulk actions for changing order status.
	 *
	 * @access public
	 * @return void
	 * @since 2.2.19.3
	 */
	public function bulk_action ( )
	{
		$wp_list_table = _get_list_table( 'WP_Posts_List_Table' ) ;
		$action = $wp_list_table->current_action( ) ;

		if ( $action != 'mark_trashed' ) return ;

		$changed = 0 ;
		$post_ids = array_map( 'absint' , ( array ) $_REQUEST[ 'post' ] ) ;

		foreach ( $post_ids as $post_id )
		{
			wp_update_post( 
				array(
					'ID' => $post_id ,
					'post_status' => 'trash'
				)
			) ;

			$changed++ ;
		}

		$sendback = add_query_arg(
			array(
				'post_type' => 'shop_order' ,
				'marked_trashed' => true ,
				'changed' => $changed ,
				'ids' => join( ',' , $post_ids )
			) , '' 
		) ;

		wp_safe_redirect( $sendback ) ;
		
		exit ;
	}

	/**
	 * Show confirmation message that order status changed for number of orders
	 *
	 * @access public
	 * @return void
	 * @since 2.2.19.3
	 */
	public function bulk_admin_notices ( )
	{
		global $post_type , $pagenow ;

		if ( isset( $_REQUEST[ 'marked_trashed' ] ) )
		{
			$number = isset( $_REQUEST[ 'changed' ] ) ? absint( $_REQUEST[ 'changed' ] ) : 0 ;

			if ( 'edit.php' == $pagenow && 'shop_order' == $post_type )
			{
				$message = sprintf( _n( 'Order trashed.' , '%s orders trashed.' , $number , 'notanotherbill' ) , number_format_i18n( $number ) ) ;
				echo '<div class="updated"><p>' . $message . '</p></div>' ;
			}
		}
	}
}

endif ;

return new WC_NAB_Admin_Post_Types( ) ;