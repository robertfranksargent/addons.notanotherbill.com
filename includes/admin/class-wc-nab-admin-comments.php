<?php
/**
 * Comments Admin
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin
 * @version     2.2.20.7
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_NAB_Admin_Comments' ) ) :

/**
 * WC_NAB_Admin_Comments Class
 */
class WC_NAB_Admin_Comments
{
	/**
	 * Constructor
	 *
	 * @since 2.2.19.7
	 */
	public function __construct ( )
	{
		add_filter( 'manage_edit-comments_columns' , array( $this , 'edit_comments_columns' ) , 10 , 1 ) ;
		add_filter( 'manage_comments_custom_column' , array( $this , 'edit_comments_content' ) , 10 , 2 ) ;

		add_action( 'pre_get_comments' , array( $this , 'pre_get_comments' ) , 11 , 1 ) ;
	}

	/**
	 * Add "Order" column to comments table.
	 *
	 * @since 2.2.20.3
	 */
	public function edit_comments_columns ( $columns )
	{
		if ( ! isset( $_GET[ 'comment_type' ] ) || $_GET[ 'comment_type' ] != 'order_note' )
		{
			$date_column_title = $columns[ 'date' ] ;

			// Remove author and date columns
			unset( $columns[ 'author' ] ) ;
			unset( $columns[ 'date' ] ) ;

			// Add order column
			$columns[ 'order' ] = __( 'Order' , 'woocommerce' ) ;

			// Edit response column title
			if ( isset( $_GET[ 'comment_type' ] ) && $_GET[ 'comment_type' ] == 'comment' )
			{
				$columns[ 'response' ] = __( 'Product' , 'woocommerce' ) ;
			}

			// Add customer column
			$customer_column = array( 'customer' => __( 'Customer' , 'woocommerce' ) ) ;
			$columns = array_slice( $columns , 0 , 1 , true ) + $customer_column + array_slice( $columns , 1 , count( $columns ) , true ) ;

			// Add rating column
			$rating_column = array( 'rating' => __( 'Rating' , 'woocommerce' ) ) ;
			$columns = array_slice( $columns , 0 , 3 , true ) + $rating_column + array_slice( $columns , 3 , count( $columns ) - 2 , true ) ;

			// Re-add date column to end of array
			$columns[ 'date' ] = $date_column_title ;
		}

		return $columns ;
	}

	/**
	 * Edit comments content
	 *
	 * @since 2.2.20.7
	 */
	public function edit_comments_content ( $column_name , $comment_id )
	{
		if ( ( ! isset( $_GET[ 'comment_type' ] ) || $_GET[ 'comment_type' ] != 'order_note' ) )
		{
			$comment = get_comment( $comment_id ) ;

			switch ( $column_name )
			{
				case 'customer' :

					$customer_id = $comment->user_id ;

					if ( $customer_id )
					{
						$customer = get_userdata( $customer_id ) ;

						echo sprintf( '<p><a href="mailto:%s"><strong>%s %s</strong></a></p>' , $customer->user_email , $customer->first_name , $customer->last_name ) ;
					
						$actions = array(
							__( 'View Profile' , 'woocommerce' ) => admin_url( 'user-edit.php?user_id=' . $customer_id ) ,
							__( 'View Comments' , 'woocommerce' ) => admin_url( 'edit-comments.php?user_id=' . $customer_id )  ,
							__( 'View Orders' , 'woocommerce' ) => admin_url( 'edit.php?post_type=shop_order&_customer_user=' . $customer_id ) ,
							__( 'View Subscriptions' , 'woocommerce' ) => admin_url( 'admin.php?page=subscriptions&_customer_user=' . $customer_id )
						) ;

						$print_actions = array( ) ;

						echo '<div class="actions">' ;

						foreach ( $actions as $title => $url )
						{
							$print_actions[ ] = sprintf( '<a href="%s">%s</a>' , $url , $title ) ;
						}

						echo '<p>' . implode( ' | ' , $print_actions ) . '</p>' ;

						echo '</div>' ;
					}
					else
					{
						echo '<strong>' . get_comment_author( $comment_id ) . '</strong>' ; 
					}

					break ;

				case 'rating' :
					
					$rating = get_comment_meta( $comment_id , 'rating' , true ) ;
					
					if ( $rating ) wp_star_rating( array( 'rating' => $rating ) ) ;
					else echo '-' ;

					break ;

				case 'order' :
				
					$order_id = get_comment_meta( $comment_id , 'order' , true ) ;

					if ( ! empty( $order_id ) )
					{
						$order = new WC_Order( $order_id ) ;

						echo sprintf( '<p><a href="%s"><strong>%s – %s</strong></a></p>' , get_edit_post_link( $order_id ) , __( 'Order' , 'woocommerce' ) , date( 'F j, Y @ h:i A' , strtotime( $order->order_date ) ) ) ;
						echo sprintf( '<p><a href="%s">%s</a></p>' , get_edit_post_link( $order_id ) , __( 'View Order' , 'woocommerce' ) ) ;
					}
					else
					{
						echo '-' ;
					}

				break ;
			}
		}
	}

	/**
	 * Filter comments by author.
	 *
	 * @param object $query
	 * @since 2.2.20.7
	 */
	public function pre_get_comments ( $query )
	{
		// Conditions
		if ( is_ajax( ) || ! is_admin( ) || ( get_current_screen( ) && get_current_screen( )->id != 'edit-comments' ) || ! isset( $_GET[ 'a' ] ) || ( isset( $_GET[ 'comment_type' ] ) && $_GET[ 'comment_type' ] == 'order_note' ) ) return ;

		$query->query_vars[ 'user_id' ] = $_GET[ 'a' ] ;
	}
}

endif ;

return new WC_NAB_Admin_Comments( ) ;