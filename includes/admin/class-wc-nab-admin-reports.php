<?php
/**
 * Reports Admin
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin
 * @version     2.2.20
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_NAB_Admin_Reports' ) ) :

/**
 * WC_NAB_Admin_Comments Class
 */
class WC_NAB_Admin_Reports
{
	/**
	 * Constructor
	 *
	 * @since 2.2.20
	 */
	public function __construct ( )
	{
		add_filter( 'woocommerce_admin_reports' , array( $this , 'admin_reports' ) , 10 , 1 ) ;
	}

	/**
	* Add reviews tab to admin reports.
	*
	* @since 2.2.20
	*/
	public function admin_reports ( $reports )
	{
		$reports[ 'reviews' ] = array(
			'title'  => __( 'Reviews' , 'notanotherbill' ) ,
			'reports' => array(
				'most_reviews' => array(
					'title' => __( 'Most Reviews' , 'notanotherbill' ) ,
					'description' => '' ,
					'hide_title' => true ,
					'callback' => array( __CLASS__ , 'get_report' )
				) ,
				'highest_rating' => array(
					'title' => __( 'Highest Rating' , 'notanotherbill' ) ,
					'description' => '' ,
					'hide_title' => true ,
					'callback' => array( __CLASS__ , 'get_report' )
				) ,
				'lowest_rating' => array(
					'title' => __( 'Lowest Rating' , 'notanotherbill' ) ,
					'description' => '' ,
					'hide_title' => true ,
					'callback' => array( __CLASS__ , 'get_report' )
				) ,
				'subscriber_satisfaction' => array(
					'title' => __( 'Subscriber Satisfaction' , 'notanotherbill' ) ,
					'description' => '' ,
					'hide_title' => true ,
					'callback' => array( __CLASS__ , 'get_report' )
				)
			)
		) ;

		return $reports ;
	}

	/**
	 * Get a report from our reports subfolder
	 *
	 * @since 2.2.20
	 */
	public static function get_report ( $name )
	{
		$name = sanitize_title( str_replace( '_' , '-' , $name ) ) ;
		$class = 'WC_Report_' . str_replace( '-' , '_' , $name ) ;

		include_once( apply_filters( 'wc_admin_reports_path' , 'reports/class-wc-nab-report-' . $name . '.php' , $name , $class ) ) ;

		if ( ! class_exists( $class ) ) return ;

		$report = new $class( ) ;
		$report->output_report( ) ;
	}
}

endif ;

return new WC_NAB_Admin_Reports( ) ;