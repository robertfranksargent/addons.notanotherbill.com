<?php

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_Report_Reviews' ) ) require_once( 'class-wc-nab-report-reviews.php' ) ;

/**
 * WC_Report_Subscriber_Satisfaction
 *
 * @author      Robert Sargent
 * @category    Admin
 * @package     WooCommerce/Admin/Reports
 * @version     2.2.20.6
 */
class WC_Report_Subscriber_Satisfaction extends WC_Report_Reviews
{
	/**
	* 2.2.20.6
	*/
	protected $row_user_data ;

	/**
	 * get_columns function.
	 *
	 * @since 2.2.20.4
	 */
	public function get_columns ( )
	{
		$columns = array(
			'subscriber' => __( 'Subscriber' , 'woocommerce' ) ,
			'response' => __( 'Reviews' , 'woocommerce' ) ,
			'average_rating' => __( 'Average Rating' , 'woocommerce' ) ,
			'most_liked_product'  => __( 'Most Liked Product' , 'woocommerce' ) ,
			'least_liked_product' => __( 'Least Liked Product' , 'woocommerce' ) ,
			'satisfaction' => __( 'Satisfaction Score' , 'woocommerce' )
		) ;

		return $columns ;
	}

	/**
	 * @since 2.2.20.6
	 */
	public function get_sortable_columns ( )
	{
		$columns = array(
			'subscriber' => array( 'display_name' , false ) ,
			'average_rating' => array( 'average_rating' , false ) ,
			'satisfaction' => array( 'satisfaction' , false )
		) ;

		return $columns ;
	}

	/**
	 * column_default function.
	 *
	 * @param mixed $item
	 * @param mixed $column_name
	 * @since 2.2.20.6
	 */
	public function column_default ( $user , $column_name )
	{
		switch ( $column_name )
		{
			case 'subscriber' :
				echo sprintf( '<p><a href="%s"><strong>%s %s</strong></p>' , admin_url( 'user-edit.php?user_id=' . $user->ID ) , $this->row_user_data->first_name , $this->row_user_data->last_name ) ;

				$actions = array(
					__( 'View Profile' , 'woocommerce' ) => admin_url( 'user-edit.php?user_id=' . $user->ID ) ,
					__( 'View Comments' , 'woocommerce' ) => admin_url( 'edit-comments.php?user_id=' . $user->ID )  ,
					__( 'View Orders' , 'woocommerce' ) => admin_url( 'edit.php?post_type=shop_order&_customer_user=' . $user->ID ) ,
					__( 'View Subscriptions' , 'woocommerce' ) => admin_url( 'admin.php?page=subscriptions&_customer_user=' . $user->ID )
				) ;

				$print_actions = array( ) ;

				echo '<div class="actions">' ;

				foreach ( $actions as $title => $url )
				{
					$print_actions[ ] = sprintf( '<a href="%s">%s</a>' , $url , $title ) ;
				}

				echo '<p>' . implode( ' | ' , $print_actions ) . '</p>' ;

				echo '</div>' ;

				break ;

			case 'response' :
				$comments_count = count( $this->row_comments ) ;
				$comments_link = add_query_arg( 'user_id' , $user->ID , admin_url( 'edit-comments.php' ) ) ;
				?>

				<span class="post-com-count-wrapper">
					<a class="post-com-count post-com-count-approved" href="<?php echo $comments_link ; ?>">
						<span class="comment-count-approved" aria-hidden="true"><?php echo $comments_count ; ?></span>
						<span class="screen-reader-text"><?php echo _n( '%d comment' , '%d comments' , $comments_count ) ; ?></span>
					</a>
				</span>

				<?php
				break ;

			case 'average_rating' :
				wp_star_rating( array( 'rating' => $user->average_rating , 'number' => $user->ratings_count ) ) ;
				break ;

			case 'most_liked_product' :
			case 'least_liked_product' :
				$ratings = array_column( $this->row_ratings , 'comment_post_ID' ) ;

				if ( $column_name == 'most_liked_product' ) $product_id = array_pop( $ratings ) ;
				else $product_id = array_shift( $ratings ) ;
				
				if ( $product_id && $product = new WC_Product( $product_id ) )
				{
				?>
					<p><a href="<?php echo admin_url( 'post.php?post=' . $product->id . '&action=edit' ) ; ?>"><?php echo $product->get_title( ) ; ?></a></p>
					<?php

					// Get variation data
					if ( $product->is_type( 'variation' ) )
					{
						$list_attributes = array( ) ;
						$attributes = $product->get_variation_attributes( ) ;

						foreach ( $attributes as $name => $attribute )
						{
							$list_attributes[ ] = wc_attribute_label( str_replace( 'attribute_' , '' , $name ) ) . ': <strong>' . $attribute . '</strong>' ;
						}

						echo '<div class="description">' . implode( ', ' , $list_attributes ) . '</div>' ;
					}	
				}
				break ;

			case 'satisfaction' :
				echo '<p>' . $user->satisfaction . '</p>' ;
				break ;
		}
	}

	/**
	 * No items found text
	 *
	 * @since 2.2.20.4
	 */
	public function no_items ( )
	{
		_e( 'No subscribers found.' , 'woocommerce' ) ;
	}

	/**
	 * @since 2.2.20.6
	 */
	public function get_items ( $current_page , $per_page )
	{
		global $wpdb ;

		$this->max_items = 0 ;
		$this->items = array( ) ;

		$start = $per_page * ( $current_page - 1 ) ;
		$end = $start + $per_page ;

		$orderby = isset( $_GET[ 'orderby' ] ) ? $_GET[ 'orderby' ] : 'satisfaction' ;
		$order = isset( $_GET[ 'order' ] ) ? $_GET[ 'order' ] : 'DESC' ;

		$subscribers = $wpdb->get_results( "
			SELECT SQL_CALC_FOUND_ROWS $wpdb->users.*,
			AVG( $wpdb->commentmeta.meta_value ) as average_rating,
			COUNT( $wpdb->commentmeta.meta_value ) as ratings_count,
			AVG( $wpdb->commentmeta.meta_value ) * COUNT( $wpdb->commentmeta.meta_value ) as satisfaction
			FROM $wpdb->users
				INNER JOIN $wpdb->usermeta ON ( $wpdb->users.ID = $wpdb->usermeta.user_id )
				LEFT OUTER JOIN $wpdb->comments ON ( $wpdb->users.ID = $wpdb->comments.user_id )
				LEFT JOIN $wpdb->commentmeta ON ( $wpdb->comments.comment_ID = $wpdb->commentmeta.comment_id )
			WHERE 1=1
				AND ( $wpdb->usermeta.meta_key = 'wp_capabilities' AND CAST( $wpdb->usermeta.meta_value AS CHAR ) LIKE '%\"subscriber\"%' )
				AND $wpdb->commentmeta.meta_key = 'rating'
			GROUP BY $wpdb->users.ID
			ORDER BY $orderby $order
			LIMIT $start, $end
		" ) ;

		$this->items = $subscribers ;

		$subscribers = $wpdb->get_results( "
			SELECT SQL_CALC_FOUND_ROWS $wpdb->users.*
			FROM $wpdb->users
				INNER JOIN $wpdb->usermeta ON ( $wpdb->users.ID = $wpdb->usermeta.user_id )
				LEFT OUTER JOIN $wpdb->comments ON ( $wpdb->users.ID = $wpdb->comments.user_id )
				LEFT JOIN $wpdb->commentmeta ON ( $wpdb->comments.comment_ID = $wpdb->commentmeta.comment_id )
			WHERE 1=1
				AND ( $wpdb->usermeta.meta_key = 'wp_capabilities' AND CAST( $wpdb->usermeta.meta_value AS CHAR ) LIKE '%\"subscriber\"%' )
				AND $wpdb->commentmeta.meta_key = 'rating'
			GROUP BY $wpdb->users.ID
			LIMIT 0, 99999999
		" ) ;

		$this->max_items = count( $subscribers ) ;
	}

	/**
	* 2.2.20.6
	*/
	protected function row_default ( $user )
	{
		$this->row_user_data = get_userdata( $user->ID ) ;
		$this->row_comments = get_comments( array( 'author__in' => $user->ID ) ) ;
		$this->row_ratings = $this->get_user_ratings( $user->ID ) ;

		usort( $this->row_ratings , function ( $a , $b ) { return strcmp( $a[ 'meta_value' ] , $b[ 'meta_value' ] ) ; } ) ;
	}
}