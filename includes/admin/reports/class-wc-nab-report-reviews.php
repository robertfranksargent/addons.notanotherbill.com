<?php

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WP_List_Table' ) ) require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ) ;

/**
 * WC_Report_Reviews
 *
 * @author      Robert Sargent
 * @category    Admin
 * @package     WooCommerce/Admin/Reports
 * @version     2.2.20.7
 */
class WC_Report_Reviews extends WP_List_Table
{
	/**
	* @since 2.2.20
	*/
	protected $max_items ;

	/**
	* @since 2.2.20.6
	*/
	protected $row_product ;
	protected $row_comments ;
	protected $row_rating_count ;
	protected $row_ratings ;

	/**
	 * __construct function.
	 *
	 * @access public
	 * @since 2.2.20
	 */
	public function __construct ( )
	{
		parent::__construct(
			array(
				'singular' => __( 'Most Reviews' , 'notanotherbill' ) ,
				'plural' => __( 'Most Reviews' , 'notanotherbill' ) ,
				'ajax' => false
			) 
		) ;

		add_filter( 'posts_where' , array( $this , 'filter_comment_count' ) ) ;

		add_action( 'init' , array( __CLASS__ , 'recount_comments' ) ) ;
	}

	/**
	* Recount product comments
	*
	* @since 2.2.20
	*/
	public static function recount_comments ( )
	{
		global $wpdb ;

		$products = $wpdb->get_results( "SELECT * FROM wp_posts WHERE post_type IN ('product', 'product_variation')" ) ;

		foreach ( $products as $product )
		{
			$product_id = $product->ID ;
			$comment_count = $wpdb->get_var( "SELECT COUNT(*) AS comment_cnt FROM wp_comments WHERE comment_post_ID = '$product_id' AND comment_approved = '1' AND comment_type = ''" ) ;
			$wpdb->query( "UPDATE wp_posts SET comment_count = '$comment_count' WHERE ID = '$product_id'" ) ;
		}
	}

	/**
	* Filter by comment count
	* 
	* @since 2.2.20
	*/
	public function filter_comment_count ( $sql )
	{
		global $wpdb ;

		$sql .= $wpdb->prepare( ' AND wp_posts.comment_count != %d ' , 0 ) ;

		return $sql ;
	}

	/**
	 * No items found text
	 *
	 * @since 2.2.20
	 */
	public function no_items ( )
	{
		_e( 'No product reviews found.' , 'notanotherbill' ) ;
	}

	/**
	 * Don't need this
	 *
	 * @since 2.2.20
	 */
	public function display_tablenav ( $position )
	{
		if ( $position != 'top' )
		{
			parent::display_tablenav( $position ) ;
		}
	}

	/**
	 * Output the report
	 *
	 * @since 2.2.20
	 */
	public function output_report ( )
	{
		$this->prepare_items( ) ;
		echo '<div id="poststuff" class="woocommerce-reports-wide">' ;
		$this->display( ) ;
		echo '</div>' ;
	}

	/**
	 * column_default function.
	 *
	 * @param mixed $item
	 * @param mixed $column_name
	 * @since 2.2.20.6
	 */
	public function column_default ( $item , $column_name )
	{
		switch ( $column_name )
		{
			case 'product' :
				?>
				<a href="<?php echo admin_url( 'post.php?post=' . $this->row_product->id . '&action=edit' ) ; ?>"><?php echo $this->row_product->get_title( ) ; ?></a>
				<?php

				// Get variation data
				if ( $this->row_product->is_type( 'variation' ) )
				{
					$list_attributes = array( ) ;
					$attributes = $this->row_product->get_variation_attributes( ) ;

					foreach ( $attributes as $name => $attribute )
					{
						$list_attributes[ ] = wc_attribute_label( str_replace( 'attribute_' , '' , $name ) ) . ': <strong>' . $attribute . '</strong>' ;
					}

					echo '<div class="description">' . implode( ', ' , $list_attributes ) . '</div>' ;
				}
			break ;

			case 'response' :
				$comments_count = count( $this->row_comments ) ;
				$comments_link = add_query_arg( 'p' , $this->row_product->id , admin_url( 'edit-comments.php' ) ) ;
				?>
				<span class="post-com-count-wrapper">
					<a class="post-com-count post-com-count-approved" href="<?php echo $comments_link ; ?>">
						<span class="comment-count-approved" aria-hidden="true"><?php echo $comments_count ; ?></span>
						<span class="screen-reader-text"><?php echo _n( '%d comment' , '%d comments' , $comments_count ) ; ?></span>
					</a>
				</span>
				<?php
				break ;

			case 'highest_rating' :
				if ( $this->row_rating_count ) wp_star_rating( array( 'rating' => $this->get_highest_rating( $this->row_product , $this->row_ratings , $this->row_rating_count ) , 'number' => $this->row_rating_count ) ) ;
				else _e( 'No ratings yet.' , 'woocommerce' ) ;
				break ;

			case 'lowest_rating' :
				if ( $this->row_rating_count ) wp_star_rating( array( 'rating' => $this->get_lowest_rating( $this->row_product , $this->row_ratings , $this->row_rating_count ) , 'number' => $this->row_rating_count ) ) ;
				else _e( 'No ratings yet.' , 'woocommerce' ) ;
				break ;

			case 'average_rating' :
				if ( $this->row_rating_count ) wp_star_rating( array( 'rating' => $this->get_average_rating( $this->row_product , $this->row_ratings , $this->row_rating_count ) , 'number' => $this->row_rating_count ) ) ;
				else _e( 'No ratings yet.' , 'woocommerce' ) ;
				break ;
		}
	}

	/**
	 * get_columns function.
	 *
	 * @since 2.2.20
	 */
	public function get_columns ( )
	{
		$columns = array(
			'product' => __( 'Product' , 'woocommerce' ) ,
			'response' => __( 'Reviews' , 'woocommerce' ) ,
			'highest_rating' => __( 'Highest Rating' , 'woocommerce' ) ,
			'lowest_rating' => __( 'Lowest Rating' , 'woocommerce' ) ,
			'average_rating' => __( 'Average Rating' , 'woocommerce' )
		) ;

		return $columns ;
	}

	/**
	 * prepare_items function.
	 *
	 * @since 2.2.20
	 */
	public function prepare_items ( )
	{
		$this->_column_headers = array( $this->get_columns( ) , array( ) , $this->get_sortable_columns( ) ) ;

		$current_page = absint( $this->get_pagenum( ) ) ;
		$per_page = apply_filters( 'woocommerce_admin_reviews_report_products_per_page' , 20 ) ;
		
		$this->get_items( $current_page , $per_page ) ;
		$this->set_pagination_args(
			array(
				'total_items' => $this->max_items ,
				'per_page' => $per_page ,
				'total_pages' => ceil( $this->max_items / $per_page )
			)
		) ;
	}

	/**
	* 2.2.20.6
	*/
	protected function single_row_columns ( $item )
	{
		$this->row_default( $item ) ;

		parent::single_row_columns( $item ) ;
	}

	/**
	* 2.2.20.6
	*/
	protected function row_default ( $item )
	{
		global $product ;

		if ( ! $product || $product->id !== $item->ID )
		{
			$this->row_product = wc_get_product( $item->ID ) ;
		}
		else
		{
			$this->row_product = $product ;
		}

		$this->row_comments = get_comments( array( 'post_id' => $this->row_product->id ) ) ;
		$this->row_rating_count = $this->row_product->get_rating_count( ) ;
		$this->row_ratings = $this->get_product_ratings( $this->row_product->id ) ;
	}
	
	/**
	 * @since 2.2.20.7
	 */
	protected function get_average_rating ( $product , $ratings , $count )
	{
		$average_rating = '' ;
		
		if ( $count > 0 )
		{
			$average_rating = number_format( array_sum( $ratings ) / $count , 2 ) ;

		}

		return $average_rating ;
	}

	/**
	 * @since 2.2.20.7
	 */
	protected function get_highest_rating ( $product , $ratings , $count )
	{
		$highest_rating = '' ;

		if ( $count > 0 )
		{
			$highest_rating = number_format( max( $ratings ) , 2 ) ;
		}

		return $highest_rating ;
	}

	/**
	 * @since 2.2.20.7
	 */
	protected function get_lowest_rating ( $product , $ratings , $count )
	{
		$lowest_rating = '' ;

		if ( $count > 0 )
		{
			$lowest_rating = number_format( min( $ratings ) , 2 ) ;
		}

		return $lowest_rating ;
	}

	/**
	 * @since 2.2.20.4
	 */
	protected function get_product_ratings ( $product_id )
	{
		global $wpdb ;

		$ratings = $wpdb->get_results(
			$wpdb->prepare( "SELECT meta_value FROM $wpdb->commentmeta
				LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
				WHERE meta_key = 'rating'
				AND comment_post_ID = %d
				AND meta_value > 0 " , 
				$product_id
			) , ARRAY_A
		) ;	

		return array_column( $ratings , 'meta_value' ) ;
	}

	/**
	 * @since 2.2.20.4
	 */
	protected function get_user_ratings ( $user_id )
	{
		global $wpdb ;

		$ratings = $wpdb->get_results(
			$wpdb->prepare( "SELECT comment_post_ID,meta_value FROM $wpdb->commentmeta
				LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
				WHERE meta_key = 'rating'
				AND user_id = %d
				AND meta_value > 0 " , 
				$user_id
			) , ARRAY_A
		) ;

		return $ratings ;
	}	
}
