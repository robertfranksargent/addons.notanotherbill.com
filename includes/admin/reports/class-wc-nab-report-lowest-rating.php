<?php

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_Report_Reviews' ) ) require_once( 'class-wc-nab-report-reviews.php' ) ;

/**
 * WC_Report_Lowest_Rating
 *
 * @author      Robert Sargent
 * @category    Admin
 * @package     WooCommerce/Admin/Reports
 * @version     2.2.20
 */
class WC_Report_Lowest_Rating extends WC_Report_Reviews
{
	/**
	 * @since 2.2.20
	 */
	public function __construct ( )
	{
		parent::__construct( ) ;

		add_filter( 'posts_clauses' , array( new WC_Query( ) , 'order_by_rating_post_clauses' ) ) ;
		add_filter( 'posts_clauses' , array( $this , 'order_by_rating_post_clauses' ) ) ;
	}

	/**
	 * @since 2.2.20
	 */
	public function order_by_rating_post_clauses ( $args )
	{
		global $wpdb ;

		$args[ 'orderby' ] = "average_rating ASC, $wpdb->posts.post_date DESC" ;

		return $args ;
	}

	/**
	 * @since 2.2.20
	 */
	public function get_items ( $current_page , $per_page )
	{
		global $wpdb ;

		$this->max_items = 0 ;
		$this->items = array( ) ;

		$args =	array(
			'post_type' => array( 'product' , 'product_variation' ) ,
			'post_status' => 'publish' ,
			'posts_per_page' => $per_page ,
			'paged' => $current_page ,
		) ;

		$query = new WP_Query( $args ) ;

		$this->items = $query->get_posts( ) ;

		$query = new WP_Query(
			array(
				'post_type' => array( 'product' , 'product_variation' ) ,
				'post_status' => 'publish' ,
				'posts_per_page' => -1 ,
			)
		) ;

		$this->max_items = $query->found_posts ;
	}
}