<?php

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_Report_Reviews' ) ) require_once( 'class-wc-nab-report-reviews.php' ) ;

/**
 * WC_Report_Most_Reviews
 *
 * @author      Robert Sargent
 * @category    Admin
 * @package     WooCommerce/Admin/Reports
 * @version     2.2.20.6
 */
class WC_Report_Most_Reviews extends WC_Report_Reviews
{
	/**
	 * @since 2.2.20.6
	 */
	public function get_sortable_columns ( )
	{
		$columns = array(
			'product' => array( 'post_title' , false ) ,
			'response' => array( 'comment_count' , false )
		) ;

		return $columns ;
	}
	
	/**
	 * @since 2.2.20.6
	 */
	public function get_items ( $current_page , $per_page )
	{
		$this->max_items = 0 ;
		$this->items = array( ) ;

		$orderby = isset( $_GET[ 'orderby' ] ) ? $_GET[ 'orderby' ] : 'comment_count' ;
		$order = isset( $_GET[ 'order' ] ) ? $_GET[ 'order' ] : 'DESC' ;

		$query = new WP_Query(
			array(
				'post_type' => array( 'product' , 'product_variation' ) ,
				'post_status' => 'publish' ,
				'posts_per_page' => $per_page ,
				'paged' => $current_page ,
				'orderby' => $orderby ,
  				'order' => $order
			)
		) ;

		$this->items = $query->get_posts( ) ;

		$query = new WP_Query(
			array(
				'post_type' => array( 'product' , 'product_variation' ) ,
				'post_status' => 'publish' ,
				'posts_per_page' => -1 ,
			)
		) ;

		$this->max_items = $query->found_posts ;
	}
}