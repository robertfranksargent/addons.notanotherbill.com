<?php

/**
 * Query
 *
 * @class 		WC_NAB_CSV
 * @version		2.2.23.9
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sandiford
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_Admin_CSV
{
	/**
	* @since 2.2.21.17
	*/
	const DEFAULT_LINE_ITEMS = 15 ;

	/**
	* @since 2.2.21.16
	*/
	const MAX_FAVOURITES = 10 ;

	/**
	 * Constructor
	 *
	 * @since 2.2.22.4
	 */
	public function __construct ( )
	{
		add_action( 'admin_enqueue_scripts' , array( $this , 'load_styles_scripts' ) ) ;

		add_filter( 'wc_customer_order_csv_export_settings' , array( $this , 'filter_wc_csv_export_settings' ) , 10 , 2 ) ;
		add_filter( 'wc_customer_order_csv_export_settings' , array( $this , 'filter_wc_csv_export_subscription_settings' ) , 10 , 2 ) ;
		add_filter( 'wc_customer_order_csv_export_admin_query_args' , array( $this , 'filter_wc_customer_order_csv_export_admin_query_args' ) , 10 , 3 ) ;
		add_filter( 'posts_join' , array( $this , 'subscription_export_join' ) , 10 , 2 ) ;
		add_filter( 'posts_where' , array( $this , 'subscription_export_where' ) , 10 , 2 ) ;
		add_filter( 'wc_customer_order_csv_export_order_headers' , array( $this , 'filter_wc_customer_order_csv_export_order_headers' ) , 25 , 2 ) ;
		add_filter( 'wc_customer_order_csv_export_order_line_item' , array( $this , 'filter_replace_item_meta' ) , 21 , 5 ) ;
		add_filter( 'wc_customer_order_csv_export_order_line_item' , array( $this , 'filter_wc_customer_order_csv_export_order_line_item' ) , 25 , 5 ) ;
		add_filter( 'wc_customer_order_csv_export_order_row_one_row_per_item' , array( $this , 'filter_wc_customer_order_csv_export_order_row_one_row_per_item' ) , 25 , 5 ) ;
		add_filter( 'wc_customer_order_csv_export_generated_csv' , array( $this , 'filter_generated_csv' ) , 10 , 2 ) ;
		add_filter( 'wc_customer_order_csv_export_add_order_note' , array( $this , 'dont_add_order_note' ) ) ;
	}

	/**
	 * Filter generated CSV
	 *
	 * @since 2.2.22.6
	 */
	public static function filter_generated_csv ( $csv , $generator )
	{
		if ( empty( $csv ) ) return $csv ;

		if ( array_key_exists( 'nab_wc_customer_order_csv_export_as_packing_list' , $_POST ) )
		{
			$as_packing_list = $_POST[ 'nab_wc_customer_order_csv_export_as_packing_list' ] ;

			if ( $as_packing_list )
			{
				// Convert CSV to array
				$csv_orders = str_getcsv( $csv , "\n" ) ;

				// Extract header
				$header = explode( ',' , array_shift( $csv_orders ) ) ;

				// Combine header and content rows into associative array
				array_walk( $csv_orders ,
					function ( &$row , $key , $header )
					{
						// Convert commas inside quotes to HTML character code
						$inside_quotes = false ;
						
						for ( $i = 0 ; $i < strlen( $row ) ; $i++ )
						{
							if ( $row[ $i ] == '"' ) $inside_quotes = ! $inside_quotes ;

							if ( $inside_quotes && $row[ $i ] == ',' )
							{
								$row[ $i ] = '&#44;' ;
							}
						}

						// Convert string to array
						$row = explode( ',' , $row ) ;

						// Handle mismatched header and order row lengths
						$count_row = count( $row ) ;
						$count_header = count( $header ) ;

						if ( $count_row < $count_header )
						{
							$row = array_merge( $row , array_fill( $count_row , $count_header - $count_row , '' ) ) ;
						}
						else if ( $count_header < $count_row )
						{
							$header = array_merge( $header , array_fill( $count_header , $count_row - $count_header , '' ) ) ;
						}

						$row = array_combine( $header , $row ) ;
					} ,
				$header ) ;

				// Remove empty elements
				$csv_orders = array_values( array_filter( $csv_orders , function ( $val ) { return ! empty( $val ) ; } ) ) ;

				if ( count( $csv_orders ) > 0 )
				{
					// Create product list
					$products = array( ) ;

					// Get item count
					$item_count = self::DEFAULT_LINE_ITEMS ;

					if ( array_key_exists( 'nab_wc_customer_order_csv_export_order_item_columns_count' , $_POST ) )
					{
						$item_count = $_POST[ 'nab_wc_customer_order_csv_export_order_item_columns_count' ] ;
					}

					// Loop through orders
					foreach ( $csv_orders as $csv_order )
					{
						// Loop through items
						for ( $i = 1 ; $i < $item_count + 1 ; $i++ )
						{
							$product_id = $csv_order[ 'item_product_id_' . $i ] ;
							$product_name = $csv_order[ 'item_name_' . $i ] ;
							$product_sku = $csv_order[ 'item_sku_' . $i ] ;
							$product_quantity = $csv_order[ 'item_quantity_' . $i ] ;

							if ( ! empty( $product_id ) )
							{
								if ( ! array_key_exists( $product_id , $products ) )
								{
									$products[ $product_id ] = array(
										'id' => $product_id ,
										'name' => $product_name ,
										'sku' => $product_sku ,
										'quantity' => 0 ,
									) ;
								}

								$products[ $product_id ][ 'quantity' ] += $product_quantity ;
							}
						}
					}

					// Generate CSV string
					$csv = 'id,name,sku,quantity' ;

					foreach ( $products as $product )
					{
						$csv .= "\n" ;
						$csv .= implode( ',' , $product ) ;
					}
				}
			}
		}

		return $csv ;
	}

	/**
	 * Load admin styles & scripts only on needed pages
	 *
	 * @since 2.2.16.1
	 * @param $hook_suffix
	 */
	public function load_styles_scripts ( $hook_suffix )
	{
		if ( $hook_suffix == 'woocommerce_page_wc_customer_order_csv_export' )
		{
			wp_enqueue_script( 'wc-nab-admin-csv' , WC_NAB_Addons( )->plugin_url( ) . '/assets/js/admin/csv.min.js', array( ) , WC_NAB_Addons( )->version , true ) ;
		}
	}

	public function dont_add_order_note ( )
	{
		return false ;
	}

	/**
	 * Add extra settings to a specified tab
	 *
	 * @param array $settings[ $tab_id ]
	 * @param string $tab_id
	 * @since 2.2.22.4
	 */
	public static function filter_wc_csv_export_settings ( $settings_for_tab , $tab_id )
	{
		if ( ! array_key_exists( 'tab' , $_GET ) || $_GET[ 'tab' ] == 'export' )
		{
			$types = array_column( $settings_for_tab , 'type' ) ;
			$section_ends = array_keys( array_filter( $types , function ( $a ) { return $a == 'sectionend' ; } ) ) ;
			$insert = array(
				array(
					'id'			=> 'nab_wc_customer_order_csv_export_order_item_columns_count' ,
					'name'			=> 'Max. order item columns' ,
					'desc'			=> 'The maximum number of order item columns in the CSV. You might want to change this if you think the exported CSV will contain an order with more than ' . self::DEFAULT_LINE_ITEMS . ' items.' ,
					'type'			=> 'text' ,
					'default'		=> self::DEFAULT_LINE_ITEMS
				) ,
				array(
					'id'	=> 'nab_wc_customer_order_csv_export_as_packing_list' ,
					'title' => 'Export as packing list' ,
					'desc'	=> '' ,
					'type'	=> 'checkbox'
				) ,
			) ;

			array_splice( $settings_for_tab , $section_ends[ 1 ] , 0 , $insert ) ;
		}

		return $settings_for_tab ;
	}

	/**
	 * Add extra settings to a specified tab
	 *
	 * @param array $settings[ $tab_id ]
	 * @param string $tab_id
	 * @since 2.2.22.4
	 */
	public static function filter_wc_csv_export_subscription_settings ( $settings_for_tab , $tab_id )
	{
		// Export tab only
		if ( ! array_key_exists( 'tab' , $_GET ) || $_GET[ 'tab' ] == 'export' )
		{
			$additional_settings = self::get_additional_settings( ) ;

			if ( array_key_exists( $tab_id , $additional_settings ) )
			{
				$section_end = array_pop( $settings_for_tab ) ;
				$settings_for_tab = array_merge( $settings_for_tab , $additional_settings[ $tab_id ] ) ;
				$settings_for_tab = array_merge( $settings_for_tab , array( $section_end ) ) ;
			}
		}

		return $settings_for_tab ;
	}

	/**
	 * @since 2.2.22.4
	 */
	private static function get_additional_settings ( )
	{
		require_once WP_PLUGIN_DIR . '/woocommerce-nab-addons/includes/utils/class-wc-utils.php' ;

		return array(
			'export' => array(
				array(
					'id'            => 'nab_wc_customer_order_csv_export_started_subscriptions' ,
					'title'         => '' ,
					'desc'          => 'Only started subscriptions' ,
					'type'          => 'checkbox'
				) ,
				array(
					'id'            => 'nab_wc_customer_order_csv_export_subscription_statuses' ,
					'name'          => 'Subscription Statuses' ,
					'desc_tip' 		=> 'Subscriptions with these subscription statuses will be included in the export.' ,
					'type'          => 'multiselect' ,
					'options'  		=> array(
						'active'    => 'Active' ,
						'on-hold'   => 'On hold' ,
						'pending'   => 'Pending' ,
						'cancelled' => 'Cancelled' ,
						'expired'	=> 'Expired'
					) ,
					'default'  		=> '' ,
					'class'    		=> 'chosen_select show_if_orders' ,
					'css'      		=> 'min-width: 250px'
				) ,				array(
					'id'			=> 'nab_wc_customer_order_csv_export_from_subscription_end_date' ,
					'name'			=> 'Ended on or after date' ,
					'desc'			=> 'Include subscriptions which <strong>ended</strong> on or after this date, in the format <code>YYYY-MM-DD.</code>' ,
					'type'			=> 'text'
				) ,
				array(
					'id'			=> 'nab_wc_customer_order_csv_export_to_subscription_end_date' ,
					'name'			=> 'Ended on or before date' ,
					'desc'			=> 'Include subscriptions with <strong>ended</strong>  on or before this date, in the format <code>YYYY-MM-DD.</code>' ,
					'type'			=> 'text'
				) ,
				array(
					'id'			=> 'nab_wc_customer_order_csv_export_previous_gift_ids' ,
					'name'			=> 'Previous gifts' ,
					'desc'			=> 'Include only subscribers who have previously received the selected product(s).' ,
					'type'			=> 'multiselect' ,
					'options'		=> WC_Utils::get_products_select_options( ) ,
					'default'		=> '' ,
					'class'			=> 'wc-enhanced-select chosen_select' ,
					'css'			=> 'min-width: 250px'
				) ,
				array(
					'type'			=> 'sectionend'
				)
			)
		) ;
	}

	/**
	 * Add arguments to query_args when exporting records
	 *
	 * @param array $query_args
	 * @param mixed $export_type
	 * @param object $WC_Customer_Order_CSV_Export_Admin
	 * @since 2.2.21.11
	 */
	public static function filter_wc_customer_order_csv_export_admin_query_args ( $query_args , $export_type , $WC_Customer_Order_CSV_Export_Admin )
	{
		if ( isset( $_POST[ 'nab_wc_customer_order_csv_export_started_subscriptions' ] ) and $_POST[ 'nab_wc_customer_order_csv_export_started_subscriptions' ] == 1 )
		{
			$query_args = $query_args + array( 'wc_subscriptions_started_subscriptions' => true ) ;
		}

		if ( array_key_exists( 'nab_wc_customer_order_csv_export_subscription_statuses' , $_POST ) and count( $_POST[ 'nab_wc_customer_order_csv_export_subscription_statuses' ] ) > 0 )
		{
			// cancelled subscriptions hack
			if ( in_array( 'active' , $_POST[ 'nab_wc_customer_order_csv_export_subscription_statuses' ] ) and ! in_array( 'cancelled' , $_POST[ 'nab_wc_customer_order_csv_export_subscription_statuses' ] ) )
			{
				$_POST[ 'nab_wc_customer_order_csv_export_subscription_statuses' ][ ] = 'cancelled_but_active' ;
			}

			$subscription_statuses = implode( ' ,' , $_POST[ 'nab_wc_customer_order_csv_export_subscription_statuses' ] ) ;
			$query_args = $query_args + array( 'wc_subscriptions_subscription_status' => $subscription_statuses ) ;
		}

		if ( array_key_exists( 'nab_wc_customer_order_csv_export_from_subscription_end_date' , $_POST ) && $_POST[ 'nab_wc_customer_order_csv_export_from_subscription_end_date' ] != '' )
		{
			$query_args = $query_args + array( 'wc_subscriptions_from_end_date' => $_POST[ 'nab_wc_customer_order_csv_export_from_subscription_end_date' ] ) ;
		}

		if ( array_key_exists( 'nab_wc_customer_order_csv_export_to_subscription_end_date' , $_POST ) && $_POST[ 'nab_wc_customer_order_csv_export_to_subscription_end_date' ] != '' )
		{
			$query_args = $query_args + array( 'wc_subscriptions_to_end_date' => $_POST[ 'nab_wc_customer_order_csv_export_to_subscription_end_date' ] ) ;
		}

		if ( array_key_exists( 'nab_wc_customer_order_csv_export_previous_gift_ids' , $_POST ) && $_POST[ 'nab_wc_customer_order_csv_export_previous_gift_ids' ] )
		{
			$previous_gift_ids = implode( ',' , $_POST[ 'nab_wc_customer_order_csv_export_previous_gift_ids' ] ) ;
			$query_args = $query_args + array( 'wc_subscriptions_previous_gift_ids' => $previous_gift_ids ) ;
		}

		return $query_args ;
	}

	/**
	 * Modify the get posts join clause for subscription statuses
	 *
	 * @param string $join
 	 * @since 2.2.21.11
	 */
	public static function subscription_export_join ( $join , $wp_query )
	{
		global $wpdb ;

		if ( isset( $wp_query->query_vars[ 'wc_subscriptions_started_subscriptions' ] ) and $wp_query->query_vars[ 'wc_subscriptions_started_subscriptions' ] == true )
		{
			// join the item meta
			$join .= " JOIN {$wpdb->prefix}woocommerce_order_items AS items1 on wp_posts.ID = items1.order_id " ;
			$join .= " JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS itemmeta1 ON items1.order_item_id = itemmeta1.order_item_id " ;
		}

		if ( isset( $wp_query->query_vars[ 'wc_subscriptions_subscription_status' ] ) )
		{
			// join the item meta
			$join .= " JOIN {$wpdb->prefix}woocommerce_order_items AS items2 on wp_posts.ID = items2.order_id " ;
			$join .= " JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS itemmeta2 ON items2.order_item_id = itemmeta2.order_item_id " ;
   			
   			// cancelled subscriptions hack
			if ( in_array( 'cancelled_but_active' , explode( ' ,' , $wp_query->query_vars[ 'wc_subscriptions_subscription_status' ] ) ) )
			{
				// $join .= " JOIN {$wpdb->prefix}woocommerce_order_items AS items3 on wp_posts.ID = items3.order_id ";
				// $join .= " JOIN {$wpdb->prefix}woocommerce_order_itemmeta$wpdb->prefix}woocommerce_order_itemmeta AS itemmeta3 ON items3.order_item_id = itemmeta3.order_item_id ";
   			}
		}

		if ( isset( $wp_query->query_vars[ 'wc_subscriptions_from_end_date' ] ) && $wp_query->query_vars[ 'wc_subscriptions_from_end_date' ] != '' )
		{
			$join .= " JOIN {$wpdb->prefix}woocommerce_order_items AS items4 on wp_posts.ID = items4.order_id " ;
			$join .= " JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS itemmeta4 ON items4.order_item_id = itemmeta4.order_item_id " ;
		}

		if ( isset( $wp_query->query_vars[ 'wc_subscriptions_to_end_date' ] ) && $wp_query->query_vars[ 'wc_subscriptions_to_end_date' ] != '' )
		{
			$join .= " JOIN {$wpdb->prefix}woocommerce_order_items AS items5 on wp_posts.ID = items5.order_id " ;
			$join .= " JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS itemmeta5 ON items5.order_item_id = itemmeta5.order_item_id " ;
		}

		if ( isset( $wp_query->query_vars[ 'wc_subscriptions_previous_gift_ids' ] ) && $wp_query->query_vars[ 'wc_subscriptions_previous_gift_ids' ] != '' )
		{
			$join .= " JOIN {$wpdb->prefix}woocommerce_order_items AS items6 on wp_posts.ID = items6.order_id " ;
			$join .= " JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS itemmeta6 ON items6.order_item_id = itemmeta6.order_item_id " ;
		}

		// pre( $join ) ; exit ;

		return $join ;
	}

	/**
	 * Modify the get posts where clause for subscription statuses
	 *
	 * @param string $where
 	 * @since 2.2.21.11
	 */
	public static function subscription_export_where ( $where , $wp_query )
	{
		global $wpdb ;

		if ( isset( $wp_query->query_vars[ 'wc_subscriptions_started_subscriptions' ] ) and $wp_query->query_vars[ 'wc_subscriptions_started_subscriptions' ] == true )
		{	
			// using Start Date - Date as all subscriptions have it , _subscription_start_date may be less accurate
			$where .= " AND itemmeta1.meta_key = 'Start Date - Date' AND itemmeta1.meta_value <= CURRENT_TIMESTAMP() " ;
		}

		if ( isset( $wp_query->query_vars[ 'wc_subscriptions_subscription_status' ] ) )
		{
			$subscription_statuses = $wp_query->query_vars[ 'wc_subscriptions_subscription_status' ] ;
			$subscription_statuses = explode( ' ,' , $subscription_statuses ) ;
			$subscription_statuses = array_map( function( $status ) { return "'{$status}'" ; } , $subscription_statuses ) ;
			$subscription_statuses = implode( ' ,' , $subscription_statuses ) ;

			$where .= " AND ( " ; // encapsulate
			$where .= " ( itemmeta2.meta_key = '_subscription_status' AND itemmeta2.meta_value IN ( {$subscription_statuses} ) ) " ;

			// cancelled subscriptions hack
			if ( in_array( 'cancelled_but_active' , explode( ' ,' , $wp_query->query_vars[ 'wc_subscriptions_subscription_status' ] ) ) )
			{
                $where .= " OR ( 
								( itemmeta2.meta_key = '_subscription_status' AND itemmeta2.meta_value = 'cancelled' ) 
					        AND
					        	( ( SELECT meta_value FROM {$wpdb->prefix}woocommerce_order_itemmeta AS itemmeta3 WHERE itemmeta3.order_item_id = itemmeta2.order_item_id AND itemmeta3.meta_key = '_subscription_expiry_date' ) >= NOW() )
					        ) " ;
   			}

			$where .= " ) " ; // encapsulate
		}

		if ( isset( $wp_query->query_vars[ 'wc_subscriptions_from_end_date' ] ) && $wp_query->query_vars[ 'wc_subscriptions_from_end_date' ] != '' )
		{
			$from_end_date = $wp_query->query_vars[ 'wc_subscriptions_from_end_date' ] ;
			$where .= " AND itemmeta4.meta_key = '_subscription_end_date' AND itemmeta4.meta_value >= '{$from_end_date} 00:00:00'" ;
		}

		if ( isset( $wp_query->query_vars[ 'wc_subscriptions_to_end_date' ] ) && $wp_query->query_vars[ 'wc_subscriptions_to_end_date' ] != '' )
		{
			$to_end_date = $wp_query->query_vars[ 'wc_subscriptions_to_end_date' ] ;
			$where .= " AND itemmeta5.meta_key = '_subscription_end_date' AND itemmeta5.meta_value <= '{$to_end_date} 23:59:59'" ;
		}

		if ( isset( $wp_query->query_vars[ 'wc_subscriptions_previous_gift_ids' ] ) && $wp_query->query_vars[ 'wc_subscriptions_previous_gift_ids' ] != '' )
		{
			$previous_gift_ids = explode( ',' , $wp_query->query_vars[ 'wc_subscriptions_previous_gift_ids' ] ) ;
			$previous_gift_ids_length = count( $previous_gift_ids ) ;
			$i = 0 ;

			$where .= " AND itemmeta6.meta_key = '_subscription_previous_gifts' AND ( " ;

			foreach ( $previous_gift_ids as $previous_gift_id )
			{
				$where .= "itemmeta6.meta_value LIKE '%:\"" . $previous_gift_id . "\";%'" ;

				if ( ++$i < $previous_gift_ids_length ) $where .= ' AND ' ;
			}

			$where .= ')' ;
		}

		// pre( $where ) ; exit ;

		return $where ;
	}

	/**
	 * Add seperate meta fields
	 *
	 * @param array $coloumn_headers
	 * @param object $generator
	 * @since 2.2.21.17
	 */
	public static function filter_wc_customer_order_csv_export_order_headers ( $column_headers , $generator )
	{
		if ( 'default_one_row_per_item' === $generator->order_format )
		{
			$column_headers = array(
				'order_id'            => 'order_id' ,
				'order_date'          => 'date' ,
				'shipping_total'      => 'shipping_total' ,
				'discount_total'      => 'discount_total' ,
				'order_total'         => 'order_total' ,
				'order_subtotal'	  => 'order_subtotal' ,
				'order_currency'      => 'order_currency' ,
				'shipping_method'     => 'shipping_method' ,
				'billing_first_name'  => 'billing_first_name' ,
				'billing_last_name'   => 'billing_last_name' ,
				'billing_company'     => 'billing_company' ,
				'billing_email'       => 'billing_email' ,
				'billing_phone'       => 'billing_phone' ,
				'billing_address_1'   => 'billing_address_1' ,
				'billing_address_2'   => 'billing_address_2' ,
				'billing_postcode'    => 'billing_postcode' ,
				'billing_city'        => 'billing_city' ,
				'billing_state'       => 'billing_state' ,
				'billing_country'     => 'billing_country' ,
				'shipping_first_name' => 'shipping_first_name' ,
				'shipping_last_name'  => 'shipping_last_name' ,
				'shipping_address_1'  => 'shipping_address_1' ,
				'shipping_address_2'  => 'shipping_address_2' ,
				'shipping_postcode'   => 'shipping_postcode' ,
				'shipping_city'       => 'shipping_city' ,
				'shipping_state'      => 'shipping_state' ,
				'shipping_country'    => 'shipping_country' ,
				'shipping_company'    => 'shipping_company' ,
				'customer_note'       => 'customer_note'
			) ;

			$line_item_column_count = self::DEFAULT_LINE_ITEMS ;

			if ( array_key_exists( 'nab_wc_customer_order_csv_export_order_item_columns_count' , $_POST ) )
			{
				$line_item_column_count = $_POST[ 'nab_wc_customer_order_csv_export_order_item_columns_count' ] ;
			}

			// Add line item headers
			for ( $i = 1 ; $i < $line_item_column_count + 1 ; $i++ )
			{
				$column_headers[ 'item_product_id_' . $i ] = 'item_product_id_' . $i ;
				$column_headers[ 'item_name_' . $i ] = 'item_name_' . $i ;
				$column_headers[ 'item_sku_' . $i ] = 'item_sku_' . $i ;
				$column_headers[ 'item_quantity_' . $i ] = 'item_quantity_' . $i ;
				$column_headers[ 'item_total_' . $i ] = 'item_total_' . $i ;
				$column_headers[ 'item_attributes_' . $i ] = 'item_attributes_' . $i ;
			}

			// Add subscription headers
			$column_headers = array_merge(
				$column_headers ,
				array( 
					'subscription_meta_plan'                     => 'subscription_meta_plan' ,
					'subscription_meta_shipping_zone'            => 'subscription_meta_shipping_zone' ,
					'subscription_meta_recipient'                => 'subscription_meta_recipient' ,
					'subscription_meta_gender'                   => 'subscription_meta_gender' ,
					'subscription_meta_recipient_name'           => 'subscription_meta_recipient_name' ,
					'subscription_meta_decade'                   => 'subscription_meta_decade' ,
					'subscription_meta_shipping_country'         => 'subscription_meta_shipping_country' ,
					'subscription_meta_subscriber_email_address' => 'subscription_meta_subscriber_email_address' ,
					'subscription_meta_recipient_email_address'  => 'subscription_meta_recipient_email_address' ,
					'subscription_meta_start_date'               => 'subscription_meta_start_date' ,
					'subscription_meta_additional_information'   => 'subscription_meta_additional_information' ,
					'subscription_meta_personalised_message'     => 'subscription_meta_personalised_message' ,
					'subscription_meta_first_present_name'	     => 'subscription_meta_first_present_name' ,
					'subscription_meta_ladies'                 	 => 'subscription_meta_ladies' ,
					'subscription_meta_for_the_wall'             => 'subscription_meta_for_the_wall' ,
					'subscription_meta_outdoors'               	 => 'subscription_meta_outdoors' ,
					'subscription_meta_apothecary'             	 => 'subscription_meta_apothecary' ,
					'subscription_meta_homewares'              	 => 'subscription_meta_homewares' ,
					'subscription_meta_art'                    	 => 'subscription_meta_art' ,
					'subscription_meta_for_your_desk'          	 => 'subscription_meta_for_your_desk' ,
					'subscription_meta_kitchen'                	 => 'subscription_meta_kitchen' ,
					'subscription_meta_stationery'             	 => 'subscription_meta_stationery' ,
					'subscription_meta_gentlemen'              	 => 'subscription_meta_gentlemen' ,
					'subscription_meta_jewellery'              	 => 'subscription_meta_jewellery' ,
					'subscription_meta_nostalgic'              	 => 'subscription_meta_nostalgic' ,
					'subscription_meta_decorative_functional'	 => 'subscription_meta_decorative_functional' ,
					'subscription_meta_niche_established'	 	 => 'subscription_meta_niche_established' ,
					'subscription_meta_quirky_traditional'	 	 => 'subscription_meta_quirky_traditional' ,
					'subscription_meta_initials'				 => 'subscription_meta_initials' ,
					'subscription_status'                        => 'subscription_status' ,
					'subscription_duration'                      => 'subscription_duration' ,
					'subscription_start'                         => 'subscription_start' ,
					'subscription_end'							 => 'subscription_end' ,
					'subscription_expiry'                        => 'subscription_expiry' ,
					'subscription_completed_payments'            => 'subscription_completed_payments' ,
					'subscription_completed_payment_dates'       => 'subscription_completed_payment_dates' ,
					'subscription_failed_payments'               => 'subscription_failed_payments' ,
					'subscription_failed_payment_dates'          => 'subscription_failed_payment_dates' ,
					'subscription_suspension_count'              => 'subscription_suspension_count' ,
					'subscription_renewals'				 		 => 'subscription_renewals' ,
					'subscription_associated_orders'			 => 'subscription_associated_orders'
				)
			) ;

			// Add line item headers
			for ( $i = 1 ; $i < self::MAX_FAVOURITES + 1 ; $i++ )
			{
				$column_headers[ 'subscription_favourite_id_' . $i ] = 'subscription_favourite_id_' . $i ;
				$column_headers[ 'subscription_favourite_name_' . $i ] = 'subscription_favourite_name_' . $i ;
				$column_headers[ 'subscription_favourite_attributes_' . $i ] = 'subscription_favourite_attributes_' . $i ;
			}
		}

		return $column_headers ;
	}

	/**
	 * Add subscription meta entries to line item.
	 *
	 * @param array $line_item
	 * @param mixed $item
	 * @param mixed $product
	 * @param mixed $order
	 * @param object $generator
	 * @since 2.2.23.14
	 */
	public function filter_wc_customer_order_csv_export_order_line_item ( $line_item , $item , $product , $order , $generator )
	{
		// Get order items
		$order_items = $order->get_items( array( 'line_item' ) ) ;

		// Get shipping packages
		$packages = get_post_meta( $order->id , '_wcms_packages' , true ) ;

		// Logic for single and multiple package orders
		if ( isset( $packages ) && count( $packages ) > 1 )
		{
			$methods = get_post_meta( $order->id  , '_shipping_methods' ) ;

			foreach ( $packages as $package )
			{
				$i = 0 ;

				foreach ( $package[ 'contents' ] as $package_item )
				{
					$is_variation = ! empty( $package_item[ 'variation_id' ] ) ;

					if ( ( $is_variation && $package_item[ 'variation_id' ] == $item[ 'variation_id' ] ) || ( ! $is_variation && $package_item[ 'product_id' ] == $item[ 'product_id' ] ) )
					{
						if ( $i == 0 )
						{
							// Check if exporting subscriptions only
							if ( isset( $_POST[ 'wc_customer_order_csv_export_subscription_orders' ] ) )
							{
								// Package must contain at least one subscription
								if ( ! WC_NAB_Checkout::package_contains_subscription( $package ) ) return false ;
							}
													
							$j = 0 ;

							foreach ( $package[ 'contents' ] as $package_key => $package_item )
							{
								$j++ ;

								$is_variation = ! empty( $package_item[ 'variation_id' ] ) ;
								$variation_id = $is_variation ? $package_item[ 'variation_id' ] : $package_item[ 'product_id' ] ;
								$product = new WC_Product( $package_item[ 'product_id' ] ) ;

								$line_item[ 'item_product_id_' . $j ] = $package_item[ 'product_id' ] ;
								$line_item[ 'item_name_' . $j ] = $product->get_title( ) ;
								$line_item[ 'item_sku_' . $j ] = $product->get_sku( ) ;
								$line_item[ 'item_quantity_' . $j ] = $package_item[ 'quantity' ] ;
								$line_item[ 'item_total_' . $j ] = $package_item[ 'line_total' ] ;

								// Item attributes
								$product_atts = get_post_meta( $package_item[ 'product_id' ] , '_product_attributes' , true ) ;
								$line_item_atts = array( ) ;

								foreach ( $product_atts as $key => $value )
								{
									if ( ( ! $is_variation && is_array( $package_item ) && array_key_exists( $key , $package_item ) ) || ( $is_variation && is_array( $package_item[ 'variation' ] ) && array_key_exists( 'attribute_' . $key , $package_item[ 'variation' ] ) ) )
									{
										$att_slug = $is_variation ? $package_item[ 'variation' ][ 'attribute_' . $key ] : $package_item[ $key ] ;
										$att = wc_get_product_terms( $package_item[ 'product_id' ] , $key , array( 'slug' => $att_slug ) ) ;

										if ( isset( $att[ 0 ] ) )
										{
											$line_item_atts[ ] = wc_attribute_label( $key , $product ) . ': ' . $att[ 0 ] ;
										}
									}
								}

								// Item addons
								$product_addons = get_product_addons( $product->id ) ;

								foreach ( $product_addons as $product_addon )
								{
									$addon_name = $product_addon[ 'name' ] ;

									foreach ( $package_item as $key => $value )
									{
										if ( stripos( $key , $addon_name ) === 0 )
										{
											$line_item_atts[ ] = $addon_name . ': ' . $value ;
										}
									}
								}

								// Destination address
								$line_item[ 'shipping_first_name' ] = $package[ 'full_address' ][ 'first_name' ] ;
								$line_item[ 'shipping_last_name' ] = $package[ 'full_address' ][ 'last_name' ] ;
								$line_item[ 'shipping_address_1' ] = $package[ 'full_address' ][ 'address_1' ] ;
								$line_item[ 'shipping_address_2' ] = $package[ 'full_address' ][ 'address_2' ] ;
								$line_item[ 'shipping_postcode' ] = $package[ 'full_address' ][ 'postcode' ] ;
								$line_item[ 'shipping_city' ] = $package[ 'full_address' ][ 'city' ] ;
								$line_item[ 'shipping_state' ] = $package[ 'full_address' ][ 'state' ] ;
								$line_item[ 'shipping_country' ] = $package[ 'full_address' ][ 'country' ] ;
								$line_item[ 'shipping_company' ] = $package[ 'full_address' ][ 'company' ] ;

								// Serialize attributes
								if ( count( $line_item_atts ) )
								{
									$line_item[ 'item_attributes_' . $j ] = htmlspecialchars_decode( implode( ', '  , $line_item_atts ) ) ;
								}

								// TODO: Set package shipping method
								// $line_item[ 'shipping_method' ] = 

								// Check if the order contains a subscription and add that data
								if ( WC_Subscriptions_Product::is_subscription( $package_item[ 'product_id' ] ) )
								{
									$subscription_item = WCS_Utils::get_order_subscription( $order ) ;

									$line_item[ 'subscription_item_id' ] = $package_key ;
									$line_item[ 'subscription_previous_gifts' ] = isset( $subscription_item[ 'subscription_previous_gifts' ] ) ? $subscription_item[ 'subscription_previous_gifts' ] : '' ;
									$line_item[ 'subscription_meta_recipient' ] = isset( $subscription_item[ 'Recipient' ] ) ? $subscription_item[ 'Recipient' ] : '' ;
									$line_item[ 'subscription_meta_gender' ] = isset( $subscription_item[ 'Gender' ] ) ? $subscription_item[ 'Gender' ] : '' ;
									$line_item[ 'subscription_meta_recipient_name' ] = isset( $subscription_item[ 'Recipient Name 1 - Text' ] ) ? $subscription_item[ 'Recipient Name 1 - Text' ] : '' ;

									if ( isset( $subscription_item[ 'Recipient Name 2 - Text' ] ) )
									{
										$line_item[ 'subscription_meta_recipient_name' ] .= ' & ' . $subscription_item[ 'Recipient Name 2 - Text' ] ;
									}
									
									$line_item[ 'subscription_meta_decade' ] = isset( $subscription_item[ 'Decade' ] ) ? $subscription_item[ 'Decade' ] : '' ;
									$line_item[ 'subscription_meta_shipping_country' ] = isset( $subscription_item[ 'Shipping Country - Text' ] ) ? $subscription_item[ 'Shipping Country - Text' ] : '' ;
									$line_item[ 'subscription_meta_subscriber_email_address' ] = isset( $subscription_item[ 'Subscriber Email Address - Text' ] ) ? $subscription_item[ 'Subscriber Email Address - Text' ] : '' ;
									$line_item[ 'subscription_meta_recipient_email_address' ] = isset( $subscription_item[ 'Recipient Email Address - Text' ] ) ? $subscription_item[ 'Recipient Email Address - Text' ] : '' ;
									$line_item[ 'subscription_meta_start_date' ] = isset( $subscription_item[ 'Start Date - Date' ] ) ? $subscription_item[ 'Start Date - Date' ] : '' ;
									$line_item[ 'subscription_meta_additional_information' ] = isset( $subscription_item[ 'Additional Information - Text' ] ) ? $subscription_item[ 'Additional Information - Text' ] : '' ;
									$line_item[ 'subscription_meta_personalised_message' ] = isset( $subscription_item[ 'Personalised Message - Text' ] ) ? $subscription_item[ 'Personalised Message - Text' ] : '' ;
									$line_item[ 'subscription_meta_first_present_name' ] = isset( $subscription_item[ 'First Present - ID' ] ) ? $subscription_item[ 'First Present - ID' ] : '' ;
									$line_item[ 'subscription_meta_favourites' ] = isset( $subscription_item[ 'Favourites - JSON' ] ) ? $subscription_item[ 'Favourites - JSON' ] : '' ;
									$line_item[ 'subscription_meta_initials' ] = isset( $subscription_item[ 'Initials - Text' ] ) ? $subscription_item[ 'Initials - Text' ] : '' ;
									$line_item[ 'subscription_meta_decorative_functional' ] = isset( $subscription_item[ 'Sliders - Decorative-Functional' ] ) ? $subscription_item[ 'Sliders - Decorative-Functional' ] : '' ;
									$line_item[ 'subscription_meta_niche_established' ] = isset( $subscription_item[ 'Sliders - Niche-Established' ] ) ? $subscription_item[ 'Sliders - Niche-Established' ] : '' ;
									$line_item[ 'subscription_meta_quirky_traditional' ] = isset( $subscription_item[ 'Sliders - Quirky-Traditional' ] ) ? $subscription_item[ 'Sliders - Quirky-Traditional' ] : '' ;

									if ( isset( $subscription_item[ 'item_meta' ] ) )
									{
										if ( isset( $subscription_item[ 'item_meta' ][ 'Suitable Presents' ] ) )
											$line_item[ 'subscription_meta' ][ 'Suitable Presents' ] = $subscription_item[ 'item_meta' ][ 'Suitable Presents' ] ;
										
										if ( isset( $subscription_item[ 'item_meta' ][ 'Unsuitable Presents' ] ) )
											$line_item[ 'subscription_meta' ][ 'Unsuitable Presents' ] = $subscription_item[ 'item_meta' ][ 'Unsuitable Presents' ] ;				

										$line_item = $this->set_subscription_item_meta( $line_item  , $subscription_item[ 'item_meta' ] ) ;
									}
								}
							}

							break ;
						}
						else
						{
							return false ;
						}
					}

					$i++ ;
				}
			}
		}
		else
		{
			// Find index of line item
			foreach ( $order_items as $order_item )
			{
				$i = isset( $i ) ? $i + 1 : 0 ;

				if ( $order_item == $item ) 
				{
					if ( $i == 0 )
					{
						foreach ( $order_items as $order_item_id => $order_item )
						{
							$j = isset( $j ) ? $j + 1 : 1 ;

							$product = new WC_Product( $order_item[ 'product_id' ] ) ;

							$line_item[ 'item_product_id_' . $j ] = $order_item[ 'product_id' ] ;
							$line_item[ 'item_name_' . $j ] = $order_item[ 'name' ] ;
							$line_item[ 'item_sku_' . $j ] = $product->get_sku( ) ;
							$line_item[ 'item_quantity_' . $j ] = $order_item[ 'qty' ] ;
							$line_item[ 'item_total_' . $j ] = $order_item[ 'line_total' ] ;

							// Item attributes
							$product_atts = get_post_meta( $order_item[ 'product_id' ]  , '_product_attributes' ) ;
							$line_item_atts = array( ) ;

							foreach ( $product_atts[ 0 ] as $key => $value )
							{
								if ( array_key_exists( $key , $order_item ) )
								{
									$att = wc_get_product_terms( $order_item[ 'product_id' ] , $key  , array( 'slug' => $order_item[ $key ] ) ) ;

									if ( isset( $att[ 0 ] ) )
									{
										$line_item_atts[ ] = wc_attribute_label( $key , $product ) . ': ' . $att[ 0 ] ;
									}
								}
							}

							// Item addons
							$product_addons = get_product_addons( $product->id ) ;

							foreach ( $product_addons as $product_addon )
							{
								$addon_name = $product_addon[ 'name' ] ;

								foreach ( $order_item as $key => $value )
								{
									if ( stripos( $key , $addon_name ) === 0 )
									{
										$line_item_atts[ ] = $addon_name . ': ' . $value ;
									}
								}
							}

							// Serialize attributes
							if ( count( $line_item_atts ) )
							{
								$line_item[ 'item_attributes_' . $j ] = htmlspecialchars_decode( implode( ', ' , $line_item_atts ) ) ;
							}

							// Check if the order contains a subscription and add that data
							if ( WC_Subscriptions_Product::is_subscription( $order_item[ 'product_id' ] ) )
							{
								$line_item[ 'subscription_item_id' ] = $order_item_id ;

								$line_item[ 'subscription_meta_recipient' ] = $order_item[ 'Recipient' ] ;
								$line_item[ 'subscription_meta_gender' ] = $order_item[ 'Gender' ] ;
								$line_item[ 'subscription_meta_recipient_name' ] = $order_item[ 'Recipient Name 1 - Text' ] ;
								
								if ( isset( $order_item[ 'Recipient Name 2 - Text' ] ) )
								{
									$line_item[ 'subscription_meta_recipient_name' ] .= ' & ' . $order_item[ 'Recipient Name 2 - Text' ] ;
								} 

								$line_item[ 'subscription_meta_decade' ] = $order_item[ 'Decade' ] ;
								$line_item[ 'subscription_meta_shipping_country' ] = $order_item[ 'Shipping Country - Text' ] ;
								$line_item[ 'subscription_meta_subscriber_email_address' ] = $order_item[ 'Subscriber Email Address - Text' ] ;
								$line_item[ 'subscription_meta_recipient_email_address' ] = isset( $order_item[ 'Recipient Email Address - Text' ] ) ? $order_item[ 'Recipient Email Address - Text' ] : '' ;
								$line_item[ 'subscription_meta_start_date' ] = $order_item[ 'Start Date - Date' ] ;
								$line_item[ 'subscription_meta_additional_information' ] = isset( $order_item[ 'Additional Information - Text' ] ) ? $order_item[ 'Additional Information - Text' ] : '' ;
								$line_item[ 'subscription_meta_personalised_message' ] = isset( $order_item[ 'Personalised Message - Text' ] ) ? $order_item[ 'Personalised Message - Text' ] : '' ;
								$line_item[ 'subscription_meta_first_present_name' ] = isset( $order_item[ 'First Present - ID' ] ) ? $order_item[ 'First Present - ID' ] : '' ;
								$line_item[ 'subscription_meta_favourites' ] = isset( $order_item[ 'Favourites - JSON' ] ) ? $order_item[ 'Favourites - JSON' ] : '' ;
								$line_item[ 'subscription_meta_initials' ] = isset( $order_item[ 'Initials - Text' ] ) ? $order_item[ 'Initials - Text' ] : '' ;
								$line_item[ 'subscription_meta_decorative_functional' ] = isset( $order_item[ 'Sliders - Decorative-Functional' ] ) ? $order_item[ 'Sliders - Decorative-Functional' ] : '' ;
								$line_item[ 'subscription_meta_niche_established' ] = isset( $order_item[ 'Sliders - Niche-Established' ] ) ? $order_item[ 'Sliders - Niche-Established' ] : '' ;
								$line_item[ 'subscription_meta_quirky_traditional' ] = isset( $order_item[ 'Sliders - Quirky-Traditional' ] ) ? $order_item[ 'Sliders - Quirky-Traditional' ] : '' ;

								if ( isset( $order_item[ 'item_meta' ][ 'Suitable Presents' ] ) )
									$line_item[ 'subscription_meta' ][ 'Suitable Presents' ] = $order_item[ 'item_meta' ][ 'Suitable Presents' ] ;
								
								if ( isset( $order_item[ 'item_meta' ][ 'Unsuitable Presents' ] ) )
									$line_item[ 'subscription_meta' ][ 'Unsuitable Presents' ] = $order_item[ 'item_meta' ][ 'Unsuitable Presents' ] ;				

								$line_item = $this->set_subscription_item_meta( $line_item  , $order_item[ 'item_meta' ] ) ;
							}

							// Get associated subscription information for first present order
							$orders = new WP_Query(
								array(
									'post_type' => 'shop_order' ,
									'post_status' => 'any' ,
									'meta_query' => array(
										array(
											'key' => '_first_present_order_id' ,
											'value' => $order->id ,
											'compare' => '=='
										)
									)
								)
							) ;

							// Get posts
							$orders->get_posts( ) ;

							// Populate subscription fields
							if ( count( $orders->posts ) )
							{
								// Get subscription
								$subscription_order = new WC_Order( $orders->posts[ 0 ] ) ;

								// Get subscription item
								$subscription = WCS_Utils::get_order_subscription( $subscription_order ) ;

								if ( isset( $subscription ) )
								{
									$line_item[ 'subscription_item_id' ] = $order_item_id ;

									// Item meta
									$line_item[ 'subscription_meta_recipient' ] = $subscription[ 'Recipient' ] ;
									$line_item[ 'subscription_meta_gender' ] = $subscription[ 'Gender' ] ;

									if ( isset( $subscription[ 'Recipient Name 1 - Text' ] ) )
									{
										$line_item[ 'subscription_meta_recipient_name' ] = $subscription[ 'Recipient Name 1 - Text' ] ;
										
										if ( isset( $subscription[ 'Recipient Name 2 - Text' ] ) )
										{
											$line_item[ 'subscription_meta_recipient_name' ] .= ' & ' . $subscription[ 'Recipient Name 2 - Text' ] ;
										}
									}

									$line_item[ 'subscription_meta_decade' ] = $subscription[ 'Decade' ] ;
									$line_item[ 'subscription_meta_shipping_country' ] = $subscription[ 'Shipping Country - Text' ] ;
									$line_item[ 'subscription_meta_subscriber_email_address' ] = $subscription[ 'Subscriber Email Address - Text' ] ;
									$line_item[ 'subscription_meta_recipient_email_address' ] = isset( $subscription[ 'Recipient Email Address - Text' ] ) ? $subscription[ 'Recipient Email Address - Text' ] : '' ;
									$line_item[ 'subscription_meta_start_date' ] = $subscription[ 'Start Date - Date' ] ;
									$line_item[ 'subscription_meta_additional_information' ] = isset( $subscription[ 'Additional Information - Text' ] ) ? $subscription[ 'Additional Information - Text' ] : '' ;
									$line_item[ 'subscription_meta_personalised_message' ] = isset( $subscription[ 'Personalised Message - Text' ] ) ? $subscription[ 'Personalised Message - Text' ] : '' ;
									$line_item[ 'subscription_meta_first_present_name' ] = isset( $subscription[ 'First Present - ID' ] ) ? $subscription[ 'First Present - ID' ] : '' ;
									$line_item[ 'subscription_meta_favourites' ] = isset( $subscription[ 'Favourites - JSON' ] ) ? $subscription[ 'Favourites - JSON' ] : '' ;
									$line_item[ 'subscription_meta_initials' ] = isset( $subscription[ 'Initials - Text' ] ) ? $subscription[ 'Initials - Text' ] : '' ;
									$line_item[ 'subscription_meta_decorative_functional' ] = isset( $subscription[ 'Sliders - Decorative-Functional' ] ) ? $subscription[ 'Sliders - Decorative-Functional' ] : '' ;
									$line_item[ 'subscription_meta_niche_established' ] = isset( $subscription[ 'Sliders - Niche-Established' ] ) ? $subscription[ 'Sliders - Niche-Established' ] : '' ;
									$line_item[ 'subscription_meta_quirky_traditional' ] = isset( $subscription[ 'Sliders - Quirky-Traditional' ] ) ? $subscription[ 'Sliders - Quirky-Traditional' ] : '' ;

									// Additional meta
									$line_item = $this->set_subscription_item_meta( $line_item  , $subscription[ 'item_meta' ] ) ;

									// Preferences
									if ( isset( $subscription[ 'item_meta' ][ 'Suitable Presents' ] ) )
										$line_item[ 'subscription_meta' ][ 'Suitable Presents' ] = $subscription[ 'item_meta' ][ 'Suitable Presents' ] ;

									if ( isset( $subscription[ 'item_meta' ][ 'Unsuitable Presents' ] ) )
										$line_item[ 'subscription_meta' ][ 'Unsuitable Presents' ] = $subscription[ 'item_meta' ][ 'Unsuitable Presents' ] ;
								}
							}

							wp_reset_postdata( ) ;
						}

						break ;
					}
					else
					{
						return false ;
					}
				}
			}

			$line_item = $this->set_subscription_item_meta( $line_item , $item[ 'item_meta' ] ) ;
		}
		
		return $line_item ;
	}

	/**
	 * Add subscription meta entries to line item
	 *
	 * @param array $line_item
	 * @param array $meta
	 * @since 2.2.18.3
	 */
	private function set_subscription_item_meta ( $line_item  , $meta )
	{
		if ( ! isset( $line_item[ 'subscription_status' ] ) || $line_item[ 'subscription_status' ] == '' )
		{
			// Cancelled status hack
			if ( isset( $meta[ '_subscription_status' ][ 0 ] ) )
			{
				$status = $meta[ '_subscription_status' ][ 0 ] ;

				if ( $status == 'cancelled' )
				{
					$expiry_date = $meta[ '_subscription_expiry_date' ][ 0 ] ;
					$expiry_date = strtotime( $expiry_date ) ;

					if ( $expiry_date > time( ) )
					{
						$status = 'cancelled (active)' ;
					}
				}
			}
			else
			{
				$status = '' ;
			}

			$line_item[ 'subscription_status' ] = $status ;
			$line_item[ 'subscription_duration' ] = ( isset( $meta[ '_subscription_interval' ] ) and isset( $meta[ '_subscription_period' ] ) ) ? $meta[ '_subscription_interval' ][ 0 ] . ' ' . $meta[ '_subscription_period' ][ 0 ] : '' ;
			$line_item[ 'subscription_start_date' ] = ( isset( $meta[ '_subscription_start_date' ][ 0 ] )  and "0" !== $meta[ '_subscription_start_date' ][ 0 ] ) ? date( 'd-m-Y'  , strtotime( $meta[ '_subscription_start_date' ][ 0 ] ) ) : '' ;
			$line_item[ 'subscription_end_date' ] = ( isset( $meta[ '_subscription_end_date' ][ 0 ] ) and "0" !== $meta[ '_subscription_end_date' ][ 0 ] ) ? date( 'd-m-Y'  , strtotime( $meta[ '_subscription_end_date' ][ 0 ] ) ) : '' ;
			$line_item[ 'subscription_expiry_date' ] = ( isset( $meta[ '_subscription_expiry_date' ][ 0 ] ) and "0" !== $meta[ '_subscription_expiry_date' ][ 0 ] ) ? date( 'd-m-Y'  , strtotime( $meta[ '_subscription_expiry_date' ][ 0 ] ) ) : '' ;
			
			// Completed payments
			if ( array_key_exists( '_subscription_completed_payments' , $meta ) )
			{
				if ( $meta[ '_subscription_completed_payments' ][ 0 ] != '0' )
				{
					// List dates
					$line_item[ 'subscription_completed_payment_dates' ] = implode( ', ' ,
						array_reverse(
							array_map( 
								function ( $date )
								{
									return date( 'd-m-Y'  , strtotime( $date ) ) ;
								}  , 
								unserialize( ( $meta[ '_subscription_completed_payments' ][ 0 ] ) )
							)
						)
					) ;

					// Count dates
					$line_item[ 'subscription_completed_payments' ] = count( explode( ',' , $line_item[ 'subscription_completed_payment_dates' ] ) ) ;
				}
				else
				{
					$line_item[ 'subscription_completed_payments' ] = 0 ;
				}
			}

			// Failed payments
			if ( array_key_exists( '_subscription_failed_payments' , $meta ) )
			{
				if ( $meta[ '_subscription_failed_payments' ][ 0 ] != '0' && $meta[ '_subscription_failed_payments' ][ 0 ] != '1' )
				{
					// List dates
					$line_item[ 'subscription_failed_payment_dates' ] = implode( ', ' ,
						array_reverse(
							array_map( 
								function ( $date )
								{
									return date( 'd-m-Y'  , strtotime( $date ) ) ;
								}  , 
								unserialize( ( $meta[ '_subscription_failed_payments' ][ 0 ] ) )
							)
						)
					) ;

					// Count dates
					$line_item[ 'subscription_failed_payments' ] = count( explode( ',' , $line_item[ 'subscription_failed_payment_dates' ] ) ) ;
				}
				else
				{
					$line_item[ 'subscription_failed_payments' ] = 0 ;
				}
			}

			$line_item[ 'subscription_suspension_count' ] = ( isset( $meta[ '_subscription_suspension_count' ] ) ) ? $meta[ '_subscription_suspension_count' ][ 0 ] : '' ;
		}

		return $line_item ;
	}

	/**
	 * Add seperate meta entries to line item
	 *
	 * @param array $line_item
	 * @param mixed $item
	 * @param mixed $product
	 * @param mixed $order
	 * @param object $generator
	 *
	 * @since 2.2.23.14
	 */
	public static function filter_wc_customer_order_csv_export_order_row_one_row_per_item ( $order_data , $item , $order , $generator )
	{
		$order_id = $order_data[ 'order_id' ] ;
		$send_today = get_post_meta( $order_id , '_dispatch_today' , true ) ;

		// Check if order is send today
		if ( $send_today && ! WCS_Utils::get_order_subscription( $order ) )
		{
			// Get original order
			$sub_query = new WP_Query(
				array(
					'post_type' => 'shop_order' ,
					'post_status' => 'any' ,
					'numberposts' => 1 ,
					'meta_query' => array(
						array( 
							'key' => '_first_present_order_id' ,
							'value' => $order_id ,
							'compare' => '=='
						)
					)
				)
			) ;
			
			$original_order = $sub_query->get_posts( ) ;

			// Replace send today order ID with original order ID
			if ( count( $original_order ) )
			{
				$order_data[ 'order_id' ] = $original_order[ 0 ]->ID ;
			}

			wp_reset_postdata( ) ;
		}

		$order_data[ 'order_date' ] = ( $order_data[ 'order_date' ] ) ? date( 'd-m-Y H:i:s' , strtotime( $order_data[ 'order_date' ] ) ) : '' ;

		$tail = array( ) ;

		for ( end( $order_data ) ; key( $order_data ) != "item_meta" ; end( $order_data ) )
		{
			$tail_item = array( key( $order_data ) => array_pop( $order_data ) ) ;
			$tail = $tail_item + $tail ;

			if ( count( $order_data ) < 1 ) break ;
		}

		preg_match_all( '/(([^,]+)="(.*?)(?<!\\\\)" )/' , $order_data[ 'item_meta' ] , $matches ) ;

		$meta_contents = array( ) ;

		foreach ( $matches[ 0 ] as $i => $match )
		{
			$key = trim( $matches[ 2 ][ $i ] ) ;
			$value = trim( str_replace( '\"' , '"' , $matches[ 3 ][ $i ] ) ) ;

			$meta_contents[ $key ] = $value ;
		}

		$order_data[ 'subscription_meta_plan' ] = ( isset( $meta_contents[ 'Plan' ] ) ) ? $meta_contents[ 'Plan' ] : ( isset( $item[ 'subscription_meta_plan' ] ) ? $item[ 'subscription_meta_plan' ] : '' ) ;
		$order_data[ 'subscription_meta_shipping_zone' ] = ( isset( $meta_contents[ 'Shipping Zone' ] ) ) ? $meta_contents[ 'Shipping Zone' ] : ( isset( $item[ 'subscription_meta_shipping_zone' ] ) ? $item[ 'subscription_meta_shipping_zone' ] : '' ) ;
		$order_data[ 'subscription_meta_recipient' ] = ( isset( $meta_contents[ 'Recipient' ] ) ) ? $meta_contents[ 'Recipient' ] : ( isset( $item[ 'subscription_meta_recipient' ] ) ? $item[ 'subscription_meta_recipient' ] : '' ) ;
		$order_data[ 'subscription_meta_gender' ] = ( isset( $meta_contents[ 'Gender' ] ) ) ? $meta_contents[ 'Gender' ] : ( isset( $item[ 'subscription_meta_gender' ] ) ? $item[ 'subscription_meta_gender' ] : '' ) ;
		
		if ( isset( $meta_contents[ 'Recipient Name 1 - Text' ] ) )
		{
			$order_data[ 'subscription_meta_recipient_name' ] = $meta_contents[ 'Recipient Name 1 - Text' ] ;

			if ( isset( $meta_contents[ 'Recipient Name 2 - Text' ] ) )
			{
				$order_data[ 'subscription_meta_recipient_name' ] .= ' & ' . $meta_contents[ 'Recipient Name 2 - Text' ] ;
			}
		}
		elseif ( isset( $item[ 'subscription_meta_recipient_name' ] ) )
		{
			$order_data[ 'subscription_meta_recipient_name' ] = $item[ 'subscription_meta_recipient_name' ] ;
		}

		$order_data[ 'subscription_meta_decade' ] = ( isset( $meta_contents[ 'Decade' ] ) ) ? $meta_contents[ 'Decade' ] : ( isset( $item[ 'subscription_meta_decade' ] ) ? $item[ 'subscription_meta_decade' ] : '' ) ;
		$order_data[ 'subscription_meta_shipping_country' ] = ( isset( $meta_contents[ 'Shipping Country - Text' ] ) ) ? $meta_contents[ 'Shipping Country - Text' ] : ( isset( $item[ 'subscription_meta_shipping_country' ] ) ? $item[ 'subscription_meta_shipping_country' ] : '' ) ;
		$order_data[ 'subscription_meta_subscriber_email_address' ] = ( isset( $meta_contents[ 'Subscriber Email Address - Text' ] ) ) ? $meta_contents[ 'Subscriber Email Address - Text' ] : ( isset( $item[ 'subscription_meta_subscriber_email_address' ] ) ? $item[ 'subscription_meta_subscriber_email_address' ] : '' ) ;
		$order_data[ 'subscription_meta_recipient_email_address' ] = ( isset( $meta_contents[ 'Recipient Email Address - Text' ] ) ) ? $meta_contents[ 'Recipient Email Address - Text' ] : ( isset( $item[ 'subscription_meta_recipient_email_address' ] ) ? $item[ 'subscription_meta_recipient_email_address' ] : '' ) ;
		$order_data[ 'subscription_meta_start_date' ] = ( isset( $meta_contents[ 'Start Date - Date' ] ) ) ? date( 'd-m-Y' , strtotime( $meta_contents[ 'Start Date - Date' ] ) ) : ( isset( $item[ 'subscription_meta_start_date' ] ) ? date( 'd-m-Y' , strtotime( $item[ 'subscription_meta_start_date' ] ) ) : '' ) ;
		$order_data[ 'subscription_meta_additional_information' ] = ( isset( $meta_contents[ 'Additional Information - Text' ] ) ) ? $meta_contents[ 'Additional Information - Text' ] : ( isset( $item[ 'subscription_meta_additional_information' ] ) ? $item[ 'subscription_meta_additional_information' ] : '' ) ;
		$order_data[ 'subscription_meta_personalised_message' ] = ( isset( $meta_contents[ 'Personalised Message - Text' ] ) ) ? $meta_contents[ 'Personalised Message - Text' ] : ( isset( $item[ 'subscription_meta_personalised_message' ] ) ? $item[ 'subscription_meta_personalised_message' ] : '' ) ;
		$order_data[ 'subscription_meta_first_present_name' ] = ( isset( $meta_contents[ 'First Present - ID' ] ) ) ? $meta_contents[ 'First Present - ID' ] : ( isset( $item[ 'subscription_meta_first_present_name' ] ) ? $item[ 'subscription_meta_first_present_name' ] : '' ) ;
		$order_data[ 'subscription_meta_initials' ] = ( isset( $meta_contents[ 'Initials - Text' ] ) ) ? $meta_contents[ 'Initials - Text' ] : ( isset( $item[ 'subscription_meta_initials' ] ) ? $item[ 'subscription_meta_initials' ] : '' ) ;
		$order_data[ 'subscription_meta_decorative_functional' ] = ( isset( $meta_contents[ 'Sliders - Decorative-Functional' ] ) ) ? $meta_contents[ 'Sliders - Decorative-Functional' ] : ( isset( $item[ 'subscription_meta_decorative_functional' ] ) ? $item[ 'subscription_meta_decorative_functional' ] : '' ) ;
		$order_data[ 'subscription_meta_niche_established' ] = ( isset( $meta_contents[ 'Sliders - Niche-Established' ] ) ) ? $meta_contents[ 'Sliders - Niche-Established' ] : ( isset( $item[ 'subscription_meta_niche_established' ] ) ? $item[ 'subscription_meta_niche_established' ] : '' ) ;
		$order_data[ 'subscription_meta_quirky_traditional' ] = ( isset( $meta_contents[ 'Sliders - Quirky-Traditional' ] ) ) ? $meta_contents[ 'Sliders - Quirky-Traditional' ] : ( isset( $item[ 'subscription_meta_quirky_traditional' ] ) ? $item[ 'subscription_meta_quirky_traditional' ] : '' ) ;

		// Decode special characters and replace line breaks with spaces
		if ( isset( $order_data[ 'subscription_meta_personalised_message' ] ) )
		{
			$order_data[ 'subscription_meta_personalised_message' ] = htmlspecialchars_decode( $order_data[ 'subscription_meta_personalised_message' ] ) ;
			$order_data[ 'subscription_meta_personalised_message' ] = trim( preg_replace( '/\s+/' , ' ' , $order_data[ 'subscription_meta_personalised_message' ] ) ) ;
		}

		// Additional information
		if ( isset( $order_data[ 'subscription_meta_additional_information' ] ) )
		{
			$order_data[ 'subscription_meta_additional_information' ] = htmlspecialchars_decode( $order_data[ 'subscription_meta_additional_information' ] ) ;
			$order_data[ 'subscription_meta_additional_information' ] = trim( preg_replace( '/\s+/' , ' ' , $order_data[ 'subscription_meta_additional_information' ] ) ) ;
		}

		// Customer note
		if ( isset( $order_data[ 'customer_note' ] ) )
		{
			$order_data[ 'customer_note' ] = htmlspecialchars_decode( $order_data[ 'customer_note' ] ) ;
			$order_data[ 'customer_note' ] = trim( preg_replace( '/\s+/' , ' ' , $order_data[ 'customer_note' ] ) ) ;
		}

		// Subscription first present
		if ( ! empty( $order_data[ 'subscription_meta_first_present_name' ] ) )
		{
			$order_data[ 'subscription_meta_first_present_name' ] = get_the_title( $order_data[ 'subscription_meta_first_present_name' ] ) ;
		}
		else
		{
			$order_data[ 'subscription_meta_first_present_name' ] = 'No you choose' ;
		}
		
		// Suitable
		$order_data[ 'subscription_meta_ladies' ] = ( self::in_meta( 'Ladies' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'Ladies' , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;
		$order_data[ 'subscription_meta_for_the_wall' ] = ( self::in_meta( 'For The Wall' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'For The Wall' , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;
		$order_data[ 'subscription_meta_outdoors' ] = ( self::in_meta( 'Outdoors' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'Outdoors' , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;
		$order_data[ 'subscription_meta_apothecary' ] = ( self::in_meta( 'Apothecary' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'Apothecary' , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;
		$order_data[ 'subscription_meta_homewares' ] = ( self::in_meta( 'Homewares' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'Homewares' , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;
		$order_data[ 'subscription_meta_art' ] = ( self::in_meta( 'Art' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'Art' , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;
		$order_data[ 'subscription_meta_for_your_desk' ] = ( self::in_meta( 'For Your Desk' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'For Your Desk' , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;
		$order_data[ 'subscription_meta_kitchen' ] = ( self::in_meta( 'Kitchen' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'Kitchen'  , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;
		$order_data[ 'subscription_meta_stationery' ] = ( self::in_meta( 'Stationery' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'Stationery' , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;
		$order_data[ 'subscription_meta_gentlemen' ] = ( self::in_meta( 'Gentlemen' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'Gentlemen' , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;
		$order_data[ 'subscription_meta_jewellery' ] = ( self::in_meta( 'Jewellery' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'Jewellery' , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;
		$order_data[ 'subscription_meta_nostalgic' ] = ( self::in_meta( 'Nostalgic' , $meta_contents , 'Suitable Presents' ) ) ? 'suitable' : ( isset( $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) && in_array( 'Nostalgic' , $item[ 'subscription_meta' ][ 'Suitable Presents' ] ) ? 'suitable' : '' ) ;

		// Unsuitable
		$order_data[ 'subscription_meta_ladies' ] = ( self::in_meta( 'Ladies' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'Ladies'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_ladies' ] ) ;
		$order_data[ 'subscription_meta_for_the_wall' ] = ( self::in_meta( 'For The Wall' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'For The Wall'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_for_the_wall' ] ) ;
		$order_data[ 'subscription_meta_outdoors' ] = ( self::in_meta( 'Outdoors' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'Outdoors'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_outdoors' ] ) ;
		$order_data[ 'subscription_meta_apothecary' ] = ( self::in_meta( 'Apothecary' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'Apothecary'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_apothecary' ] ) ;
		$order_data[ 'subscription_meta_homewares' ] = ( self::in_meta( 'Homewares' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'Homewares'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_homewares' ] ) ;
		$order_data[ 'subscription_meta_art' ] = ( self::in_meta( 'Art' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'Art'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_art' ] ) ;
		$order_data[ 'subscription_meta_for_your_desk' ] = ( self::in_meta( 'For Your Desk' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'For Your Desk'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_for_your_desk' ] ) ;
		$order_data[ 'subscription_meta_kitchen' ] = ( self::in_meta( 'Kitchen' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'Kitchen'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_kitchen' ] ) ;
		$order_data[ 'subscription_meta_stationery' ] = ( self::in_meta( 'Stationery' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'Stationery'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_stationery' ] ) ;
		$order_data[ 'subscription_meta_gentlemen' ] = ( self::in_meta( 'Gentlemen' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'Gentlemen'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_gentlemen' ] ) ;
		$order_data[ 'subscription_meta_jewellery' ] = ( self::in_meta( 'Jewellery' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'Jewellery'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_jewellery' ] ) ;
		$order_data[ 'subscription_meta_nostalgic' ] = ( self::in_meta( 'Nostalgic' , $meta_contents , 'Unsuitable Presents' ) ) ? 'unsuitable' : ( isset( $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) && in_array( 'Nostalgic'  , $item[ 'subscription_meta' ][ 'Unsuitable Presents' ] ) ? 'unsuitable' : $order_data[ 'subscription_meta_nostalgic' ] ) ;

		// Subscription data
		$head[ 'subscription_status' ] = ( isset( $item[ 'subscription_status' ] ) ) ? $item[ 'subscription_status' ] : '' ;
		$head[ 'subscription_duration' ] = ( isset( $item[ 'subscription_duration' ] ) ) ? $item[ 'subscription_duration' ] : '' ;
		$head[ 'subscription_start' ] = ( isset( $item[ 'subscription_start_date' ] ) ) ? $item[ 'subscription_start_date' ] : '' ;
		$head[ 'subscription_end' ] = ( isset( $item[ 'subscription_end_date' ] ) ) ? $item[ 'subscription_end_date' ] : '' ;
		$head[ 'subscription_expiry' ] = ( isset( $item[ 'subscription_expiry_date' ] ) ) ? $item[ 'subscription_expiry_date' ] : '' ;
		$head[ 'subscription_completed_payments' ] = ( isset( $item[ 'subscription_completed_payments' ] ) ) ? $item[ 'subscription_completed_payments' ] : '' ;
		$head[ 'subscription_completed_payment_dates' ] = ( isset( $item[ 'subscription_completed_payment_dates' ] ) ) ? $item[ 'subscription_completed_payment_dates' ] : '' ;
		$head[ 'subscription_failed_payments' ] = ( isset( $item[ 'subscription_failed_payments' ] ) ) ? $item[ 'subscription_failed_payments' ] : '' ;
		$head[ 'subscription_failed_payment_dates' ] = ( isset( $item[ 'subscription_failed_payment_dates' ] ) ) ? $item[ 'subscription_failed_payment_dates' ] : '' ;
		$head[ 'subscription_suspension_count' ] = ( isset( $item[ 'subscription_suspension_count' ] ) ) ? $item[ 'subscription_suspension_count' ] : '' ;

		// Copy package shipping data from item to order
		$order_data[ 'shipping_first_name' ] = $order_data[ 'shipping_first_name' ]	!= '' ? $order_data[ 'shipping_first_name' ] : ( isset( $item[ 'shipping_first_name' ] ) ? $item[ 'shipping_first_name' ] : '' ) ;
		$order_data[ 'shipping_last_name' ] = $order_data[ 'shipping_last_name' ] != '' ? $order_data[ 'shipping_last_name' ] : ( isset( $item[ 'shipping_last_name' ] ) ? $item[ 'shipping_last_name' ] : '' ) ;
		$order_data[ 'shipping_address_1' ] = $order_data[ 'shipping_address_1' ] != '' ? $order_data[ 'shipping_address_1' ] : ( isset( $item[ 'shipping_address_1' ] ) ? $item[ 'shipping_address_1' ] : '' ) ;
		$order_data[ 'shipping_address_2' ] = $order_data[ 'shipping_address_2' ] != '' ? $order_data[ 'shipping_address_2' ] : ( isset( $item[ 'shipping_address_2' ] ) ? $item[ 'shipping_address_2' ] : '' ) ;
		$order_data[ 'shipping_city' ] = $order_data[ 'shipping_city' ] != '' ? $order_data[ 'shipping_city' ] : ( isset( $item[ 'shipping_city' ] ) ? $item[ 'shipping_city' ] : '' ) ;
		$order_data[ 'shipping_state' ] = $order_data[ 'shipping_state' ] != '' ? $order_data[ 'shipping_state' ] : ( isset( $item[ 'shipping_state' ] ) ? $item[ 'shipping_state' ] : '' ) ;
		$order_data[ 'shipping_country' ] = $order_data[ 'shipping_country' ] != '' ? $order_data[ 'shipping_country' ] : ( isset( $item[ 'shipping_country' ] ) ? $item[ 'shipping_country' ] : '' ) ;
		$order_data[ 'shipping_company' ] = $order_data[ 'shipping_company' ] != '' ? $order_data[ 'shipping_company' ] : ( isset( $item[ 'shipping_company' ] ) ? $item[ 'shipping_company' ] : '' ) ;

		// Force postcodes with leading zeros to be strings
		$billing_postcode = $order_data[ 'billing_postcode' ] ;

		if ( $billing_postcode != '' && substr( $billing_postcode , 0 , 1 ) == '0' )
		{
			$billing_postcode .= '	' ;
		}

		$order_data[ 'billing_postcode' ] = $billing_postcode ;

		$shipping_postcode = $order_data[ 'shipping_postcode' ] != '' ? $order_data[ 'shipping_postcode' ] : ( isset( $item[ 'shipping_postcode' ] ) ? $item[ 'shipping_postcode' ] : '' ) ;

		if ( $shipping_postcode != '' && substr( $shipping_postcode , 0 , 1 ) == '0' )
		{
			$shipping_postcode .= '	' ;
		}

		$order_data[ 'shipping_postcode' ] = $shipping_postcode ;

		$line_item_column_count = self::DEFAULT_LINE_ITEMS ;

		if ( array_key_exists( 'nab_wc_customer_order_csv_export_order_item_columns_count' , $_POST ) )
		{
			$line_item_column_count = $_POST[ 'nab_wc_customer_order_csv_export_order_item_columns_count' ] ;
		}

		// Order items
		for ( $i = 1 ; $i < $line_item_column_count + 1 ; $i++ )
		{
			$head[ 'item_product_id_' . $i ] = isset( $item[ 'item_product_id_' . $i ] ) ? $item[ 'item_product_id_' . $i ] : '' ;
			$head[ 'item_name_' . $i ] = isset( $item[ 'item_name_' . $i ] ) ? $item[ 'item_name_' . $i ] : '' ;
			$head[ 'item_sku_' . $i ] = isset( $item[ 'item_sku_' . $i ] ) ? $item[ 'item_sku_' . $i ] : '' ;
			$head[ 'item_quantity_' . $i ] = isset( $item[ 'item_quantity_' . $i ] ) ? $item[ 'item_quantity_' . $i ] : '' ;
			$head[ 'item_total_' . $i ] = isset( $item[ 'item_total_' . $i ] ) ? $item[ 'item_total_' . $i ] : '' ;

			if ( ! WC_Subscriptions_Product::is_subscription( $head[ 'item_product_id_' . $i ] ) )
			{			
				$head[ 'item_attributes_' . $i ] = isset( $item[ 'item_attributes_' . $i ] ) ? $item[ 'item_attributes_' . $i ] : '' ;
			}
		}

		$head[ 'order_subtotal' ] = $order->get_total( ) - $order->get_total_shipping( ) ;

		// Subscription renewal count
		$renewal_count = WC_Subscriptions_Renewal_Order::get_renewal_order_count( $order->id ) ;
		$head[ 'subscription_renewals' ] = ! empty( $renewal_count ) || isset( $item[ 'subscription_status' ] ) ? $renewal_count : '' ;
		
		// Associated subscriptions (using order ID from order data so that "send today" orders are included)
		$associated_subscriptions = get_post_meta( $order_data[ 'order_id' ] , '_associated_subscription_orders' , true ) ;

		if ( ! empty( $associated_subscriptions ) ) $head[ 'subscription_associated_orders' ] = implode( ', ' , $associated_subscriptions ) ;

		// Subscriptions favourites
		if ( ! empty( $item[ 'subscription_meta_favourites' ] ) )
		{
			$favourites = json_decode( $item[ 'subscription_meta_favourites' ] ) ;

			if ( array_key_exists( 'subscription_previous_gifts' , $item ) )
			{
				$previous_gifts = json_decode( $item[ 'subscription_previous_gifts' ] ) ;
			}
			else if ( array_key_exists( 'subscription_item_id' , $item ) )
			{
				$previous_gifts = wc_get_order_item_meta( $item[ 'subscription_item_id' ] , '_subscription_previous_gifts' ) ;
			}

			if ( json_last_error( ) == JSON_ERROR_NONE )
			{
				$i = 0 ;
				$factory = new WC_Product_Factory( ) ;

				foreach ( $favourites as $favourite )
				{
					if ( ! isset( $favourite->id ) )
					{
						$i++ ;
						continue ;
					}

					if ( ! is_array( $previous_gifts ) || array_search( $favourite->id , $previous_gifts ) === false )
					{
						$product = $factory->get_product( $favourite->id ) ;

						if ( $product && isset( $product->post ) && in_array( $product->post->post_type , array( 'product' , 'product_variation' ) ) )
						{
							$head[ 'subscription_favourite_id_' . ++$i ] = $favourite->id ;
							$head[ 'subscription_favourite_name_' . $i ] = htmlspecialchars_decode( get_the_title( $favourite->id ) ) ;

							if ( ! empty( $favourite->attributes ) )
							{
								$meta_attributes = '' ;
								$attributes = $product->get_variation_attributes( ) ;

								foreach ( $favourite->attributes as $name => $option )
								{
									$name = str_replace( 'attribute_' , '' , $name ) ;
									$terms = get_terms( $name , array( 'slug' => $option ) ) ;
									$meta_attributes .= sprintf( '%s: %s, ' , wc_attribute_label( $name ) , $terms[ 0 ]->name ) ;
								}

								$head[ 'subscription_favourite_attributes_' . $i ] = htmlspecialchars_decode( substr( $meta_attributes , 0 , -2 ) ) ;
							}
						}
					}
				}
			}
		}

		$order_data = $order_data + $tail + $head ;

		return $order_data ;
	}

	public static function in_meta ( $item , $meta , $key )
	{
		if ( array_key_exists( $key , $meta ) )
		{
			$values = explode( ' ,' , $meta[ $key ] ) ;
			if ( in_array( $item , $values) )
			{
				return true ;
			}
		}

		return false ;
	}

	public static function filter_replace_item_meta ( $line_item , $item )
	{
		$meta_items = array( ) ;

		foreach ( $item[ 'item_meta' ] as $meta_name => $meta_values )
		{
			// ignore hidden meta
			if ( substr( $meta_name , 0 , 1 ) != '_' )
			{
				$meta_items[ ] .= $meta_name . '="' . implode( ' ,' , str_replace( '"' , '\"' , $meta_values) ) . '"' ; // format: name="value"
			}

		}

		$meta = implode( ' ,' , $meta_items ) ; // format: name1="value1",name2="value2"
		
		$line_item[ 'meta' ] = $meta ;

		return $line_item ;
	}
}

new WC_NAB_Admin_CSV( ) ;