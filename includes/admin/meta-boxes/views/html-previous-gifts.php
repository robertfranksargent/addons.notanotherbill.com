<?php

	if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

	if ( sizeof( $previous_gifts ) ) :
?>

<div class="woocommerce_order_items_wrapper wc-order-items-editable">
	<table cellpadding="0" cellspacing="0" class="woocommerce_order_items">
		<thead>
			<tr>
				<!--<th><input type="checkbox" class="check-column" disabled /></th>-->
				<th class="item" colspan="2"><?php _e( 'Gift' , 'notanotherbill' ) ; ?></th>
				<th class="item" colspan="1"><?php _e( 'Date' , 'notanotherbill' ) ; ?></th>
				<th class="item" colspan="1"><?php _e( 'Categories' , 'notanotherbill' ) ; ?></th>
				<th class="item" colspan="1"><?php _e( 'Customer Rating' , 'notanotherbill' ) ; ?></th>
				<th class="item" colspan="1"><?php _e( 'Average Rating' , 'notanotherbill' ) ; ?></th>
				<th class="item" colspan="1"><?php _e( 'Comments' , 'notanotherbill' ) ; ?></th>
				<th class="wc-order-edit-line-item" width="1%">&nbsp;</th>
			</tr>
		</thead>
		<tbody id="order_line_items">
			<?php
				if ( $previous_gifts )
				{
					$factory = new WC_Product_Factory( ) ;

					foreach ( $previous_gifts as $timestamp => $previous_gift )
					{
						if ( empty( $previous_gift ) || get_post_type( $previous_gift ) != 'product' ) continue ;
						
						include( 'html-previous-gift.php' ) ;
					}
				}
			?>
		</tbody>
	</table>
</div>
<!--
<div class="wc-order-data-row wc-order-bulk-actions">
	<p class="bulk-actions">
		<select>
			<option value=""><?php _e( 'Actions' , 'woocommerce' ) ; ?></option>
			<optgroup label="<?php _e( 'Edit' , 'woocommerce' ) ; ?>">
				<option value="delete"><?php _e( 'Delete selected gift(s)' , 'notanotherbill' ) ; ?></option>
			</optgroup>
		</select>
		<button type="button" class="button do_bulk_action wc-reload" title="<?php _e( 'Apply' , 'woocommerce' ) ; ?>"><span><?php _e( 'Apply' , 'woocommerce' ) ; ?></span></button>
	</p>
	<p class="add-gifts">
		<button type="button" class="button add-gift"><?php _e( 'Add gift(s)' , 'notanotherbill' ) ; ?></button>
	</p>
</div>
<div class="wc-order-data-row wc-order-add-gift">
	<button type="button" class="button cancel-action"><?php _e( 'Cancel' , 'woocommerce' ) ; ?></button>
	<button type="button" class="button button-primary save-action"><?php _e( 'Save' , 'woocommerce' ) ; ?></button>
</div>
-->
<?php else : ?>
	<div><?php _e( 'No previous gifts yet.' , 'notanotherbill' ) ; ?>
<?php endif ; ?>