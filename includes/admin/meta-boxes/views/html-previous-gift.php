<?php

	if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

	$_product = $factory->get_product( $previous_gift ) ;
?>
<tr class="item" data-order_item_id="<?php echo $previous_gift ; ?>">
	<!--<td class="check-column"><input type="checkbox" disabled /></td>-->
	<td class="thumb">
		<?php if ( $_product ) : ?>
			<a href="<?php echo esc_url( admin_url( 'post.php?post=' . absint( $_product->id ) . '&action=edit' ) ) ; ?>" class="tips" data-tip="<?php

				echo '<strong>' . __( 'Product ID:' , 'woocommerce' ) . '</strong> ' . absint( $previous_gift ) ;

				if ( $_product && $_product->get_sku( ) )
				{
					echo '<br/><strong>' . __( 'Product SKU:' , 'woocommerce' ) . '</strong> ' . esc_html( $_product->get_sku( ) ) ;
				}

			?>"><?php echo $_product->get_image( 'shop_thumbnail' , array( 'title' => '' ) ) ; ?></a>
		<?php else : ?>
			<?php echo wc_placeholder_img( 'shop_thumbnail' ) ; ?>
		<?php endif ; ?>
	</td>
	<td class="name">
		<?php if ( $_product ) : ?>
			<a target="_blank" href="<?php echo esc_url( admin_url( 'post.php?post=' . absint( $_product->id ) . '&action=edit' ) ) ; ?>">
				<?php echo $_product->get_title( ) ; ?>
			</a>
		<?php else : ?>
			<?php echo sprintf( 'Product #%s does not exist. <a href="%s" target="_blank">Create a new product?</a>' , $previous_gift , 'http://dev.notanotherbill.local/wp/wp-admin/post-new.php?post_type=product' ) ; ?>
		<?php endif ; ?>
	</td>
	<td class="date">
		<?php if ( $_product && $timestamp ) : ?>
			<?php echo date( 'M, Y' , $timestamp ) ; ?>
		<?php else : ?>
			&ndash;
		<?php endif ; ?>
	</td>
	<td class="categories">
		<?php if ( $_product ) : ?>
			<?php echo $_product->get_categories( ) ; ?>
		<?php else : ?>
			&ndash;
		<?php endif ; ?>
	</td>
	<td class="customer-rating">
		<?php if ( $_product ) : ?>
			<?php $customer_rating = WCS_Utils::get_user_rating( $_product->id , $order->get_user_id( ) ) ; ?>
			<?php if ( $customer_rating ) : ?>
				<?php wp_star_rating( array( 'rating' => $customer_rating ) ) ; ?>
			<?php else : ?>
				<p><?php _e( 'No rating yet.' , 'notanotherbill' ) ; ?></p>
			<?php endif ; ?>
		<?php else : ?>
			&ndash;
		<?php endif ; ?>
	</td>
	<td class="average-rating">
		<?php if ( $_product ) : ?>
			<?php $rating_count = $_product->get_rating_count( ) ; ?>
			<?php if ( $rating_count ) : ?>
				<?php wp_star_rating( array( 'rating' => $_product->get_average_rating( ) , 'number' => $rating_count ) ) ; ?>
			<?php else : ?>
				<p><?php _e( 'No ratings yet.' , 'notanotherbill' ) ; ?></p>
			<?php endif ; ?>
		<?php else : ?>
			&ndash;
		<?php endif ; ?>
	</td>
	<td class="feedback response column-response">
		<?php if ( $_product ) : ?>
			<?php
				$comments = get_comments( 
					array(
						'post_id' => $_product->id ,
						'meta_key' => 'feedback' ,
						'meta_value' => 1 ,
						'user_id' => $order->get_user_id( )
					)
				) ;

				$comments_link_args = array(
					'p' => $_product->id ,
					'a' => $order->get_user_id( )
				) ;
				$comments_link = add_query_arg( $comments_link_args , admin_url( 'edit-comments.php' ) ) ;
			?>
			<span class="post-com-count-wrapper">
				<a class="post-com-count post-com-count-approved" href="<?php echo $comments_link ; ?>">
					<span class="comment-count-approved" aria-hidden="true"><?php echo count( $comments ) ; ?></span>
					<span class="screen-reader-text"><?php echo _n( '%d comment' , '%d comments' , count( $comments ) ) ; ?></span>
				</a>
			</span>
		<?php else : ?>
			&ndash;
		<?php endif ; ?>
	</td>
	<td class="wc-order-edit-gift">
		<div class="wc-order-edit-gift-actions">
			<a class="edit-gift" href="#"></a><a class="delete-gift" href="#"></a>
		</div>
	</td>
</tr>