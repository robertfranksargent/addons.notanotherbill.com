<?php
/**
 * Previous Gifts
 *
 * Functions for displaying the previous gifts meta box.
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin/Meta Boxes
 * @version     2.2.18.1
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

/**
 * WC_Meta_Box_Previous_Gifts Class
 */
class WC_Meta_Box_Previous_Gifts {

	/**
	 * Output the metabox
	 *
	 * @since 2.2.18.1
	 */
	public static function output ( $post )
	{
		$order = new WC_Order( $post->ID ) ;
		$previous_gifts = WC_Subscriptions_Order::get_item_meta( $post->ID , '_subscription_previous_gifts' ) ;

		if ( isset( $previous_gifts ) )
		{
			$previous_gifts = maybe_unserialize( $previous_gifts ) ;
			$timestamps = array( ) ;

			if ( is_array( $previous_gifts ) )
			{
				foreach ( $previous_gifts as $timestamp => $previous_gift )
				{
					if ( $timestamp < strtotime( '2000-01-01' ) )
					{
						$timestamp = strtotime( get_field( 'past_present_delivery_date' , $previous_gift ) ) ;
					}

					$timestamps[ $previous_gift ] = $timestamp ;
				}

				// Sort by date
				arsort( $timestamps ) ;

				// Flip keys (ID) and values (date)
				$previous_gifts = array_flip( $timestamps ) ;
			}

			include( 'views/html-previous-gifts.php' ) ;
		}
	}
}
