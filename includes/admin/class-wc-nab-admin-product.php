<?php
/**
 * Product page
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin
 * @version	    2.2.21.15
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_NAB_Admin_Product' ) ) :

/**
 * WC_NAB_Admin_Product Class
 */
class WC_NAB_Admin_Product
{
	/**
	 * Constructor
	 *
	 * @since 2.2.20.9
	 */
	public function __construct ( )
	{
		add_action( 'woocommerce_product_write_panels', array( $this , 'output_data' ) ) ;
		add_action( 'woocommerce_process_product_meta' , array( $this , 'process_meta_boxes' ) , 1 ) ;
	}

	/**
	 * Process meta box
	 *
	 * @param int $post_id
	 */
	public function process_meta_boxes ( $post_id )
	{
		if ( isset( $_POST[ '_product_addons_bind_required' ] ) )
		{
			update_post_meta( $post_id , '_product_addons_bind_required' , $_POST[ '_product_addons_bind_required' ] ) ;
		}
	}

	/**
	 * @since 2.2.21.15
	 */
	public function output_data ( )
	{
		global $post ;

		$product_addons_bind_required = get_post_meta( $post->ID , '_product_addons_bind_required' , true ) ;

		if ( isset( $product_addons_bind_required ) && is_array( $product_addons_bind_required ) && count( $product_addons_bind_required ) > 0 )
		{
			$options = '' ;
			$index = 1 ;

			foreach ( $product_addons_bind_required as $option )
			{
				$options .= ' "' . $option . '" ' ;

				if ( $index++ < count( $product_addons_bind_required ) )
				{
					$options .= ',' ;
				}
			}

			?>
			<script type="text/javascript">var wc_product_addons_bind_required = [<?php echo $options ; ?>] ;</script>	
			<?php
		}
	}
}

endif ;

return new WC_NAB_Admin_Product( ) ;