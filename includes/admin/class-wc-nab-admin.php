<?php 

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

/**
 * WooCommerce Admin.
 *
 * @class 		WC_NAB_Admin 
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin
 * @version     2.2.20
 */
class WC_NAB_Admin
{
	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		add_action( 'init' , array( $this , 'includes' ) , 1 ) ;
	}

	/**
	 * Include any classes we need within admin.
	 *
	 * @since 2.2.20
	 */
	public function includes ( )
	{
		// Classes
		include_once( 'class-wc-nab-admin-order.php' ) ;
		include_once( 'class-wc-nab-admin-product.php' ) ;
		include_once( 'class-wc-nab-admin-post-types.php' ) ;
		include_once( 'class-wc-nab-admin-subscriptions.php' ) ;
		include_once( 'class-wc-nab-admin-importer.php' ) ;
		include_once( 'class-wc-nab-admin-csv.php' ) ;
		include_once( 'class-wc-nab-admin-comments.php' ) ;
		include_once( 'class-wc-nab-admin-reports.php' ) ;

		// Classes we only need if the ajax is not-ajax
		if ( ! defined( 'DOING_AJAX' ) )
		{
			include( 'class-wc-nab-admin-assets.php' ) ;
		}
	}
}

return new WC_NAB_Admin( ) ;