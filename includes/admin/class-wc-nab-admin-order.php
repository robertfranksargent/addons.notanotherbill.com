<?php
/**
 * Order page
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin
 * @version	    2.2.22.9
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

if ( ! class_exists( 'WC_NAB_Admin_Order' ) ) :

/**
 * WC_NAB_Admin_Order Class
 */
class WC_NAB_Admin_Order
{
	/**
	 * Constructor
	 *
	 * @since 2.2.22.9
	 */
	public function __construct ( )
	{
		add_action( 'add_meta_boxes' , array( $this , 'add_meta_boxes' ) ) ;
		add_action( 'woocommerce_admin_order_data_after_order_details' , array( $this , 'add_customer_subscriptions_link' ) , 10 , 1 ) ;

		// Make orders of any status editable
		add_filter( 'wc_order_is_editable' , '__return_true' ) ;
	}

	/**
	 * Add meta boxes
	 *
	 * @since 2.2.22.9
	 */
	public function add_customer_subscriptions_link ( $order )
	{
		$user_data = get_userdata( $order->customer_user ) ;

		if ( WC_Subscriptions_Manager::user_has_subscription( $order->customer_user ) ) : ?>

		<p class="form-field form-field-wide"><a href="<?php echo admin_url( 'admin.php?page=subscriptions&_customer_user=' . $order->customer_user ) ; ?>"><?php echo sprintf( __( 'See %s\'s subscriptions' , 'woocommerce' ) , $user_data->display_name ) ; ?></a></p>
		
		<?php endif ;
	}

	/**
	 * Add meta boxes
	 *
	 * @since 2.2.9
	 */
	public function add_meta_boxes ( )
	{
		global $current_screen , $post_id ;

		// Only display the meta box if an order relates to a subscription
		if ( 'shop_order' == $current_screen->id )
		{
			$order_contains_subscription = WC_Subscriptions_Order::order_contains_subscription( $post_id ) ;

			if ( $order_contains_subscription || WC_Subscriptions_Renewal_Order::is_renewal( $post_id , array( 'order_role' => 'child' ) ) )
			{
				include( 'meta-boxes/class-wc-nab-meta-box-previous-gifts.php' ) ;

				add_meta_box( 'nab_previous_gifts' , __( 'Previous Gifts' , 'notanotherbill' ) , 'WC_Meta_Box_Previous_Gifts::output' , 'shop_order' , 'normal' , 'default' ) ;
			}
		}
	}
}

endif ;

return new WC_NAB_Admin_Order( ) ;