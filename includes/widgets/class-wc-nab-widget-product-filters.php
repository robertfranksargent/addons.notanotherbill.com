<?php
/**
 * Product Filters Widget
 *
 * @author 		Robert Sargent
 * @category 	Widgets
 * @package 	WooCommerce/Widgets
 * @version 	2.2.16.2
 * @extends 	WC_Widget
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

include_once( WC( )->plugin_path( ) . '/includes/abstracts/abstract-wc-widget.php' ) ;

class WC_NAB_Widget_Product_Filters extends WC_Widget
{
	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		$this->widget_cssclass = 'woocommerce widget_product_filters' ;
		$this->widget_description = __( 'A group of product filters.' , 'notanotherbill' ) ;
		$this->widget_id = 'nab_product_filters' ;
		$this->widget_name = __( 'NAB Product Filters' , 'notanotherbill' ) ;
		$this->settings = array(
			'title' => array(
				'type' => 'text',
				'std' => __( 'Filters' , 'notanotherbill' ) ,
				'label' => __( 'Title' , 'notanotherbill' )
			)
		) ;

		parent::__construct( ) ;
	}

	/**
	 * Widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	public function widget ( $args , $instance )
	{
		extract( $args ) ;

		global $wp_query , $post, $woocommerce , $product_cat ;

		$wc_permalinks = get_option( 'woocommerce_permalinks' ) ;
		$request_uri = explode( '?' , $_SERVER[ 'REQUEST_URI' ] ) ;
		$query_str = $_SERVER[ 'QUERY_STRING' ] ;
		$root_url = get_bloginfo( 'url' ) ;
		$shop_url = $root_url . $wc_permalinks[ 'product_base' ] ;

		if ( $wp_query->get( 'product_cat' ) == 'past-presents' )
		{
			$root_url .= '/past-presents/' ;

			if ( array_key_exists( 'filter' , $_GET ) )
			{
				$root_url .= '?filter=' . $_GET[ 'filter' ] ;
			}
		}
		else
		{
			if ( $product_cat )
			{
				$root_url .= '/' . $wc_permalinks[ 'category_base' ] . '/' . $product_cat ;
			}
			else
			{
				$root_url = $shop_url ;
			}
		}

		?>

		<?php if ( $wp_query->get( 'product_cat' ) == 'past-presents' ) : ?>

			<div class="product-filters panel panel-default">
				<div id="filters" class="panel-collapse collapse">
					<div class="row">
						<div class="col-lg-3">
							<ul>
								<li><a href="<?php echo $shop_url . '?orderby=date' ; ?>"><?php echo __( 'Latest' , 'notanotherbill' ) ; ?></a></li>
								<li><a href="<?php echo $shop_url ; ?>"><?php echo __( 'View All' , 'notanotherbill' ) ; ?></a></li>
								<?php

									include_once( WC_NAB_Addons( )->plugin_path( ) . '/includes/walkers/class-product-filter-cat-walker.php' ) ;

									$include_args = array(
										'for-him' ,
										'for-her'
									) ;

									foreach ( $include_args as $include_slug )
									{
										$category = get_term_by( 'slug' , $include_slug , 'product_cat' ) ;
										$include[ ] = $category->term_id ;
									}

									$args = array(
										'orderby' => 'name' ,
										'style' => 'list' ,
										'hide_empty' => false ,
										'taxonomy' => 'product_cat' ,
										'title_li' => '' ,
										'include' => isset( $include ) ? implode( ',' , $include ) : '' ,
										'walker' => new WC_NAB_Product_Filter_Cat_Walker ,
										'column' => 6
									) ;

									wp_list_categories( $args ) ;

								?>
							</ul>
						</div>
						<?php if ( $wp_query->get( 'product_cat' ) != 'past-presents' ) : ?>
							<div class="by-price col-lg-3">
								<h3><?php echo __( 'Price' , 'notanotherbill' ) ; ?></h3>
								<ul>
									<li<?php if ( isset( $_GET[ 'max_price' ] ) && $_GET[ 'max_price' ] == 20 ) echo ' class="current-cat"' ; ?>><a href="<?php echo $root_url . '?max_price=20' ; ?>"><?php printf( __( 'Under %s' , 'notanotherbill' ) , '£20' ) ; ?></a></li>
									<li<?php if ( isset( $_GET[ 'max_price' ] ) && $_GET[ 'max_price' ] == 30 ) echo ' class="current-cat"' ; ?>><a href="<?php echo $root_url . '?max_price=30' ; ?>"><?php printf( __( 'Under %s' , 'notanotherbill' ) , '£30' ) ; ?></a></li>
									<li<?php if ( isset( $_GET[ 'max_price' ] ) && $_GET[ 'max_price' ] == 50 ) echo ' class="current-cat"' ; ?>><a href="<?php echo $root_url . '?max_price=50' ; ?>"><?php printf( __( 'Under %s' , 'notanotherbill' ) , '£50' ) ; ?></a></li>
									<li<?php if ( isset( $_GET[ 'min_price' ] ) && $_GET[ 'min_price' ] == 50 ) echo ' class="current-cat"' ; ?>><a href="<?php echo $root_url . '?min_price=50' ; ?>"><?php printf( __( 'Over %s' , 'notanotherbill' ) , '£50' ) ; ?></a></li>
								</ul>
							</div>
						<?php endif ?>
						<div class="by-category col-lg-6">
							<h3><?php echo __( 'Category' , 'notanotherbill' ) ; ?></h3>
							<ul class="col-lg-6">
								<?php

									$exclude_slugs = array(
										'past-presents' ,
										'for-him' ,
										'for-her'
									) ;

									foreach ( $exclude_slugs as $exclude_slug )
									{
										$category = get_term_by( 'slug' , $exclude_slug , 'product_cat' ) ;
										$exclude[ ] = $category->term_id ;
									}

									$args = array(
										'orderby' => 'name' ,
										'style' => 'list' ,
										'hide_empty' => false ,
										'taxonomy' => 'product_cat' ,
										'title_li' => '' ,
										'exclude' => isset( $exclude ) ? implode( ',' , $exclude ) : '' ,
										'walker' => new WC_NAB_Product_Filter_Cat_Walker ,
										'column' => 6
									) ;

									wp_list_categories( $args ) ;

								?>
							</ul>
						</div>
					</div>
				</div>
				<a class="toggle" data-toggle="collapse" href="#filters">
					<span><?php echo apply_filters( 'widget_title' , $instance[ 'title' ] , $instance , $this->id_base ) ; ?></span>
					<span class="icon-arrow-down"></span>
				</a>
			</div>

		<?php endif ; ?>

		<?php
	}
}

register_widget( 'WC_NAB_Widget_Product_Filters' ) ;