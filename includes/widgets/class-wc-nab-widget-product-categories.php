<?php
/**
 * Product Categories Widget
 *
 * @author 		Robert Sargent
 * @category 	Widgets
 * @package 	WooCommerce/Widgets
 * @version 	2.2.4
 * @extends 	WC_Widget
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

include_once( WC( )->plugin_path( ) . '/includes/abstracts/abstract-wc-widget.php' ) ;

class WC_NAB_Widget_Product_Categories extends WC_Widget
{
	public $cat_ancestors ;
	public $current_cat ;

	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		$this->widget_cssclass = 'woocommerce widget_product_categories' ;
		$this->widget_description = __( 'A list or dropdown of product categories.' , 'notanotherbill' ) ;
		$this->widget_id = 'nab_product_categories' ;
		$this->widget_name = __( 'NAB Product Categories' , 'notanotherbill' ) ;
		$this->settings = array(
			'title'  => array(
				'type'  => 'text',
				'std'   => __( 'Product category' , 'woocommerce' ) ,
				'label' => __( 'Title' , 'woocommerce' )
			) ,
			'orderby' => array(
				'type'  => 'select' ,
				'std'   => 'name' ,
				'label' => __( 'Order by', 'woocommerce' ) ,
				'options' => array(
					'order' => __( 'Category Order' , 'woocommerce' ) ,
					'name'  => __( 'Name' , 'woocommerce' )
				)
			) ,
			'count' => array(
				'type'  => 'checkbox' ,
				'std'   => 0 ,
				'label' => __( 'Show post counts' , 'woocommerce' )
			) ,
			'hierarchical' => array(
				'type'  => 'checkbox' ,
				'std'   => 1 ,
				'label' => __( 'Show hierarchy' , 'woocommerce' )
			) ,
			'show_children_only' => array(
				'type'  => 'checkbox' ,
				'std'   => 0 ,
				'label' => __( 'Only show children of the current category' , 'woocommerce' )
			)
		) ;

		parent::__construct( ) ;
	}

	/**
	 * Widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	public function widget ( $args , $instance )
	{
		extract( $args ) ;

		global $wp_query , $post, $woocommerce ;

		$c = ! empty( $instance[ 'count' ] ) ;
		$h = ! empty( $instance[ 'hierarchical' ] ) ;
		$s = ! empty( $instance[ 'show_children_only' ] ) ;
		$o = $instance[ 'orderby' ] ? $instance['orderby'] : 'order' ;
		$list_args = array( 'show_count' => $c , 'hierarchical' => $h , 'taxonomy' => 'product_cat' , 'hide_empty' => false ) ;

		// Menu Order
		$list_args[ 'menu_order' ] = false ;

		if ( $o == 'order' )
		{
			$list_args[ 'menu_order' ] = 'asc' ;
		}
		else
		{
			$list_args[ 'orderby' ]    = 'title' ;
		}
		
		// Setup Current Category
		$this->current_cat = false ;
		$this->cat_ancestors = array( ) ;

		if ( is_tax( 'product_cat' ) )
		{
			$this->current_cat = $wp_query->queried_object ;
			$this->cat_ancestors = get_ancestors( $this->current_cat->term_id , 'product_cat' ) ;

		}
		elseif ( is_singular( 'product' ) )
		{
			$product_category = wc_get_product_terms( $post->ID , 'product_cat', array( 'orderby' => 'parent' ) ) ;

			if ( $product_category )
			{
				$this->current_cat = end( $product_category ) ;
				$this->cat_ancestors = get_ancestors( $this->current_cat->term_id , 'product_cat' ) ;
			}

		}
		
		// Show Siblings and Children Only
		if ( $s && $this->current_cat )
		{
			// Top level is needed
			$top_level = get_terms( 
				'product_cat' ,
				array( 
					'fields' => 'ids' ,
					'parent' => 0 , 
					'hierarchical' => true , 
					'hide_empty' => false
				) 
			);

			// Direct children are wanted
			$direct_children = get_terms( 
				'product_cat' ,
				array( 
					'fields' => 'ids' , 
					'parent' => $this->current_cat->term_id , 
					'hierarchical' => true , 
					'hide_empty' => false 
				) 
			);
			
			// Gather siblings of ancestors
			$siblings  = array( ) ;
			if ( $this->cat_ancestors )
			{
				foreach ( $this->cat_ancestors as $ancestor )
				{
					$ancestor_siblings = get_terms( 
						'product_cat' ,
						array( 
							'fields' => 'ids' , 
							'parent' => $ancestor , 
							'hierarchical' => false , 
							'hide_empty' => false 
						)
					) ;

					$siblings = array_merge( $siblings , $ancestor_siblings ) ;
				}
			}

			if ( $h ) {
				$include = array_merge( $top_level, $this->cat_ancestors, $siblings, $direct_children, array( $this->current_cat->term_id ) );
			} else {
				$include = array_merge( $direct_children );
			}
			
			$dropdown_args['include'] = implode( ',', $include );
			$list_args['include']     = implode( ',', $include );

			if ( empty( $include ) ) {
				return;
			}
			
		} 
		elseif ( $s )
		{
			$list_args[ 'depth' ] = 1 ;
			$list_args[ 'child_of' ] = 0 ;
			$list_args[ 'hierarchical' ] = 1 ;
		}

		echo $before_widget ;

		// List
		include_once( WC( )->plugin_path( ) . '/includes/walkers/class-product-cat-list-walker.php' ) ;

		$list_args[ 'walker' ] = new WC_Product_Cat_List_Walker ;
		$list_args[ 'title_li' ] = '' ;
		$list_args[ 'pad_counts'] = 1 ;
		$list_args[ 'show_option_none' ] = __( 'No product categories exist.' , 'woocommerce' ) ;
		$list_args[ 'current_category' ] = ( $this->current_cat ) ? $this->current_cat->term_id : '' ;
		$list_args[ 'current_category_ancestors' ] = $this->cat_ancestors ;

		?>

		<div class="dropdown">
			<a data-toggle="dropdown" href="#">
				<span><?php echo apply_filters( 'widget_title' , $instance[ 'title' ] , $instance , $this->id_base ) ; ?></span>
				<span class="icon-arrow-down"></span>
			</a>
			<ul class="product-categories dropdown-menu " role="menu" aria-labelledby="dLabel">
				<li class="cat-item">
					<a href="/shop/"><?php echo __( 'View all' , 'notanotherbill' ) ; ?></a>
				</li>

		<?php

		wp_list_categories( apply_filters( 'woocommerce_product_categories_widget_args' , $list_args ) ) ;

		?>

			</ul>
		</div>

		<?php

		echo $after_widget ;
	}
}

register_widget( 'WC_NAB_Widget_Product_Categories' ) ;