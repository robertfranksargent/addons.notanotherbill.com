<?php
/**
 * Product Categories Widget
 *
 * @author 		Robert Sargent
 * @category 	Widgets
 * @package 	WooCommerce/Widgets
 * @version 	2.2.4
 * @extends 	WC_Widget
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

include_once( WC( )->plugin_path( ) . '/includes/abstracts/abstract-wc-widget.php' ) ;

class WC_NAB_Widget_Product_Order_Options extends WC_Widget
{
	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		$this->widget_cssclass = 'woocommerce widget_product_order_options' ;
		$this->widget_description = __( 'A list or dropdown of order options.' , 'notanotherbill' ) ;
		$this->widget_id = 'nab_product_order_options' ;
		$this->widget_name = __( 'NAB Product Order Options' , 'notanotherbill' ) ;
		$this->settings = array(
			'title'  => array(
				'type'  => 'text',
				'std'   => __( 'Sort products by' , 'woocommerce' ),
				'label' => __( 'Title' , 'woocommerce' )
			)
		) ;

		parent::__construct( ) ;
	}

	/**
	 * Widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	public function widget ( $args , $instance )
	{
		extract( $args ) ;

		echo $before_widget ;

		?>

		<div class="dropdown">
			<a data-toggle="dropdown" href="#">
				<span><?php echo apply_filters( 'widget_title' , $instance[ 'title' ] , $instance , $this->id_base ) ; ?></span>
				<span class="icon-arrow-down"></span>
			</a>
			<ul class="product-categories dropdown-menu " role="menu" aria-labelledby="dLabel">

			<?php
				$catalog_orderby = apply_filters( 'woocommerce_catalog_orderby' ,
					array(
						'menu_order' => __( 'Default sorting' , 'woocommerce' ) ,
						'popularity' => __( 'Sort by popularity' , 'woocommerce' ) ,
						'rating' => __( 'Sort by average rating' , 'woocommerce' ) ,
						'date' => __( 'Sort by newness' , 'woocommerce' ) ,
						'price' => __( 'Sort by price: low to high' , 'woocommerce' ) ,
						'price-desc' => __( 'Sort by price: high to low' , 'woocommerce' )
					)
				) ;
				
				foreach ( $catalog_orderby as $id => $name ) :
			?>

				<li>
					<a href="?orderby=<?php echo $id ; ?>" title="<?php echo $name ; ?>"><?php echo $name ; ?></a>
				</li>

			<?php endforeach ; ?>
				
			</ul>

		</div>

		<?php

		echo $after_widget ;
	}
}

register_widget( 'WC_NAB_Widget_Product_Order_Options' ) ;