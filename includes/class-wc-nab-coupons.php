<?php

/**
 * Query
 *
 * @class 		WC_NAB_Coupons
 * @version		2.2.13.20
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_Coupons
{
	/**
	 * Constructor
	 */
	public function __construct ( )
	{
		add_action( 'init' , array( $this , 'maybe_save_mm_coupon_code' ) ) ;
		add_action( 'woocommerce_add_to_cart' , array( $this , 'maybe_apply_mm_coupon' ) ) ;
		add_action( 'woocommerce_payment_complete' , array( $this , 'remove_mm_coupon' ) ) ;
	}

	/**
	 * Remove the saved Mention Me coupon.
	 *
	 * @since 2.2.13.20
	 */
	public function remove_mm_coupon ( )
	{
		$session = WC( )->session ;

		// Check session
		if ( $session )
		{
			// Try to get coupon code
			$coupon_code = $session->get( 'mm_coupon_code' ) ;

			// Unset existing coupon code
			if ( $coupon_code && $coupon_code != '' )
			{
				$session->set( 'mm_coupon_code' , '' ) ;
			}
		}
	}

	/**
	 * Decide whether to apply a Mention Me coupon saved in the session.
	 *
	 * @since 2.2.13.20
	 */
	public function maybe_apply_mm_coupon ( )
	{
		$session = WC( )->session ;

		// Check session
		if ( $session )
		{
			// Get coupon code
			$coupon_code = $session->get( 'mm_coupon_code' ) ;

			// Check coupon code
			if ( $coupon_code )
			{
				// Get the coupon
				$the_coupon = new WC_Coupon( $coupon_code ) ;

				// Check the coupon exists
				if ( $the_coupon->id )
				{
					// Coupon exists, check if it's valid
					if ( $the_coupon->is_valid( ) )
					{
						// Coupon is valid, add it to cart
						WC( )->cart->add_discount( $coupon_code ) ; 
					}
				}
			}
		}
	}

	/**
	 * Decide whether to apply or save a Mention Me coupon passed through the query string.
	 *
	 * @since 2.2.13.20
	 */
	public function maybe_save_mm_coupon_code ( )
	{
		if ( ! WC( )->cart->coupons_enabled( ) ) return ;

		// Get URL coupon
		if ( array_key_exists( 'mm_coupon' , $_GET ) )
		{
			$coupon_code = $_GET[ 'mm_coupon' ] ;

			if ( $coupon_code && $coupon_code != '' )
			{
				// Sanitize coupon code
				$coupon_code = apply_filters( 'woocommerce_coupon_code' , $coupon_code ) ;

				// Get the coupon
				$the_coupon = new WC_Coupon( $coupon_code ) ;

				// Check the coupon exists
				if ( $the_coupon->id )
				{
					// Coupon exists, check if it's valid
					if ( $the_coupon->is_valid( ) )
					{
						// Coupon is valid, add it to cart
						WC( )->cart->add_discount( $coupon_code ) ; 
					}
					else
					{
						// Coupon is not valid but might be in future
						$session = WC( )->session ;

						// Save coupon in session
						if ( $session )
						{
							$session->set( 'mm_coupon_code' , $coupon_code ) ;

							wc_add_notice( sprintf( __( 'Coupon %s will be applied when an applicable item is added to the basket.' , 'notanotherbill' ) , $coupon_code ) ) ;
						}
					}
				}
				else
				{
					// Coupon doesn't exist
					$the_coupon->add_coupon_message( WC_Coupon::E_WC_COUPON_NOT_EXIST ) ;
					return ;
				}
			}
		}
	}
}

new WC_NAB_Coupons( ) ;