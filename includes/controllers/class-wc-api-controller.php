<?php
/**
 * API controller
 *
 * @author 		Robert Sargent
 * @category 	Admin
 * @package 	WooCommerce/Admin
 * @version     2.2.23.9
 */

/*
Controller name: WooCommerce
Controller description: Supporting utility methods
*/
class JSON_API_WC_Controller
{	
	/**
	* @since 2.2.21.4
	*/
	public function associate_orders ( )
	{
		if ( is_super_admin( ) && array_key_exists( 'order_id' , $_POST ) && ! empty( $_POST[ 'order_id' ] ) )
		{
			$order_id = $_POST[ 'order_id' ] ;
			$success = false ;

			require_once WP_PLUGIN_DIR . '/woocommerce-subscriptions/classes/class-wc-subscriptions-order.php' ;

			if ( WC_Subscriptions_Order::order_contains_subscription( $order_id ) )
			{
				require_once WP_PLUGIN_DIR . '/woocommerce-nab-addons/includes/class-wc-nab-subscriptions.php' ;

				$associated_order_ids = WC_NAB_Subscriptions::get_associated_subscription_orders( $order_id ) ;

				if ( is_array( $associated_order_ids ) && count( $associated_order_ids ) > 0 )
				{
					$success = update_post_meta( $order_id , '_associated_subscription_orders' , $associated_order_ids ) ;
				}
				else
				{
					$existing_meta = get_post_meta( $order_id , '_associated_subscription_orders' , true ) ;

					if ( $existing_meta )
					{
						$success = delete_post_meta( $order_id , '_associated_subscription_orders' , $existing_meta ) ;
					}
				}

				return array( 'meta_id' => $success ) ;
			}

			return array( 'Order does not contain subscription.' ) ;
		}

		return array( 'Not allowed.' ) ;
	}

	/**
	* @since 2.2.19.6
	*/
	public function add_review ( )
	{
		if ( ! is_user_logged_in( ) ) return array( 'status' => 'user-error' ) ;
		if ( ! array_key_exists( 'order_id' , $_POST ) ) return array( 'status' => 'order-id-error' ) ;
		if ( ! array_key_exists( 'product_id' , $_POST ) ) return array( 'status' => 'product-id-error' ) ;
		
		if ( ! array_key_exists( 'review' , $_POST ) || empty( $_POST[ 'review' ] ) )
		{
			return array( 
				'status' => 'review-error' ,
				'message' => __( 'Please enter your feedback.' , 'notanotherbill' )
			) ;
		}

		$order = new WC_Order( $_POST[ 'order_id' ] ) ;

		if ( empty( $order ) ) return array( 'status' => 'order-id-error' ) ;

		$factory = new WC_Product_Factory( ) ;
		$product = $factory->get_product( $_POST[ 'product_id' ] ) ;

		if ( empty( $product ) ) return array( 'status' => 'product-id-error' ) ;

		$sanitized = implode( "\n" , array_map( 'sanitize_text_field' , explode( "\n" , $_POST[ 'review' ] ) ) ) ;

		$comment_id = wp_insert_comment(
			array(
				'comment_post_ID' => $product->id ,
				'comment_date' => current_time( 'mysql' ) ,
				'comment_approved' => 1 ,
				'comment_content' => $sanitized ,
				'user_id' => get_current_user_id( ) ,
			)
		) ;

		if ( ! empty( $comment_id ) )
		{
			update_comment_meta( $comment_id , 'order' , $order->id ) ;
			update_comment_meta( $comment_id , 'feedback' , 1 ) ;

			return array( 'success' => true ) ;
		}
		else
		{
			return array( 'status' => 'Error inserting comment.' ) ;
		}
	}

	public function get_addresses ( )
	{
		require_once WP_PLUGIN_DIR . '/woocommerce-shipping-multiple-addresses/woocommerce-shipping-multiple-address.php' ;

		$wc_ship_multiple = new WC_Ship_Multiple( ) ;
		$addresses = $wc_ship_multiple->get_user_addresses( wp_get_current_user( ) ) ;

		return array( 'addresses' => $addresses ) ;
	}

	public function apply_coupon ( )
	{
		global $woocommerce ;

		$coupon_code = $_GET[ 'coupon_code' ] ;

		if ( $coupon_code != '' )
		{
			$success = $woocommerce->cart->add_discount( $coupon_code ) ;
		}

		return array( 'success' => $success ) ;
	}

	public function add_to_cart ( )
	{
		require_once WP_PLUGIN_DIR . '/woocommerce-subscriptions/woocommerce-subscriptions.php' ;

		global $woocommerce ;

		$product_id = $_POST[ 'product_id' ] ;
		$quantity = $_POST[ 'quantity' ] ;
		$variation_id = $_POST[ 'variation_id' ] ;
		$coupon_code = $_POST[ 'coupon_code' ] ;

		// Edit post data
		$data = array_diff_key( $_POST ,
			array(
				'product_id' => '' ,
				'quantity' => '' ,
				'variation_id' => ''
			)
		) ;

		WC_Subscriptions::maybe_empty_cart( true , $product_id , $quantity ) ;

		$success = $woocommerce->cart->add_to_cart( $product_id , $quantity , $variation_id , null , $data ) ;
		$url = $woocommerce->cart->get_cart_url( ) ;

		if ( $success && $coupon_code != '' )
		{
			$woocommerce->cart->add_discount( $coupon_code ) ;

			if ( $woocommerce->cart->get_total( ) == 0 )
			{
				$url = $woocommerce->cart->get_checkout_url( ) ;
			}
		}

		return array(
			'success' => $success ,
			'url' => $url
		) ;
	}

	/**
	* @since 2.2.18.8
	*/
	public function add_to_cart_renew ( )
	{
		global $woocommerce ;

		if ( ! is_user_logged_in( ) ) return array( 'status' => 'User is not logged in.' ) ;

		$original_order_id = $_POST[ 'original_order_id' ] ;
		$variation_id = $_POST[ 'variation_id' ] ;

		if ( ! empty( $variation_id ) && ! empty( $original_order_id ) )
		{
			$original_order = new WC_Order( $original_order_id ) ;

			// Check that current user owns this order
			if ( get_current_user_id( ) != $original_order->get_user( )->ID ) return array( 'status' => 'User does not own the original order.' ) ;

			// Get product and original subscription details
			$factory = new WC_Product_Factory( ) ;
			$product = $factory->get_product( $variation_id ) ;
			$original_subscription = WCS_Utils::get_order_subscription( $original_order ) ;
			$original_subscription_meta = new WC_Order_Item_Meta( $original_subscription[ 'item_meta' ] , $product ) ;
			$original_subscription_meta = $original_subscription_meta->meta ;

			if ( ! empty( $original_order ) && ! empty( $product ) )
			{
				$addons = get_product_addons( $product->id ) ;
				$data = array( ) ;

				// Build addons data
				foreach ( $addons as $addon )
				{
					switch ( $addon[ 'type' ] )
					{
						case 'select' :
							
							$original_value = $original_subscription_meta[ $addon[ 'name' ] ] ;

							// Get option value index
							foreach ( $addon[ 'options' ] as $key => $value )
							{
								if ( $value[ 'label' ] == $original_value[ 0 ] ) break ;
							}

							$data[ 'addon-' . $addon[ 'field-name' ] ] = sprintf( '%s-%d' , sanitize_title( $original_value[ 0 ] ) , $key + 1 ) ;

							break ;

						case 'checkbox' :
							
							$original_value = $original_subscription_meta[ $addon[ 'name' ] ] ;

							if ( count( $original_value ) )
							{
								if ( is_array( $original_value ) )
								{
									foreach ( $original_value as $key => $value )
									{
										$original_value[ $key ] = sanitize_title( $value ) ;
									}
								}

								$data[ 'addon-' . $addon[ 'field-name' ] ] = $original_value ;
							}

							break ;

						case 'custom' :

							$name = $addon[ 'name' ] ;
							$label = $addon[ 'options' ][ 0 ][ 'label' ] ;
							$key = sprintf( '%s - %s' , $name , $label ) ;
							$original_value = $original_subscription_meta[ $key ] ;
							$custom_value = '' ;

							if ( $name == 'Start Date' )
							{
								// Get next shipping date
								$shipping_dates = explode( ',' , get_option( 'nab-shipping-dates' ) ) ;

								// Sort the shipping dates in time order
								asort( $shipping_dates ) ;

								// Get next shipping date in the future
								foreach ( $shipping_dates as $shipping_date )
								{
									if ( strtotime( $shipping_date ) > time( ) )
									{
										$future_shipping_date = strtotime( $shipping_date ) ;
										break ;
									}
								}								
								
								// Future shipping date exists
								if ( isset( $future_shipping_date ) )
								{
									// Get time to future shipping date
									$time_to_shipping_date = $future_shipping_date - time( ) ;

									// First of the month of future shipping date
									$first_of_month = strtotime( date( 'Y-m-01 00:00:00' , $future_shipping_date ) ) ;

									// The current time is after the 1st of the future shipping date month and the future shipping date is more than 24 hours away
									if ( time( ) > $first_of_month && $time_to_shipping_date > 86400 )
									{
										// Renew this month
										$custom_value = date( 'Y-m-01' , time( ) ) ;
									}
								}
								
								// Default to the 1st of next month
								if ( empty( $custom_value ) )
								{
									$custom_value = date( 'Y-m-01' , strtotime( '+1 month' , time( ) ) ) ;
								}
							}
							elseif ( $name == 'First Present' )
							{
								$custom_value = 'No you choose' ;
							}
							else
							{
								$custom_value = $original_value[ 0 ] ;	
							}

							$data[ 'addon-' . $addon[ 'field-name' ] ][ sanitize_title( $label ) ] = $custom_value ;

							break ;
					}
				}

				// Add attributes to post data
				$data = array_merge( $data , $product->get_variation_attributes( ) ) ;
				$_POST = $data ;

				// Empty the cart of subscriptions
				WC_Subscriptions::maybe_empty_cart( true , $product->parent->id , 1 ) ;

				// Add the new subscription to the cart
				$cart_item_hash = $woocommerce->cart->add_to_cart( $product->parent->id , 1 , $variation_id , null , $data ) ;
				$url = $woocommerce->cart->get_checkout_url( ) ;

				// Save cart item key
				update_post_meta( $original_order_id , '_renew_cart_item_hash' , $cart_item_hash ) ;

				return array(
					'success' => ! empty( $cart_item_hash ) ,
					'url' => $url
				) ;
			}
		}

		return array( 'success' => false ) ;
	}

	public function get_current_user_meta ( )
	{
		if ( ! is_user_logged_in( ) ) return array( 'status' => 'User is not logged in.' ) ;

		return array( 'user_meta' => array_map( function( $a ) { return $a[ 0 ] ; } , get_user_meta( get_current_user_id( ) ) ) ) ;
	}

	public function get_coupon ( )
	{
		$coupon = new WC_Coupon( $_GET[ 'coupon_code' ] ) ;
		$valid = count( $coupon->product_ids ) > 0 ;
		$data = array( 'valid' => $valid ) ;

		if ( $valid ) $data[ 'coupon' ] = $coupon ;

		return $data ;
	}

	public function update_product_rating ( )
	{
		if ( ! is_user_logged_in( ) ) return array( 'status' => 'Not logged in.' ) ;

		if ( array_key_exists( 'order_id' , $_POST ) ) $order_id = $_POST[ 'order_id' ] ;

		$product_id = $_POST[ 'product_id' ] ;
		$rating = $_POST[ 'rating' ] ;

		if ( ! $product_id ) return array( 'status' => 'Product ID required.' ) ;
		if ( ! $rating ) return array( 'status' => 'Rating required.' ) ;

		$comments = get_comments( 
			array(
				'post_id' => $product_id ,
				'meta_key' => 'rating' ,
				'user_id' => get_current_user_id( )
			)
		) ;

		// Update existing rating or create new rating
		if ( sizeof( $comments ) )
		{
			$comment = $comments[ 0 ] ;

			$success = wp_update_comment(
				array(
					'comment_ID' => $comment->comment_ID ,
					'comment_content' => sprintf( __( 'Customer rated product %d out of 5.' ) , $rating )
				)
			) ;

			if ( $success )
			{
				update_comment_meta( $comment->comment_ID , 'rating' , $rating ) ;

				if ( isset( $order_id ) )
				{
					update_comment_meta( $comment->comment_ID , 'order' , $order_id ) ;
				}

				return array( 'success' => true ) ;
			}
		}
		else
		{
			$comment_id = wp_insert_comment(
				array(
					'comment_post_ID' => $product_id ,
					'comment_date' => current_time( 'mysql' ) ,
					'comment_approved' => 1 ,
					'comment_content' => sprintf( __( 'Customer rated product %d out of 5.' ) , $rating ) ,
					'user_id' => get_current_user_id( ) ,
				)
			) ;

			if ( $comment_id )
			{
				add_comment_meta( $comment_id , 'rating' , $rating ) ;

				if ( isset( $order_id ) )
				{
					add_comment_meta( $comment_id , 'order' , $order_id ) ;
				}

				return array( 'success' => true ) ;
			}
		}
	}

	/**
	* @since 2.2.22
	*/
	public function update_order_item_meta ( )
	{
		if ( ! is_user_logged_in( ) ) return array( 'status' => 'Not logged in.' ) ;

		$order_id = $_POST[ 'order_id' ] ;

		if ( ! isset( $order_id ) || empty( $order_id ) ) return array( 'status' => 'Order ID not defined.' ) ;

		$order = new WC_Order( $order_id ) ;

		if ( get_current_user_id( ) != $order->get_user_id( ) )
		{
			$order_items = $order->get_items( ) ;

			// Find item with recipient email address
			foreach ( $order_items as $order_item_id => $order_item )
			{
				$recipient_email = wc_get_order_item_meta( $order_item_id , 'Recipient Email Address - Text' , true ) ;

				if ( ! empty( $recipient_email ) ) break ;
			}

			$recipient_user = get_user_by( 'email' , $recipient_email ) ;

			if ( get_current_user_id( ) != $recipient_user->ID )
			{
				return array( 'status' => 'You do not have permission to do that.' ) ;
			}
		}

		require_once WP_PLUGIN_DIR . '/woocommerce-subscriptions/classes/class-wc-subscriptions-order.php' ;

		$product_id = $_POST[ 'product_id' ] ;
		$key = $_POST[ 'key' ] ;
		$value = $_POST[ 'value' ] ;

		$item_id = WC_Subscriptions_Order::get_item_id_by_subscription_key( $order_id . '_' . $product_id ) ;
		$meta_id = wc_update_order_item_meta( $item_id , $key , $value ) ;

		return array(
			'success' => true ,
			'meta' => $meta_id
		) ;
	}

	/**
	* @since 2.2.22
	*/
	public function save_order_meta ( )
	{
		if ( ! is_user_logged_in( ) ) return array( 'status' => 'Not logged in.' ) ;

		$order_id = $_POST[ 'order_id' ] ;

		if ( ! isset( $order_id ) || empty( $order_id ) ) return array( 'status' => 'Order ID not defined.' ) ;

		$order = new WC_Order( $order_id ) ;

		if ( get_current_user_id( ) != $order->get_user_id( ) )
		{
			$order_items = $order->get_items( ) ;

			// Find item with recipient email address
			foreach ( $order_items as $order_item_id => $order_item )
			{
				$recipient_email = wc_get_order_item_meta( $order_item_id , 'Recipient Email Address - Text' , true ) ;

				if ( ! empty( $recipient_email ) ) break ;
			}

			$recipient_user = get_user_by( 'email' , $recipient_email ) ;

			if ( get_current_user_id( ) != $recipient_user->ID )
			{
				return array( 'status' => 'You do not have permission to do that.' ) ;
			}
		}

		require_once WP_PLUGIN_DIR . '/woocommerce-subscriptions/classes/class-wc-subscriptions-order.php' ;

		$product_id = $_POST[ 'product_id' ] ;
		$key = $_POST[ 'key' ] ;
		$value = $_POST[ 'value' ] ;

		$item_id = WC_Subscriptions_Order::get_item_id_by_subscription_key( $order_id . '_' . $product_id ) ;
		$item_meta = wc_get_order_item_meta( $item_id , $key , false ) ;

		$exists = array_search( $value , $item_meta ) ;

		if ( $exists === false )
		{
			$meta_id = wc_add_order_item_meta( $item_id , $key , $value ) ;

			return array(
				'success' => true ,
				'meta' => $meta_id
			) ;
		}
		else
		{
			return array(
				'success' => false
			) ;
		}
	}

	/**
	* @since 2.2.22
	*/
	public function update_order_meta ( )
	{
		if ( ! is_user_logged_in( ) ) return array( 'status' => 'Not logged in.' ) ;

		$order_id = $_POST[ 'order_id' ] ;

		if ( ! isset( $order_id ) || empty( $order_id ) ) return array( 'status' => 'Post ID not defined.' ) ;

		$order = new WC_Order( $order_id ) ;

		if ( get_current_user_id( ) != $order->get_user_id( ) )
		{
			$order_items = $order->get_items( ) ;

			// Find item with recipient email address
			foreach ( $order_items as $order_item_id => $order_item )
			{
				$recipient_email = wc_get_order_item_meta( $order_item_id , 'Recipient Email Address - Text' , true ) ;

				if ( ! empty( $recipient_email ) ) break ;
			}

			$recipient_user = get_user_by( 'email' , $recipient_email ) ;

			if ( get_current_user_id( ) != $recipient_user->ID )
			{
				return array( 'status' => 'You do not have permission to do that.' ) ;
			}
		}

		$errors = array( ) ;
		$fields = $_POST[ 'fields' ] ;

		foreach ( $fields as $key => $value )
		{
			$current_value = get_post_meta( $order_id , '_' . $key , true ) ;

			if ( $value == $current_value ) continue ;
			
			if ( ! update_post_meta( $order_id , '_' . $key , $value ) )
			{
				$errors[ ] = $key ;
			}
		}

		if ( count( $errors ) > 0 )
		{
			return array(
				'success' => false ,
				'errors' => json_encode( $errors )
			) ;
		}
		else
		{
			global $woocommerce ;

			$woocommerce->mailer( ) ;
			
			do_action( 'woocommerce_subscription_meta_update_notification' , $order_id , $fields ) ;

			return array(
				'success' => true
			) ;
		}
	}

	/**
	* @since 2.2.22
	*/
	public function delete_order_meta ( )
	{
		if ( ! is_user_logged_in( ) ) return array( 'status' => 'Not logged in.' ) ;

		$order_id = $_POST[ 'order_id' ] ;

		if ( ! isset( $order_id ) || empty( $order_id ) ) return array( 'status' => 'Order ID missing.' ) ;

		$order = new WC_Order( $order_id ) ;

		if ( get_current_user_id( ) != $order->get_user_id( ) )
		{
			$order_items = $order->get_items( ) ;

			// Find item with recipient email address
			foreach ( $order_items as $order_item_id => $order_item )
			{
				$recipient_email = wc_get_order_item_meta( $order_item_id , 'Recipient Email Address - Text' , true ) ;

				if ( ! empty( $recipient_email ) ) break ;
			}

			$recipient_user = get_user_by( 'email' , $recipient_email ) ;

			if ( get_current_user_id( ) != $recipient_user->ID )
			{
				return array( 'status' => 'You do not have permission to do that.' ) ;
			}
		}

		require_once WP_PLUGIN_DIR . '/woocommerce-subscriptions/classes/class-wc-subscriptions-order.php' ;

		$product_id = $_POST[ 'product_id' ] ;
		$key = $_POST[ 'key' ] ;
		$value = $_POST[ 'value' ] ;

		$item_id = WC_Subscriptions_Order::get_item_id_by_subscription_key( $order_id . '_' . $product_id ) ;

		return array(
			'success' => wc_delete_order_item_meta( $item_id , $key , $value )
		) ;
	}

	public function cart_total ( )
	{
		return array(
			'success' => true  ,
			'amount' => WC( )->cart->get_total( ) ,
			'count' => WC( )->cart->cart_contents_count
		) ;
	}

	public function cart_subtotal_html ( )
	{
		WC( )->cart->calculate_shipping( ) ;

		return array(
			'count' => WC( )->cart->cart_contents_count ,
			'html' => wc_price( WC( )->cart->cart_contents_total - WC( )->cart->discount_total + WC( )->cart->shipping_total )
		) ;
	}

	public function product_subtotal_html ( )
	{
		if ( ! empty( $_POST[ 'product' ] )  )
		{
			$quantity = ! empty( $_POST[ 'quantity' ] ) ? $_POST[ 'quantity' ] : 1 ;
			$product = new WC_Product( $_POST[ 'product' ] ) ;

			return array(
				'html' => WC( )->cart->get_product_subtotal( $product , $quantity ) 
			) ;
		}
	}

	public function geoip ( )
	{
		if ( ! empty( $_SERVER[ 'HTTP_CLIENT_IP' ] ) )
		{
			$ip = $_SERVER[ 'HTTP_CLIENT_IP' ] ;
		}
		elseif ( ! empty( $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] ) )
		{
			$ip = $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] ;
		}
		else
		{
			$ip = $_SERVER[ 'REMOTE_ADDR' ] ;
		}

		$ch = curl_init( ) ;
		curl_setopt( $ch , CURLOPT_URL , 'http://ipinfo.io/' . $ip ) ;
		curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 ) ;
		$output = curl_exec( $ch ) ;
		curl_close( $ch ) ;

		$output = json_decode( $output ) ;

		// Fallback
		if ( empty( $output ) || empty( $output->country ) )
		{
			$output->country = WC( )->customer->get_default_country( ) ;
		}

		$countries = WC( )->countries->get_shipping_countries( ) ;

		return array( 'country_code' => $output->country , 'country' => $countries[ $output->country ] ) ;
	}

	public function shipping_countries ( )
	{
		// Get all shipping countries
		$countries = WC( )->countries->get_shipping_countries( ) ;

		if ( array_key_exists( 'product_id' , $_GET ) )
		{
			// Get product object
			$product_factory = new WC_Product_Factory( ) ;
			$product = $product_factory->get_product( $_GET[ 'product_id' ] ) ;

			if ( $product )
			{
				// Get shipping zone attribute
				$atts = $product->get_variation_attributes( ) ;

				if ( $atts && array_key_exists( 'attribute_pa_shipping-zone' , $atts ) )
				{
					global $wpdb ;

					// Create shipping zone
					$shipping_zone = new WC_Shipping_Zone( $atts[ 'attribute_pa_shipping-zone' ] ) ;

					// Special condition for null shipping zone
					if ( $shipping_zone->zone_id == 0 )
					{
						// Get shipping zone locations
						$locations = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_shipping_zone_locations;" ) ) ;

						if ( $locations )
						{
							// Filter shipping zone locations array by "country" location type
							$locations = array_filter( $locations , function ( $location ) { return $location->location_type == 'country' ; } ) ;

							// Simplify shipping zone locations array
							$locations = array_map( function ( $location ) { return $location->location_code ; } , $locations ) ;

							// Filter countries array by shipping zone locations
							$countries = array_diff_key( $countries , array_flip( $locations ) ) ;
						}
					}
					else
					{
						// Get shipping zone locations
						$locations = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_shipping_zone_locations WHERE zone_id = %s;" , $shipping_zone->zone_id ) ) ;

						if ( $locations )
						{
							// Filter shipping zone locations array by "country" location type
							$locations = array_filter( $locations , function ( $location ) { return $location->location_type == 'country' ; } ) ;

							// Simplify shipping zone locations array
							$locations = array_map( function ( $location ) { return $location->location_code ; } , $locations ) ;

							// Filter countries array by shipping zone locations
							$countries = array_intersect_key( $countries , array_flip( $locations ) ) ;
						}
					}
				}
			}
		}

		// if ( $shipping_zone->zone_id == 0 )
		// $countries = array_diff_key( $countries , array_flip( $locations ) ) ;

		// Return list of countries
		return array( 'shipping_countries' => $countries ) ;
	}

	public function shipping_dates ( )
	{
		return array(
			'shipping_dates' => explode( ',' , get_option( 'nab-shipping-dates' ) ) ,
			'shipping_message' => get_option( 'nab-shipping-message' ) ,
			'shipping_default_message' => get_option( 'nab-shipping-default-message' ) ,
			'today' => date( 'Y-m-d' )
		) ;
	}

	public function shipping_calculator ( )
	{
		if ( isset( $_POST[ 'country' ] ) )
		{
			$country = wc_clean( $_POST[ 'country' ] ) ;

			WC( )->shipping->reset_shipping( ) ;
			WC( )->customer->set_country( $country ) ;
			WC( )->customer->set_shipping_country( $country ) ;
			WC( )->customer->calculated_shipping( true ) ;
			WC( )->cart->calculate_shipping( ) ;
		}

		return array( 'shipping_total' => get_woocommerce_currency_symbol( ) . WC( )->cart->shipping_total ) ;
	}

	/**
	* @since 2.2.23
	*/
	public function subscription_prices ( )
	{
		require_once WP_PLUGIN_DIR . '/woocommerce-nab-addons/includes/utils/class-wcs-utils.php' ;
		require_once WP_PLUGIN_DIR . '/woocommerce-subscriptions/woocommerce-subscriptions.php' ;

		$variations = WCS_Utils::get_subscription_variations( ) ;

		$shipping_zone = woocommerce_get_shipping_zone(
			array( 
				'destination' => array(
					'country' => $_GET[ 'country' ] ,
					'state' => '' ,
					'postcode' => ''
				)
			)
		) ;

		$product_factory = new WC_Product_Factory( ) ;
		$currency_symbol = get_woocommerce_currency_symbol( ) ;
		$output ;

		foreach ( $variations as $variation )
		{
			if ( $variation[ 'attributes' ][ 'attribute_pa_shipping-zone' ] == $shipping_zone->zone_id )
			{
				$product = $product_factory->get_product( $variation[ 'variation_id' ] ) ;

				switch ( $product->subscription_period )
				{
					case 'month' :
						$interval = $product->subscription_period_interval ;
						break ;

					case 'year' :
						$interval = $product->subscription_period_interval * 12 ;
						break ;
				}

				$output[ ] = array(
					'id' => $variation[ 'variation_id' ] ,
					'zone' => $shipping_zone->zone_id ,
					'plan' => $variation[ 'attributes' ][ 'attribute_pa_plan' ] ,
					'price' => sprintf( get_woocommerce_price_format( ) , $currency_symbol , $product->price ) ,
					'monthly' => sprintf( get_woocommerce_price_format( ) , $currency_symbol ,  round( $product->price / $interval ) )
				) ;
			}
		}

		return array( 'subscription_prices' => $output ) ;
	}

	public function pause_subscription ( )
	{
		if ( array_key_exists( 'subscription_key' , $_POST ) && array_key_exists( 'months' , $_POST ) )
		{
			$subscription_key = $_POST[ 'subscription_key' ] ;
			$months = $_POST[ 'months' ] ;

			$order_and_item_id = explode( '_' , $subscription_key ) ;
			$order_id = $order_and_item_id[ 0 ] ;
			$order = new WC_Order( $order_id ) ;

			if ( $order->user_id )
			{
				$reactivation_date = gmdate( 'U' , strtotime( '+' . $months . ' month' . ( $months > 1 ? 's' : '' ) ) ) ;

				wc_schedule_single_action( $reactivation_date , 'scheduled_subscription_reactivation' , array( 'user_id' => $order->user_id , 'subscription_key' => $subscription_key ) ) ;	

				$action_link = add_query_arg( array( 'subscription_key' => $subscription_key , 'change_subscription_to' => 'on-hold' ) , $_POST[ 'url' ] ) ;
				$action_link = wp_nonce_url( $action_link , $subscription_key ) ;

				return array( 
					'success' => true ,
					'action_link' => $action_link
				) ;
			}
		}

		return array( 'success' => false ) ;
	}

	/**
	* @since 2.2.22
	*/
	public function send_verification_email ( )
	{
		if ( ! get_current_user_id( ) ) return array( 'status' => 'You must be logged in to do that.' ) ;

		$current_user = wp_get_current_user( ) ;
		$current_user_verified = get_user_meta( $current_user_id , 'user_verified' , true ) ;

		if ( $current_user_verified ) return array( 'status' => 'This email address has already been verified.' ) ;

		$key = wp_generate_password( 30 , false ) ;
		$key_meta_id = update_user_meta( $current_user->ID , 'user_verification_key' , $key , false ) ;

		if ( $key_meta_id )
		{
			$expiry = time( ) + 86400 ;
			$expiry_meta_id = update_user_meta( $current_user->ID , 'user_verification_expiry' , $expiry , false ) ;

			if ( $expiry_meta_id )
			{
				$user_login = $current_user->user_login ;
				$mailer = WC( )->mailer( ) ;

				do_action( 'woocommerce_account_verification_notification' , $user_login , $key ) ;

				return array(
					'success' => true
				) ;
			}
		}

		return array( 'status' => '' ) ;
	}

	/**
	* @since 2.2.23.9
	*/
	public function next_shipping_options ( )
	{
		// Get the current month's shipping date

		// Get options
		$disable_send_today = get_option( 'nab-shipping-disable-send-today' ) == 'yes' ;
		$shipping_dates = explode( ',' , get_option( 'nab-shipping-dates' ) ) ;
		$fallback_day = get_option( 'nab-shipping-fallback-day' ) ;
		$current_ym_date = date( 'Y-m' ) ;
		$today = time( ) ;
		$send_today = false ;

		// Get closest future shipping date
		foreach ( $shipping_dates as $shipping_date )
		{
			$shipping_ym_date = date( 'Y-m' , strtotime( $shipping_date ) ) ;

			if ( $shipping_ym_date == $current_ym_date )
			{
				$current_shipping_date = $shipping_date ;
				break ;
			}
		}

		if ( ! isset( $current_shipping_date ) && $fallback_day )
		{
			// Get fallback date if no future shipping date exists
			$current_shipping_date = date( 'Y-m-' . $fallback_day ) ;
		}

		if ( isset( $current_shipping_date ) )
		{
			$end_of_month_date = date( 'Y-m-t' ) ;

			if ( ! $disable_send_today && $today > strtotime( $current_shipping_date ) && $today < strtotime( $end_of_month_date ) )
			{
				// If send today is enabled and today is after this month's shipping date but before the end of the month
				$next_shipping_option_date = date( 'Y-m-d' ) ;
				$next_shipping_option_name = __( 'Today' , 'notanotherbill' ) ;
				$send_today = true ;
			}
			else if ( $today < strtotime( $current_shipping_date ) )
			{
				// If today is before this month's shipping date
				$this_month = strtotime( $current_shipping_date ) ;
				$next_shipping_option_date = date( 'Y-m-01' , $this_month ) ;
				$next_shipping_option_name = date( 'F' , $this_month ) ;
			}
			else
			{
				$next_month = strtotime( '+1 month' , strtotime( $current_shipping_date ) ) ;
				$next_shipping_option_date = date( 'Y-m-01' , $next_month ) ;
				$next_shipping_option_name = date( 'F' , $next_month ) ;
			}
		}
		else if ( ! $disable_send_today )
		{
			$next_shipping_option_date = date( 'Y-m-d' ) ;
			$next_shipping_option_name = __( 'Today' , 'notanotherbill' ) ;
			$send_today = true ;
		}
		else
		{
			// Get the first of next month from today's date
			$first_of_this_month = date( 'Y-m-01' , $today ) ;
			$first_of_next_month = strtotime( '+1 month' , strtotime( $first_of_this_month ) ) ;
			$next_shipping_option_date = date( 'Y-m-d' , $first_of_next_month ) ;
			$next_shipping_option_name = date( 'F' , $first_of_next_month ) ;
		}

		$options = sprintf( '<option value="%s">%s</option>' , $next_shipping_option_date , $next_shipping_option_name ) ;

		for ( $i = 1 ; $i < 4 ; $i++ )
		{
			$option_date = strtotime( '+' . $i . ' month' , strtotime( $next_shipping_option_date ) ) ;
			$options .= sprintf( '<option value="%s-01">%s</option>' , date( 'Y-m' , $option_date ) , date( 'F' , $option_date ) ) ;
		}

		return array( 'options' => $options , 'send_today' => $send_today ) ;
	}
}