<?php

/**
 * Conversion
 *
 * @class 		WC_NAB_Conversion
 * @version		2.2.23.15
 * @package		WooCommerce/Classes
 * @category	Class
 * @author 		Robert Sargent
 */

if ( ! defined( 'ABSPATH' ) ) exit ; // Exit if accessed directly

class WC_NAB_Conversion
{
	/**
	 * Constructor
	 *
	 * @since 2.2.23.15
	 */
	public function __construct ( )
	{
		add_action( 'wp_head' , array( $this , 'insert_fb_pixel' ) , 10 ) ;
		add_action( 'wp_head' , array( $this , 'insert_criteo_tag' ) , 10 ) ;
		add_action( 'wp_footer' , array( $this , 'insert_adwords_tag' ) , 10 ) ;
		add_action( 'wp_footer' , array( $this , 'insert_remarketing_tag' ) , 10 ) ;
		add_action( 'user_register' , array( $this , 'track_user_registration' ) , 10 , 1 ) ;
		add_action( 'woocommerce_thankyou' , array( $this , 'track_purchase' ) , 10 , 1 ) ;
	}

	/**
	* @since 2.2.23.12
	*/
	public function insert_fb_pixel ( )
	{
		if ( is_dev( ) || is_staging( ) ) return ; ?>

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '1089893137748731');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=1089893137748731&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->

		<?php
		
		// Product
		if ( is_product( ) )
		{
			$product_id = get_the_ID( ) ;

			$args = array(
				'content_type' => 'product' ,
				'content_name' => get_the_title( ) ,
				'content_ids' => array( $product_id )
			) ;

			$product_cats = get_the_terms( $product_id , 'product_cat' ) ;

			if ( $product_cats )
			{
				$product_cat = $product_cats[ 0 ] ;
				$args[ 'content_category' ] = $product_cat->name ;
			}

			$this->fb_track_event( 'ViewContent' , $args ) ;
		}

		// Subscribe
		if ( is_subscribe( ) )
		{
			$args = array(
				'content_name' => 'Subscribe'
			) ;

			$this->fb_track_event( 'Lead' , $args ) ;
		}

		// Checkout
		if ( is_checkout( ) )
		{
			$cart_item_ids = array( ) ;

			foreach ( WC( )->cart->get_cart( ) as $cart_item )
			{
				$cart_item_ids[ ] = $cart_item[ 'product_id' ] ;
			}

			$args = array(
				'content_ids' => $cart_item_ids ,
				'num_items' => WC( )->cart->cart_contents_count
			) ;

			$this->fb_track_event( 'InitiateCheckout' , $args ) ;
		}

		// Search
		if ( is_search( ) )
		{
			global $wp_query ;

			foreach ( $wp_query->posts as $post )
			{
				$content_ids[ ] = $post->ID ;
			}

			$args = array(
				'search_string' => esc_html( get_search_query( false ) ) ,
				'content_category' => 'Product Search' ,
				'content_ids' => $content_ids
			) ;

			$this->fb_track_event( 'Search' , $args ) ;
		}
	}

	/**
	* @since 2.2.23.15
	*/
	public function insert_remarketing_tag ( )
	{
		if ( is_dev( ) || is_staging( ) ) return ; ?>

			<!-- Google Code for Remarketing Tag -->
			<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 880376238;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			/* ]]> */
			</script>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
			</script>
			<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/880376238/?value=0&amp;guid=ON&amp;script=0"/>
			</div>
			</noscript>
			
		<?php
	}

	/**
	* @since 2.2.23.15
	*/
	public function insert_adwords_tag ( )
	{
		if ( is_dev( ) || is_staging( ) ) return ;

		?>
			<!-- Google Adwords Tag -->
		<?php

		global $woocommerce ;

		$woo_prefix = '' ; /* Merchant center prefix for products */
		$is_sku = false ;	/* If sku is used in merchant center change $is_sku variable to true */

		// Check if is homepage
		if ( is_front_page( ) )
		{
			?>
			<script type="text/javascript">
				var google_tag_params = {
					ecomm_pagetype : 'home'
				} ;
			</script>
			<?php
		} // Check if it is a product category page and set the category parameters.
		elseif ( is_product_category( ) )
		{
			?>
			<script type="text/javascript">
				var google_tag_params = {
					ecomm_pagetype : 'category'
				} ;
			</script>
			<?php
		}

		// Check if it a search results page and set the searchresults parameters.
		elseif ( is_search( ) )
		{
			?>
			<script type="text/javascript">
				var google_tag_params = {
					ecomm_pagetype : 'searchresults'
				} ;
			</script>
			<?php
		}

		// Check if it is a product page and set the product parameters.
		elseif ( is_product( ) )
		{
			$product_id = get_the_ID( ) ;
			$product = wc_get_product( $product_id ) ;

			if ( $is_sku ) 
			{
				$product_id	= $woo_prefix . $product->get_sku( ) ;
			}
			else
			{
				$product_id	= $woo_prefix . $product->id ;
			}

			$price = $product->get_price( ) ;
		?>
		<script type="text/javascript">
			var google_tag_params = {
				ecomm_prodid : '<?php echo $product_id ; ?>' ,
				ecomm_pagetype : 'product' ,
				ecomm_totalvalue : <?php echo $price ; ?>
			};
		</script>
		<?php

		}
		// Check if it is the cart page and set the cart parameters.
		elseif ( is_cart( ) )
		{
			$cartprods = $woocommerce->cart->get_cart( ) ;
			$cartprods_items = array( ) ;

			foreach ( ( array ) $cartprods as $entry )
			{
				if ( $is_sku )
				{
					$sku = $entry[ 'data' ]->get_sku( ) ;
					//echo "SKU is ".$woo_prefix.$sku;
					array_push( $cartprods_items , "'" . $woo_prefix . $sku . "'" ) ;
				}
				else
				{
					array_push( $cartprods_items , "'" . $woo_prefix . $entry[ 'product_id' ] . "'" ) ;
				}
			}
			?>
			<script type="text/javascript">
				var google_tag_params = {
					ecomm_prodid : <?php echo '[' .implode( ',' , $cartprods_items ) . ']' ; ?> ,
					ecomm_pagetype : 'cart' ,
					ecomm_totalvalue : <?php echo $woocommerce->cart->cart_contents_total ; ?>
				} ;
			</script>
			<?php
		}

		// Check if it the order received page and set the according parameters
		elseif ( is_order_received_page( ) )
		{
			$order_key = $_GET[ 'key' ] ;
			$order = new WC_Order( wc_get_order_id_by_order_key( $order_key ) ) ;
			$order_total = $order->get_total( ) ;

			// Only run conversion script if the payment has not failed. (has_status('completed') is too restrictive)
			// And use the order meta to check if the conversion code has already run for this order ID. If yes, don't run it again.
			if ( ! $order->has_status( 'failed' ) )
			{
				$order_items = $order->get_items( ) ;
				$order_items_array = array( ) ;

				foreach ( ( array ) $order_items as $item )
				{
					if ( $is_sku )
					{
						$product = new WC_Product( $item[ 'product_id' ] ) ;
						$sku = $product->get_sku( ) ;
						array_push( $order_items_array , "'" . $woo_prefix . $sku . "'" ) ;
					}
					else
					{
						array_push( $order_items_array , "'" . $woo_prefix . $item[ 'product_id' ] . "'" ) ;
					}
				}

				?>

				<script type="text/javascript">
					var google_tag_params = {
						ecomm_prodid : <?php echo '[' . implode( ',' , $order_items_array ) . ']' ; ?> ,
						ecomm_pagetype : 'purchase' ,
						ecomm_totalvalue : <?php echo $order_total ; ?>

					} ;
				</script>

				<script>
					ga( 'require' , 'ecommerce' ) ;
					ga( 'ecommerce:addTransaction' , {
					  'id' : '<?php echo $order->id ; ?>' ,
					  'affiliation' : '<?php echo get_bloginfo( 'name' ) ; ?>' ,
					  'revenue' : '<?php echo $order->get_total( ) ; ?>',
					  'shipping' : '<?php echo $order->get_total_shipping( ) ; ?>' ,
					  'tax' : '<?php echo $order->get_total_tax( ) ; ?>'
					} ) ;
					<?php
						foreach ( ( array ) $order_items as $item )
						{
							$product = new WC_Product( $item[ 'product_id' ] ) ;
							$product_cats = get_the_terms( $item[ 'product_id' ] , 'product_cat' ) ;

							if ( $product_cats ) $product_cat = $product_cats[ 0 ] ;
					?>
					ga( 'ecommerce:addItem' , {
					  'id' : '<?php echo $order->id ; ?>' ,
					  'name' : '<?php echo $item[ 'name' ] ; ?>' ,
					  'sku' : '<?php echo $product->get_sku( ) ; ?>' ,
					  'category' : '<?php if ( ! empty( $product_cat ) ) echo $product_cat->name ; ?>' ,
					  'price' : '<?php echo $item[ 'line_total' ] ; ?>' ,
					  'quantity' : '<?php echo $item[ 'qty' ] ; ?>'
					} ) ;
					<?php } ?>
					ga( 'ecommerce:send' ) ;
				</script>
				<?php
			} // end if order status
		}

		// For all other pages set the parameters for other.
		else {
			?>
			<script type="text/javascript">
				var google_tag_params = {
					ecomm_pagetype : 'other'
				} ;
			</script>
			<?php
		}
	}

	/**
	* @since 2.2.17.12
	*/
	public function insert_criteo_tag ( $confirm_order_id = null )
	{
		// Home, shop, product, subscribe and basket only unless order confirmation ID present
		if ( empty( $confirm_order_id )
			&& ! is_front_page( )
			&& ! is_shop( )
			&& ! is_subscribe( )
			&& ! is_product( )
			&& ! is_cart( )
			&& ! is_search( )
			&& ! is_product_category( ) ) return ;

		// Not for empty baskets
		if ( is_cart( ) && sizeof( WC( )->cart->get_cart( ) ) == 0 ) return ;

		if ( ! is_dev( ) && ! is_staging( ) )
		{
			$current_user = wp_get_current_user( ) ;

			?>

				<!-- Criteo Tag -->
				<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
				<script type="text/javascript">
				window.criteo_q = window.criteo_q || [ ] ;
				window.criteo_q.push(
				{ event : 'setAccount' , account : 30647 } ,
				{ event : 'setEmail' , email : '<?php if ( ! empty( $current_user->user_email ) ) echo md5( $current_user->user_email ) ; ?>' } ,
				{ event : 'setSiteType' , type : window.innerWidth > 767 ? ( window.innerWidth > 960 ? 'd' : 't' ) : 'm' } ,
				<?php echo json_encode( $this->get_criteo_object( $confirm_order_id ) ) ; ?>
				) ;
				</script>

			<?php
		}
	}

	/**
	* @since 2.2.15.12
	*/
	public function track_user_registration ( )
	{
		$this->fb_track_event( 'CompleteRegistration' ) ;
	}

	/**
	* @since 2.2.17.4
	*/
	public function track_purchase ( $order_id )
	{
		if ( ! is_dev( ) && ! is_staging( ) )
		{
			$order = new WC_Order( $order_id ) ;
			$args = array(
				'value' => $order->get_total( ) ,
				'currency' => $order->get_order_currency( )
			) ;

			$this->fb_track_event( 'Purchase' , $args ) ;
		}

		$this->insert_criteo_tag( $order_id ) ;
	}	

	/**
	* @since 2.2.17.4
	*/
	private function fb_track_event ( $event , $args = null )
	{
		if ( empty( $args ) )
		{
			echo sprintf( "<script>fbq('track', '%s');</script>\n" , $event ) ;
		}
		else
		{
			echo sprintf( "<script>fbq('track', '%s', %s);</script>\n" , $event , json_encode( $args ) ) ;	
		}
	}

	/**
	* @since 2.2.18.5
	*/
	private function get_criteo_object ( $confirm_order_id = null )
	{
		$object = new stdClass ;

		// Check for product
		if ( array_key_exists( 'p' , $_GET ) && $_GET[ 'p' ] != '' )
		{
			$path_parts = explode( '?' , $_GET[ 'p' ] ) ;
			$path_parts = explode( '/' , $path_parts[ 0 ] ) ;
			$slug = $path_parts[ sizeof( $path_parts ) - 2 ] ;

			$posts = get_posts( 
				array( 
					'name' => $slug ,
					'post_type' => 'product' ,
					'numberposts' => 1
				)
			) ;

			if ( $posts && sizeof( $posts ) )
			{
				$product = new WC_Product( $posts[ 0 ] ) ;
			}
		}

		if ( is_front_page( ) )
		{
			$object->event = 'viewHome' ;
		}
		elseif ( is_subscribe( ) )
		{
			$subscription = new WC_Product( 163 ) ;

			$object->event = 'viewItem' ;
			$object->item = 163 ;
		}
		elseif ( is_product( ) || isset( $product ) )
		{
			if ( ! isset( $product ) )
			{
				$product = new WC_Product( get_the_ID( ) ) ;
			}

			$object->event = 'viewItem' ;
			$object->item = ( ! empty( $product->get_sku( ) ) ) ? htmlspecialchars( strtolower( str_replace( ' ' , '_' , $product->get_sku( ) ) ) ) : ( string ) get_the_ID( ) ;
		}
		elseif ( is_shop( ) || is_search( ) || is_product_category( ) )
		{
			global $wp_query ;

			$items = array( ) ;
			$count = 0 ;

			while ( $wp_query->have_posts( ) )
			{
				$wp_query->the_post( ) ;

				if ( $count++ < 3 )
				{
					global $product ;
					$items[ ] = ( ! empty( $product->get_sku( ) ) ) ? htmlspecialchars( strtolower( str_replace( ' ' , '_' , $product->get_sku( ) ) ) ) : ( string ) get_the_ID( ) ;
				}
			}

			$object->event = 'viewList' ;
			$object->item = $items ;
		}
		elseif ( is_cart( ) )
		{
			$object->event = 'viewBasket' ;
			$track_items = array( ) ;
			$cart = WC( )->cart ;

			foreach ( WC( )->cart->get_cart( ) as $cart_item_key => $cart_item )
			{
				$product_id = ! empty( $cart_item[ 'variation_id' ] ) ? $cart_item[ 'variation_id' ] : $cart_item[ 'product_id' ] ;
				$product = new WC_Product( $product_id ) ;
				$price = $product->get_price( ) ;

				// Calculate product-level discount
				if ( ! empty( $cart->applied_coupons ) )
				{
					foreach ( $cart->applied_coupons as $code )
					{
						$coupon = new WC_Coupon( $code ) ;

						if ( $coupon->is_valid( ) && ( $coupon->is_valid_for_product( $cart_item[ 'data' ] ) || $coupon->is_valid_for_cart( ) ) )
						{
							$discount_amount = $coupon->get_discount_amount( $product->get_price( ) , $cart_item , true ) ;
							$price = max( $price - $discount_amount , 0 ) ;
						}
					}
				}

				$track_object = new stdClass ;
				$track_object->id = ( ! empty( $product->get_sku( ) ) ) ? htmlspecialchars( strtolower( str_replace( ' ' , '_' , $product->get_sku( ) ) ) ) : ( string ) $product_id ;
				$track_object->price = number_format( $price , 2 ) ;
				$track_object->quantity = $cart_item[ 'quantity' ] ;

				$track_items[ ] = $track_object ;
			}

			$object->item = $track_items ;
		}
		elseif ( ! empty( $confirm_order_id ) )
		{
			$order = new WC_Order( $confirm_order_id ) ;
			$order_items = $order->get_items( ) ;
			$used_coupons = $order->get_used_coupons( ) ;
			$track_items = array( ) ;

			foreach ( $order_items as $order_item )
			{
				$product_id = ! empty( $order_item[ 'variation_id' ] ) ? $order_item[ 'variation_id' ] : $order_item[ 'product_id' ] ;
				$product = new WC_Product( $product_id ) ;
				$price = $product->get_price( ) ;
				
				// Calculate product-level discount
				if ( ! empty( $used_coupons ) )
				{
					foreach ( $used_coupons as $code )
					{
						$coupon = new WC_Coupon( $code ) ;
						$discount_amount = $coupon->get_discount_amount( $price , null , true ) ;
						$price = max( $price - $discount_amount , 0 ) ;
					}
				}
				
				$track_object = new stdClass ;
				$track_object->id = ( ! empty( $product->get_sku( ) ) ) ? htmlspecialchars( strtolower( str_replace( ' ' , '_' , $product->get_sku( ) ) ) ) : ( string ) $product_id ;
				$track_object->price = number_format( $price , 2 ) ;
				$track_object->quantity = $order_item[ 'qty' ] ;

				$track_items[ ] = $track_object ;
			}

			$object->event = 'trackTransaction' ;
			$object->id = $confirm_order_id ;
			$object->item = $track_items ;
		}

		return $object ;
	}
}

new WC_NAB_Conversion( ) ;